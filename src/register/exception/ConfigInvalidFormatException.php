<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\register\exception;

use Exception;

use liberty_code\sql\register\library\ConstRegister;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRegister::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid table name
            isset($config[ConstRegister::TAB_CONFIG_KEY_TABLE_NAME]) &&
            is_string($config[ConstRegister::TAB_CONFIG_KEY_TABLE_NAME]) &&
            (trim($config[ConstRegister::TAB_CONFIG_KEY_TABLE_NAME]) != '') &&

            // Check valid where pattern
            (
                (!isset($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE])) ||
                (
                    is_string($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE]) &&
                    (trim($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE]) != '')
                )
            ) &&

            // Check valid order pattern
            (
                (!isset($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_ORDER])) ||
                (
                    is_string($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_ORDER]) &&
                    (trim($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_ORDER]) != '')
                )
            ) &&

            // Check valid column pattern
            (
                (!isset($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_COLUMN])) ||
                (
                    is_string($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_COLUMN]) &&
                    (trim($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_COLUMN]) != '')
                )
            ) &&

            // Check valid value pattern
            (
                (!isset($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_VALUE])) ||
                (
                    is_string($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_VALUE]) &&
                    (trim($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_VALUE]) != '')
                )
            ) &&

            // Check valid set pattern
            (
                (!isset($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_SET])) ||
                (
                    is_string($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_SET]) &&
                    (trim($config[ConstRegister::TAB_CONFIG_KEY_PATTERN_SET]) != '')
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}