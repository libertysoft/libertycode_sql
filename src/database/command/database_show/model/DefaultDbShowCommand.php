<?php
/**
 * Description :
 *
 * This class allows to define default database show SQL command class.
 * Can be consider is base of all database show SQL command type.
 *
 * Default database show command allows to design string database show SQL command,
 * from following specified configuration: @see DbShowCommandInterface.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_show\model;

use liberty_code\sql\database\command\model\DefaultCommand;
use liberty_code\sql\database\command\database_show\api\DbShowCommandInterface;

use liberty_code\sql\database\command\library\ConstCommand;
use liberty_code\sql\database\command\database_show\exception\ConfigInvalidFormatException;



abstract class DefaultDbShowCommand extends DefaultCommand implements DbShowCommandInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		// $result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstCommand::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute()
    {
        // Init var
        $result = $this->executeResult();

        if($result !== false)
        {
            $result = 0;
        }

        // Return result
        return $result;
    }



}