<?php
/**
 * Description :
 *
 * This class allows to define default insert SQL command class.
 * Can be consider is base of all insert SQL command type.
 *
 * Default insert command allows to design string SQL insert command,
 * from following specified configuration: @see InsertCommandInterface.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\insert\model;

use liberty_code\sql\database\command\model\DefaultCommand;
use liberty_code\sql\database\command\insert\api\InsertCommandInterface;

use liberty_code\sql\database\command\library\ConstCommand;
use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxValueClause;
use liberty_code\sql\database\command\clause\exception\ClauseConfigInvalidFormatException;
use liberty_code\sql\database\command\insert\library\ConstInsertCommand;
use liberty_code\sql\database\command\insert\exception\ConfigInvalidFormatException;



abstract class DefaultInsertCommand extends DefaultCommand implements InsertCommandInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		// $result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstCommand::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseFrom(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxFromClause::checkClauseFromIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = $tabConfig;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseColumn(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxExpressionClause::checkClauseExpressionIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN] = $tabConfig;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseValue(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxValueClause::checkClauseValueIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE] = $tabConfig;

        // Return result
        return $this;
    }



}