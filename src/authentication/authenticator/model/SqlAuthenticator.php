<?php
/**
 * Description :
 * This class allows to define SQL authenticator class.
 * SQL authenticator is an authentication support,
 * using SQL data storage to check identification and authentication.
 *
 * SQL authenticator uses the following specified configuration:
 * [
 *     query(required): array (select command configuration: @see SelectCommandInterface ),
 *
 *     mapping_identification_data(optional: got [], if not found) => [
 *         // Mapping identification data 1
 *         [
 *             data_key(required): "string identification data key",
 *             data_format_callable(optional):
 *                 Get data formatted value callback function: mixed function(mixed $value),
 *             column_name(optional: got default_identification_data_column_name or data_key, if not found):
 *                 "string column name" OR ["string column name 1", ..., "string column name N"],
 *             operator_like_require(optional: got false (means equal operator used), if not found): true / false
 *         ],
 *         ...,
 *         // Mapping identification data N
 *         [...]
 *     ],
 *
 *     mapping_authentication_data(optional: got [], if not found) => [
 *         // Mapping authentication data 1
 *         [
 *             data_key(required): "string authentication data key",
 *             data_format_callable(optional):
 *                 Get data formatted value callback function: mixed function(mixed $value),
 *             column_name(optional: got default_authentication_data_column_name or data_key, if not found):
 *                 "string column name" OR ["string column name 1", ..., "string column name N"],
 *             check_data_callable(optional: equal comparison used ($value == $queryValue), if not found):
 *                 Check data callback function: boolean function(mixed $value, mixed $queryValue)
 *         ],
 *         ...,
 *         // Mapping authentication data N
 *         [...]
 *     ],
 *
 *     default_identification_data_column_name(optional: got null, if not found) =>
 *         'string column name, for not-mapped identification data',
 *
 *     default_authentication_data_column_name(optional: got null, if not found) =>
 *         'string column name, for not-mapped authentication data'
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\authentication\authenticator\model;

use liberty_code\authentication\authenticator\model\DefaultAuthenticator;

use liberty_code\authentication\authentication\api\AuthenticationInterface;
use liberty_code\authentication\authenticator\library\ConstAuthenticator;
use liberty_code\sql\database\command\clause\library\ConstExpressionClause;
use liberty_code\sql\database\command\clause\library\ConstConditionClause;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;
use liberty_code\sql\database\command\select\api\SelectCommandInterface;
use liberty_code\sql\database\command\factory\api\CommandFactoryInterface;
use liberty_code\sql\authentication\authenticator\library\ConstSqlAuthenticator;
use liberty_code\sql\authentication\authenticator\exception\CommandFactoryInvalidFormatException;
use liberty_code\sql\authentication\authenticator\exception\ConfigInvalidFormatException;



/**
 * @method null|CommandFactoryInterface getObjCommandFactory() Get command factory object.
 * @method void setObjCommandFactory(CommandFactoryInterface $objCommandFactory) Set command factory object.
 */
class SqlAuthenticator extends DefaultAuthenticator
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************

    /**
     * @inheritdoc
     * @param CommandFactoryInterface $objCommandFactory = null
     */
    public function __construct(array $tabConfig = null, CommandFactoryInterface $objCommandFactory = null)
    {
        // Call parent constructor
        parent::__construct($tabConfig);

        // Init command factory if required
        if(!is_null($objCommandFactory))
        {
            $this->setObjCommandFactory($objCommandFactory);
        }
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstSqlAuthenticator::DATA_KEY_DEFAULT_COMMAND_FACTORY))
        {
            $this->__beanTabData[ConstSqlAuthenticator::DATA_KEY_DEFAULT_COMMAND_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstSqlAuthenticator::DATA_KEY_DEFAULT_COMMAND_FACTORY
		);
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstSqlAuthenticator::DATA_KEY_DEFAULT_COMMAND_FACTORY:
                    CommandFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAuthenticator::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkIsIdentified(AuthenticationInterface $objAuthentication)
    {
        // Init var
        $result = false;
        $objCommand = $this->getObjCommand($objAuthentication);

        // Run query result
        if(($resultQuery = $objCommand->executeResult()) !== false)
        {
            // Check data found
            $result = ($resultQuery->getIntCountRow() > 0);

            $resultQuery->close();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkIsAuthenticated(AuthenticationInterface $objAuthentication)
    {
        // Init var
        $result = false;
        $tabAuthData = $objAuthentication->getTabAuthData();
        $tabCheckConfig = array_values($this->getTabCheckConfigAuth($tabAuthData));
        $objCommand = $this->getObjCommand($objAuthentication);

        // Run query result
        if(($resultQuery = $objCommand->executeResult()) !== false)
        {
            // Check data found
            $result = ($resultQuery->getIntCountRow() > 0);
            while(
                (($tabQueryData = $resultQuery->getFetchData()) !== false) &&
                $result
            )
            {
                // Run each check configuration
                for($intCpt = 0; ($intCpt < count($tabCheckConfig)) && $result; $intCpt++)
                {
                    // Get check configuration info
                    $checkConfig = $tabCheckConfig[$intCpt];
                    $tabColNm = $checkConfig[0];
                    $value = $checkConfig[1];
                    $callCheckData = $checkConfig[2];

                    // Check data, if required
                    $boolCheckData = false;
                    for($intCpt2 = 0; ($intCpt2 < count($tabColNm)) && (!$boolCheckData); $intCpt2++)
                    {
                        $strColNm = $tabColNm[$intCpt2];
                        if(array_key_exists($strColNm, $tabQueryData))
                        {
                            $boolCheckData =
                                (
                                    (
                                        is_callable($callCheckData) &&
                                        $callCheckData($value, $tabQueryData[$strColNm])
                                    ) ||
                                    (
                                        (!is_callable($callCheckData)) &&
                                        ($value === $tabQueryData[$strColNm])
                                    )
                                );
                        }
                    }

                    // Register check
                    $result = $boolCheckData;
                }
            }

            $resultQuery->close();
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get check configuration,
     * from specified authentication data.
     *
     * @param array $tabData
     * @return array
     */
    protected function getTabCheckConfigAuth(array $tabData)
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = array();

        // Run each data
        foreach($tabData as $strKey => $value)
        {
            $tabConfigMapData = $this->getTabConfigMapData(
                ConstSqlAuthenticator::TAB_CONFIG_KEY_MAPPING_AUTH_DATA,
                $strKey
            );
            $strColNm = (
            isset($tabConfig[ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_AUTH_DATA_COLUMN_NAME]) ?
                $tabConfig[ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_AUTH_DATA_COLUMN_NAME] :
                $strKey
            );

            // Build check configuration
            $tabCheck = array();
            foreach($tabConfigMapData as $configMapData)
            {
                $callGetDataFormat = (
                isset($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE]) ?
                    $configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE] :
                    null
                );
                $value = (is_callable($callGetDataFormat) ? $callGetDataFormat($value) : $value);
                $tabColNm = (
                isset($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) ?
                    (
                    is_array($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) ?
                        array_values($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) :
                        array($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME])
                    ) :
                    array($strColNm)
                );
                $callCheckData = (
                isset($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_CHECK_DATA_CALLABLE]) ?
                    $configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_CHECK_DATA_CALLABLE] :
                    null
                );

                $tabCheck[] = array(
                    $tabColNm,
                    $value,
                    $callCheckData
                );
            }

            $tabCheck = (
            (count($tabCheck) > 0) ?
                $tabCheck :
                array(
                    [
                        array($strColNm),
                        $value,
                        null
                    ]
                )
            );

            // Register condition
            $result = array_merge($result, $tabCheck);
        }

        // Return result
        return $result;
    }



    /**
     * Get specified index array of configured mapping data.
     *
     * @param string $strConfigKey
     * @param $strDataKey = null
     * @return array
     */
    protected function getTabConfigMapData($strConfigKey, $strDataKey = null)
    {
        // Init var
        $strDataKey = (is_string($strDataKey) ? $strDataKey : null);
        $tabConfig = $this->getTabAuthConfig();
        $tabConfigMapData = (
            isset($tabConfig[$strConfigKey]) ?
                $tabConfig[$strConfigKey] :
                array()
        );
        $result = array();

        // Run each mapping data
        foreach($tabConfigMapData as $configMapData)
        {
            // Register mapping data, if required
            if(
                is_null($strDataKey) ||
                ($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_DATA_KEY] == $strDataKey)
            )
            {
                $result[] = $configMapData;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get command query expression configuration,
     * from specified data.
     *
     * @param string $strConfigKey
     * @param string $strDefaultColNmKey
     * @param array $tabData
     * @return array
     */
    protected function getTabCmdExpressionConfig($strConfigKey, $strDefaultColNmKey, array $tabData)
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = array();

        // Run each data
        foreach($tabData as $strKey => $value)
        {
            $tabConfigMapData = $this->getTabConfigMapData(
                $strConfigKey,
                $strKey
            );
            $strColNm = (
                isset($tabConfig[$strDefaultColNmKey]) ?
                    $tabConfig[$strDefaultColNmKey] :
                    $strKey
            );

            // Build condition
            $tabExpression = array();
            foreach($tabConfigMapData as $configMapData)
            {
                $tabColNm = (
                isset($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) ?
                    (
                        is_array($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) ?
                            array_values($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) :
                            array($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME])
                    ) :
                    array($strColNm)
                );

                $tabExpression = $tabColNm;
            }

            $tabExpression = (
                (count($tabExpression) > 0) ?
                    $tabExpression :
                    array($strColNm)
            );

            // Register condition
            $result = array_merge($result, $tabExpression);
        }

        // Return result
        return $result;
    }



    /**
     * Get command query condition configuration,
     * from specified identification data.
     *
     * @param array $tabData
     * @return array
     */
    protected function getTabCmdConditionConfigId(array $tabData)
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $result = array();

        // Run each data
        foreach($tabData as $strKey => $value)
        {
            $tabConfigMapData = $this->getTabConfigMapData(
                ConstSqlAuthenticator::TAB_CONFIG_KEY_MAPPING_ID_DATA,
                $strKey
            );
            $strColNm = (
                isset($tabConfig[ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_ID_DATA_COLUMN_NAME]) ?
                    $tabConfig[ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_ID_DATA_COLUMN_NAME] :
                    $strKey
            );

            // Build condition
            $tabCondition = array();
            foreach($tabConfigMapData as $configMapData)
            {
                // Get condition info
                $callGetDataFormat = (
                    isset($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE]) ?
                        $configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE] :
                        null
                );
                $value = (is_callable($callGetDataFormat) ? $callGetDataFormat($value) : $value);
                $tabColNm = (
                    isset($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) ?
                        (
                            is_array($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) ?
                                array_values($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) :
                                array($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME])
                        ) :
                        array($strColNm)
                );
                $boolOpeLike = (
                    array_key_exists(ConstSqlAuthenticator::TAB_CONFIG_KEY_OPERATOR_LIKE_REQUIRE, $configMapData) &&
                    (intval($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_OPERATOR_LIKE_REQUIRE]) != 0)
                );

                // Build condition for each column name
                foreach($tabColNm as $strColNm)
                {
                    $tabCondition[] = array(
                        ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNm,
                        ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => (
                        $boolOpeLike ?
                            ConstConditionClause::OPERATOR_CONFIG_LIKE :
                            ConstConditionClause::OPERATOR_CONFIG_EQUAL
                        ),
                        ConstConditionClause::TAB_CONFIG_KEY_VALUE => [ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $value]
                    );
                }

                // Finalize condition
                $tabCondition = array(
                    [
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_OR,
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => $tabCondition
                    ]
                );
            }

            $tabCondition = (
                (count($tabCondition) > 0) ?
                    $tabCondition :
                    array(
                        [
                            ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNm,
                            ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_EQUAL,
                            ConstConditionClause::TAB_CONFIG_KEY_VALUE => [ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $value]
                        ]
                    )
            );

            // Register condition
            $result = array_merge($result, $tabCondition);
        }

        // Return result
        return $result;
    }



    /**
     * Get select command query configuration,
     * from specified authentication object.
     *
     * @param AuthenticationInterface $objAuthentication
     * @return array
     */
    protected function getTabCommandConfig(AuthenticationInterface $objAuthentication)
    {
        // Init var
        $tabConfig = $this->getTabAuthConfig();
        $tabIdData = $objAuthentication->getTabIdData();
        $tabAuthData = $objAuthentication->getTabAuthData();
        $result = $tabConfig[ConstSqlAuthenticator::TAB_CONFIG_KEY_QUERY];

        // Init expression, from identification data
        $tabExpressionConfigId = $this->getTabCmdExpressionConfig(
            ConstSqlAuthenticator::TAB_CONFIG_KEY_MAPPING_ID_DATA,
            ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_ID_DATA_COLUMN_NAME,
            $tabIdData
        );

        // Init expression, from authentication data
        $tabExpressionConfigAuth = $this->getTabCmdExpressionConfig(
            ConstSqlAuthenticator::TAB_CONFIG_KEY_MAPPING_AUTH_DATA,
            ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_AUTH_DATA_COLUMN_NAME,
            $tabAuthData
        );

        // Init select clause, if required
        if(
            (count($tabExpressionConfigId) > 0) ||
            (count($tabExpressionConfigAuth) > 0)
        )
        {
            $tabSelectConfig = (
                isset($result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT]) ?
                    $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT] :
                    array()
            );
            $tabSelectConfig = array_merge(
                $tabSelectConfig,
                $tabExpressionConfigId,
                $tabExpressionConfigAuth
            );

            // Register select clause in result
            $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT] = $tabSelectConfig;
        }

        // Init condition, from identification data
        $tabIdData = $objAuthentication->getTabIdData();
        $tabConditionConfigId = $this->getTabCmdConditionConfigId($tabIdData);

        // Finalize condition
        $tabConditionConfig = array(
            ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
            ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => $tabConditionConfigId
        );

        // Init where clause, if required
        if(count($tabConditionConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]) > 0)
        {
            $tabWhereConfig = (
            isset($result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]) ?
                $tabWhereConfig = array(
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                        $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE],
                        $tabConditionConfig
                    ]
                ) :
                $tabConditionConfig
            );

            // Register where clause in result
            $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = $tabWhereConfig;
        }

        // Return result
        return $result;
    }



    /**
     * Get new select SQL command object,
     * to get entity data,
     * from specified authentication object.
     *
     * @param AuthenticationInterface $objAuthentication
     * @return SelectCommandInterface
     */
    public function getObjCommand(AuthenticationInterface $objAuthentication)
    {
        // Init var
        $tabConfig = $this->getTabCommandConfig($objAuthentication);
        $result = $this->getObjCommandFactory()->getObjSelectCommand($tabConfig);

        // Return result
        return $result;
    }



}