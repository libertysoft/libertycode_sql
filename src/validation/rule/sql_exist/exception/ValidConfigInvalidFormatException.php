<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\validation\rule\sql_exist\exception;

use Exception;

use liberty_code\sql\validation\rule\sql_exist\library\ConstExistSqlRule;



class ValidConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstExistSqlRule::EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init scope values index array check function
        $checkTabScopeValueIsValid = function(array $tabValue)
        {
            $result = (
                is_array($tabValue) &&
                (count($tabValue) > 0)
            );

            // Check each exception value is valid, if required
            if($result)
            {
                $tabValue = array_values($tabValue);
                for($intCpt = 0; ($intCpt < count($tabValue)) && $result; $intCpt++)
                {
                    $value = $tabValue[$intCpt];
                    $result = (
                        // Check valid value
                        (
                            is_string($value) ||
                            is_numeric($value) ||
                            is_bool($value) ||
                            is_null($value)
                        )
                    );
                }
            }

            return $result;
        };

        // Init scope configuration associative array check function
        $checkTabScopeConfigIsValid = function(array $tabConfig) use ($checkTabScopeValueIsValid)
        {
            $result = (
                is_array($tabConfig) &&
                (count($tabConfig) > 0)
            );

            // Check each exception configuration is valid, if required
            if($result)
            {
                $tabKey = array_keys($tabConfig);
                for($intCpt = 0; ($intCpt < count($tabKey)) && $result; $intCpt++)
                {
                    $strKey = $tabKey[$intCpt];
                    $value = $tabConfig[$strKey];
                    $result = (
                        // Check valid key
                        is_string($strKey) &&
                        (trim($strKey) != '') &&

                        // Check valid value
                        (
                            is_string($value) ||
                            is_numeric($value) ||
                            is_bool($value) ||
                            is_null($value) ||
                            (
                                is_array($value) &&
                                $checkTabScopeValueIsValid($value)
                            )
                        )
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid db name
            (
                (!isset($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_DB_NAME])) ||
                (
                    is_string($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_DB_NAME]) &&
                    (trim($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_DB_NAME]) != '')
                )
            ) &&

            // Check valid table name
            isset($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_TABLE_NAME]) &&
            is_string($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_TABLE_NAME]) &&
            (trim($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_TABLE_NAME]) != '') &&

            // Check valid column name
            isset($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_COLUMN_NAME]) &&
            is_string($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_COLUMN_NAME]) &&
            (trim($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_COLUMN_NAME]) != '') &&

            // Check valid inclusion scope
            (
                (!isset($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_INCLUDE])) ||
                $checkTabScopeConfigIsValid($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_INCLUDE])
            ) &&

            // Check valid exclusion scope
            (
                (!isset($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_EXCLUDE])) ||
                $checkTabScopeConfigIsValid($config[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_EXCLUDE])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}