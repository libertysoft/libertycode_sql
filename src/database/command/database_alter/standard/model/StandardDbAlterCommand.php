<?php
/**
 * Description :
 * This class allows to define standard database alter SQL command class.
 * Standard database alter command allows to design string SQL database alter command,
 * from default configuration,
 * and uses standard SQL.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_alter\standard\model;

use liberty_code\sql\database\command\database_alter\model\DefaultDbAlterCommand;

use liberty_code\sql\database\command\database_alter\library\ConstDbAlterCommand;
use liberty_code\sql\database\command\database_alter\exception\ConfigInvalidFormatException;
use liberty_code\sql\database\command\database_alter\standard\library\ConstStandardDbAlterCommand;



class StandardDbAlterCommand extends DefaultDbAlterCommand
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for alter database clause.
     *
     * @return string
     */
    protected function getStrClauseAlterCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = sprintf(
            ConstStandardDbAlterCommand::SQL_CONFIG_PATTERN_ALTER,
            $objConnection->getStrEscapeName($tabConfig[ConstDbAlterCommand::TAB_CONFIG_KEY_DB_NAME])
        );

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for charset clause.
     *
     * @return string
     */
    protected function getStrClauseCharsetCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
            isset($tabConfig[ConstDbAlterCommand::TAB_CONFIG_KEY_CHARSET]) ?
                sprintf(
                    ConstStandardDbAlterCommand::SQL_CONFIG_PATTERN_CHARSET,
                    $objConnection->getStrEscapeName($tabConfig[ConstDbAlterCommand::TAB_CONFIG_KEY_CHARSET])
                ) :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for collation clause.
     *
     * @return string
     */
    protected function getStrClauseCollationCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
            isset($tabConfig[ConstDbAlterCommand::TAB_CONFIG_KEY_COLLATION]) ?
                sprintf(
                    ConstStandardDbAlterCommand::SQL_CONFIG_PATTERN_COLLATION,
                    $objConnection->getStrEscapeName($tabConfig[ConstDbAlterCommand::TAB_CONFIG_KEY_COLLATION])
                ) :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrCommand()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Build SQL command
        $strAlter = $this->getStrClauseAlterCommand();
        $strCharset= $this->getStrClauseCharsetCommand();
        $strCollation = $this->getStrClauseCollationCommand();
        $result =
            $strAlter . ((trim($strCharset) != '') ? ' ' : '') .
            $strCharset . ((trim($strCollation) != '') ? ' ' : '') .
            $strCollation;

        // Return result
        return $result;
    }



}