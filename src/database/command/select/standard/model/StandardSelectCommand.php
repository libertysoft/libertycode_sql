<?php
/**
 * Description :
 * This class allows to define standard select SQL command class.
 * Standard select command allows to design string SQL select command,
 * from default configuration,
 * and uses standard SQL.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\select\standard\model;

use liberty_code\sql\database\command\select\model\DefaultSelectCommand;

use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardExpressionClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardFromClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardConditionClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardOrderClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardLimitClause;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;
use liberty_code\sql\database\command\select\exception\ConfigInvalidFormatException;
use liberty_code\sql\database\command\select\standard\library\ConstStandardSelectCommand;



class StandardSelectCommand extends DefaultSelectCommand
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for select clause.
     *
     * @return string
     */
    protected function getStrClauseSelectCommand()
    {
        // Init var
        $result = '';
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        if(isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT]))
        {
            $result = sprintf(
                (
                (
                    array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT, $tabConfig) &&
                    (intval($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT]) !== 0)
                ) ?
                    ConstStandardSelectCommand::SQL_CONFIG_PATTERN_SELECT_DISTINCT :
                    ConstStandardSelectCommand::SQL_CONFIG_PATTERN_SELECT
                ),
                ToolBoxStandardExpressionClause::getStrClauseExpressionCommand(
                    $objConnection,
                    $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT]
                )
            );
        }

        // Get pattern value, if required
        if(isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for from clause.
     *
     * @return string
     */
    protected function getStrClauseFromCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM]) ?
            sprintf(
                ConstStandardSelectCommand::SQL_CONFIG_PATTERN_FROM,
                ToolBoxStandardFromClause::getStrClauseFromCommand(
                    $objConnection,
                    $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for where clause.
     *
     * @return string
     */
    protected function getStrClauseWhereCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]) ?
            sprintf(
                ConstStandardSelectCommand::SQL_CONFIG_PATTERN_WHERE,
                ToolBoxStandardConditionClause::getStrClauseConditionCommand(
                    $objConnection,
                    $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for group clause.
     *
     * @return string
     */
    protected function getStrClauseGroupCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP]) ?
            sprintf(
                ConstStandardSelectCommand::SQL_CONFIG_PATTERN_GROUP,
                ToolBoxStandardExpressionClause::getStrClauseExpressionCommand(
                    $objConnection,
                    $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for having clause.
     *
     * @return string
     */
    protected function getStrClauseHavingCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_HAVING]) ?
            sprintf(
                ConstStandardSelectCommand::SQL_CONFIG_PATTERN_HAVING,
                ToolBoxStandardConditionClause::getStrClauseConditionCommand(
                    $objConnection,
                    $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_HAVING]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_HAVING_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_HAVING_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for order clause.
     *
     * @return string
     */
    protected function getStrClauseOrderCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER]) ?
            sprintf(
                ConstStandardSelectCommand::SQL_CONFIG_PATTERN_ORDER,
                ToolBoxStandardOrderClause::getStrClauseOrderCommand(
                    $objConnection,
                    $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for limit clause.
     *
     * @return string
     */
    protected function getStrClauseLimitCommand()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT]) ?
            sprintf(
                ConstStandardSelectCommand::SQL_CONFIG_PATTERN_LIMIT,
                ToolBoxStandardLimitClause::getStrClauseLimitCommand(
                    $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrCommand()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Build SQL command
        $strSelect = $this->getStrClauseSelectCommand();
        $strFrom = $this->getStrClauseFromCommand();
        $strWhere = $this->getStrClauseWhereCommand();
        $strGroup = $this->getStrClauseGroupCommand();
        $strHaving = $this->getStrClauseHavingCommand();
        $strOrder = $this->getStrClauseOrderCommand();
        $strLimit = $this->getStrClauseLimitCommand();
        $result =
            $strSelect . ((trim($strFrom) != '') ? ' ' : '') .
            $strFrom . ((trim($strWhere) != '') ? ' ' : '') .
            $strWhere . ((trim($strGroup) != '') ? ' ' : '') .
            $strGroup . ((trim($strHaving) != '') ? ' ' : '') .
            $strHaving . ((trim($strOrder) != '') ? ' ' : '') .
            $strOrder . ((trim($strLimit) != '') ? ' ' : '') .
            $strLimit;

        // Return result
        return $result;
    }



}