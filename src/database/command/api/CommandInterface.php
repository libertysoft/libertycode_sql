<?php
/**
 * Description :
 * This class allows to describe behavior of SQL database command class.
 * Command allows to get string SQL command,
 * from specified configuration,
 * to use specific SQL data storage.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\api;

use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\statement\api\StatementInterface;
use liberty_code\sql\database\result\api\ResultInterface;



interface CommandInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get connection object.
     *
     * @return ConnectionInterface
     */
    public function getObjConnection();



    /**
     * Get config array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get string SQL command.
     *
     * @return string
     */
    public function getStrCommand();





    // Methods setters
    // ******************************************************************************

    /**
     * Set connection object.
     *
     * @param ConnectionInterface $objConnection
     */
    public function setConnection(ConnectionInterface $objConnection);



    /**
     * Set config array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);





    // Methods execute
    // ******************************************************************************

    /**
     * Execute configured SQL query.
     * Return false if doesn't run, true or the number of row affected else, if possible.
     * Correct select command will return true or 0 (no affected rows).
     *
     * @return boolean|integer
     */
    public function execute();



    /**
     * Execute configured SQL query with data returns.
     * Return object ResultInterface if run correctly, false else.
     * Correct no-select command return empty ResultInterface.
     *
     * @return boolean|ResultInterface
     */
    public function executeResult();



    /**
     * Get statement (prepared query).
     * Return object StatementInterface if run correctly, false else.
     *
     * @param array $tabConfig = array()
     * @param array $tabParam = array()
     * @return StatementInterface
     */
    public function getObjStatement(array $tabConfig = array(), array $tabParam = array());



}