<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/clause/test/Include.php');

// Use
use liberty_code\sql\database\command\clause\library\ToolBoxLimitClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardLimitClause;



// Init var
$config_1 = array(
    'count' => 1,
    'start' => 2
);
$config_2 = array(
    'count' => 1
);
$config_3 = array(
    'start' => 2
);
$config_4 = array(
    'count' => 1,
    'start' => 2,
    'pattern' => '[%s]'
);
$config_5 = array(
    'start' => 2,
    'pattern' => '[%s]'
);
$config_6 = array(
    'pattern' => '[%s]'
);
$config_7 = array(
    'pattern' => 'test'
);
$config_8 = 'test';
$config_9 = 1.1;
$config_10 = 1;

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ok
    $config_3, // Ko
    $config_4, // Ok
    $config_5, // Ok
    $config_6, // Ok
    $config_7, // Ok
    $config_8, // Ko
    $config_9, // Ko
    $config_10, // Ok
);



// Test config
process(
    $tabConfig, 
    function($config)
    {
        return ToolBoxLimitClause::checkClauseLimitIsValid($config);
    },
    function($config)
    {
        return ToolBoxStandardLimitClause::getStrClauseLimitCommand($config);
    }
);


