<?php
/**
 * Description :
 * This class allows to describe behavior of database drop SQL command class.
 * Database drop command allows to design string SQL database drop command,
 * from following specified configuration:
 * [
 *     db_name(required):"string database name"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_drop\api;

use liberty_code\sql\database\command\api\CommandInterface;



interface DbDropCommandInterface extends CommandInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Set string database name config.
     *
     * @param string $strDbNm
     * @return static
     */
    public function setDbName($strDbNm);
}