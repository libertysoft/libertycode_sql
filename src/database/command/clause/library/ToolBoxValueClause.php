<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;



class ToolBoxValueClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

    /**
     * Check if value clause is valid.
     * Configuration is an index array of expression clauses,
     * where each expression clause uses following format:
     * @see ToolBoxExpressionClause::checkClauseExpressionIsValid().
     *
     * @param array $tabConfig
     * @return boolean
     */
    public static function checkClauseValueIsValid(array $tabConfig)
    {
        // Init var
        $result = true;

        // Check each sub-config valid
        $tabKey = array_keys($tabConfig);
        for($intCpt = 0; ($intCpt < count($tabKey)) && $result; $intCpt++)
        {
            $config = $tabConfig[$tabKey[$intCpt]];
            $result =
                is_array($config) &&
                ToolBoxExpressionClause::checkClauseExpressionIsValid($config);
        }

        // Return result
        return $result;
    }



}