<?php
/**
 * GET arguments:
 * - delete: = 1: Remove data.
 * - hydrate: = 1: Hydrate data.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/test/HelpInsertTestTable.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;
use liberty_code\sql\register\data\model\DataRegister;



// Init var
$objDtNow = new DateTime();
$objCommandFacto = new StandardCommandFactory($objConnection);
$tabConfig = array(
    'table_name' => $strTableNmCnf,
    'column_name_key' => $strColNmCnfNm,
    'column_name_item' => $strColNmCnfVal,
    'column_name_expire_timeout_datetime' => $strColNmCnfDateExpire,
    'pattern_where' => sprintf(
        '%1$s = %2$s',
        $objConnection->getStrEscapeName($strColNmCnfType),
        $objConnection->getStrEscapeValue('test-register')
    ),
    'pattern_order' => sprintf(
        '%1$s ASC',
        $objConnection->getStrEscapeName($strColNmCnfNm)
    ),
    'pattern_column' => sprintf(
        '%1$s, %2$s, %3$s',
        $objConnection->getStrEscapeName($strColNmCnfType),
        $objConnection->getStrEscapeName($strColNmCnfDateCreate),
        $objConnection->getStrEscapeName($strColNmCnfDateUpdate)
    ),
    'pattern_value' => sprintf(
        '%1$s, NOW(), NOW()',
        $objConnection->getStrEscapeValue('test-register')
    ),
    'pattern_set' => sprintf(
        '%1$s = NOW()',
        $objConnection->getStrEscapeName($strColNmCnfDateUpdate)
    )
);
$objRegister = new DataRegister($tabConfig, $objCommandFacto);



// Datetime now
echo('Datetime now: <pre>');
var_dump((clone $objDtNow)->setTimezone(new DateTimeZone($objRegister->getStrSetTimezoneName())));
echo('</pre>');

echo('<br /><br /><br />');



// Test item not valid remove
echo('Test remove all items, not valid: <br />');
echo('<pre>');var_dump($objRegister->removeItemAllNotValid());echo('</pre>');
echo('<br />');
//die;

echo('<br /><br /><br />');



// Test item add
$test1 = 'Test 1';
$test3 = true;
$tabData = array(
    'key_1_1' => [
        $test1,
        ['expire_timeout_time' => (clone $objDtNow)->add(new DateInterval('PT5S'))]
    ], // Ok
    'key_1_2' => [
        $test1
    ], // Ok
    'key_2' => [
        2
    ], // Ok
    'key_3' => [
        $test3
    ], // Ok

    // Ok
    'key_4' => [
        [
            'key_test_get_1' => 'Value test 1',
            'key_test_get_2' => 'Value test 2',
            'key_test_get_N' => 'Value test N',
        ],
        ['expire_timeout_second' => 20]
    ],

    'key_5' => [
        $test3
    ], // Ok
    'key_6' => [
        6.7
    ], // Ok
    'key_7' => [
        false
    ], // Ok
    3 => [
        3.7
    ], // Ko: bad key format
    'key_8' => [
        null
    ], // Ko: bad value format
    '-key_1_1' => [
        'Test 1 duplicate'
    ], // Ko: key already exists
);

foreach($tabData as $strKey => $tabInfo)
{
    $value = $tabInfo[0];
    $tabConfig = (isset($tabInfo[1]) ? $tabInfo[1] : null);

    // Format key if required
    if(is_string($strKey) && (preg_match('#\-(.+)#', $strKey, $tabMatch) == 1))
    {
        $strKey = $tabMatch[1];
    }

    echo('Test add item "'.$strKey.'": <br />');
    try{
        echo('<pre>');var_dump($objRegister->addItem($strKey, $value, $tabConfig));echo('</pre>');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test item check/get
$tabKey = array(
    'key_1_1', // Found
    'key_1_2', // Found
    'key_2', // Found
    'key_3', // Found
    'key_4', // Found
    'key_5', // Found
    'key_6', // Found
    'key_7', // Found
    'key_8', // Not found
    'key_9', // Not found
    'test', // Not found
    3, // Bad key format
);

foreach($tabKey as $strKey)
{
    echo('Test check, get item "'.$strKey.'": <br />');
    try{
        echo('Exists: <pre>');var_dump($objRegister->checkItemExists($strKey));echo('</pre>');
        echo('Get: <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test item set
$tabData = array(
    // Ok
    'key_4' => [
        [
            'key_test_get_1' => 'Value test 1 updated',
            'key_test_get_2' => 'Value test 2 updated',
            'key_test_get_3' => 'Value test 3',
            'key_test_get_N' => 'Value test N updated',
        ],
        ['expire_timeout_second' => 40]
    ],

    'key_5' => [
        'Test 5 update'
    ], // Ok
    'key_2' => [
        null
    ], // Ko: bad value format
    'test' => [
        'test'
    ] // Ko: Not found
);

foreach($tabData as $strKey => $tabInfo)
{
    $value = $tabInfo[0];
    $tabConfig = (isset($tabInfo[1]) ? $tabInfo[1] : null);

    echo('Test set item "'.$strKey.'": <br />');
    try{
        echo('<pre>');var_dump($objRegister->setItem($strKey, $value, $tabConfig));echo('</pre>');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test setting item: <br />');
echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test item get, with pattern key option
echo('Test item get, with pattern key option: <br />');
$tabGetConfig = array(
    'pattern_key' => '%key_1_%'
);
foreach($objRegister->getTabKey($tabGetConfig) as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test item get, with list key option
echo('Test item get, with list key option: <br />');
$tabGetConfig = array(
    'list_key' => [
        'key_2',
        'key_4',
        'key_6'
    ]
);
foreach($objRegister->getTabKey($tabGetConfig) as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test item put
$tabData = array(
    'key_7' => [
        true
    ], // Ok: update
    'key_8' => [
        8,
        ['expire_timeout_second' => 30]
    ], // Ok: create
    'key_1_1' => [
        'Test 1 updated',
        ['expire_timeout_time' => (clone $objDtNow)->add(new DateInterval('PT15S'))]
    ], // Ok: update
    true => [
        'Test N'
    ] // Ko: bad key format
);

foreach($tabData as $strKey => $tabInfo)
{
    $value = $tabInfo[0];
    $tabConfig = (isset($tabInfo[1]) ? $tabInfo[1] : null);

    echo('Test put item "'.$strKey.'": <br />');
    try{
        echo('<pre>');var_dump($objRegister->putItem($strKey, $value, $tabConfig));echo('</pre>');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test putting item: <br />');
echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test item remove, if required
if(trim(ToolBoxTable::getItem($_GET, 'delete', '0')) == '1')
{
    $tabKey = array(
        'key_2', // Ok
        'key_6', // Ok
        'key_1', // Ko: not found
        'test', // Ko: not found
        3, // Ko: bad key format
    );

    foreach($tabKey as $strKey)
    {
        echo('Test remove item "'.$strKey.'": <br />');
        try{
            echo('<pre>');var_dump($objRegister->removeItem($strKey));echo('</pre>');
        } catch(\Exception $e) {
            echo(htmlentities($e->getMessage()));
            echo('<br />');
        }
        echo('<br />');
    }


    echo('Test removing item: <br />');
    echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
    echo('<br />');

    echo('<br /><br /><br />');



    // Test item remove, with pattern key option
    $tabGetConfig = array(
        'pattern_key' => '%key_1_%'
    );
    $objRegister->removeItemAll($tabGetConfig);

    echo('Test removing item, with pattern key option: <br />');
    echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
    echo('<br />');

    echo('<br /><br /><br />');
}



// Test item hydrate, if required
if(trim(ToolBoxTable::getItem($_GET, 'hydrate', '0')) == '1')
{
    $tabItem = array(
        'key_9' => 'Test 9', // Ok: create
        'key_10' => 10 // Ok: create
    );
    $objRegister->hydrateItem($tabItem);

    echo('Test hydrate item: <br />');
    echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
    echo('<br />');

    echo('<br /><br /><br />');



    // Test item hydrate flush, if required
    if(trim(ToolBoxTable::getItem($_GET, 'delete', '0')) == '1')
    {
        $objRegister->hydrateItem();

        echo('Test hydrate flush: <br />');
        echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
        echo('<br />');

        echo('<br /><br /><br />');
    }
}



// Remove test database, if required
require_once($strRootAppPath . '/src/database/test/HelpRemoveTestDb.php');


