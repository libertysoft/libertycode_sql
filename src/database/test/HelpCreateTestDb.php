<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/test/MysqlPdoConnection.php');

// Use
use liberty_code\sql\database\connection\library\ConstConnection;
use liberty_code\sql\database\connection\pdo\library\ConstPdoConnection;
use liberty_code\sql\database\test\MysqlPdoConnection;



// Init var
$strDbNm = 'libertycode_sql_test';

$tabConfig = array(
    ConstConnection::TAB_CONFIG_KEY_HOST => 'localhost',
    ConstConnection::TAB_CONFIG_KEY_CHARSET => 'utf8',
    ConstConnection::TAB_CONFIG_KEY_LOGIN => 'root',
    ConstConnection::TAB_CONFIG_KEY_PASSWORD => '',
    ConstPdoConnection::TAB_CONFIG_KEY_OPTION => [
        //PDO::ATTR_AUTOCOMMIT => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT
    ]
);
$objConnection = new MysqlPdoConnection($tabConfig);



// Check database exists
$boolFind = false;
$strSql = "SHOW DATABASES;";
$objResult = $objConnection->executeResult($strSql);
while((($data = $objResult->getFetchData()) !== false) && (!$boolFind))
{
    $boolFind = ($data['Database'] == $strDbNm);
}

$objResult->close();



// Create database, if required
if(!$boolFind)
{
    $strSql = "CREATE DATABASE " . $objConnection->getStrEscapeName($strDbNm) . ";";
    $objConnection->execute($strSql);

    echo(sprintf('Database "%s" created.', $strDbNm));
    echo('<br />' . PHP_EOL);
}



// Re-connect to database
$tabConfig[ConstConnection::TAB_CONFIG_KEY_DB_NAME] = $strDbNm;
$objConnection->setConfig($tabConfig);



echo('<br /><br /><br />' . PHP_EOL . PHP_EOL . PHP_EOL);


