<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\connection\pdo\library;



class ConstPdoConnection
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_DRIVER = 'driver';
    const TAB_CONFIG_KEY_OPTION = 'option';

    // DSN configuration
    const DSN_CONFIG_PATTERN_MAIN = '%1$s:%2$s';

    // Null configuration
    const NULL_CONFIG_VALUE = 'NULL';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the PDO connection configuration standard.';



}