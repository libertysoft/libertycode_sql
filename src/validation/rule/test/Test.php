<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/database/test/HelpInsertTestTable.php');
require_once($strRootAppPath . '/src/validation/rule/test/ValidatorTest.php');

// Use
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;



// Init var
$objCommandFacto = new StandardCommandFactory($objConnection);



// Test data validation
echo('Test validation : <br />');

$tabInfo = array(
    // Test exist Ko
    [
        // Data configuration
        [
            'key_1' => 'lg_1'
        ],
        // Rule configuration
        [
            'key_1' => [
                'sql_exist',
                []
            ]
        ]
    ],

    // Test exist 1 Ko/exist 2 Ko
    [
        // Data configuration
        [
            'key_1' => 'lg_1'
        ],
        // Rule configuration
        [
            'key_1' => [
                [
                    'sql_exist',
                    [
                        'command_factory' => $objCommandFacto,
                        'table_name' => $strTableNmUsr,
                        'column_name' => $strColNmUsrLg,
                        'data_format_callable' => function($value){
                            return strtoupper($value);
                        }
                    ]
                ],
                [
                    'rule_name' => 'sql_exist',
                    'rule_config' => [
                        'command_factory' => $objCommandFacto,
                        'table_name' => $strTableNmUsr,
                        'column_name' => $strColNmUsrLg,
                        'exclude' => [
                            $strColNmUsrId => $strUsrId1
                        ]
                    ]
                ]
            ]
        ]
    ],

    // Test exist 1 Ko/exist 2 Ok
    [
        // Data configuration
        [
            'key_1' => 'lg_2'
        ],
        // Rule configuration
        [
            'key_1' => [
                [
                    'sub_rule_not',
                    [
                        'rule_config' => [
                            [
                                'rule_name' => 'sql_exist',
                                'rule_config' => [
                                    'command_factory' => $objCommandFacto,
                                    'table_name' => $strTableNmUsr,
                                    'column_name' => $strColNmUsrLg,
                                    'exclude' => [
                                        $strColNmUsrId => $strUsrId1
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be unique.'
                    ]
                ],
                [
                    'sql_exist',
                    [
                        'command_factory' => $objCommandFacto,
                        'table_name' => $strTableNmUsr,
                        'column_name' => $strColNmUsrLg
                    ]
                ]
            ]
        ]
    ],

    // Test exist Ok
    [
        // Data configuration
        [
            'key_1' => 'lg_3'
        ],
        // Rule configuration
        [
            'key_1' => [
                [
                    'sub_rule_not',
                    [
                        'rule_config' => [
                            [
                                'sql_exist',
                                [
                                    'command_factory' => $objCommandFacto,
                                    'table_name' => $strTableNmUsr,
                                    'column_name' => $strColNmUsrLg,
                                    'exclude' => [
                                        $strColNmUsrId => $strUsrId3
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be unique.'
                    ]
                ],
                [
                    'sql_exist',
                    [
                        'command_factory' => $objCommandFacto,
                        'table_name' => $strTableNmUsr,
                        'column_name' => $strColNmUsrLg
                    ]
                ]
            ]
        ]
    ]
);

foreach($tabInfo as $info)
{
    echo('Test validation info: <br />');

    try{
        $tabData = $info[0];
        $tabRuleConfig = $info[1];

        echo('Data: <pre>');var_dump(array_keys($tabData));echo('</pre>');
        //echo('Rule configurations: <pre>');var_dump($tabRuleConfig);echo('</pre>');

        // Get validation
        $tabErrorMessage = array();
        $tabErrorException = array();
        $boolIsValid = $objValidator->checkTabDataIsValid(
            $tabData,
            $tabRuleConfig,
            $tabErrorMessage,
            $tabErrorException
        );
        $tabFormatErrorException = array_map(
            function($tabErrorException)
            {
                return array_map(
                    function($objException)
                    {
                        return (
                        ($objException instanceof Exception) ?
                            [
                                get_class($objException),
                                $objException->getMessage()
                            ]:
                            null
                        );
                    },
                    $tabErrorException
                );
            },
            $tabErrorException
        );

        echo('Is valid: <pre>');var_dump($boolIsValid);echo('</pre>');
        echo('Get error messages: <pre>');var_dump($tabErrorMessage);echo('</pre>');
        echo('Get error exceptions: <pre>');var_dump($tabFormatErrorException);echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Remove test database, if required
require_once($strRootAppPath . '/src/database/test/HelpRemoveTestDb.php');


