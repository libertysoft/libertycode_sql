<?php
/**
 * GET arguments:
 * - delete: = 1: Remove data.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/test/HelpCreateTestTable.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;



// Init var
$objCommandFacto = new StandardCommandFactory($objConnection);
$objPersistor = new TablePersistor($objCommandFacto);
$tabConfig = array(
    'table_name' => $strTableNmUsr,
    'column_name_id' => $strColNmUsrId,
    'opt_id_new' => 'connection',
    //*
    'column_name_select' => [
        $strColNmUsrId,
        $strColNmUsrLg,
        $strColNmUsrPw,
        $strColNmUsrNm,
        $strColNmUsrFnm,
        $strColNmUsrEmail
    ],
    //*/
    //'pattern_select' => '',
    //'pattern_where' => '',
    //'pattern_order' => '',
    'pattern_column' => sprintf(
        '%1$s, %2$s',
        $objConnection->getStrEscapeName($strColNmUsrDateCreate),
        $objConnection->getStrEscapeName($strColNmUsrDateUpdate)
    ),
    'pattern_value' => 'NOW(), NOW()',
    'pattern_set' => sprintf(
        '%1$s = NOW()',
        $objConnection->getStrEscapeName($strColNmUsrDateUpdate)
    )
);



// Test insert 1 user
echo('Test insert 1 user: <br />');
$data = array(
    $strColNmUsrId => null,
    $strColNmUsrLg => 'lg_test',
    $strColNmUsrPw => 'pw-test',
    $strColNmUsrNm => 'NM test',
    $strColNmUsrFnm => 'Fnm test',
    $strColNmUsrEmail => 'fnm.nm@test.com'
);
$id = null;

echo('Execute: <pre>');var_dump($objPersistor->createData($data, $tabConfig, $id));echo('</pre>');
echo('Id: <pre>');var_dump($id);echo('</pre>');

echo('<br /><br /><br />');



// Test insert several users
echo('Test insert multi users: <br />');
$data1 = array(
    $strColNmUsrId => null,
    $strColNmUsrLg => 'lg_1',
    $strColNmUsrPw => 'pw-1',
    $strColNmUsrNm => 'NM 1',
    $strColNmUsrFnm => 'Fnm 1',
    $strColNmUsrEmail => null
);
$data2 = array(
    $strColNmUsrId => null,
    $strColNmUsrLg => 'lg_2',
    $strColNmUsrPw => 'pw-2',
    $strColNmUsrNm => 'NM 2',
    $strColNmUsrFnm => 'Fnm 2',
    $strColNmUsrEmail => 2
);
$data3 = array(
    $strColNmUsrId => null,
    $strColNmUsrLg => 'lg_3',
    $strColNmUsrPw => 'pw-3',
    $strColNmUsrNm => 'NM 3',
    $strColNmUsrFnm => 'Fnm 3',
    $strColNmUsrEmail => 'fnm3.nm3@test.com'
);
$tabData = array(
    $data1,
    $data2,
    $data3
);
$tabId = array();

echo('Execute: <pre>');var_dump($objPersistor->createTabData($tabData, $tabConfig, $tabId));echo('</pre>');
echo('Id: <pre>');var_dump($tabId);echo('</pre>');

echo('<br /><br /><br />');



// Test select 1 user
echo('Test select 1 user: <br />');

echo('Get: <pre>');print_r($objPersistor->getData($id, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test select several users
echo('Test select multi users: <br />');

echo('Get: <pre>');var_dump($objPersistor->getTabData($tabId, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test search several users
echo('Test search multi users: <br />');

$tabConfigRequest = array(
    'select' => [
        ['column_name' => 'usr_id'],
        ['column_name' => 'usr_login'],
        ['column_name' => 'usr_password'],
        ['column_name' => 'usr_name'],
        ['column_name' => 'usr_first_name'],
        ['column_name' => 'usr_email']
    ],
    'from' => [
        ['table_name' => 'user']
    ],
    'where' => [
        'content' => [
            [
                'operand' => ['column_name' => 'usr_email'],
                'operator' => 'like',
                'value' => ['value' => '%fnm%']
            ]
        ]
    ],
    'order' => [
        [
            'operand' => ['column_name' => 'usr_login']
        ]
    ]
);

echo('Search: <pre>');var_dump($objPersistor->getTabSearchData($tabConfigRequest));echo('</pre>');

echo('<br /><br /><br />');



// Test update 1 user
echo('Test update 1 user: <br />');
$data = array(
    $strColNmUsrId => $id,
    $strColNmUsrLg => 'lg_test updated',
    $strColNmUsrPw => 'pw-test updated',
    $strColNmUsrNm => 'NM test updated',
    $strColNmUsrFnm => 'Fnm test updated',
    $strColNmUsrEmail => 'fnm.nm@test.com'
);

echo('Execute: <pre>');var_dump($objPersistor->updateData($data, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test update several users
echo('Test update multi users: <br />');
$tabData[0][$strColNmUsrId] = $tabId[0];
$tabData[0][$strColNmUsrLg] = 'lg_1 updated';
$tabData[0][$strColNmUsrPw] = 'pw-1 updated';
$tabData[0][$strColNmUsrNm] = 'NM 1 updated';
$tabData[0][$strColNmUsrFnm] = 'Fnm 1 updated';

$tabData[1][$strColNmUsrId] = $tabId[1];
$tabData[1][$strColNmUsrLg] = 'lg_2 updated';
$tabData[1][$strColNmUsrPw] = 'pw-2 updated';
$tabData[1][$strColNmUsrNm] = 'NM 2 updated';
$tabData[1][$strColNmUsrFnm] = 'Fnm 2 updated';

$tabData[2][$strColNmUsrId] = $tabId[2];
$tabData[2][$strColNmUsrLg] = 'lg_3 updated';
$tabData[2][$strColNmUsrPw] = 'pw-3 updated';
$tabData[2][$strColNmUsrNm] = 'NM 3 updated';
$tabData[2][$strColNmUsrFnm] = 'Fnm 3 updated';

echo('Execute: <pre>');var_dump($objPersistor->updateTabData($tabData, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test select several users
echo('Test select multi users: <br />');

echo('Get: <pre>');var_dump($objPersistor->getTabData(array_merge(array($id), $tabId), $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Delete users, if required
if(trim(ToolBoxTable::getItem($_GET, 'delete', '0')) == '1')
{
    // Test delete 1 user
    echo('Test delete 1 user: <br />');

    echo('Execute: <pre>');var_dump($objPersistor->deleteData($data, $tabConfig));echo('</pre>');

    echo('<br /><br /><br />');



    // Test delete several users
    echo('Test delete multi users: <br />');

    echo('Execute: <pre>');var_dump($objPersistor->deleteTabData($tabData, $tabConfig));echo('</pre>');

    echo('<br /><br /><br />');



    // Test select several users
    echo('Test select multi users: <br />');

    echo('Get: <pre>');var_dump($objPersistor->getTabData(array_merge(array($id), $tabId), $tabConfig));echo('</pre>');

    echo('<br /><br /><br />');
}



// Remove test database, if required
require_once($strRootAppPath . '/src/database/test/HelpRemoveTestDb.php');


