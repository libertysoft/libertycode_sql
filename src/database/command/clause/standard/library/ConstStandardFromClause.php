<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\sql\database\command\clause\library\ConstFromClause;



class ConstStandardFromClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_TABLE_DB = '%1$s.%2$s';
    const SQL_CONFIG_PATTERN_TABLE_ALIAS = '%1$s AS %2$s';

    const SQL_CONFIG_PATTERN_JOIN_MAIN = '(%1$s)';
    const SQL_CONFIG_PATTERN_JOIN_TYPE_INNER = '%1$s INNER JOIN %2$s';
    const SQL_CONFIG_PATTERN_JOIN_TYPE_LEFT_OUTER = '%1$s LEFT OUTER JOIN %2$s';
    const SQL_CONFIG_PATTERN_JOIN_TYPE_RIGHT_OUTER = '%1$s RIGHT OUTER JOIN %2$s';
    const SQL_CONFIG_PATTERN_JOIN_TYPE_FULL_OUTER = '%1$s FULL OUTER JOIN %2$s';
    const SQL_CONFIG_PATTERN_JOIN_ON = '%1$s ON %2$s';

    const SQL_CONFIG_PATTERN_LINK = '%1$s, %2$s';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get configuration join type options array.
     *
     * @return array
     */
    static public function getTabConfigJoinType()
    {
        // Init var
        $result = array(
            ConstFromClause::JOIN_TYPE_CONFIG_INNER => self::SQL_CONFIG_PATTERN_JOIN_TYPE_INNER,
            ConstFromClause::JOIN_TYPE_CONFIG_LEFT_OUTER => self::SQL_CONFIG_PATTERN_JOIN_TYPE_LEFT_OUTER,
            ConstFromClause::JOIN_TYPE_CONFIG_RIGHT_OUTER => self::SQL_CONFIG_PATTERN_JOIN_TYPE_RIGHT_OUTER,
            ConstFromClause::JOIN_TYPE_CONFIG_FULL_OUTER => self::SQL_CONFIG_PATTERN_JOIN_TYPE_FULL_OUTER
        );

        // Return result
        return $result;
    }



}