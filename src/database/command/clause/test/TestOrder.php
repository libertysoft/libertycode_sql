<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/clause/test/Include.php');

// Use
use liberty_code\sql\database\command\clause\library\ToolBoxOrderClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardOrderClause;



// Init var
$config_1 = array(
    [
        'operand' => [
            'db_name' => 'db',
            'table_name' => 'table',
            'column_name' => 'col_1'
        ],
        'operator' => 'asc'
    ],
    [
        'operand' => 'col_2',
        'operator' => 'desc'
    ],
    [
        'operand' => 'col_3',
    ],
    [
        'operator' => 'desc',
        'pattern' => '%s',
    ],
    [
        'pattern' => '[%s]',
    ],
    [
        'pattern' => 'Test',
    ],
    [
        'db_name' => 'db',
        'table_name' => 'table',
        'column_name' => 'col_7'
    ],
    'col_8'
);

$config_2 = array(
    [
        'operator' => 'asc'
    ]
);

$config_3 = array(
    '1',
    ['value' => 1]
);

$config_4 = array(
    [
        'pattern' => 1
    ]
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3, // Ok
    $config_4 // Ko
);



// Test config
process(
    $tabConfig, 
    function($config)
    {
        return ToolBoxOrderClause::checkClauseOrderIsValid($config);
    },
    function($config) use ($objConnection)
    {
        return ToolBoxStandardOrderClause::getStrClauseOrderCommand($objConnection, $config);
    }
);


