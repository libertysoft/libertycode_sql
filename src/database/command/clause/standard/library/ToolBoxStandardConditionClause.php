<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\clause\library\ConstConditionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxConditionClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardExpressionClause;
use liberty_code\sql\database\command\clause\standard\library\ConstStandardConditionClause;



class ToolBoxStandardConditionClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for condition.
     * Condition configuration format: @see ToolBoxConditionClause::checkConditionIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param array $tabConfig
     * @return string
     */
    public static function getStrConditionCommand(ConnectionInterface $objConnection, array $tabConfig)
    {
        // Init var
        //$result = '';
        $tabOperator = ConstStandardConditionClause::getTabConfigOperator();

        // Get operand expression
        $strOperand = '';
        if(isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERAND]))
        {
            $strOperand = ToolBoxStandardExpressionClause::getStrExpressionCommand(
                $objConnection,
                $tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERAND]
            );
        }

        // Get operator
        $strOperator = ConstConditionClause::OPERATOR_CONFIG_EQUAL;
        if(isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERATOR]))
        {
            $strOperator = $tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERATOR];
        }

        // Get value expression
        $strValue = '';
        if(isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_VALUE]))
        {
            $strValue = ToolBoxStandardExpressionClause::getStrExpressionCommand(
                $objConnection,
                $tabConfig[ConstConditionClause::TAB_CONFIG_KEY_VALUE]
            );
        }

        // Get condition
        $tabOperatorMatch = array();
        if(preg_match(
            ConstConditionClause::OPERATOR_CONFIG_REGEXP_PATTERN,
                $strOperator,
                $tabOperatorMatch
        ) == 1)
        {
            $strOperatorPattern = $tabOperatorMatch[1];
            if(
                (strpos($strOperatorPattern,'%2$s') !== false) ||
                (substr_count($strOperatorPattern, '%s') == 2)
            )
            {
                $result = sprintf($strOperatorPattern, $strOperand, $strValue);
            }
            else
            {
                $result = sprintf($strOperatorPattern, $strOperand);
            }
        }
        else
        {
            switch($strOperator)
            {
                case ConstConditionClause::OPERATOR_CONFIG_NULL:
                    $result = sprintf($tabOperator[$strOperator], $strOperand);
                    break;

                default:
                    $result = sprintf($tabOperator[$strOperator], $strOperand, $strValue);
                    break;
            }
        }

        // Set NOT operator, if required
        $not = false;
        if(isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPT_NOT]))
        {
            $not = $tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPT_NOT];
        }
        if(intval($not) != 0)
        {
            $result = sprintf(ConstStandardConditionClause::SQL_CONFIG_PATTERN_NOT, $result);
        }

        // Get pattern value, if required
        if(isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_PATTERN], $result);
        }

        // Set pattern main
        $result = sprintf(ConstStandardConditionClause::SQL_CONFIG_PATTERN_MAIN, $result);

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for condition clause.
     * Configuration array format: @see ToolBoxConditionClause::checkClauseConditionIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param array $tabConfig
     * @return string
     */
    public static function getStrClauseConditionCommand(ConnectionInterface $objConnection, array $tabConfig)
    {
        // Init var
        $result = '';
        $tabType = ConstStandardConditionClause::getTabConfigGroupType();

        // Get group type
        $strType = ConstConditionClause::GROUP_TYPE_CONFIG_AND;
        if(isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE]))
        {
            $strType = $tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE];
        }

        // Run each content
        foreach($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT] as $content)
        {
            // Get content command
            if(ToolBoxConditionClause::checkClauseConditionIsValid($content))
            {
                $strContentCommand = static::getStrClauseConditionCommand($objConnection, $content);
            }
            else
            {
                $strContentCommand = static::getStrConditionCommand($objConnection, $content);
            }

            // Register content command
            if(trim($result) == '')
            {
                $result = $strContentCommand;
            }
            else
            {
                $result = sprintf($tabType[$strType], $result, $strContentCommand);
            }
        }

        // Get pattern value, if required
        if(isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_PATTERN]))
        {
            $result = sprintf(
                $tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_PATTERN],
                $result
            );
        }

        // Set pattern main, if required
        if(trim($result) != '')
        {
            $result = sprintf(ConstStandardConditionClause::SQL_CONFIG_PATTERN_GROUP_MAIN, $result);
        }

        // Return result
        return $result;
    }



}