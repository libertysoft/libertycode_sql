<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/test/HelpGetCommandFacto.php');



// Init var
$objCommand = $objCommandFacto->getObjSelectCommand();

$config_1 = array(
    'select' => [
        [
            'table_name' => 'usr',
            'column_name' => 'usr_login',
            'alias' => 'Login'
        ],
        [
            'table_name' => 'msg',
            'column_name' => 'msg_dt_create',
            'alias' => 'Date creation'
        ],
        [
            'table_name' => 'msg',
            'column_name' => 'msg_txt',
            'alias' => 'Message'
        ]
    ],
    'from' => [
        [
            'type' => 'left_outer',
            'left' => [
                'db_name' => 'libertycode_sql_test',
                'table_name' => 'user',
                'alias' => 'usr'
            ],
            'right' => [
                'db_name' => 'libertycode_sql_test',
                'table_name' => 'message',
                'alias' => 'msg'
            ],
            'on' => [
                'content' => [
                    [
                        'operand' => [
                            'table_name' => 'usr',
                            'column_name' => 'usr_id'
                        ],
                        'value' => [
                            'table_name' => 'msg',
                            'column_name' => 'msg_usr_id'
                        ]
                    ]
                ]
            ]
        ]
    ],
    'where' => [
        'content' => [
            [
                'operand' => [
                    'table_name' => 'msg',
                    'column_name' => 'msg_id'
                ],
                'operator' => 'null',
                'not' => 1
            ]
        ]
    ],
    'order' => [
        [
            'operand' => [
                'table_name' => 'usr',
                'column_name' => 'usr_login'
            ]
        ],
        [
            'operand' => [
                'table_name' => 'msg',
                'column_name' => 'msg_dt_create'
            ]
        ]
    ],
    'limit' => [
        'count' => 4,
        'start' => 1
    ]
);

$config_2 = array(
    'from' => [
        [
            'db_name' => 'libertycode_sql_test',
            'table_name' => 'user',
            'alias' => 'usr'
        ]
    ]
);

$config_3 = array(
    'select' => [
        [
            'table_name' => 'usr',
            'column_name' => 'usr_login',
            'alias' => 'Login'
        ],
        [
            'table_name' => 'msg',
            'column_name' => 'msg_id',
            'pattern' => 'COUNT(%s) AS `Count message`'
        ]
    ],
    'from' => [
        [
            'left' => [
                'db_name' => 'libertycode_sql_test',
                'table_name' => 'user',
                'alias' => 'usr'
            ],
            'right' => [
                'db_name' => 'libertycode_sql_test',
                'table_name' => 'message',
                'alias' => 'msg'
            ],
            'on' => [
                'content' => [
                    [
                        'operand' => [
                            'table_name' => 'usr',
                            'column_name' => 'usr_id'
                        ],
                        'value' => [
                            'table_name' => 'msg',
                            'column_name' => 'msg_usr_id'
                        ]
                    ]
                ]
            ]
        ]
    ],
    'group' => [
        [
            'table_name' => 'usr',
            'column_name' => 'usr_id'
        ]
    ],
    'having' => [
        'content' => [
            [
                'operand' => [
                    'table_name' => 'msg',
                    'column_name' => 'msg_id',
                    'pattern' => 'COUNT(%s)'
                ],
                'operator' => 'greater_equal',
                'value' => ['value' => 2]
            ]
        ]
    ],
    'order' => [
        [
            'operand' => [
                'table_name' => 'usr',
                'column_name' => 'usr_login'
            ]
        ]
    ]
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3 // Ok
);



// Test command
$_GET['clean'] = '1';
require($strRootAppPath . '/src/database/command/test/HelpRunCommand.php');



// Reconnection, if required
if(!$objConnection->checkIsConnect())$objConnection->connect();



// Test build clause: Ko
$tabConfig = array(
    'Select' => array(['test' => 'test']),
    'From' => array(['test' => 'test']),
    'Where' => array(['test' => 'test']),
    'Group' => array(['test' => 'test']),
    'Having' => array(['test' => 'test']),
    'Order' => array(['test' => 'test']),
    'Limit' => array(['test' => 'test'])
);

foreach($tabConfig as $strClause => $config)
{
    // Get info
    $strClauseCall = sprintf('setClause%s', $strClause);

    echo('Test build clause: Ko ' . $strClause . ':<br />');

    try
    {
        $objCommand = $objCommandFacto->getObjSelectCommand();
        $objCommand->$strClauseCall($config);

        echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
        echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
    }
    catch(\Exception $e)
    {
        echo('Config failed: ' . $e->getMessage());
    }

    echo('<br /><br /><br />');
}



// Test build clause: Ok
echo('Test build clause: <br />');

try
{
    $objCommand = $objCommandFacto->getObjSelectCommand(
        array(
            'select_pattern' => '[%s]',
            'from_pattern' => '{%s}',
            'where_pattern' => '[%s]',
            'order_pattern' => '{%s}',
            'limit_pattern' => '[%s]'
        )
    );
    $objCommand->setClauseSelect(
        array(
            [
                'table_name' => 'usr',
                'column_name' => 'usr_login',
                'alias' => 'Login'
            ],
            [
                'table_name' => 'msg',
                'column_name' => 'msg_dt_create',
                'alias' => 'Date creation'
            ],
            [
                'table_name' => 'msg',
                'column_name' => 'msg_txt',
                'alias' => 'Message'
            ]
        ),
        true
    )->setClauseFrom(
        array(
            [
                'type' => 'left_outer',
                'left' => [
                    'db_name' => 'libertycode_sql_test',
                    'table_name' => 'user',
                    'alias' => 'usr'
                ],
                'right' => [
                    'db_name' => 'libertycode_sql_test',
                    'table_name' => 'message',
                    'alias' => 'msg'
                ],
                'on' => [
                    'content' => [
                        [
                            'operand' => [
                                'table_name' => 'usr',
                                'column_name' => 'usr_id'
                            ],
                            'value' => [
                                'table_name' => 'msg',
                                'column_name' => 'msg_usr_id'
                            ]
                        ]
                    ]
                ]
            ]
        )
    )->setClauseWhere(
        array(
            'content' => [
                [
                    'operand' => [
                        'table_name' => 'msg',
                        'column_name' => 'msg_id'
                    ],
                    'operator' => 'null',
                    'not' => 1
                ]
            ]
        )
    )->setClauseOrder(
        array(
            [
                'operand' => [
                    'table_name' => 'usr',
                    'column_name' => 'usr_login'
                ]
            ],
            [
                'operand' => [
                    'table_name' => 'msg',
                    'column_name' => 'msg_dt_create'
                ]
            ]
        )
    )->setClauseLimit(
        array(
            'count' => 4,
            'start' => 1
        )
    );

    echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
    echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
}
catch(\Exception $e)
{
    echo('Config failed: ' . $e->getMessage());
}

echo('<br /><br /><br />');



// Test build clause: Ok
echo('Test build clause: <br />');

try
{
    $objCommand = $objCommandFacto->getObjSelectCommand(
        array(
            'select_pattern' => '[SELECT *]',
            'from_pattern' => '{%s}',
            'group_pattern' => '[%s]',
            'having_pattern' => '{%s}',
            'order_pattern' => '[%s]'
        )
    );
    $objCommand->setClauseFrom(
        array(
            [
                'left' => [
                    'db_name' => 'libertycode_sql_test',
                    'table_name' => 'user',
                    'alias' => 'usr'
                ],
                'right' => [
                    'db_name' => 'libertycode_sql_test',
                    'table_name' => 'message',
                    'alias' => 'msg'
                ],
                'on' => [
                    'content' => [
                        [
                            'operand' => [
                                'table_name' => 'usr',
                                'column_name' => 'usr_id'
                            ],
                            'value' => [
                                'table_name' => 'msg',
                                'column_name' => 'msg_usr_id'
                            ]
                        ]
                    ]
                ]
            ]
        )
    )->setClauseGroup(
        array(
            [
                'table_name' => 'usr',
                'column_name' => 'usr_id'
            ]
        )
    )->setClauseHaving(
        array(
            'content' => [
                [
                    'operand' => [
                        'table_name' => 'msg',
                        'column_name' => 'msg_id',
                        'pattern' => 'COUNT(%s)'
                    ],
                    'operator' => 'greater_equal',
                    'value' => ['value' => 2]
                ]
            ]
        )
    )->setClauseOrder(
        array(
            [
                'operand' => [
                    'table_name' => 'usr',
                    'column_name' => 'usr_login'
                ]
            ]
        )
    );

    echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
    echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
}
catch(\Exception $e)
{
    echo('Config failed: ' . $e->getMessage());
}

echo('<br /><br /><br />');


