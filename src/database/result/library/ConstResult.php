<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\result\library;



class ConstResult
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Exception message constants
    const EXCEPT_MSG_DATA_INDEX_INVALID_FORMAT = 'Following index "%1$s" invalid! The index must be a strictly positive integer.';
    const EXCEPT_MSG_DATA_NAME_INVALID_FORMAT = 'Following name "%1$s" invalid! The name must be a string, not empty.';
    const EXCEPT_MSG_DATA_INDEX_NOT_FOUND = 'Following index "%1$s" not found in result!';
    const EXCEPT_MSG_COL_NOT_FOUND = 'Following column "%1$s" not found in result!';
    const EXCEPT_MSG_ROW_NOT_FOUND = 'Following row "%1$s" not found in result!';



}