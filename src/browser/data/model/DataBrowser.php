<?php
/**
 * Description :
 * This class allows to define data SQL browser class.
 * Data SQL browser is default SQL browser,
 * where items are stored like:
 * Specific column name = list of item keys - Specific column name = list of item values
 *
 * Data SQL browser uses the following specified browsing configuration:
 * [
 *     Default SQL browser configuration,
 *
 *     query_page(optional): array (select command configuration: @see SelectCommandInterface ),
 *
 *     column_name_item_id(required): "string column name where item ids stored",
 *
 *     column_name_item_key(required): "string column name where item keys stored",
 *
 *     column_name_item_value(required): "string column name where item values stored"
 * ]
 *
 * Note:
 * -> Page calculation:
 * query_page, on browsing configuration, is used for page calculation.
 * If query_page not found, try automatically to build query for page calculation.
 * Each data (row) on first column is considered as item id.
 * This query is used to get list of ids, could be scoped from page selection (limitation).
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\browser\data\model;

use liberty_code\sql\browser\model\DefaultBrowser;

use liberty_code\item_browser\browser\library\ConstBrowser as BaseConstBrowser;
use liberty_code\sql\database\command\clause\library\ConstExpressionClause;
use liberty_code\sql\database\command\clause\library\ConstConditionClause;
use liberty_code\sql\database\command\clause\library\ConstLimitClause;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;
use liberty_code\sql\database\command\select\api\SelectCommandInterface;
use liberty_code\sql\persistence\data\library\ToolBoxDataPersistor;
use liberty_code\sql\browser\data\library\ConstDataBrowser;
use liberty_code\sql\browser\data\exception\ConfigInvalidFormatException;
use liberty_code\sql\browser\data\exception\QueryPageInvalidFormatException;



class DataBrowser extends DefaultBrowser
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case BaseConstBrowser::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getQueryItemCountEngine()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strColNmId = $tabConfig[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_ID];

        // Determine query for total count of item
        $result = $this->getQuery();
        if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT, $result))
        {
            unset($result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT]);
        }
        if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN, $result))
        {
            unset($result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN]);
        }
        if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT, $result))
        {
            unset($result[ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT]);
        }
        $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT] = array(
            [
                ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME => $strColNmId,
                ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => ConstDataBrowser::ITEM_COUNT_EXPRESSION_PATTERN
            ]
        );

        // Return result
        return $result;
    }



    /**
     * Get select command query (configuration),
     * for page calculation.
     * This select query allows to get list of ids,
     * scoped from current page selection.
     *
     * @return array
     */
    protected function getQueryPage()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strColNmId = $tabConfig[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_ID];

        // Determine query for page
        $result = $this->getQuery();
        if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT, $result))
        {
            unset($result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT]);
        }
        if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN, $result))
        {
            unset($result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN]);
        }
        if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT, $result))
        {
            unset($result[ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT]);
        }
        $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT] = array(
            [
                ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME => $strColNmId
            ]
        );

        if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP, $result))
        {
            unset($result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP]);
        }
        if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP_PATTERN, $result))
        {
            unset($result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP_PATTERN]);
        }
        $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP] = array(
            [
                ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME => $strColNmId
            ]
        );

        // Get query for page calculation, in configuration, if found
        if(array_key_exists(ConstDataBrowser::TAB_CONFIG_KEY_QUERY_PAGE, $tabConfig))
        {
            $result = $tabConfig[ConstDataBrowser::TAB_CONFIG_KEY_QUERY_PAGE];
        }

        // Set limit clause
        $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT] = array(
            ConstLimitClause::TAB_CONFIG_KEY_COUNT => $this->getIntItemCountOnActivePage(),
            ConstLimitClause::TAB_CONFIG_KEY_START => ($this->getIntActivePageIndex() * $this->getIntItemCountPerPage())
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws QueryPageInvalidFormatException
     */
    protected function getQueryCalculatePage(array $query)
    {
        // Init var
        $result = $query;
        $objConnection = $this->getObjCommandFactory()->getObjConnection();
        $objCommand = $this->getObjCommandPage();
        $tabConfig = $this->getTabConfig();
        $strColNmId = $tabConfig[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_ID];

        // Run query page to get list of ids, scoped from page selection
        $tabId = array();
        $boolFound = false;
        if(($resultQuery = $objCommand->executeResult()) !== false)
        {
            $boolFound = true;
            while(
                (($data = $resultQuery->getFetchData()) !== false) &&
                $boolFound
            )
            {
                $tabKey = array_keys($data);
                $boolFound = (count($tabKey) >= 1);
                if($boolFound)
                {
                    $tabId[] = $objConnection->getStrEscapeValue($data[$tabKey[0]]);
                }
            }
            $resultQuery->close();
        }

        // Check id selection found
        if(!$boolFound)
        {
            throw new QueryPageInvalidFormatException(serialize($objCommand->getTabConfig()));
        }

        // Set where clause, with list of ids selection
        // if(count($tabId) > 0)
        {
            $strListId = implode(', ', $tabId);
            $tabClauseWhere = array(
                [
                    ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmId,
                    ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_IN,
                    ConstConditionClause::TAB_CONFIG_KEY_VALUE => (
                        (trim($strListId) == '') ?
                            [
                                // Page selection out of scope, no id found
                                ConstExpressionClause::TAB_CONFIG_KEY_VALUE => null
                            ] :
                            [
                                ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $strListId
                            ]
                    )
                ]
            );

            // Include previous where clause, if required
            if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE, $result))
            {
                array_unshift(
                    $tabClauseWhere,
                    $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                );
            }

            $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => $tabClauseWhere
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get new select SQL command object,
     * to get list of ids, scoped from page selection.
     *
     * @param boolean $boolSort = true
     * @param boolean $boolCriteria = true
     * @return SelectCommandInterface
     */
    public function getObjCommandPage(
        $boolSort = true,
        $boolCriteria = true
    )
    {
        // Init var
        $query = $this->getQueryPage();
        $tabConfig = $this->getQueryCalculate(
            $query,
            false,
            $boolSort,
            $boolCriteria
        );
        $result = $this->getObjCommandFactory()->getObjSelectCommand($tabConfig);

        // Return result
        return $result;
    }





    // Methods getters (Browser interface implementation)
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabItem()
    {
        // Init var
        $result = parent::getTabItem();
        $tabConfig = $this->getTabConfig();
        $strColNmId = $tabConfig[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_ID];
        $strColNmKey = $tabConfig[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_KEY];
        $strColNmValue = $tabConfig[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_VALUE];

        // Format data, if required
        if(count($result) > 0)
        {
            $result = ToolBoxDataPersistor::getTabDataFormatResult(
                $result,
                $strColNmKey,
                $strColNmValue,
                $strColNmId
            );
        }

        // Return result
        return $result;
    }



}