<?php
/**
 * Description :
 * This class allows to describe behavior of SQL database result class.
 * Result allows to read and get query results,
 * from specific SQL data storage.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\result\api;



interface ResultInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Initialize reading data.
     * Reset cursor position.
     */
    public function initialize();



    /**
     * Close result to free resources.
     */
    public function close();





    // Methods fetching
    // ******************************************************************************

    /**
     * Get data from cursor current position.
     * Return data if found, false else
     * Data format:
     * {
     *     column_name_1 => value 1,
     *     ...,
     *     column_name_N => value N
     * }
     *
     * @return boolean|array
     */
    public function getFetchData();





    // Methods count
    // ******************************************************************************

    /**
     * Get number of columns.
     *
     * @return integer
     */
    public function getIntCountCol();



    /**
     * Get number of rows.
     *
     * @return integer
     */
    public function getIntCountRow();





    // Methods data
    // ******************************************************************************

    /**
     * Get index array of data.
     * Result format:
     * {
     *     // Data (row) 1
     *     {
     *         column_name_1 => value 1,
     *         ...,
     *         column_name_N => value N
     *     },
     *     ...,
     *     // Data (row) N
     *     {
     *         column_name_1 => value 1,
     *         ...,
     *         column_name_N => value N
     *     }
     * }
     *
     * @return array
     */
    public function getTabData();



    /**
     * Get column name,
     * from specified column index.
     *
     * @param integer $intColIndex
     * @return string
     */
    public function getStrColName($intColIndex);



    /**
     * Get index column,
     * from specified column name.
     *
     * @param string $strColNm
     * @return integer
     */
    public function getIntColIndex($strColNm);



    /**
     * Get data,
     * from specified row index,
     * and specified column index or name.
     *
     * @param int $intRowIndex
     * @param integer|string $col
     * @return mixed
     */
    public function getRowData($intRowIndex, $col);



}