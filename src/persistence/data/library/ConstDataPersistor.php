<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\persistence\data\library;



class ConstDataPersistor
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_NAME = 'column_name_attribute_name';
    const TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_VALUE = 'column_name_attribute_value';
    const TAB_CONFIG_KEY_ATTRIBUTE_NAME_SELECT = 'attribute_name_select';
    

	
    // Exception message constants
    const EXCEPT_MSG_ACTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid for action "%2$s"! 
        The config must be an array, not empty and following the adequat data persistor action configuration standard.';



}