<?php
/**
 * Description :
 * This class allows to define default SQL browser class.
 * Default SQL browser is criteria operator browser,
 * that uses specified selection query and specified browsing configuration,
 * to build select command query (configuration)
 * to provide items, from specific SQL data storage.
 *
 * Default SQL browser uses the following specified browsing configuration:
 * [
 *     Criteria operator browser configuration,
 *
 *     query(required): array (select command configuration: @see SelectCommandInterface ),
 *
 *     query_item_count(optional): array (select command configuration: @see SelectCommandInterface )
 * ]
 *
 * Default SQL browser uses the following specified sort operator configuration:
 * -> Configuration:
 * [
 *     clause(required): array (@see ToolBoxOrderClause::checkClauseOrderIsValid() ),
 *     default_value(optional): mixed value (string|boolean|numeric|null)
 * ]
 * -> Value:
 * mixed value (string|boolean|numeric|null)
 *
 * Default SQL browser uses the following specified criteria operator configuration:
 * -> Configuration:
 * [
 *     clause(required): array (
 *         @see ToolBoxConditionClause::checkClauseConditionIsValid() or
 *         @see ToolBoxConditionClause::checkConditionIsValid()
 *     ),
 *     default_value(optional): mixed value (string|boolean|numeric|null)
 * ]
 * -> Value:
 * mixed value (string|boolean|numeric|null)
 *
 * Note:
 * -> Total count of item calculation:
 * query_item_count, on browsing configuration, is used for total count of item calculation.
 * If query_item_count not found, try automatically to build query for count.
 * The data on first row, first column, is considered as count.
 * -> Operation (sort and criteria) configuration default value:
 * If no operation value found, operation not considered.
 * Unless if default_value is set on operation configuration, then default_value used as operation value.
 * -> Operator (sort and criteria) configuration:
 * All tag values ('[%value%]') found, in operator configuration, on array values, will be replaced by associated operator value.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\browser\model;

use liberty_code\item_browser\browser\operation\criteria\model\CriteriaBrowser;

use liberty_code\register\register\api\RegisterInterface;
use liberty_code\item_browser\operation\model\OperatorData;
use liberty_code\item_browser\browser\library\ConstBrowser as BaseConstBrowser;
use liberty_code\item_browser\browser\operation\sort\library\ConstSortBrowser;
use liberty_code\item_browser\browser\operation\criteria\library\ConstCriteriaBrowser;
use liberty_code\sql\database\command\clause\library\ConstConditionClause;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;
use liberty_code\sql\database\command\select\api\SelectCommandInterface;
use liberty_code\sql\database\command\select\exception\ConfigInvalidFormatException as CommandConfigInvalidFormatException;
use liberty_code\sql\database\command\factory\api\CommandFactoryInterface;
use liberty_code\sql\browser\library\ConstBrowser;
use liberty_code\sql\browser\library\ToolBoxBrowser;
use liberty_code\sql\browser\exception\CommandFactoryInvalidFormatException;
use liberty_code\sql\browser\exception\ConfigInvalidFormatException;
use liberty_code\sql\browser\exception\QueryItemCountInvalidFormatException;



/**
 * @method null|CommandFactoryInterface getObjCommandFactory() Get command factory object.
 */
abstract class DefaultBrowser extends CriteriaBrowser
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************

    /**
     * @inheritdoc
     * @param CommandFactoryInterface $objCommandFactory
     */
    public function __construct(
        OperatorData $objOperatorData,
        CommandFactoryInterface $objCommandFactory,
        RegisterInterface $objCacheRegister = null,
        array $tabConfig = null,
        array $tabOperationConfig = array(),
        array $tabOperationValue = array()
    )
    {
        // Call parent constructor
        parent::__construct(
            $objOperatorData,
            $objCacheRegister,
            $tabConfig,
            $tabOperationConfig,
            $tabOperationValue
        );

        // Init command factory
        $this->setCommandFactory($objCommandFactory);
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBrowser::DATA_KEY_DEFAULT_COMMAND_FACTORY))
        {
            $this->__beanTabData[ConstBrowser::DATA_KEY_DEFAULT_COMMAND_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstBrowser::DATA_KEY_DEFAULT_COMMAND_FACTORY
		);
		$result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstBrowser::DATA_KEY_DEFAULT_COMMAND_FACTORY:
                    CommandFactoryInvalidFormatException::setCheck($value);
                    break;

                case BaseConstBrowser::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkOperationConfigValid($strType, $strKey, $config, &$error = null)
    {
        // Init var
        $result =
            // Check is array
            is_array($config) &&

            // Check valid clause
            array_key_exists(ConstBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE, $config) &&
            (!is_null($config[ConstBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE])) &&

            // Check valid default value
            (
                (!array_key_exists(ConstBrowser::TAB_OPERATION_CONFIG_KEY_DEFAULT_VALUE, $config)) ||
                $this->checkOperationValueValid(
                    $strType,
                    $strKey,
                    $config[ConstBrowser::TAB_OPERATION_CONFIG_KEY_DEFAULT_VALUE]
                )
            );

        // Set error message, if check not pass
        if(!$result)
        {
            $error = ConstBrowser::EXCEPT_MSG_OPERATION_CONFIG_INVALID_FORMAT;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function checkOperationValueValid($strType, $strKey, $value, &$error = null)
    {
        // Init var
        $result =
            // Check valid value
            (
                is_string($value) ||
                is_bool($value) ||
                is_numeric($value) ||
                is_null($value)
            );

        // Set error message, if check not pass
        if(!$result)
        {
            $error = ConstBrowser::EXCEPT_MSG_OPERATION_VALUE_INVALID_FORMAT;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************



    /**
     * Get calculated select command query (configuration),
     * for total count of item calculation.
     *
     * @return array
     */
    abstract protected function getQueryItemCountEngine();



    /**
     * Get select command query (configuration),
     * for total count of item calculation.
     *
     * @return array
     */
    protected function getQueryItemCount()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getQueryItemCountEngine();

        // Get query for total count of item calculation, in configuration, if found
        if(array_key_exists(ConstBrowser::TAB_CONFIG_KEY_QUERY_ITEM_COUNT, $tabConfig))
        {
            $result = $tabConfig[ConstBrowser::TAB_CONFIG_KEY_QUERY_ITEM_COUNT];
        }

        // Return result
        return $result;
    }



    /**
     * Get select command query (configuration), calculated with page browsing configuration,
     * from specified select command query.
     *
     * @param array $query
     * @return array
     */
    abstract protected function getQueryCalculatePage(array $query);



    /**
     * Get clause from specified operation,
     * build from operation configuration and operation value.
     *
     * @param string $strType
     * @param string $strKey
     * @return null|mixed
     */
    protected function getClauseOperation($strType, $strKey)
    {
        // Init var
        $result = null;
        $tabConfig = $this->getOperationConfig($strType, $strKey);
        $clause = $tabConfig[ConstBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE];

        // Get value
        $boolFind = false;
        $value = null;
        // Get value from operator data, if found
        if($this->checkOperationValueExists($strType, $strKey))
        {
            $value = $this->getOperationValue($strType, $strKey);
            $boolFind = true;
        }
        // Get configured default value, if found
        else if(array_key_exists(ConstBrowser::TAB_OPERATION_CONFIG_KEY_DEFAULT_VALUE, $tabConfig))
        {
            $value = $tabConfig[ConstBrowser::TAB_OPERATION_CONFIG_KEY_DEFAULT_VALUE];
            $boolFind = true;
        }

        // Get operation from clause and value, if required (value found)
        if($boolFind)
        {
            $result = ToolBoxBrowser::getOperation($clause, $value);
        }

        // Return result
        return $result;
    }



    /**
     * Get select command query (configuration), calculated with sort operator browsing configuration,
     * from specified select command query.
     *
     * @param array $query
     * @return array
     * @throws CommandConfigInvalidFormatException
     */
    protected function getQueryCalculateSort(array $query)
    {
        // Init var
        $result = $query;

        // Get order clause
        $tabClauseOrder = array();
        $tabSortKey = array_keys($this->getTabSortConfig());
        foreach($tabSortKey as $strKey)
        {
            $clause = $this->getClauseOperation(ConstSortBrowser::OPERATION_TYPE_SORT, $strKey);
            if(!is_null($clause))
            {
                $tabClauseOrder[] = $clause;
            }
        }

        // Set order clause, if required
        if(count($tabClauseOrder) > 0)
        {
            // Include previous order clause, if required
            if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER, $result))
            {
                $tabClauseOrder = array_merge($result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER], $tabClauseOrder);
            }

            $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER] = $tabClauseOrder;
        }

        // Check valid query
        CommandConfigInvalidFormatException::setCheck($result);

        // Return result
        return $result;
    }



    /**
     * Get select command query (configuration), calculated with criteria operator browsing configuration,
     * from specified select command query.
     *
     * @param array $query
     * @return array
     * @throws CommandConfigInvalidFormatException
     */
    protected function getQueryCalculateCriteria(array $query)
    {
        // Init var
        $result = $query;

        // Get criteria clause
        $tabClauseCrit = array();
        $tabCritKey = array_keys($this->getTabCriteriaConfig());
        foreach($tabCritKey as $strKey)
        {
            $clause = $this->getClauseOperation(ConstCriteriaBrowser::OPERATION_TYPE_CRITERIA, $strKey);
            if(!is_null($clause))
            {
                $tabClauseCrit[] = $clause;
            }
        }

        // Set criteria clause, if required
        if(count($tabClauseCrit) > 0)
        {
            // Include previous criteria clause, if required
            if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE, $result))
            {
                array_unshift(
                    $tabClauseCrit,
                    $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                );
            }

            $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => $tabClauseCrit
            );
        }

        // Check valid query
        CommandConfigInvalidFormatException::setCheck($result);

        // Return result
        return $result;
    }



    /**
     * Get select command query (configuration), calculated with browsing configuration, if specified,
     * from specified select command query.
     * If select command query is null, it takes default query (@see DefaultBrowser::getQuery() ).
     *
     * @param array $query = null
     * @param boolean $boolPage = true
     * @param boolean $boolSort = true
     * @param boolean $boolCriteria = true
     * @return array
     */
    protected function getQueryCalculate(
        array $query = null,
        $boolPage = true,
        $boolSort = true,
        $boolCriteria = true
    )
    {
        // Init var
        $result = (is_null($query) ? $this->getQuery() : $query);
        $boolPage = (is_bool($boolPage) ? $boolPage : true);
        $boolSort = (is_bool($boolSort) ? $boolSort : true);
        $boolCriteria = (is_bool($boolCriteria) ? $boolCriteria : true);

        // Calculate page browsing configuration, if required
        if($boolPage)
        {
            $result = $this->getQueryCalculatePage($result);
        }

        // Calculate sort operator configuration, if required
        if($boolSort)
        {
            $result = $this->getQueryCalculateSort($result);
        }

        // Calculate criteria operator configuration, if required
        if($boolCriteria)
        {
            $result = $this->getQueryCalculateCriteria($result);
        }

        // Return result
        return $result;
    }



    /**
     * Get new select SQL command object,
     * to get index array of items.
     *
     * @param boolean $boolPage = true
     * @param boolean $boolSort = true
     * @param boolean $boolCriteria = true
     * @return SelectCommandInterface
     */
    public function getObjCommand(
        $boolPage = true,
        $boolSort = true,
        $boolCriteria = true
    )
    {
        // Init var
        $query = $this->getQuery();
        $tabConfig = $this->getQueryCalculate(
            $query,
            $boolPage,
            $boolSort,
            $boolCriteria
        );
        $result = $this->getObjCommandFactory()->getObjSelectCommand($tabConfig);

        // Return result
        return $result;
    }



    /**
     * Get new select SQL command object,
     * to get total count of item.
     *
     * @param boolean $boolSort = true
     * @param boolean $boolCriteria = true
     * @return SelectCommandInterface
     */
    public function getObjCommandItemCount(
        $boolSort = true,
        $boolCriteria = true
    )
    {
        // Init var
        $query = $this->getQueryItemCount();
        $tabConfig = $this->getQueryCalculate(
            $query,
            false,
            $boolSort,
            $boolCriteria
        );
        $result = $this->getObjCommandFactory()->getObjSelectCommand($tabConfig);

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set command factory object.
     *
     * @param CommandFactoryInterface $objCommandFactory
     */
    public function setCommandFactory(CommandFactoryInterface $objCommandFactory)
    {
        $this->beanSet(ConstBrowser::DATA_KEY_DEFAULT_COMMAND_FACTORY, $objCommandFactory);
    }





    // Methods getters (Browser interface implementation)
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws QueryItemCountInvalidFormatException
     */
    protected function getIntItemCountEngine()
    {
        // Init var
        $result = null;
        $objCommand = $this->getObjCommandItemCount();

        // Run query result
        if(($resultQuery = $objCommand->executeResult()) !== false)
        {
            // Try to get item count on first row and first column
            if(($data = $resultQuery->getFetchData()) !== false)
            {
                $tabKey = array_keys($data);
                if(count($tabKey) >= 1)
                {
                    $intCount = $data[$tabKey[0]];

                    // Register item count, if valid
                    if(
                        (
                            is_int($intCount) ||
                            (is_string($intCount) && ctype_digit($intCount))
                        ) &&
                        (intval($intCount) >= 0)
                    )
                    {
                        $result = intval($intCount);
                    }
                }
            }
            $resultQuery->close();
        }

        // Check item count found
        if(is_null($result))
        {
            throw new QueryItemCountInvalidFormatException(serialize($objCommand->getTabConfig()));
        }


        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabItem()
    {
        // Init var
        $result = array();
        $objCommand = $this->getObjCommand();

        // Run query result
        if(($resultQuery = $objCommand->executeResult()) !== false)
        {
            while(($data = $resultQuery->getFetchData()) !== false)
            {
                // Register data in result
                $result[] = $data;
            }
            $resultQuery->close();
        }

        // Return result
        return $result;
    }



}