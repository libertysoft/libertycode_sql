<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\insert\standard\library;



class ConstStandardInsertCommand
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_FROM = 'INSERT INTO %1$s';
    const SQL_CONFIG_PATTERN_COLUMN = '(%1$s)';
    const SQL_CONFIG_PATTERN_VALUE = 'VALUES %1$s';
}