<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;



class ConstTableClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_TABLE_DB_NAME = 'db_name';
    const TAB_CONFIG_KEY_TABLE_NAME = 'table_name';
    const TAB_CONFIG_KEY_TABLE_PATTERN = 'pattern';
}