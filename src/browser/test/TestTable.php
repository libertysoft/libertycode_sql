<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/test/HelpInsertTestTable.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\item_browser\operation\model\OperatorData;
use liberty_code\item_browser\browser\library\ConstBrowser as BaseConstBrowser;
use liberty_code\item_browser\browser\page\library\ConstPageBrowser;
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;
use liberty_code\sql\browser\library\ConstBrowser;
use liberty_code\sql\browser\table\model\TableBrowser;



// Init var
$tabConfig = array(
    BaseConstBrowser::TAB_CONFIG_KEY_QUERY => [
        'select' => [
            [
                'column_name' => $strColNmUsrId,
            ],
            [
                'column_name' => $strColNmUsrLg,
            ],
            [
                'column_name' => $strColNmUsrNm,
            ],
            [
                'column_name' => $strColNmUsrFnm,
            ]
        ],
        'from' => [
            [
                'table_name' => $strTableNmUsr
            ]
        ],
        'where' => [
            'content' => [
                [
                    'operand' => $strColNmUsrId,
                    'operator' => 'less_equal',
                    'value' => ['value' => 3]
                ]
            ]
        ]
    ],
    ConstPageBrowser::TAB_CONFIG_KEY_ITEM_COUNT_PER_PAGE => 2,
    /*
    ConstBrowser::TAB_CONFIG_KEY_QUERY_ITEM_COUNT => [
        'select' => [
            [
                'pattern' => '(COUNT(*) - 1) AS count_item'
            ]
        ],
        'from' => [
            [
                'table_name' => $strTableNmUsr
            ]
        ],
        'where' => [
            'content' => [
                [
                    'operand' => $strColNmUsrId,
                    'operator' => 'less_equal',
                    'value' => ['value' => 3]
                ]
            ]
        ]
    ]
    //*/
);

$tabConfigCriteria = array(
    $strColNmUsrLg => [
        ConstBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
            'operand' => $strColNmUsrLg,
            'operator' => 'like',
            'value' => ['value' => '%' . ConstBrowser::OPERATION_TAG_VALUE . '%']
        ],
        ConstBrowser::TAB_OPERATION_CONFIG_KEY_DEFAULT_VALUE => '_'
    ]
);

$tabConfigSort = array(
    $strColNmUsrLg => [
        ConstBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
            'operand' => $strColNmUsrLg,
            'operator' => ConstBrowser::OPERATION_TAG_VALUE
        ]
    ],
    $strColNmUsrNm => [
        ConstBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
            'operand' => $strColNmUsrNm,
            'operator' => ConstBrowser::OPERATION_TAG_VALUE
        ]
    ]
);

$objOperatorData = new OperatorData();
$objCommandFacto = new StandardCommandFactory($objConnection);
$objRegister = new MemoryRegister();
$objBrowser = new TableBrowser(
    $objOperatorData,
    $objCommandFacto,
    $objRegister,
    $tabConfig,
    array(),
    array()
);

$objBrowser->hydrateCriteriaConfig($tabConfigCriteria);
$objBrowser->hydrateSortConfig($tabConfigSort);



// Test get items
$tabConfig = array(
    [
        [
            $strColNmUsrLg => '1'
        ],
        [],
        -1
    ], // Ko: bad format on active page index
    [
        [
            $strColNmUsrLg => '1'
        ],
        [],
        0
    ], // Ok
    [
        [],
        [
            $strColNmUsrLg => 'desc',
            $strColNmUsrNm => 'desc'
        ],
        0
    ], // Ok
    [
        [
            $strColNmUsrLg => 'lg'
        ],
        [
            $strColNmUsrLg => 'desc',
            $strColNmUsrNm => 'asc'
        ],
        1
    ], // Ok
    [
        [],
        [],
        2
    ] // Ko: active page index out of bounds
);

foreach($tabConfig as $config)
{
    echo('Test get items: <br />');
    try{
        $tabValueCriteria = $config[0];
        $tabValueSort = $config[1];
        $intActivePageIndex = $config[2];

        $objBrowser->hydrateCriteriaValue($tabValueCriteria);
        $objBrowser->hydrateSortValue($tabValueSort);
        $objBrowser->setActivePageIndex($intActivePageIndex);

        echo('Get: SQL: <pre>');var_dump($objBrowser->getObjCommand()->getStrCommand());echo('</pre>');
        echo('Get: Item count: <pre>');var_dump($objBrowser->getIntItemCount());echo('</pre>');
        echo('Get: Item count per page: <pre>');var_dump($objBrowser->getIntItemCountPerPage());echo('</pre>');
        echo('Get: Item count on active page: <pre>');var_dump($objBrowser->getIntItemCountOnActivePage());echo('</pre>');
        echo('Get: Page count: <pre>');var_dump($objBrowser->getIntPageCount());echo('</pre>');
        echo('Get: Active page index: <pre>');var_dump($objBrowser->getIntActivePageIndex());echo('</pre>');

        echo('Get: Items: <pre>');var_dump($objBrowser->getTabItem());echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get cache register items
$tabCacheItem = $objBrowser->getObjCacheRegister()->getTabItem();
echo('Cache register items: <pre>');var_dump($tabCacheItem);echo('</pre>');

echo('<br /><br /><br />');



// Remove test database, if required
$_GET['clean'] = '1';
require_once($strRootAppPath . '/src/database/test/HelpRemoveTestDb.php');


