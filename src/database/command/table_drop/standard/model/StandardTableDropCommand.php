<?php
/**
 * Description :
 * This class allows to define standard table drop SQL command class.
 * Standard table drop command allows to design string SQL table drop command,
 * from default configuration,
 * and uses standard SQL.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\table_drop\standard\model;

use liberty_code\sql\database\command\table_drop\model\DefaultTableDropCommand;

use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardTableClause;
use liberty_code\sql\database\command\table_drop\library\ConstTableDropCommand;
use liberty_code\sql\database\command\table_drop\exception\ConfigInvalidFormatException;
use liberty_code\sql\database\command\table_drop\standard\library\ConstStandardTableDropCommand;



class StandardTableDropCommand extends DefaultTableDropCommand
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for table clause.
     *
     * @return string
     */
    protected function getStrClauseTableCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
            isset($tabConfig[ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE]) ?
                sprintf(
                    ConstStandardTableDropCommand::SQL_CONFIG_PATTERN_TABLE,
                    ToolBoxStandardTableClause::getStrClauseTableCommand(
                        $objConnection,
                        $tabConfig[ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE]
                    )
                ) :
                ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrCommand()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Build SQL command
        $result = $this->getStrClauseTableCommand();

        // Return result
        return $result;
    }



}