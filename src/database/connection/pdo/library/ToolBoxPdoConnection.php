<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\connection\pdo\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxPdoConnection extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************
	
	/**
     * Get constant PDO value type, from specified value.
     *
     * @param mixed $value
	 * @return int
     */
    public static function getIntValueType($value)
    {
        // Init var
        $result = \PDO::PARAM_STR;

        if(is_int($value))
        {
            $result = \PDO::PARAM_INT;
        }
        else if(is_bool($value))
        {
            $result = \PDO::PARAM_BOOL;
        }
        else if(is_null($value))
        {
            $result = \PDO::PARAM_NULL;
        }

        // Return result
        return $result;
    }
	
	
	
}