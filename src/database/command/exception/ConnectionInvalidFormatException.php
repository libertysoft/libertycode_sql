<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\exception;

use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\library\ConstCommand;



class ConnectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $connection
     */
	public function __construct($connection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstCommand::EXCEPT_MSG_CONNECTION_INVALID_FORMAT,
            mb_strimwidth(strval($connection), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified connection has valid format.
	 * 
     * @param mixed $connection
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($connection)
    {
		// Init var
		$result = (
			(is_null($connection)) ||
			($connection instanceof ConnectionInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($connection);
		}
		
		// Return result
		return $result;
    }
	
	
	
}