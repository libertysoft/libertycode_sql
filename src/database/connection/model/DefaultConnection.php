<?php
/**
 * Description :
 * This class allows to define default SQL database connection class.
 * Can be consider is base of all SQL database connection type.
 * Default SQL database connection allows to specify following configuration:
 * [
 *     host(required):"string host of database server",
 *
 *     port(optional):"numeric port of database server",
 *
 *     db_name(optional: connection without database selected if db_name not found):"string database name",
 *
 *     charset(optional):"string charset",
 *
 *     login(required):"string user login",
 *
 *     password(optional: empty string got if password not found):"string user password"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\connection\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\sql\database\connection\api\ConnectionInterface;

use liberty_code\sql\database\connection\library\ConstConnection;
use liberty_code\sql\database\connection\exception\ConfigInvalidFormatException;



abstract class DefaultConnection extends FixBean implements ConnectionInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig
     * @param boolean $boolConnect = true
     */
    public function __construct(array $tabConfig, $boolConnect = true)
    {
        // Init var
        $boolConnect = (is_bool($boolConnect) ? $boolConnect : true);

        // Call parent constructor
        parent::__construct();

        // Init configuration
        $this->setConfig($tabConfig, $boolConnect);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstConnection::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstConnection::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstConnection::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstConnection::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstConnection::DATA_KEY_DEFAULT_CONFIG);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig, $boolConnect = true)
    {
        // Init var
        $boolConnect = (is_bool($boolConnect) ? $boolConnect : true);

        // Set configuration
        $this->beanSet(ConstConnection::DATA_KEY_DEFAULT_CONFIG, $tabConfig);

        // Connect or reconnect, if required
        if($boolConnect)
        {
            if($this->checkIsConnect())
            {
                $this->close();
            }

            $this->connect();
        }
    }



}