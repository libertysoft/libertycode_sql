<?php
/**
 * Description :
 * This class allows to define standard table show SQL command class.
 * Standard table show command allows to design string SQL table show command,
 * from default configuration,
 * and uses standard SQL.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\table_show\standard\model;

use liberty_code\sql\database\command\table_show\model\DefaultTableShowCommand;

use liberty_code\sql\database\command\table_show\library\ConstTableShowCommand;
use liberty_code\sql\database\command\table_show\exception\ConfigInvalidFormatException;
use liberty_code\sql\database\command\table_show\standard\library\ConstStandardTableShowCommand;



class StandardTableShowCommand extends DefaultTableShowCommand
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for database clause.
     *
     * @return string
     */
    protected function getStrClauseDbCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
            isset($tabConfig[ConstTableShowCommand::TAB_CONFIG_KEY_DB_NAME]) ?
                sprintf(
                    ConstStandardTableShowCommand::SQL_CONFIG_PATTERN_DATABASE,
                    $objConnection->getStrEscapeName($tabConfig[ConstTableShowCommand::TAB_CONFIG_KEY_DB_NAME])
                ) :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrCommand()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Build SQL command
        $strShow = ConstStandardTableShowCommand::SQL_CONFIG_PATTERN_SHOW;
        $strDb = $this->getStrClauseDbCommand();
        $result =
            $strShow . ((trim($strDb) != '') ? ' ' : '') .
            $strDb;

        // Return result
        return $result;
    }



}