<?php
/**
 * Description :
 * This class allows to define default SQL database result class.
 * Can be consider is base of all SQL database result type.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\result\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\sql\database\result\api\ResultInterface;

use liberty_code\sql\database\result\exception\DataIndexInvalidFormatException;
use liberty_code\sql\database\result\exception\DataNameInvalidFormatException;
use liberty_code\sql\database\result\exception\ColNotFoundException;
use liberty_code\sql\database\result\exception\RowNotFoundException;



abstract class DefaultResult extends FixBean implements ResultInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Cache for number of columns
     * @var integer
     */
    protected $intCountCol;



    /**
     * Cache for number of rows
     * @var integer
     */
    protected $intCountRow;



    /**
     * Cache for index array of data
     * @var array
     */
    protected $tabData;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param boolean $boolInitialize = true
     */
    public function __construct($boolInitialize = true)
    {
        // Init var
        $boolInitialize = (is_bool($boolInitialize) ? $boolInitialize : true);

        // Init properties
        $this->intCountCol = 0;
        $this->intCountRow = 0;
        $this->tabData = array();

        // Call parent constructor
        parent::__construct();

        // Init reading data, if required
        if($boolInitialize)
        {
            $this->initialize();
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Reset cached results.
     *
     * @inheritdoc
     */
    public function initialize()
    {
        // Init var
        $this->intCountCol = 0;
        $this->intCountRow = 0;
        $this->tabData = array();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Return result
        return false;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Return result
        return true;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods count
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getIntCountCol()
    {
        // Check search count required
        if(
            ($this->intCountCol == 0) &&
            ($this->getIntCountRow() > 0)
        )
        {
            // Init var
            $tabData = $this->getTabData();

            // Get count
            $this->intCountCol = count($tabData[0]);
        }

        // Return result
        return $this->intCountCol;
    }



    /**
     * @inheritdoc
     */
    public function getIntCountRow()
    {
        // Check search count required
        if($this->intCountRow == 0)
        {
            // Init var
            $tabData = $this->getTabData();

            // Get count
            $this->intCountRow = count($tabData);
        }

        // Return result
        return $this->intCountRow;
    }





    // Methods data
    // ******************************************************************************

    /**
     * Initialization of reading data included.
     *
     * @inheritdoc
     */
    public function getTabData()
    {
        // Check search data required
        if(count($this->tabData) == 0)
        {
            // Init reading data
            $this->initialize();

            // Run all data
            while(($data = $this->getFetchData()) !== false)
            {
                // Register data
                $this->tabData[] = $data;
            }
        }

        // Return result
        return $this->tabData;
    }



    /**
     * @inheritdoc
     * @throws DataIndexInvalidFormatException
     * @throws ColNotFoundException
     */
    public function getStrColName($intColIndex)
    {
        // Check arguments
        DataIndexInvalidFormatException::setCheck($intColIndex);

        // Check at least 1 row found
        if($this->getIntCountRow() == 0) throw new ColNotFoundException($intColIndex);

        // Get info
        $tabData = $this->getTabData();
        $tabCol = array_keys($tabData[0]);

        // Check column index found
        if(!array_key_exists($intColIndex, $tabCol)) throw new ColNotFoundException($intColIndex);

        // Return result
        return $tabCol[$intColIndex];
    }



    /**
     * @inheritdoc
     * @throws DataNameInvalidFormatException
     * @throws ColNotFoundException
     */
    public function getIntColIndex($strColNm)
    {
        // Check arguments
        DataNameInvalidFormatException::setCheck($strColNm);

        // Check at least 1 row found
        if($this->getIntCountRow() == 0) throw new ColNotFoundException($strColNm);

        // Get info
        $tabData = $this->getTabData();
        $tabCol = array_keys($tabData[0]);
        $result = array_search($strColNm, $tabCol);

        // Check column index valid
        if(($result === false) || (!is_int($result))) throw new ColNotFoundException($strColNm);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws DataIndexInvalidFormatException
     * @throws DataNameInvalidFormatException
     * @throws ColNotFoundException
     * @throws RowNotFoundException
     */
    public function getRowData($intRowIndex, $col)
    {
        // Check arguments
        DataIndexInvalidFormatException::setCheck($intRowIndex);

        // Get column name
        $strColNm = $col;
        if(is_int($col))
        {
            $strColNm = $this->getStrColName($col);
        }
        DataNameInvalidFormatException::setCheck($strColNm);

        // Get info
        $tabData = $this->getTabData();

        // Check row index found
        if(!array_key_exists($intRowIndex, $tabData)) throw new RowNotFoundException($intRowIndex);

        // Check column name found
        if(!array_key_exists($strColNm, $tabData[$intRowIndex])) throw new ColNotFoundException($strColNm);

        // Return result
        return $tabData[$intRowIndex][$strColNm];
    }



}