<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ConstConditionClause;



class ToolBoxConditionClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

    /**
     * Check if condition configuration is valid.
     * Condition configuration format:
     * {
     *     operand(required: if pattern not found): Expression format (@see ToolBoxExpressionClause::checkExpressionIsValid() ),
     *
     *     operator(optional: got 'equal' if operator not found)):
     *         "string operator" (@see ConstConditionClause::getTabConfigOperator() )
     *         OR
     *         "operator:string sprintf pattern,
     *         where '%1$s' or '%s' replaced by calculated operand,
     *         and '%2$s' or '%s' replaced by calculated value, if found",
     *
     *     value(required: if operator not sprintf pattern and different from 'null'): Expression format,
     *
     *     not(optional: got false if not not found): true / false,
     *
     *     pattern(required: if operand not found):
     *         "string sprintf pattern,
     *         where '%1$s' or '%s' replaced by calculated condition, if found"
     * }
     *
     * @param array $tabConfig
     * @return boolean
     */
    public static function checkConditionIsValid(array $tabConfig)
    {
        // Init var
        $tabOperatorMatch = array();
        $result =
            // Check valid operand expression
            (
                (!isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERAND])) ||
                ToolBoxExpressionClause::checkExpressionIsValid($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERAND])
            ) &&

            // Check valid operator
            (
                (!isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERATOR])) ||
                (
                    is_string($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERATOR]) &&
                    (
                        // Check valid operator pattern
                        (
                            (preg_match(
                                ConstConditionClause::OPERATOR_CONFIG_REGEXP_PATTERN,
                                $tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERATOR],
                                $tabOperatorMatch
                            ) == 1) &&
                            isset($tabOperatorMatch[1]) &&
                            (
                                (strpos($tabOperatorMatch[1],'%1$s') !== false) ||
                                in_array(substr_count($tabOperatorMatch[1], '%s'), array(1, 2))
                            )
                        ) ||
                        // Check valid operator
                        in_array(
                            $tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERATOR],
                            ConstConditionClause::getTabConfigOperator()
                        )
                    )

                )
            ) &&

            // Check valid value expression
            (
                (!isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_VALUE])) ||
                ToolBoxExpressionClause::checkExpressionIsValid($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_VALUE])
            ) &&

            // Check valid not option
            (
                (!isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPT_NOT])) ||
                (
                    (
                        (
                            (
                                is_string($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPT_NOT]) &&
                                ctype_digit($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPT_NOT])
                            ) ||
                            is_int($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPT_NOT])
                        ) &&
                        (intval($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPT_NOT]) >= 0)
                    ) ||
                    is_bool($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPT_NOT])
                )
            ) &&

            // Check valid pattern
            (
                (!isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_PATTERN])) ||
                (
                    is_string($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_PATTERN]) &&
                    (trim($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_PATTERN]) != '')
                )
            ) &&

            // Check minimum requirement
            (
                isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_PATTERN]) ||
                (
                    isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERAND]) &&
                    (
                        isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_VALUE]) ||
                        (
                            (isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERATOR]))  &&
                            (
                                (preg_match(
                                    ConstConditionClause::OPERATOR_CONFIG_REGEXP_PATTERN,
                                    $tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERATOR]
                                ) == 1) ||
                                ($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_OPERATOR] == ConstConditionClause::OPERATOR_CONFIG_NULL)
                            )
                        )
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if condition clause is valid.
     * Condition clause configuration format:
     * {
     *     type(optional: got 'and' if type not found)): "string group type" (@see ConstConditionClause::getTabConfigGroupType() ),
     *
     *     content(required: if pattern not found): [
     *         'sub-group (following this format) or condition 1 (@see ToolBoxConditionClause::checkConditionIsValid() )',
     *         ...,
     *         'sub-group or condition N'
     *     ]
     *
     *     pattern(optional):
     *         "string sprintf pattern,
     *         where '%1$s' or '%s' replaced by calculated condition, if found"
     * }
     *
     * @param array $tabConfig
     * @return boolean
     */
    public static function checkClauseConditionIsValid(array $tabConfig)
    {
        // Init var
        $result =
            // Check valid group type
            (
                (!isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE])) ||
                (
                    is_string($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE]) &&
                    in_array($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE], ConstConditionClause::getTabConfigGroupType())
                )
            ) &&

            // Check valid group content
            isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]) &&
            is_array($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]) &&

            // Check valid pattern
            (
                (!isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_PATTERN])) ||
                (
                    is_string($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_PATTERN]) &&
                    (trim($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_PATTERN]) != '') &&
                    (
                        (strpos($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_PATTERN],'%1$s') !== false) ||
                        (strpos($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_PATTERN],'%s') !== false)
                    )
                )
            );

        // Run each content, if required
        if($result && isset($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]))
        {
            $tabContent = array_values($tabConfig[ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]);
            for($intCpt = 0; ($intCpt < count($tabContent)) && $result; $intCpt++)
            {
                // Check content valid
                $content = $tabContent[$intCpt];
                $result =
                    is_array($content) &&
                    (
                        static::checkConditionIsValid($content) ||
                        static::checkClauseConditionIsValid($content)
                    );
            }
        }

        // Return result
        return $result;
    }



}