<?php
/**
 * Description :
 * This class allows to define table SQL persistor class.
 * Attribute values are stored in table like:
 * Each column name = attribute name - Row = attribute value
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\persistence\table\model;

use liberty_code\sql\persistence\model\DefaultPersistor;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\database\command\clause\library\ConstExpressionClause;
use liberty_code\sql\database\command\clause\library\ConstConditionClause;
use liberty_code\sql\database\command\clause\library\ConstFromClause;
use liberty_code\sql\database\command\clause\library\ConstOrderClause;
use liberty_code\sql\database\command\clause\library\ConstSetClause;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;
use liberty_code\sql\database\command\insert\library\ConstInsertCommand;
use liberty_code\sql\database\command\update\library\ConstUpdateCommand;
use liberty_code\sql\database\command\delete\library\ConstDeleteCommand;
use liberty_code\sql\persistence\library\ConstPersistor;
use liberty_code\sql\persistence\table\library\ConstTablePersistor;
use liberty_code\sql\persistence\table\exception\ActionConfigInvalidFormatException;



class TablePersistor extends DefaultPersistor
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters (Persistor interface implementation)
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     @see getData() configuration array format,
     *
     *     column_name_select(optional: considered select all columns table, if column_name_select not found)): [
     *         'string column name 1 to select',
     *         ...,
     *         'string column name N to select',
     *     ]
     * ]
     *
     * @inheritdoc
     */
    public function getTabData(
        array $tabId = array(),
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Check arguments
        ActionConfigInvalidFormatException::setCheck(ConstPersistor::TAB_ACTION_KEY_GET, $tabConfig);

        // Init var
        $result = false;
        $objCommandFactory = $this->getObjCommandFactory();

        // Check empty data return required
        if(
            array_key_exists(ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID, $tabConfig) &&
            (count($tabId) == 0)
        )
        {
            $result = array();
        }
        // Else: process selection if command factory valid
        else if(!is_null($objCommandFactory))
        {
            // Get info
            $tabCmdConfig = array();
            $objConnection = $this->getObjCommandFactory()->getObjConnection();

            // Set select clause
            $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT] = ToolBoxTable::getItem(
                $tabConfig,
                ConstTablePersistor::TAB_CONFIG_KEY_COLUMN_NAME_SELECT,
                array([ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => '*'])
            );

            // Set from clause
            $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME]]
            );

            // Set id info, if required
            if(
                array_key_exists(ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID, $tabConfig) &&
                (count($tabId) > 0)
            )
            {
                $strColNmId = $tabConfig[ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID];
                $tabEscapeId = array_map(
                    function($value) use($objConnection) {
                        return $objConnection->getStrEscapeValue($value);
                    },
                    $tabId
                );
                $strListId = implode(', ', $tabEscapeId);

                // Set where clause
                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                        [
                            ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmId,
                            ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_IN,
                            ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                                ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $strListId
                            ]
                        ]
                    ]
                );

                // Set order clause
                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER] = array(
                    [
                        ConstOrderClause::TAB_CONFIG_KEY_OPERAND => [
                            ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => sprintf(
                                'FIELD(%1$s, %2$s)',
                                $objConnection->getStrEscapeName($strColNmId),
                                $strListId
                            )
                        ],
                        ConstOrderClause::TAB_CONFIG_KEY_OPERATOR => ConstOrderClause::OPERATOR_CONFIG_ASC
                    ]
                );
            }

            // Set pattern
            if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_SELECT, $tabConfig))
            {
                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT][] = array(
                    ConstExpressionClause::TAB_CONFIG_KEY_PATTERN =>
                        $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_SELECT]
                );
            }

            if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE, $tabConfig))
            {
                // Init where clause, if required
                if(!isset(
                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                    [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]
                ))
                {
                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => []
                    );
                }

                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT][] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_PATTERN =>
                        $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE]
                );
            }

            if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_ORDER, $tabConfig))
            {
                // Init where clause, if required
                if(!isset($tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER]))
                {
                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER] = array();
                }

                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER][] = array(
                    ConstOrderClause::TAB_CONFIG_KEY_PATTERN =>
                        $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_ORDER]
                );
            }

            // Get command
            $objCommand = $objCommandFactory->getObjSelectCommand($tabCmdConfig);

            // Run query result
            if(($resultQuery = $objCommand->executeResult()) !== false)
            {
                $result = array();
                while(($data = $resultQuery->getFetchData()) !== false)
                {
                    $result[] = $data;
                }
                $resultQuery->close();
            }
        }

        // Return result
        return $result;
    }





    // Methods setters (Persistor interface implementation)
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     @see createData() configuration array format,
     *
     *     opt_id_new(optional: got 'connection' if opt_id_new not found): "string option ID new calculation: persistor | connection"
     * ]
     *
     * Note:
     * -> Configuration opt_id_new (option ID new calculation):
     *     @see createData() .
     *     If opt_id_new = persistor and @see getIdNew() return null,
     *     try to use connection feature to get new id.
     *
     * @inheritdoc
     */
    public function createTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabId = null,
        array &$tabInfo = null
    )
    {
        // Check arguments
        ActionConfigInvalidFormatException::setCheck(ConstPersistor::TAB_ACTION_KEY_CREATE, $tabConfig);

        // Init var
        $result = true;
        $tabCreateData = $tabData;

        // Check data found
        if(count($tabCreateData) > 0)
        {
            // Init var
            $result = false;
            $objCommandFactory = $this->getObjCommandFactory();

            // Check command factory valid
            if(!is_null($objCommandFactory))
            {
                // Get info
                $tabCmdConfig = array();
                $objConnection = $this->getObjCommandFactory()->getObjConnection();
                $strOptIdNew = ToolBoxTable::getItem(
                    $tabConfig,
                    ConstPersistor::TAB_CONFIG_KEY_OPT_ID_NEW,
                    ConstPersistor::TAB_CONFIG_OPT_ID_NEW_CONNECTION
                );

                // Init set index array of command config value function
                $setTabCmdConfigValue = function(array $data, array &$tabCmdConfig) use ($tabConfig)
                {
                    $tabCmdConfigValue = array();

                    // Set value clause
                    foreach(array_values($data) as $value)
                    {
                        $tabCmdConfigValue[] = array(
                            ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $value
                        );
                    }

                    // Set pattern
                    if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_VALUE, $tabConfig))
                    {
                        $tabCmdConfigValue[] = array(
                            ConstExpressionClause::TAB_CONFIG_KEY_PATTERN =>
                                $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_VALUE]
                        );
                    }

                    $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE][] = $tabCmdConfigValue;
                };

                // Set from clause
                $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                    [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME]]
                );

                // Set column clause
                if(count($tabCreateData) > 0)
                {
                    $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN] = array_keys($tabCreateData[0]);
                }

                // Set pattern
                if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_COLUMN, $tabConfig))
                {
                    if(!isset($tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN]))
                    {
                        $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN] = array();
                    }
                    $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN][] = array(
                        ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_COLUMN]
                    );
                }

                // Set id from persistor, if required
                if(
                    array_key_exists(ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID, $tabConfig) &&
                    ($strOptIdNew == ConstPersistor::TAB_CONFIG_OPT_ID_NEW_PERSISTOR)
                )
                {
                    $strColNmId = $tabConfig[ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID];

                    // Run all data
                    for($intCpt = 0; $intCpt < count($tabCreateData); $intCpt++)
                    {
                        $idNew = $this->getIdNew();

                        // Register new id, if required
                        if(!is_null($idNew))
                        {
                            $tabCreateData[$intCpt][$strColNmId] = $idNew;
                            if(!is_null($tabId)) $tabId[] = $idNew;
                        }
                    }
                }

                // Case: single insert: return id required and not already provided
                if(
                    // ($strOptIdNew == ConstPersistor::TAB_CONFIG_OPT_ID_NEW_CONNECTION) &&
                    (is_array($tabId)) && (count($tabId) == 0)
                )
                {
                    if(count($tabCreateData) > 0)
                    {
                        // Run each data
                        $result = true;
                        $tabDataKey = array_keys($tabCreateData);
                        for($intCpt = 0; ($intCpt < count($tabDataKey)) && $result; $intCpt++)
                        {
                            $data = $tabCreateData[$tabDataKey[$intCpt]];

                            // Set value clause
                            $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE] = array();
                            $setTabCmdConfigValue($data, $tabCmdConfig);

                            // Get command
                            $objCommand = $objCommandFactory->getObjInsertCommand($tabCmdConfig);

                            // Run query
                            $result = ($objCommand->execute() !== false);

                            // Register new id, from connection, if required
                            if($result)
                            {
                                $idNew = $objConnection->getStrLastInsertId();
                                if(!is_null($idNew)) $tabId[] = $idNew;
                            }
                        }
                    }
                }
                // Case else: bulk insert
                else
                {
                    // Set value clause
                    $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE] = array();
                    foreach($tabCreateData as $data)
                    {
                        $setTabCmdConfigValue($data, $tabCmdConfig);
                    }

                    // Get command
                    $objCommand = $objCommandFactory->getObjInsertCommand($tabCmdConfig);

                    // Run query
                    $result = ($objCommand->execute() !== false);
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see updateData() configuration array format,
     * ]
     *
     * @inheritdoc
     */
    public function updateTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Check arguments
        ActionConfigInvalidFormatException::setCheck(ConstPersistor::TAB_ACTION_KEY_UPDATE, $tabConfig);

        // Init var
        $result = true;

        // Check data found
        if(count($tabData) > 0)
        {
            // Init var
            $result = false;
            $objCommandFactory = $this->getObjCommandFactory();

            // Check command factory valid
            if(!is_null($objCommandFactory))
            {
                // Get info
                $tabCmdConfig = array();

                // Set from clause
                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                    [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME]]
                );

                if(count($tabData) > 0)
                {
                    // Run each data
                    $result = true;
                    $tabDataKey = array_keys($tabData);
                    for($intCpt = 0; ($intCpt < count($tabDataKey)) && $result; $intCpt++)
                    {
                        $data = $tabData[$tabDataKey[$intCpt]];

                        // Set set clause
                        $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET] = array();
                        foreach($data as $strKey => $value)
                        {
                            $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET][] = array(
                                ConstSetClause::TAB_CONFIG_KEY_OPERAND => $strKey,
                                ConstSetClause::TAB_CONFIG_KEY_VALUE => [
                                    ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $value
                                ]
                            );
                        }

                        // Set pattern
                        if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_SET, $tabConfig))
                        {
                            $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET][] = array(
                                ConstSetClause::TAB_CONFIG_KEY_PATTERN =>
                                    $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_SET]
                            );
                        }

                        // Set id info, if required
                        if(array_key_exists(ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE, $tabCmdConfig))
                        {
                            unset($tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]);
                        }
                        if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID, $tabConfig))
                        {
                            $strColNmId = $tabConfig[ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID];
                            if(array_key_exists($strColNmId, $data))
                            {
                                // Set where clause
                                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                                        [
                                            ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmId,
                                            ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_EQUAL,
                                            ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                                                ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $data[$strColNmId]
                                            ]
                                        ]
                                    ]
                                );
                            }
                        }

                        // Set pattern
                        if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE, $tabConfig))
                        {
                            // Init where clause, if required
                            if(!isset(
                                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                                [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]
                            ))
                            {
                                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => []
                                );
                            }

                            $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                            [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT][] = array(
                                ConstConditionClause::TAB_CONFIG_KEY_PATTERN =>
                                    $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE]
                            );
                        }

                        // Get command
                        $objCommand = $objCommandFactory->getObjUpdateCommand($tabCmdConfig);

                        // Run query
                        $result = ($objCommand->execute() !== false);
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see deleteData() configuration array format
     * ]
     *
     * @inheritdoc
     */
    public function deleteTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Check arguments
        ActionConfigInvalidFormatException::setCheck(ConstPersistor::TAB_ACTION_KEY_DELETE, $tabConfig);

        // Init var
        $result = true;

        // Check data found
        if(count($tabData) > 0)
        {
            // Init var
            $result = false;
            $objCommandFactory = $this->getObjCommandFactory();

            // Check command factory valid
            if(!is_null($objCommandFactory))
            {
                // Get info
                $tabCmdConfig = array();
                $objConnection = $this->getObjCommandFactory()->getObjConnection();

                // Set from clause
                $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                    [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME]]
                );

                // Set id info, if required
                if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID, $tabConfig))
                {
                    $strColNmId = $tabConfig[ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID];
                    $tabId = array();
                    foreach($tabData as $data)
                    {
                        if(array_key_exists($strColNmId, $data))
                        {
                            $tabId[] = $data[$strColNmId];
                        }
                    }
                    $tabEscapeId = array_map(
                        function($value) use($objConnection) {
                            return $objConnection->getStrEscapeValue($value);
                        },
                        $tabId
                    );
                    $strListId = implode(', ', $tabEscapeId);

                    // Set where clause
                    $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                            [
                                ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmId,
                                ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_IN,
                                ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                                    ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $strListId
                                ]
                            ]
                        ]
                    );
                }

                // Set pattern
                if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE, $tabConfig))
                {
                    // Init where clause, if required
                    if(!isset(
                        $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                        [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]
                    ))
                    {
                        $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                            ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                            ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => []
                        );
                    }

                    $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                    [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT][] = array(
                        ConstConditionClause::TAB_CONFIG_KEY_PATTERN =>
                            $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE]
                    );
                }

                // Get command
                $objCommand = $objCommandFactory->getObjDeleteCommand($tabCmdConfig);

                // Run query
                $result = ($objCommand->execute() !== false);
            }
        }

        // Return result
        return $result;
    }



}