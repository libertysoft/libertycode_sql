<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;



class ConstExpressionClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_DB_NAME = 'db_name';
    const TAB_CONFIG_KEY_TABLE_NAME = 'table_name';
    const TAB_CONFIG_KEY_COLUMN_NAME = 'column_name';
    const TAB_CONFIG_KEY_ALIAS = 'alias';
    const TAB_CONFIG_KEY_VALUE = 'value';
    const TAB_CONFIG_KEY_PATTERN = 'pattern';
}