<?php
/**
 * Description :
 * This class allows to define default table drop SQL command class.
 * Can be consider is base of all table drop SQL command type.
 *
 * Default table drop command allows to design string SQL table drop command,
 * from following specified configuration: @see TableDropCommandInterface.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\table_drop\model;

use liberty_code\sql\database\command\model\DefaultCommand;
use liberty_code\sql\database\command\table_drop\api\TableDropCommandInterface;

use liberty_code\sql\database\command\library\ConstCommand;
use liberty_code\sql\database\command\clause\library\ToolBoxTableClause;
use liberty_code\sql\database\command\clause\exception\ClauseConfigInvalidFormatException;
use liberty_code\sql\database\command\table_drop\library\ConstTableDropCommand;
use liberty_code\sql\database\command\table_drop\exception\ConfigInvalidFormatException;



abstract class DefaultTableDropCommand extends DefaultCommand  implements TableDropCommandInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		// $result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstCommand::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseTable($config)
    {
        // Check clause valid
        if(!ToolBoxTableClause::checkClauseTableIsValid($config))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE,
                (is_array($config) ? serialize($config) : $config)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE] = $config;

        // Return result
        return $this;
    }



}