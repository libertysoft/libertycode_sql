<?php
/**
 * Description :
 * This class allows to define default SQL register class.
 * Default (SQL) register is default register, using SQL data storage as support.
 * Can be consider is base of all SQL register type.
 *
 * Default SQL register uses the following specified configuration:
 * [
 *     table_name(required): "string table name",
 *
 *     pattern_where(optional: got null if pattern not found): "string where pattern",
 *
 *     pattern_order(optional: got null if pattern not found): "string order pattern",
 *
 *     pattern_column(optional: got null if pattern not found): "string insert pattern",
 *
 *     pattern_value(optional: got null if pattern not found): "string insert pattern",
 *
 *     pattern_set(optional: got null if pattern not found): "string update pattern"
 * ]
 *
 * Patterns format:
 * Patterns are strings added at the end of the calculated SQL clause.
 * It transforms as sprintf format during operation.
 * That's reason why sprintf syntax must be excluded from patterns.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\register\model;

use liberty_code\register\register\model\DefaultRegister as BaseDefaultRegister;

use liberty_code\sql\database\command\factory\api\CommandFactoryInterface;
use liberty_code\sql\register\library\ConstRegister;
use liberty_code\sql\register\exception\CommandFactoryInvalidFormatException;
use liberty_code\sql\register\exception\ConfigInvalidFormatException;



/**
 * @method null|CommandFactoryInterface getObjCommandFactory() Get command factory object.
 * @method array getTabConfig() Get config array.
 * @method void setTabConfig(array $tabConfig) Set config array.
 */
abstract class DefaultRegister extends BaseDefaultRegister
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param CommandFactoryInterface $objCommandFactory = null
     */
    public function __construct(array $tabConfig = null, CommandFactoryInterface $objCommandFactory = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init command factory if required
        if(!is_null($objCommandFactory))
        {
            $this->setCommandFactory($objCommandFactory);
        }
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRegister::DATA_KEY_DEFAULT_COMMAND_FACTORY))
        {
            $this->__beanTabData[ConstRegister::DATA_KEY_DEFAULT_COMMAND_FACTORY] = null;
        }

        if(!$this->beanExists(ConstRegister::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstRegister::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstRegister::DATA_KEY_DEFAULT_COMMAND_FACTORY,
            ConstRegister::DATA_KEY_DEFAULT_CONFIG
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstRegister::DATA_KEY_DEFAULT_COMMAND_FACTORY:
                    CommandFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstRegister::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods setters
    // ******************************************************************************

    /**
     * Set command factory object.
     *
     * @param CommandFactoryInterface $objCommandFactory
     */
    public function setCommandFactory(CommandFactoryInterface $objCommandFactory)
    {
        $this->beanSet(ConstRegister::DATA_KEY_DEFAULT_COMMAND_FACTORY, $objCommandFactory);
    }





    // Methods getters (Register interface implementation)
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabKey(array $tabConfig = null)
    {
        // Return result
        return array_keys($this->getTabItem($tabConfig));
    }



    /**
     * Overwrite it to get all register items.
     *
     * @inheritdoc
     */
    public function getTabItem(array $tabConfig = null)
    {
        // Return result
        return array();
    }



}