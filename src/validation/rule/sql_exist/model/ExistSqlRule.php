<?php
/**
 * Description :
 * This class allows to define exist SQL rule class.
 * Exist SQL rule allows to check if specified data exists,
 * in specified SQL data storage.
 *
 * Exist SQL rule uses the following specified configuration:
 * [
 *     SQL rule configuration,
 *
 *     cache_require(optional: got true if not found): true / false
 * ]
 *
 * Exist SQL rule uses the following specified validation configuration:
 * [
 *     SQL rule validation configuration,
 *
 *     db_name(optional): "string database name",
 *
 *     table_name(required): "string table name",
 *
 *     column_name(required): "string column name",
 *
 *     include(optional: got [], if not found): [
 *         // Value or list of values 1 to include in exist check
 *         column_name(required): mixed value OR [mixed value 1, ..., mixed value N],
 *
 *         ...,
 *
 *         // Value or list of values N to include in exist check
 *         ...
 *     ],
 *
 *     exclude(optional: got [], if not found): [
 *         // Value or list of values 1 to exclude from exist check
 *         column_name(required): mixed value OR [mixed value 1, ..., mixed value N],
 *
 *         ...,
 *
 *         // Value or list of values N to exclude from exist check
 *         ...
 *     ]
 * ]
 *
 * Exist SQL rule uses the following specified error configuration:
 * [
 *     SQL rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\validation\rule\sql_exist\model;

use liberty_code\sql\validation\rule\model\SqlRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\sql\database\command\clause\library\ConstExpressionClause;
use liberty_code\sql\database\command\clause\library\ConstFromClause;
use liberty_code\sql\database\command\clause\library\ConstConditionClause;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;
use liberty_code\sql\database\command\select\api\SelectCommandInterface;
use liberty_code\sql\validation\rule\sql_exist\library\ConstExistSqlRule;
use liberty_code\sql\validation\rule\sql_exist\exception\ConfigInvalidFormatException;
use liberty_code\sql\validation\rule\sql_exist\exception\ValidConfigInvalidFormatException;



class ExistSqlRule extends SqlRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRule::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check cache required.
     *
     * @return boolean
     */
    protected function checkCacheRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!array_key_exists(ConstExistSqlRule::TAB_CONFIG_KEY_CACHE_REQUIRE, $tabConfig)) ||
            (intval($tabConfig[ConstExistSqlRule::TAB_CONFIG_KEY_CACHE_REQUIRE]) !== 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function checkIsValidEngine(
        $strName,
        $value,
        array $tabConfig
    )
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;

        // Check valid value
        if(
            is_string($value) ||
            is_numeric($value) ||
            is_bool($value) ||
            is_null($value)
        )
        {
            // Get command
            $objCommand = $this->getObjCommand($value, $tabConfig);

            // Run query result
            if(($resultQuery = $objCommand->executeResult()) !== false)
            {
                // Check data found
                $result = ($resultQuery->getIntCountRow() > 0);

                $resultQuery->close();
            }
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstExistSqlRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstExistSqlRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * Get select command query configuration,
     * to check if specified data exists,
     * from specified configuration.
     *
     * Configuration format:
     * @see ExistSqlRule validation configuration.
     *
     * @param mixed $value
     * @param array $tabConfig
     * @return array
     */
    protected function getTabCommandConfig($value, array $tabConfig)
    {
        // Init var
        $objCommandFactory = $this->getObjCommandFactory($tabConfig);
        $objConnection = $objCommandFactory->getObjConnection();

        // Init result
        $result = array(
            ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT => [
                $tabConfig[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_COLUMN_NAME]
            ],
            ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM => [
                [
                    ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_TABLE_NAME]
                ]
            ],
            ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE => [
                ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                    [
                        ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $tabConfig[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_COLUMN_NAME],
                        ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_EQUAL,
                        ConstConditionClause::TAB_CONFIG_KEY_VALUE => [ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $value]
                    ]
                ]
            ]
        );

        // Set DB name, if required
        if(isset($tabConfig[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_DB_NAME]))
        {
            $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM][0][ConstFromClause::TAB_CONFIG_KEY_TABLE_DB_NAME] =
                $tabConfig[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_DB_NAME];
        }

        // Init set scope condition function
        $setScopeConditionConfig = function(array $tabScopeConfig, $boolExclude) use (&$result, $objConnection)
        {
            // Run each column value, to scope
            $tabConditionConfig = array();
            foreach($tabScopeConfig as $strColNm => $value)
            {
                // Get value
                $tabValue = (is_array($value) ? $value : array($value));
                $tabEscapeValue = array_map(
                    function($value) use($objConnection) {
                        return $objConnection->getStrEscapeValue($value);
                    },
                    $tabValue
                );
                $strListValue = implode(', ', $tabEscapeValue);

                // Register condition
                $tabConditionConfig[] =
                    [
                        ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNm,
                        ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_IN,
                        ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                            ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $strListValue
                        ],
                        ConstConditionClause::TAB_CONFIG_KEY_OPT_NOT => $boolExclude
                    ];
            }

            // Set scope condition in where clause, if required
            if(count($tabConditionConfig) > 0)
            {
                $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE][ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT] =
                    array_merge(
                        $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE][ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT],
                        $tabConditionConfig
                    );
            }
        };

        // Set include scope condition, if required
        if(isset($tabConfig[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_INCLUDE]))
        {
            $setScopeConditionConfig($tabConfig[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_INCLUDE], false);
        }

        // Set exclude scope condition, if required
        if(isset($tabConfig[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_EXCLUDE]))
        {
            $setScopeConditionConfig($tabConfig[ConstExistSqlRule::TAB_VALID_CONFIG_KEY_EXCLUDE], true);
        }

        // Return result
        return $result;
    }



    /**
     * Get new select SQL command object,
     * to check if specified data exists,
     * from specified configuration.
     *
     * Configuration format:
     * @see ExistSqlRule validation configuration.
     *
     * @param mixed $value
     * @param array $tabConfig
     * @return SelectCommandInterface
     */
    protected function getObjCommand($value, array $tabConfig)
    {
        // Init var
        $objCommandFactory = $this->getObjCommandFactory($tabConfig);
        $tabCmdConfig = $this->getTabCommandConfig($value, $tabConfig);
        $result = $objCommandFactory->getObjSelectCommand($tabCmdConfig);

        // Return result
        return $result;
    }



}