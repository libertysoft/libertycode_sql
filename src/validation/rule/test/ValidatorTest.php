<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load external library test
require_once($strRootAppPath . '/vendor/liberty_code/validation/test/validator/boot/ValidatorBootstrap.php');

// Use
use liberty_code\sql\validation\rule\sql_exist\model\ExistSqlRule;



// Init rules
$objExistSqlRule = new ExistSqlRule();



// Add rules
$tabRule = array(
    $objExistSqlRule
);
$objRuleCollection->setTabRule($tabRule);


