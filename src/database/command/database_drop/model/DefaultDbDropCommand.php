<?php
/**
 * Description :
 *
 * This class allows to define default database drop SQL command class.
 * Can be consider is base of all database drop SQL command type.
 *
 * Default database drop command allows to design string database drop SQL command,
 * from following specified configuration: @see DbDropCommandInterface.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_drop\model;

use liberty_code\sql\database\command\model\DefaultCommand;
use liberty_code\sql\database\command\database_drop\api\DbDropCommandInterface;

use liberty_code\sql\database\command\library\ConstCommand;
use liberty_code\sql\database\command\database_drop\library\ConstDbDropCommand;
use liberty_code\sql\database\command\database_drop\exception\ConfigInvalidFormatException;



abstract class DefaultDbDropCommand extends DefaultCommand implements DbDropCommandInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		// $result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstCommand::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function setDbName($strDbNm)
    {
        // Check config valid
        if(
            (!is_string($strDbNm)) ||
            (trim($strDbNm) == ''))
        {
            throw new ConfigInvalidFormatException($strDbNm);
        }

        // Set config
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstDbDropCommand::TAB_CONFIG_KEY_DB_NAME] = $strDbNm;

        // Return result
        return $this;
    }



}