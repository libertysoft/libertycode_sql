<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/test/HelpInsertTestTable.php');

// Use
use liberty_code\authentication\authentication\api\AuthenticationInterface;
use liberty_code\authentication\authentication\secret\library\ConstSecretAuthentication;
use liberty_code\authentication\authentication\secret\model\SecretAuthentication;
use liberty_code\authentication\authentication\token\library\ConstTokenAuthentication;
use liberty_code\authentication\authentication\token\model\TokenAuthentication;
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;
use liberty_code\sql\authentication\authenticator\model\SqlAuthenticator;



// Init var
$objCommandFacto = new StandardCommandFactory($objConnection);
$tabConfig = array(
    'query' => [
        'select' => [
            $strColNmUsrLg,
            $strColNmUsrPw,
            $strColNmUsrEmail
        ],
        'from' => [
            $strTableNmUsr
        ]
    ],
    //*
    'mapping_identification_data' => [
        [
            'data_key' => ConstSecretAuthentication::TAB_DATA_KEY_IDENTIFIER,
            'column_name' => [$strColNmUsrEmail, $strColNmUsrLg]
        ],
        [
            'data_key' => ConstTokenAuthentication::TAB_DATA_KEY_TOKEN,
            'column_name' => [$strColNmUsrLg, $strColNmUsrEmail]
        ]
    ],
    'mapping_authentication_data' => [
        [
            'data_key' => ConstTokenAuthentication::TAB_DATA_KEY_TOKEN,
            'column_name' => [$strColNmUsrLg, $strColNmUsrEmail]
        ]
    ],
    //*/
    'default_identification_data_column_name' => $strColNmUsrLg,
    'default_authentication_data_column_name' => $strColNmUsrPw
);
$objAuthenticator = new SqlAuthenticator($tabConfig, $objCommandFacto);



// Test authenticator
$tabAuthentication = array(
    new SecretAuthentication(array(
        ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 'lg_1',
        ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw1'
    )), // Ko: Identification ok, authentication ko

    new SecretAuthentication(array(
        ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => 'lg_2',
        ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw-1'
    )), // Ko: Identification ok, authentication ko

    new SecretAuthentication(array(
        ConstSecretAuthentication::TAB_CONFIG_KEY_IDENTIFIER => '2',
        ConstSecretAuthentication::TAB_CONFIG_KEY_SECRET => 'pw-2'
    )), // Ok

    new TokenAuthentication(array(
        ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN => 'fnm-3.nm3@test.com'
    )), // Ko: Identification ko, authentication ko

    new TokenAuthentication(array(
        ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN => 'fnm3.nm3@test.com'
    )), // Ok

    new TokenAuthentication(array(
        ConstTokenAuthentication::TAB_CONFIG_KEY_TOKEN => 'lg_3'
    )) // Ok
);

foreach($tabAuthentication as $objAuthentication)
{
    /** @var AuthenticationInterface $objAuthentication */

    echo('Test authenticator : <pre>');
    var_dump($objAuthentication->getTabAuthConfig());
    echo('</pre>');

    try{
        echo('Check identification: <pre>');
        var_dump($objAuthenticator->checkIsIdentified($objAuthentication));
        echo('</pre>');

        echo('Check authentication: <pre>');
        var_dump($objAuthenticator->checkIsAuthenticated($objAuthentication));
        echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Remove test database, if required
require_once($strRootAppPath . '/src/database/test/HelpRemoveTestDb.php');


