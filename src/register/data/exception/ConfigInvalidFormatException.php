<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\register\data\exception;

use Exception;

use liberty_code\sql\register\data\library\ConstDataRegister;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDataRegister::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $tabTzNm = timezone_identifiers_list();
        $result =
            // Check valid column name key
            isset($config[ConstDataRegister::TAB_CONFIG_KEY_COLUMN_NAME_KEY]) &&
            is_string($config[ConstDataRegister::TAB_CONFIG_KEY_COLUMN_NAME_KEY]) &&
            (trim($config[ConstDataRegister::TAB_CONFIG_KEY_COLUMN_NAME_KEY]) != '') &&

            // Check valid column name item
            isset($config[ConstDataRegister::TAB_CONFIG_KEY_COLUMN_NAME_ITEM]) &&
            is_string($config[ConstDataRegister::TAB_CONFIG_KEY_COLUMN_NAME_ITEM]) &&
            (trim($config[ConstDataRegister::TAB_CONFIG_KEY_COLUMN_NAME_ITEM]) != '') &&

            // Check valid get timezone name
            (
                (!isset($config[ConstDataRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME])) ||
                (
                    is_string($config[ConstDataRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME]) &&
                    (in_array($config[ConstDataRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME], $tabTzNm))
                )
            ) &&

            // Check valid set timezone name
            (
                (!isset($config[ConstDataRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME])) ||
                (
                    is_string($config[ConstDataRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME]) &&
                    (in_array($config[ConstDataRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME], $tabTzNm))
                )
            ) &&

            // Check valid set datetime format
            (
                (!isset($config[ConstDataRegister::TAB_CONFIG_KEY_SET_DATETIME_FORMAT])) ||
                (
                    is_string($config[ConstDataRegister::TAB_CONFIG_KEY_SET_DATETIME_FORMAT]) &&
                    (trim($config[ConstDataRegister::TAB_CONFIG_KEY_SET_DATETIME_FORMAT]) != '')
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}