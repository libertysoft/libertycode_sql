<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\select\standard\library;



class ConstStandardSelectCommand
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_SELECT = 'SELECT %1$s';
    const SQL_CONFIG_PATTERN_SELECT_DISTINCT = 'SELECT DISTINCT %1$s';
    const SQL_CONFIG_PATTERN_FROM = 'FROM %1$s';
    const SQL_CONFIG_PATTERN_WHERE = 'WHERE %1$s';
    const SQL_CONFIG_PATTERN_GROUP = 'GROUP BY %1$s';
    const SQL_CONFIG_PATTERN_HAVING = 'HAVING %1$s';
    const SQL_CONFIG_PATTERN_ORDER = 'ORDER BY %1$s';
    const SQL_CONFIG_PATTERN_LIMIT = 'LIMIT %1$s';
}