<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\statement\library;



class ConstStatement
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PARAM = 'tabParam';



    // Exception message constants
    const EXCEPT_MSG_PARAM_INVALID_FORMAT =
        'Following parameters config "%1$s" invalid! 
        The config must be an array, following the default statement execution parameters configuration standard.';



}