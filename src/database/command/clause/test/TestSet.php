<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/clause/test/Include.php');

// Use
use liberty_code\sql\database\command\clause\library\ToolBoxSetClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardSetClause;



// Init var
$config_1 = array(
    [
        'operand' => [
            'db_name' => 'db',
            'table_name' => 'table',
            'column_name' => 'col_1'
        ],
        'value' => 'col_value_1'
    ],
    [
        'operand' => [
            'db_name' => 'db',
            'table_name' => 'table',
            'column_name' => 'col_2'
        ],
        'value' => ['value' => 2],
        'pattern' => '[%s]'
    ],
    [
        'operand' => 'col_3',
        'pattern' => '[%s true]'
    ],
    [
        'value' => ['value' => false],
        'pattern' => '[`col_4` %s]'
    ],
    [
        'pattern' => '[test]'
    ]
);

$config_2 = array(
    [
        'operand' => [
            'db_name' => 'db',
            'table_name' => 'table',
            'column_name' => 'col_1'
        ]
    ]
);

$config_3 = array(
    [
        'pattern' => 'test'
    ]
);

$config_4 = array(
    [
        'value' => ['value' => 2]
    ]
);

$config_5 = array(
    [
        'pattern' => '[%s]'
    ]
);

$config_6 = array(
    [
        'operand' => 1
    ]
);

$config_7 = array(
    [
        'value' => 1
    ]
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3, // Ok
    $config_4, // Ko
    $config_5, // Ok
    $config_6, // Ko
    $config_7 // Ko
);



// Test config
process(
    $tabConfig, 
    function($config)
    {
        return ToolBoxSetClause::checkClauseSetIsValid($config);
    },
    function($config) use ($objConnection)
    {
        return ToolBoxStandardSetClause::getStrClauseSetCommand($objConnection, $config);
    }
);


