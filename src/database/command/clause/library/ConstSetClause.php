<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;



class ConstSetClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_OPERAND = 'operand';
    const TAB_CONFIG_KEY_VALUE = 'value';
    const TAB_CONFIG_KEY_PATTERN = 'pattern';
}