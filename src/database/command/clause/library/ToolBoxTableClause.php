<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\command\clause\library\ConstTableClause;



class ToolBoxTableClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

    /**
     * Check if table clause is valid.
     * Table configuration format:
     * "string table name" (table_name)
     *
     * OR
     *
     * {
     *     db_name(optional): "string database name",
     *
     *     table_name(required: if pattern not found): "string table name",
     *
     *     pattern(required: if table_name not found):
     *         "string sprintf pattern,
     *         where '%1$s' or '%s' replaced by calculated table, if found"
     * }
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkClauseTableIsValid($config)
    {
        // Init var
        $result =
            // Case simple config
            (
                // Check valid table name
                is_string($config) &&
                (trim($config) != '')
            ) ||
            // Case complex config
            (
                // Check valid array
                is_array($config) &&

                // Check valid db name
                (
                    (!isset($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_DB_NAME])) ||
                    (
                        is_string($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_DB_NAME]) &&
                        (trim($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_DB_NAME]) != '')
                    )
                ) &&

                // Check valid table name
                (
                    (!isset($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_NAME])) ||
                    (
                        is_string($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_NAME]) &&
                        (trim($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_NAME]) != '')
                    )
                ) &&

                // Check valid pattern
                (
                    (!isset($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_PATTERN])) ||
                    (
                        is_string($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_PATTERN]) &&
                        (trim($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_PATTERN]) != '')
                    )
                ) &&

                // Check minimum requirement
                (
                    isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME]) ||
                    isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_PATTERN])
                )
            );

        // Return result
        return $result;
    }



}