<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/test/HelpGetCommandFacto.php');



// Init var
$objCommand = $objCommandFacto->getObjDbCreateCommand();

$config_1 = array(
    'db_name' => 'libertycode_sql_test_create',
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci'
);

$config_2 = array(
    'db_name' => 'libertycode_sql_test_create'
);

$config_3 = array(
    'db_name' => 'libertycode_sql_test_create_2',
    'charset' => 'utf8mb4d'
);

$config_4 = array(
    'db_name' => 'libertycode_sql_test_create_2',
    'charset' => 'utf8mb4'
);

$config_5 = array(
    'db_name' => 'libertycode_sql_test_create_3',
    'collation' => 'utf8mb4_unicode_cid',
);

$config_6 = array(
    'db_name' => 'libertycode_sql_test_create_3',
    'collation' => 'utf8mb4_unicode_ci',
);

$config_7 = array(
    'db_name' => 'libertycode_sql_test_create_4'
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3, // Ko
    $config_4, // Ok
    $config_5, // Ko
    $config_6, // Ok
    $config_7 // Ok
);



// Remove databases, if required
$strSql = "DROP DATABASE IF EXISTS " . $objConnection->getStrEscapeName('libertycode_sql_test_create') . ";";
$objConnection->execute($strSql);

$strSql = "DROP DATABASE IF EXISTS " . $objConnection->getStrEscapeName('libertycode_sql_test_create_2') . ";";
$objConnection->execute($strSql);

$strSql = "DROP DATABASE IF EXISTS " . $objConnection->getStrEscapeName('libertycode_sql_test_create_3') . ";";
$objConnection->execute($strSql);

$strSql = "DROP DATABASE IF EXISTS " . $objConnection->getStrEscapeName('libertycode_sql_test_create_4') . ";";
$objConnection->execute($strSql);



// Test command
$_GET['exec'] = '1';
$_GET['exec-result'] = '0';
$_GET['exec-stmt'] = '0';
$_GET['exec-stmt-result'] = '0';
$_GET['clean'] = '1';
require($strRootAppPath . '/src/database/command/test/HelpRunCommand.php');



// Reconnection, if required
if(!$objConnection->checkIsConnect())$objConnection->connect();



// Test build config: Ko
$tabConfig = array(
    'DbName' => 'test_nm',
    'Charset' => 7,
    'Collation' => 'test_collation'
);

$objCommand = $objCommandFacto->getObjDbCreateCommand();
foreach($tabConfig as $strConfig => $config)
{
    // Get info
    $strConfigCall = sprintf('set%s', $strConfig);

    echo('Test build config: Ko ' . $strConfig . ':<br />');

    try
    {
        $objCommand->$strConfigCall($config);

        echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
        echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
    }
    catch(\Exception $e)
    {
        echo('Config failed: ' . $e->getMessage());
    }

    echo('<br /><br /><br />');
}



// Test build config: Ok
$tabConfig = array(
    'DbName' => 'test_nm',
    'Charset' => 'test_charset',
    'Collation' => 'test_collation'
);

$objCommand = $objCommandFacto->getObjDbCreateCommand();
foreach($tabConfig as $strConfig => $config)
{
    // Get info
    $strConfigCall = sprintf('set%s', $strConfig);

    echo('Test build config: Ok ' . $strConfig . ':<br />');

    try
    {
        $objCommand->$strConfigCall($config);

        echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
        echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
    }
    catch(\Exception $e)
    {
        echo('Config failed: ' . $e->getMessage());
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


