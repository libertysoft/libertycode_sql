<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\factory\library;



class ConstCommandFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONNECTION = 'objConnection';


	
    // Exception message constants
    const EXCEPT_MSG_CONNECTION_INVALID_FORMAT = 'Following connection "%1$s" invalid! It must be a connection object.';



}