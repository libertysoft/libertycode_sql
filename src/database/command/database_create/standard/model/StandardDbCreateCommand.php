<?php
/**
 * Description :
 * This class allows to define standard database create SQL command class.
 * Standard database create command allows to design string SQL database create command,
 * from default configuration,
 * and uses standard SQL.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_create\standard\model;

use liberty_code\sql\database\command\database_create\model\DefaultDbCreateCommand;

use liberty_code\sql\database\command\database_create\library\ConstDbCreateCommand;
use liberty_code\sql\database\command\database_create\exception\ConfigInvalidFormatException;
use liberty_code\sql\database\command\database_create\standard\library\ConstStandardDbCreateCommand;



class StandardDbCreateCommand extends DefaultDbCreateCommand
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for create database clause.
     *
     * @return string
     */
    protected function getStrClauseCreateCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = sprintf(
            ConstStandardDbCreateCommand::SQL_CONFIG_PATTERN_CREATE,
            $objConnection->getStrEscapeName($tabConfig[ConstDbCreateCommand::TAB_CONFIG_KEY_DB_NAME])
        );

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for charset clause.
     *
     * @return string
     */
    protected function getStrClauseCharsetCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
            isset($tabConfig[ConstDbCreateCommand::TAB_CONFIG_KEY_CHARSET]) ?
                sprintf(
                    ConstStandardDbCreateCommand::SQL_CONFIG_PATTERN_CHARSET,
                    $objConnection->getStrEscapeName($tabConfig[ConstDbCreateCommand::TAB_CONFIG_KEY_CHARSET])
                ) :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for collation clause.
     *
     * @return string
     */
    protected function getStrClauseCollationCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
            isset($tabConfig[ConstDbCreateCommand::TAB_CONFIG_KEY_COLLATION]) ?
                sprintf(
                    ConstStandardDbCreateCommand::SQL_CONFIG_PATTERN_COLLATION,
                    $objConnection->getStrEscapeName($tabConfig[ConstDbCreateCommand::TAB_CONFIG_KEY_COLLATION])
                ) :
                ''
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrCommand()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Build SQL command
        $strCreate = $this->getStrClauseCreateCommand();
        $strCharset= $this->getStrClauseCharsetCommand();
        $strCollation = $this->getStrClauseCollationCommand();
        $result =
            $strCreate . ((trim($strCharset) != '') ? ' ' : '') .
            $strCharset . ((trim($strCollation) != '') ? ' ' : '') .
            $strCollation;

        // Return result
        return $result;
    }



}