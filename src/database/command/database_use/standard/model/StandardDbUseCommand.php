<?php
/**
 * Description :
 * This class allows to define standard database use SQL command class.
 * Standard database use command allows to design string SQL database use command,
 * from default configuration,
 * and uses standard SQL.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_use\standard\model;

use liberty_code\sql\database\command\database_use\model\DefaultDbUseCommand;

use liberty_code\sql\database\command\database_use\library\ConstDbUseCommand;
use liberty_code\sql\database\command\database_use\exception\ConfigInvalidFormatException;
use liberty_code\sql\database\command\database_use\standard\library\ConstStandardDbUseCommand;



class StandardDbUseCommand extends DefaultDbUseCommand
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Build SQL command
        $result = sprintf(
            ConstStandardDbUseCommand::SQL_CONFIG_PATTERN_USE,
            $objConnection->getStrEscapeName($tabConfig[ConstDbUseCommand::TAB_CONFIG_KEY_DB_NAME])
        );

        // Return result
        return $result;
    }



}