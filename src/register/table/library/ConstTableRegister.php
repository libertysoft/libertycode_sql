<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\register\table\library;



class ConstTableRegister
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_COLUMN_NAME_SELECT = 'column_name_select';
    const TAB_CONFIG_KEY_GET_TIMEZONE_NAME = 'get_timezone_name';
    const TAB_CONFIG_KEY_SET_TIMEZONE_NAME = 'set_timezone_name';

    // Configuration get
    const TAB_GET_CONFIG_KEY_LIST_KEY = 'list_key';

    // Configuration set
    const TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME = 'expire_timeout_time';
    const TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND = 'expire_timeout_second';

    // Item data configuration
    const TAB_ITEM_DATA_KEY_ITEM = 'item';
    const TAB_ITEM_DATA_KEY_EXPIRE_TIMEOUT_DATETIME = 'expire_timeout_datetime';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the table register configuration standard.';
    const EXCEPT_MSG_KEY_INVALID_FORMAT = 'Following key "%1$s" invalid! The key must be in valid scope.';
    const EXCEPT_MSG_GET_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the table register get configuration standard.';
    const EXCEPT_MSG_SET_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the table register set configuration standard.';



}