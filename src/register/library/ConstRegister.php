<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\register\library;



class ConstRegister
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_COMMAND_FACTORY = 'objCommandFactory';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_TABLE_NAME = 'table_name';
    const TAB_CONFIG_KEY_PATTERN_WHERE = 'pattern_where';
    const TAB_CONFIG_KEY_PATTERN_ORDER = 'pattern_order';
    const TAB_CONFIG_KEY_PATTERN_COLUMN = 'pattern_column';
    const TAB_CONFIG_KEY_PATTERN_VALUE = 'pattern_value';
    const TAB_CONFIG_KEY_PATTERN_SET = 'pattern_set';


	
    // Exception message constants
    const EXCEPT_MSG_COMMAND_FACTORY_INVALID_FORMAT = 'Following factory "%1$s" invalid! It must be a command factory object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the default register configuration standard.';



}