<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\command\clause\library\ConstSetClause;



class ToolBoxSetClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

    /**
     * Check if set configuration is valid.
     * Condition configuration format:
     * {
     *     operand(required: if pattern not found): Expression format (@see ToolBoxExpressionClause::checkExpressionIsValid() ),
     *
     *     value(required: if pattern not found): Expression format,
     *
     *     pattern(required: if operand or value not found):
     *         "string sprintf pattern,
     *         where '%1$s' or '%s' replaced by calculated set, if found"
     * }
     *
     * @param array $tabConfig
     * @return boolean
     */
    public static function checkSetIsValid(array $tabConfig)
    {
        // Init var
        $result =
            // Check valid operand expression
            (
                (!isset($tabConfig[ConstSetClause::TAB_CONFIG_KEY_OPERAND])) ||
                ToolBoxExpressionClause::checkExpressionIsValid($tabConfig[ConstSetClause::TAB_CONFIG_KEY_OPERAND])
            ) &&

            // Check valid value expression
            (
                (!isset($tabConfig[ConstSetClause::TAB_CONFIG_KEY_VALUE])) ||
                ToolBoxExpressionClause::checkExpressionIsValid($tabConfig[ConstSetClause::TAB_CONFIG_KEY_VALUE])
            ) &&

            // Check valid pattern
            (
                (!isset($tabConfig[ConstSetClause::TAB_CONFIG_KEY_PATTERN])) ||
                (
                    is_string($tabConfig[ConstSetClause::TAB_CONFIG_KEY_PATTERN]) &&
                    (trim($tabConfig[ConstSetClause::TAB_CONFIG_KEY_PATTERN]) != '')
                )
            ) &&

            // Check minimum requirement
            (
                (
                    isset($tabConfig[ConstSetClause::TAB_CONFIG_KEY_OPERAND]) &&
                    isset($tabConfig[ConstSetClause::TAB_CONFIG_KEY_VALUE])
                ) ||
                isset($tabConfig[ConstSetClause::TAB_CONFIG_KEY_PATTERN])
            );

        // Return result
        return $result;
    }



    /**
     * Check if set clause is valid.
     * Configuration is an index array of sets,
     * where each set uses following format:
     * @see ToolBoxSetClause::checkSetIsValid().
     *
     * @param array $tabConfig
     * @return boolean
     */
    public static function checkClauseSetIsValid(array $tabConfig)
    {
        // Init var
        $result = true;

        // Check each sub-config valid
        $tabKey = array_keys($tabConfig);
        for($intCpt = 0; ($intCpt < count($tabKey)) && $result; $intCpt++)
        {
            $result = static::checkSetIsValid($tabConfig[$tabKey[$intCpt]]);
        }

        // Return result
        return $result;
    }



}