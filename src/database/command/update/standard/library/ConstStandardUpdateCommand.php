<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\update\standard\library;



class ConstStandardUpdateCommand
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_FROM = 'UPDATE %1$s';
    const SQL_CONFIG_PATTERN_SET = 'SET %1$s';
    const SQL_CONFIG_PATTERN_WHERE = 'WHERE %1$s';
    const SQL_CONFIG_PATTERN_LIMIT = 'LIMIT %1$s';
}