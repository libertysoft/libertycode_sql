<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\clause\library\ConstSetClause;
use liberty_code\sql\database\command\clause\library\ToolBoxSetClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardExpressionClause;
use liberty_code\sql\database\command\clause\standard\library\ConstStandardSetClause;



class ToolBoxStandardSetClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for set.
     * Set configuration format: @see ToolBoxSetClause::checkSetIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param array $tabConfig
     * @return string
     */
    public static function getStrSetCommand(ConnectionInterface $objConnection, array $tabConfig)
    {
        // Init var
        // Get operand expression
        $strOperand = '';
        if(isset($tabConfig[ConstSetClause::TAB_CONFIG_KEY_OPERAND]))
        {
            $strOperand = ToolBoxStandardExpressionClause::getStrExpressionCommand(
                $objConnection,
                $tabConfig[ConstSetClause::TAB_CONFIG_KEY_OPERAND]
            );
        }

        // Get value expression
        $strValue = '';
        if(isset($tabConfig[ConstSetClause::TAB_CONFIG_KEY_VALUE]))
        {
            $strValue = ToolBoxStandardExpressionClause::getStrExpressionCommand(
                $objConnection,
                $tabConfig[ConstSetClause::TAB_CONFIG_KEY_VALUE]
            );
        }

        // Get set
        $result = sprintf(ConstStandardSetClause::SQL_CONFIG_PATTERN_MAIN, $strOperand, $strValue);

        // Get pattern value, if required
        if(isset($tabConfig[ConstSetClause::TAB_CONFIG_KEY_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue($tabConfig[ConstSetClause::TAB_CONFIG_KEY_PATTERN], $result);
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for set clause.
     * Configuration array format: @see ToolBoxSetClause::checkClauseSetIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param array $tabConfig
     * @return string
     */
    public static function getStrClauseSetCommand(ConnectionInterface $objConnection, array $tabConfig)
    {
        // Init var
        $result = '';

        // Run each sub-config
        foreach($tabConfig as $config)
        {
            // Get sub-config string command
            $result =
                (trim($result) == '') ?
                    static::getStrSetCommand($objConnection, $config) :
                    sprintf(
                        ConstStandardSetClause::SQL_CONFIG_PATTERN_LINK,
                        $result,
                        static::getStrSetCommand($objConnection, $config)
                    );
        }

        // Return result
        return $result;
    }



}