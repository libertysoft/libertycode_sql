<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\browser\library;



class ConstBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_COMMAND_FACTORY = 'objCommandFactory';



    // Configuration
    const TAB_CONFIG_KEY_QUERY_ITEM_COUNT = 'query_item_count';

    // Operation configuration
    const TAB_OPERATION_CONFIG_KEY_CLAUSE = 'clause';
    const TAB_OPERATION_CONFIG_KEY_DEFAULT_VALUE = 'default_value';

    const OPERATION_TAG_VALUE = '[%value%]';


	
    // Exception message constants
    const EXCEPT_MSG_COMMAND_FACTORY_INVALID_FORMAT = 'Following factory "%1$s" invalid! It must be a command factory object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the SQL default browser configuration standard.';
    const EXCEPT_MSG_QUERY_ITEM_COUNT_INVALID_FORMAT =
        'Following query "%1$s" invalid! 
        The query used for total count of item calculation, must have the count on first row, first column.';
    const EXCEPT_MSG_OPERATION_CONFIG_INVALID_FORMAT = 'Operation configuration must be an array, not empty and following the SQL default browser operation configuration standard.';
    const EXCEPT_MSG_OPERATION_VALUE_INVALID_FORMAT = 'Operation value can be a string, numeric, boolean or null value.';



}