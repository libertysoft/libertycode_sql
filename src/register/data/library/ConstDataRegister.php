<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\register\data\library;



class ConstDataRegister
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_COLUMN_NAME_KEY = 'column_name_key';
    const TAB_CONFIG_KEY_COLUMN_NAME_ITEM = 'column_name_item';
    const TAB_CONFIG_KEY_COLUMN_NAME_EXPIRE_TIMEOUT_DATETIME = 'column_name_expire_timeout_datetime';
    const TAB_CONFIG_KEY_GET_TIMEZONE_NAME = 'get_timezone_name';
    const TAB_CONFIG_KEY_SET_TIMEZONE_NAME = 'set_timezone_name';
    const TAB_CONFIG_KEY_SET_DATETIME_FORMAT = 'set_datetime_format';
    const TAB_CONFIG_KEY_SET_DATETIME_VALUE_NULL = 'set_datetime_value_null';

    // Configuration get
    const TAB_GET_CONFIG_KEY_PATTERN_KEY = 'pattern_key';
    const TAB_GET_CONFIG_KEY_LIST_KEY = 'list_key';

    // Configuration set
    const TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME = 'expire_timeout_time';
    const TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND = 'expire_timeout_second';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the data register configuration standard.';
    const EXCEPT_MSG_GET_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the data register get configuration standard.';
    const EXCEPT_MSG_SET_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the data register set configuration standard.';



}