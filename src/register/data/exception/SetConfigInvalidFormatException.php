<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\register\data\exception;

use Exception;

use DateTime;
use liberty_code\sql\register\data\library\ConstDataRegister;



class SetConfigInvalidFormatException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstDataRegister::EXCEPT_MSG_SET_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid expire timeout time
            (
                (!isset($config[ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME])) ||
                (
                    is_int($config[ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME]) &&
                    ($config[ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME] >= 0)
                ) ||
                ($config[ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME] instanceof DateTime)
            ) &&
            
            // Check valid expire timeout second
            (
                (!isset($config[ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND])) ||
                (
                    is_int($config[ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND]) &&
                    ($config[ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND] >= 0)
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}