<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/test/HelpGetCommandFacto.php');



// Init var
$objCommand = $objCommandFacto->getObjUpdateCommand();

$config_1 = array(
    'from' => [
        [
            'db_name' => 'libertycode_sql_test',
            'table_name' => 'message'
        ]
    ],
    'set' => [
            [
                'operand' => [
                    'table_name' => 'message',
                    'column_name' => 'msg_txt'
                ],
                'value' => [
                    'table_name' => 'message',
                    'column_name' => 'msg_txt',
                    'pattern' => 'CONCAT(%s, \' updated\')'
                ],
            ],
            [
                'operand' => [
                    'table_name' => 'message',
                    'column_name' => 'msg_dt_create'
                ],
                'value' => ['pattern' => 'NOW()'],
            ]
    ],
    'where' => [
        'content' => [
            [
                'operand' => [
                    'table_name' => 'message',
                    'column_name' => 'msg_usr_id'
                ],
                'value' => ['value' => 1]
            ],
            [
                'operand' => [
                    'table_name' => 'message',
                    'column_name' => 'msg_txt'
                ],
                'operator' => 'null',
                'not' => 1
            ]
        ]
    ],
    'limit' => 2
);

$config_2 = array(
    'from' => [
        [
            'db_name' => 'libertycode_sql_test',
            'table_name' => 'message'
        ]
    ]
);

$config_3 = array(
    'from' => [
        'user'
    ],
    'set' => [
        [
            'operand' => 'usr_name',
            'value' => [
                'column_name' => 'usr_name',
                'pattern' => 'CONCAT(%s, \' updated\')'
            ],
        ],
        [
            'operand' => 'usr_first_name',
            'value' => ['pattern' => 'CONCAT(`usr_first_name`, \' updated\')'],
        ]
    ]
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3 // Ok
);



// Test command
require_once($strRootAppPath . '/src/database/command/test/HelpRunCommand.php');



// Reconnection, if required
if(!$objConnection->checkIsConnect())$objConnection->connect();



// Test build clause: Ko
$tabConfig = array(
    'From' => array(['test' => 'test']),
    'Set' => array(['test' => 'test']),
    'Where' => array(['test' => 'test']),
    'Limit' => array(['test' => 'test'])
);

foreach($tabConfig as $strClause => $config)
{
    // Get info
    $strClauseCall = sprintf('setClause%s', $strClause);

    echo('Test build clause: Ko ' . $strClause . ':<br />');

    try
    {
        $objCommand = $objCommandFacto->getObjUpdateCommand();
        $objCommand->$strClauseCall($config);

        echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
        echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
    }
    catch(\Exception $e)
    {
        echo('Config failed: ' . $e->getMessage());
    }

    echo('<br /><br /><br />');
}



// Test build clause: Ok
echo('Test build clause: <br />');

try
{
    $objCommand = $objCommandFacto->getObjUpdateCommand(
        array(
            'from_pattern' => '[%s]',
            'set_pattern' => '{%s}',
            'where_pattern' => '[%s]',
            'limit_pattern' => '{%s}'
        )
    );
    $objCommand->setClauseFrom(
        array(
            [
                'db_name' => 'libertycode_sql_test',
                'table_name' => 'message'
            ]
        )
    )->setClauseSet(
        array(
            [
                'operand' => [
                    'table_name' => 'message',
                    'column_name' => 'msg_txt'
                ],
                'value' => [
                    'table_name' => 'message',
                    'column_name' => 'msg_txt',
                    'pattern' => 'CONCAT(%s, \' updated\')'
                ],
            ],
            [
                'operand' => [
                    'table_name' => 'message',
                    'column_name' => 'msg_dt_create'
                ],
                'value' => ['pattern' => 'NOW()'],
            ]
        )
    )->setClauseWhere(
        array(
            'content' => [
                [
                    'operand' => [
                        'table_name' => 'message',
                        'column_name' => 'msg_usr_id'
                    ],
                    'value' => ['value' => 1]
                ],
                [
                    'operand' => [
                        'table_name' => 'message',
                        'column_name' => 'msg_txt'
                    ],
                    'operator' => 'null',
                    'not' => 1
                ]
            ]
        )
    )->setClauseLimit(2);

    echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
    echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
}
catch(\Exception $e)
{
    echo('Config failed: ' . $e->getMessage());
}

echo('<br /><br /><br />');



// Test build clause: Ok
echo('Test build clause: <br />');

try
{
    $objCommand = $objCommandFacto->getObjUpdateCommand(
        array(
            'from_pattern' => '[UPDATE ...]',
            'set_pattern' => '{SET  ...}',
            'where_pattern' => '[WHERE ...]',
            'limit_pattern' => '{LIMIT ...}'
        )
    );

    echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
    echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
}
catch(\Exception $e)
{
    echo('Config failed: ' . $e->getMessage());
}

echo('<br /><br /><br />');


