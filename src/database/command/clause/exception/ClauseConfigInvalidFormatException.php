<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\exception;

use liberty_code\sql\database\command\clause\library\ConstClause;



class ClauseConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     *
     * @param string $strClause
	 * @param mixed $config
     */
	public function __construct($strClause, $config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstClause::EXCEPT_MSG_CLAUSE_CONFIG_INVALID_FORMAT,
            strval($strClause),
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
}