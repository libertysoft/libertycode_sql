<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\command\clause\library\ConstLimitClause;
use liberty_code\sql\database\command\clause\library\ToolBoxLimitClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardClause;
use liberty_code\sql\database\command\clause\standard\library\ConstStandardLimitClause;



class ToolBoxStandardLimitClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for limit clause.
     * Limit configuration format: @see ToolBoxLimitClause::checkClauseLimitIsValid().
     *
     * @param integer|array $config
     * @return string
     */
    public static function getStrClauseLimitCommand($config)
    {
        // Init var
        $result = '';

        // Get row count
        if(is_int($config))
        {
            $result = strval($config);
        }
        else if(isset($config[ConstLimitClause::TAB_CONFIG_KEY_COUNT]))
        {
            $result = strval($config[ConstLimitClause::TAB_CONFIG_KEY_COUNT]);
        }

        // Get row index start, if required
        if(isset($config[ConstLimitClause::TAB_CONFIG_KEY_START]))
        {
            $result = sprintf(
                ConstStandardLimitClause::SQL_CONFIG_PATTERN_START,
                $result,
                strval($config[ConstLimitClause::TAB_CONFIG_KEY_START])
            );
        }

        // Get pattern value, if required
        if(isset($config[ConstLimitClause::TAB_CONFIG_KEY_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue($config[ConstLimitClause::TAB_CONFIG_KEY_PATTERN], $result);
        }

        // Return result
        return $result;
    }



}