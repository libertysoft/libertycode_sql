<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_alter\standard\library;



class ConstStandardDbAlterCommand
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_ALTER = 'ALTER DATABASE %1$s';
    const SQL_CONFIG_PATTERN_CHARSET = 'CHARACTER SET %1$s';
    const SQL_CONFIG_PATTERN_COLLATION = 'COLLATE %1$s';
}