<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/test/HelpGetCommandFacto.php');



// Init var
$objCommand = $objCommandFacto->getObjDeleteCommand();

$config_1 = array(
    'from' => [
        [
            'db_name' => 'libertycode_sql_test',
            'table_name' => 'message'
        ]
    ],
    'where' => [
        'content' => [
            [
                'operand' => [
                    'table_name' => 'message',
                    'column_name' => 'msg_txt'
                ],
                'operator' => 'null',
                'not' => 1
            ]
        ]
    ],
    'order' => [
        [
            'operand' => [
                'table_name' => 'message',
                'column_name' => 'msg_id'
            ],
            'operator' => 'desc'
        ]
    ],
    'limit' => 2
);

$config_2 = array(
    'where' => [
        'content' => [
            [
                'operand' => [
                    'table_name' => 'message',
                    'column_name' => 'msg_txt'
                ],
                'operator' => 'null',
                'not' => 1
            ]
        ]
    ],
    'order' => [
        [
            'operand' => [
                'table_name' => 'message',
                'column_name' => 'msg_id'
            ],
            'operator' => 'desc'
        ]
    ],
    'limit' => 2
);

$config_3 = array(
    'delete' => [
        'user'
    ],
    'from' => [
        'user'
    ]
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3 // Ok
);



// Test command
require_once($strRootAppPath . '/src/database/command/test/HelpRunCommand.php');



// Reconnection, if required
if(!$objConnection->checkIsConnect())$objConnection->connect();



// Test build clause: Ko
$tabConfig = array(
    'From' => array(['test' => 'test']),
    'Where' => array(['test' => 'test']),
    'Order' => array(['test' => 'test']),
    'Limit' => array(['test' => 'test'])
);

foreach($tabConfig as $strClause => $config)
{
    // Get info
    $strClauseCall = sprintf('setClause%s', $strClause);

    echo('Test build clause: Ko ' . $strClause . ':<br />');

    try
    {
        $objCommand = $objCommandFacto->getObjDeleteCommand();
        $objCommand->$strClauseCall($config);

        echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
        echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
    }
    catch(\Exception $e)
    {
        echo('Config failed: ' . $e->getMessage());
    }

    echo('<br /><br /><br />');
}



// Test build clause: Ok
echo('Test build clause: <br />');

try
{
    $objCommand = $objCommandFacto->getObjDeleteCommand(
        array(
            'from_pattern' => '[%s]',
            'where_pattern' => '{%s}',
            'order_pattern' => '[%s]',
            'limit_pattern' => '{%s}'
        )
    );
    $objCommand->setClauseFrom(
        array(
            [
                'db_name' => 'libertycode_sql_test',
                'table_name' => 'message'
            ]
        )
    )->setClauseWhere(
        array(
            'content' => [
                [
                    'operand' => [
                        'table_name' => 'message',
                        'column_name' => 'msg_txt'
                    ],
                    'operator' => 'null',
                    'not' => 1
                ]
            ]
        )
    )->setClauseOrder(
        array(
            [
                'operand' => [
                    'table_name' => 'message',
                    'column_name' => 'msg_id'
                ],
                'operator' => 'desc'
            ]
        )
    )->setClauseLimit(2);

    echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
    echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
}
catch(\Exception $e)
{
    echo('Config failed: ' . $e->getMessage());
}

echo('<br /><br /><br />');



// Test build clause: Ok
echo('Test build clause: <br />');

try
{
    $objCommand = $objCommandFacto->getObjDeleteCommand(
        array(
            'from_pattern' => '[DELETE FROM ...]',
            'where_pattern' => '{WHERE ...}',
            'order_pattern' => '[ORDER ...]',
            'limit_pattern' => '{LIMIT ...}'
        )
    );

    echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
    echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
}
catch(\Exception $e)
{
    echo('Config failed: ' . $e->getMessage());
}

echo('<br /><br /><br />');


