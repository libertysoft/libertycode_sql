<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_show\library;



class ConstDbShowCommand
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the default database show command configuration standard.';



}