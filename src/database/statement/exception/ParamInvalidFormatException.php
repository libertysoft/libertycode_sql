<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\statement\exception;

use liberty_code\sql\database\statement\library\ConstStatement;



class ParamInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $param
     */
	public function __construct($param)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStatement::EXCEPT_MSG_PARAM_INVALID_FORMAT,
            mb_strimwidth(strval($param), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

	/**
	 * Check if specified parameters config has valid format.
	 * 
     * @param mixed $param
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($param)
    {
		// Init var
		$result =
            // Check valid array
            is_array($param);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($param) ? serialize($param) : $param));
		}
		
		// Return result
		return $result;
    }
	
	
	
}