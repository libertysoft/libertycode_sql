<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\result\exception;

use liberty_code\sql\database\result\library\ConstResult;



class DataNameInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $name
     */
	public function __construct($name)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
        $this->message = sprintf(ConstResult::EXCEPT_MSG_DATA_NAME_INVALID_FORMAT, strval($name));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

	/**
	 * Check if specified name has valid format.
	 * 
     * @param mixed $name
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($name)
    {
		// Init var
		$result =
            // Check valid string
            is_string($name) && (trim($name) != '');

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($name);
		}
		
		// Return result
		return $result;
    }
	
	
	
}