<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\validation\rule\sql_exist\library;



class ConstExistSqlRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';

    const CONFIG_DEFAULT_VALUE_KEY = 'sql_exist';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s not found.';

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_DB_NAME = 'db_name';
    const TAB_VALID_CONFIG_KEY_TABLE_NAME = 'table_name';
    const TAB_VALID_CONFIG_KEY_COLUMN_NAME = 'column_name';
    const TAB_VALID_CONFIG_KEY_INCLUDE = 'include';
    const TAB_VALID_CONFIG_KEY_EXCLUDE = 'exclude';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the SQL exist rule configuration standard.';
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the SQL exist rule validation configuration standard.';



}