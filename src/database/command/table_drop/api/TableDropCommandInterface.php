<?php
/**
 * Description :
 * This class allows to describe behavior of table drop SQL command class.
 * Table drop command allows to design string SQL table drop command,
 * from following specified configuration:
 * [
 *     table(required: if table_pattern not found)): array (@see ToolBoxTableClause::checkClauseTableIsValid() ),
 *
 *     table_pattern(required: if table not found):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated drop clause, if found"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\table_drop\api;

use liberty_code\sql\database\command\api\CommandInterface;

use liberty_code\sql\database\command\clause\library\ToolBoxTableClause;



interface TableDropCommandInterface extends CommandInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Set table clause config array.
     * Configuration format follows @see ToolBoxTableClause::checkClauseTableIsValid().
     *
     * @param string|array $config
     * @return static
     */
    public function setClauseTable($config);
}