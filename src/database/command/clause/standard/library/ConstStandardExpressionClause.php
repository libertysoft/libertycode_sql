<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;



class ConstStandardExpressionClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_DB = '%1$s.%2$s';
    const SQL_CONFIG_PATTERN_TABLE = '%1$s.%2$s';
    const SQL_CONFIG_PATTERN_ALIAS = '%1$s AS %2$s';
    const SQL_CONFIG_PATTERN_LINK = '%1$s, %2$s';
}