<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\clause\library\ConstTableClause;
use liberty_code\sql\database\command\clause\library\ToolBoxTableClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardClause;
use liberty_code\sql\database\command\clause\standard\library\ConstStandardTableClause;



class ToolBoxStandardTableClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for table clause.
     * Table configuration format: @see ToolBoxTableClause::checkClauseTableIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param string|array $config
     * @return string
     */
    public static function getStrClauseTableCommand(ConnectionInterface $objConnection, $config)
    {
        // Init var
        $result = '';

        // Get table name or value
        if(is_string($config))
        {
            $result = $objConnection->getStrEscapeName($config);
        }
        else if(isset($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_NAME]))
        {
            $result = $objConnection->getStrEscapeName($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_NAME]);
        }

        // Get database, if required
        if(isset($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_DB_NAME]))
        {
            $result = sprintf(
                ConstStandardTableClause::SQL_CONFIG_PATTERN_TABLE_DB,
                $objConnection->getStrEscapeName($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_DB_NAME]),
                $result
            );
        }

        // Get pattern value, if required
        if(isset($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue($config[ConstTableClause::TAB_CONFIG_KEY_TABLE_PATTERN], $result);
        }

        // Return result
        return $result;
    }



}