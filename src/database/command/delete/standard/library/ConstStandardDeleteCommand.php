<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\delete\standard\library;



class ConstStandardDeleteCommand
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_DELETE = 'DELETE %1$s';
    const SQL_CONFIG_PATTERN_FROM = 'FROM %1$s';
    const SQL_CONFIG_PATTERN_WHERE = 'WHERE %1$s';
    const SQL_CONFIG_PATTERN_ORDER = 'ORDER BY %1$s';
    const SQL_CONFIG_PATTERN_LIMIT = 'LIMIT %1$s';
}