<?php
/**
 * Description :
 * This class allows to define standard update SQL command class.
 * Standard update command allows to design string SQL update command,
 * from default configuration,
 * and uses standard SQL.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\update\standard\model;

use liberty_code\sql\database\command\update\model\DefaultUpdateCommand;

use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardFromClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardConditionClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardLimitClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardSetClause;
use liberty_code\sql\database\command\update\library\ConstUpdateCommand;
use liberty_code\sql\database\command\update\exception\ConfigInvalidFormatException;
use liberty_code\sql\database\command\update\standard\library\ConstStandardUpdateCommand;



class StandardUpdateCommand extends DefaultUpdateCommand
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for from clause.
     *
     * @return string
     */
    protected function getStrClauseFromCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM]) ?
            sprintf(
                ConstStandardUpdateCommand::SQL_CONFIG_PATTERN_FROM,
                ToolBoxStandardFromClause::getStrClauseFromCommand(
                    $objConnection,
                    $tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for set clause.
     *
     * @return string
     */
    protected function getStrClauseSetCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET]) ?
            sprintf(
                ConstStandardUpdateCommand::SQL_CONFIG_PATTERN_SET,
                ToolBoxStandardSetClause::getStrClauseSetCommand(
                    $objConnection,
                    $tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for where clause.
     *
     * @return string
     */
    protected function getStrClauseWhereCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]) ?
            sprintf(
                ConstStandardUpdateCommand::SQL_CONFIG_PATTERN_WHERE,
                ToolBoxStandardConditionClause::getStrClauseConditionCommand(
                    $objConnection,
                    $tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for limit clause.
     *
     * @return string
     */
    protected function getStrClauseLimitCommand()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT]) ?
            sprintf(
                ConstStandardUpdateCommand::SQL_CONFIG_PATTERN_LIMIT,
                ToolBoxStandardLimitClause::getStrClauseLimitCommand(
                    $tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrCommand()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Build SQL command
        $strFrom = $this->getStrClauseFromCommand();
        $strSet = $this->getStrClauseSetCommand();
        $strWhere = $this->getStrClauseWhereCommand();
        $strLimit = $this->getStrClauseLimitCommand();
        $result =
            $strFrom . ((trim($strSet) != '') ? ' ' : '') .
            $strSet . ((trim($strWhere) != '') ? ' ' : '') .
            $strWhere . ((trim($strLimit) != '') ? ' ' : '') .
            $strLimit;

        // Return result
        return $result;
    }



}