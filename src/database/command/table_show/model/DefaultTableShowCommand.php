<?php
/**
 * Description :
 *
 * This class allows to define default table show SQL command class.
 * Can be consider is base of all table show SQL command type.
 *
 * Default table show command allows to design string table show SQL command,
 * from following specified configuration: @see TableShowCommandInterface.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\table_show\model;

use liberty_code\sql\database\command\model\DefaultCommand;
use liberty_code\sql\database\command\table_show\api\TableShowCommandInterface;

use liberty_code\sql\database\command\library\ConstCommand;
use liberty_code\sql\database\command\table_show\library\ConstTableShowCommand;
use liberty_code\sql\database\command\table_show\exception\ConfigInvalidFormatException;



abstract class DefaultTableShowCommand extends DefaultCommand implements TableShowCommandInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		// $result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstCommand::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function setDbName($strDbNm)
    {
        // Check config valid
        if(
            (!is_string($strDbNm)) ||
            (trim($strDbNm) == ''))
        {
            throw new ConfigInvalidFormatException($strDbNm);
        }

        // Set config
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstTableShowCommand::TAB_CONFIG_KEY_DB_NAME] = $strDbNm;

        // Return result
        return $this;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute()
    {
        // Init var
        $result = $this->executeResult();

        if($result !== false)
        {
            $result = 0;
        }

        // Return result
        return $result;
    }



}