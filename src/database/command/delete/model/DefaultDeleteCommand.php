<?php
/**
 * Description :
 * This class allows to define default delete SQL command class.
 * Can be consider is base of all delete SQL command type.
 *
 * Default delete command allows to design string SQL delete command,
 * from following specified configuration: @see DeleteCommandInterface.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\delete\model;

use liberty_code\sql\database\command\model\DefaultCommand;
use liberty_code\sql\database\command\delete\api\DeleteCommandInterface;

use liberty_code\sql\database\command\library\ConstCommand;
use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxConditionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxOrderClause;
use liberty_code\sql\database\command\clause\library\ToolBoxLimitClause;
use liberty_code\sql\database\command\clause\exception\ClauseConfigInvalidFormatException;
use liberty_code\sql\database\command\delete\library\ConstDeleteCommand;
use liberty_code\sql\database\command\delete\exception\ConfigInvalidFormatException;



abstract class DefaultDeleteCommand extends DefaultCommand  implements DeleteCommandInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		// $result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstCommand::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseDelete(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxExpressionClause::checkClauseExpressionIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_DELETE,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_DELETE] = $tabConfig;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseFrom(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxFromClause::checkClauseFromIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = $tabConfig;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseWhere(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxConditionClause::checkClauseConditionIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = $tabConfig;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseOrder(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxOrderClause::checkClauseOrderIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_ORDER,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_ORDER] = $tabConfig;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseLimit($config)
    {
        // Check clause valid
        if(!ToolBoxLimitClause::checkClauseLimitIsValid($config))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT,
                (is_array($config) ? serialize($config) : $config)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT] = $config;

        // Return result
        return $this;
    }



}