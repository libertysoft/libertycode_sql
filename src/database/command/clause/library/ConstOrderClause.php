<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;



class ConstOrderClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_OPERAND = 'operand';
    const TAB_CONFIG_KEY_OPERATOR = 'operator';
    const TAB_CONFIG_KEY_PATTERN = 'pattern';

    // Operator configuration
    const OPERATOR_CONFIG_ASC = 'asc';
    const OPERATOR_CONFIG_DESC = 'desc';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get configuration operator options array.
     *
     * @return array
     */
    static public function getTabConfigOperator()
    {
        // Init var
        $result = array(
            self::OPERATOR_CONFIG_ASC,
            self::OPERATOR_CONFIG_DESC
        );

        // Return result
        return $result;
    }



}