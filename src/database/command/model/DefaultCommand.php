<?php
/**
 * Description :
 * This class allows to define default SQL command class.
 * Can be consider is base of all SQL command type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\sql\database\command\api\CommandInterface;

use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\library\ConstCommand;
use liberty_code\sql\database\command\exception\ConnectionInvalidFormatException;
use liberty_code\sql\database\command\exception\ConfigInvalidFormatException;



abstract class DefaultCommand extends FixBean implements CommandInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************

    /**
     * @inheritdoc
     * @param ConnectionInterface $objConnection
     * @param array $tabConfig = null
     */
    public function __construct(ConnectionInterface $objConnection, array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init connection
        $this->setConnection($objConnection);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstCommand::DATA_KEY_DEFAULT_CONNECTION))
        {
            $this->__beanTabData[ConstCommand::DATA_KEY_DEFAULT_CONNECTION] = null;
        }

        if(!$this->beanExists(ConstCommand::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstCommand::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstCommand::DATA_KEY_DEFAULT_CONNECTION,
            ConstCommand::DATA_KEY_DEFAULT_CONFIG
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstCommand::DATA_KEY_DEFAULT_CONNECTION:
                    ConnectionInvalidFormatException::setCheck($value);
                    break;

                case ConstCommand::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjConnection()
    {
        // Return result
        return $this->beanGet(ConstCommand::DATA_KEY_DEFAULT_CONNECTION);
    }



    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstCommand::DATA_KEY_DEFAULT_CONFIG);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConnection(ConnectionInterface $objConnection)
    {
        $this->beanSet(ConstCommand::DATA_KEY_DEFAULT_CONNECTION, $objConnection);
    }



    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstCommand::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute()
    {
        // Return result
        return $this->getObjConnection()->execute($this->getStrCommand());
    }



    /**
     * @inheritdoc
     */
    public function executeResult()
    {
        // Return result
        return $this->getObjConnection()->executeResult($this->getStrCommand());
    }



    /**
     * @inheritdoc
     */
    public function getObjStatement(array $tabConfig = array(), array $tabParam = array())
    {
        // Return result
        return $this->getObjConnection()->getObjStatement(
            $this->getStrCommand(),
            $tabConfig,
            $tabParam
        );
    }



}