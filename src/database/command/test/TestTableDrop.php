<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/test/HelpGetCommandFacto.php');



// Init var
$objCommand = $objCommandFacto->getObjTableDropCommand();

$config_1 = array(
    'table' => [
        'db_name' => $strDbNm,
        'table_name' => $strTableNmAdmin
    ]
);

$config_2 = array(
    'table' => [
        'db_name' => $strDbNm
    ]
);

$config_3 = array(
    'table' => [
        'table_name' => $strTableNmMsg
    ]
);

$config_4 = array(
    'table' => $strTableNmCnf
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3, // Ok
    $config_4 // Ok
);



// Test command
$_GET['exec'] = '1';
$_GET['exec-result'] = '0';
$_GET['exec-stmt'] = '0';
$_GET['exec-stmt-result'] = '0';
$_GET['clean'] = '0';
require($strRootAppPath . '/src/database/command/test/HelpRunCommand.php');



// Reconnection, if required
if(!$objConnection->checkIsConnect())$objConnection->connect();



// Test build clause: Ko
$tabConfig = array(
    'Table' => array('test' => 'test')
);

foreach($tabConfig as $strClause => $config)
{
    // Get info
    $strClauseCall = sprintf('setClause%s', $strClause);

    echo('Test build clause: Ko ' . $strClause . ':<br />');

    try
    {
        $objCommand = $objCommandFacto->getObjTableDropCommand();
        $objCommand->$strClauseCall($config);

        echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
        echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
    }
    catch(\Exception $e)
    {
        echo('Config failed: ' . $e->getMessage());
    }

    echo('<br /><br /><br />');
}



// Test build clause: Ok
echo('Test build clause: <br />');

try
{
    $objCommand = $objCommandFacto->getObjTableDropCommand(
        array(
            'table_pattern' => '[%s]'
        )
    );
    $objCommand->setClauseTable(
        array(
            'db_name' => $strDbNm,
            'table_name' => $strTableNmUsr
        )
    );

    echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
    echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
}
catch(\Exception $e)
{
    echo('Config failed: ' . $e->getMessage());
}

echo('<br /><br /><br />');


