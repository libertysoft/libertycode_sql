<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\persistence\exception;

use liberty_code\sql\persistence\library\ConstPersistor;



class ActionConfigInvalidFormatException extends \Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param string $strAction
     * @param mixed $config
     */
    public function __construct($strAction, $config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstPersistor::EXCEPT_MSG_ACTION_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "..."),
            mb_strimwidth(strval($strAction), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format, for specified action.
     *
     * @param string $strAction
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($strAction, $config)
    {
        // Init var
        $result =
            is_array($config) &&

            // Check valid table name
            (
                (!isset($config[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME])) ||
                (
                    is_string($config[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME]) &&
                    (trim($config[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME]) != '')
                )
            ) &&

            // Check valid column name id
            (
                (!array_key_exists(ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID, $config)) ||
                (
                    is_string($config[ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID]) &&
                    (trim($config[ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID]) != '')
                )
            ) &&

            // Check valid id new option
            (
                (!isset($config[ConstPersistor::TAB_CONFIG_KEY_OPT_ID_NEW])) ||
                (
                    is_string($config[ConstPersistor::TAB_CONFIG_KEY_OPT_ID_NEW]) &&
                    in_array($config[ConstPersistor::TAB_CONFIG_KEY_OPT_ID_NEW], ConstPersistor::getTabConfigOptIdNew())
                )
            ) &&

            // Check valid select pattern
            (
                (!isset($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_SELECT])) ||
                (
                    is_string($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_SELECT]) &&
                    (trim($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_SELECT]) != '')
                )
            ) &&

            // Check valid where pattern
            (
                (!isset($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE])) ||
                (
                    is_string($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE]) &&
                    (trim($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE]) != '')
                )
            ) &&

            // Check valid order pattern
            (
                (!isset($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_ORDER])) ||
                (
                    is_string($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_ORDER]) &&
                    (trim($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_ORDER]) != '')
                )
            ) &&

            // Check valid column pattern
            (
                (!isset($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_COLUMN])) ||
                (
                    is_string($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_COLUMN]) &&
                    (trim($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_COLUMN]) != '')
                )
            ) &&

            // Check valid value pattern
            (
                (!isset($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_VALUE])) ||
                (
                    is_string($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_VALUE]) &&
                    (trim($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_VALUE]) != '')
                )
            ) &&

            // Check valid set pattern
            (
                (!isset($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_SET])) ||
                (
                    is_string($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_SET]) &&
                    (trim($config[ConstPersistor::TAB_CONFIG_KEY_PATTERN_SET]) != '')
                )
            );

        // Check by action, if required
        if($result)
        {
            switch($strAction)
            {
                case ConstPersistor::TAB_ACTION_KEY_SEARCH:
                    // No specific check
                    break;

                case ConstPersistor::TAB_ACTION_KEY_GET:
                case ConstPersistor::TAB_ACTION_KEY_CREATE:
                case ConstPersistor::TAB_ACTION_KEY_UPDATE:
                case ConstPersistor::TAB_ACTION_KEY_DELETE:
                    $result =
                        // Table name required
                        isset($config[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME]);
                break;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format, for specified action.
     *
     * @param string $strAction
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    static public function setCheck($strAction, $config)
    {
        // Init var
        $result =
            // Check valid action
            is_string($strAction) &&
            in_array($strAction, ConstPersistor::getTabAction()) &&

            // Check valid configuration
            (
                is_null($config) ||
                (
                    // Check valid array
                    is_array($config) &&
                    static::checkConfigIsValid($strAction, $config)
                )
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($strAction, (is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}