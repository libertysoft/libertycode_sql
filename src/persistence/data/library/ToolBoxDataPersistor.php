<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\persistence\data\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\crypto\library\ToolBoxHash;



class ToolBoxDataPersistor extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get data associative array formatted,
     * from specified data associative array get from query.
     *
     * Result format:
     * {
     *     attribute_name_1 => Data value 1,
     *     ...,
     *     attribute_name_N => Data value N
     * }
     *
     * @param array $tabDataQuery
     * @param string $strColNmAttrNm
     * @param string $strColNmAttrValue
     * @param null|string $strColNmId
     * @return array
     */
    public static function getTabDataFormatResult(array $tabDataQuery, $strColNmAttrNm, $strColNmAttrValue, $strColNmId = null)
    {
        // Init var
        $result = array();
        $strIdHashPrefix = 'id_';

        // Case without ID
        if(is_null($strColNmId))
        {
            // Build data
            $data = array();
            foreach($tabDataQuery as $dataQuery)
            {
                // Get data info
                $strKey = $dataQuery[$strColNmAttrNm];
                $value = $dataQuery[$strColNmAttrValue];
                $data[$strKey] = $value;
            }

            // Register data in result
            $result[] = $data;
        }
        // Case with ID
        else
        {
            // Build data
            $tabData = array();
            foreach($tabDataQuery as $dataQuery)
            {
                // Get data info
                $id = $dataQuery[$strColNmId];
                $strId = $strIdHashPrefix . ToolBoxHash::getStrHash($id);
                $strKey = $dataQuery[$strColNmAttrNm];
                $value = $dataQuery[$strColNmAttrValue];

                // Create data if not exists
                if(!array_key_exists($strId, $tabData))
                {
                    $tabData[$strId] = array($strColNmId => $id);
                }

                // Register data info
                $tabData[$strId][$strKey] = $value;
            }

            // Register data in result
            $result = array_values($tabData);
        }

        // Return result
        return $result;
    }
	
	
	
}