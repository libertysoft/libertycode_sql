<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/test/MysqlPdoConnection.php');

// Use
use liberty_code\sql\database\connection\library\ConstConnection;
use liberty_code\sql\database\test\MysqlPdoConnection;



// Init var
$tabConfig = array(
    ConstConnection::TAB_CONFIG_KEY_HOST => 'localhost',
    ConstConnection::TAB_CONFIG_KEY_CHARSET => 'utf8',
    ConstConnection::TAB_CONFIG_KEY_LOGIN => 'root',
    ConstConnection::TAB_CONFIG_KEY_PASSWORD => ''
);
$objConnection = new MysqlPdoConnection($tabConfig);



// Utility
function process($tabConfig, $callCheckConfigIsValid, $callGetStrCommand)
{
    // Run all config
    foreach($tabConfig as $config)
    {
        // Test config
        echo('Test config: <br />');
        echo('Config: <pre>');var_dump($config);echo('</pre>');

        // Check config valid: get string command
        if($callCheckConfigIsValid($config))
        {
            echo('Command: <pre>');var_dump($callGetStrCommand($config));echo('</pre>');
        }
        // Check config valid failed
        else
        {
            echo('Check failed!');
        }

        echo('<br /><br /><br />');
    }
}


