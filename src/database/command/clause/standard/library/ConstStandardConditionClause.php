<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\sql\database\command\clause\library\ConstConditionClause;



class ConstStandardConditionClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_MAIN = '(%1$s)';
    const SQL_CONFIG_PATTERN_NOT = 'NOT (%1$s)';
    const SQL_CONFIG_PATTERN_OPERATOR_EQUAL = '%1$s = %2$s';
    const SQL_CONFIG_PATTERN_OPERATOR_LIKE = '%1$s LIKE %2$s';
    const SQL_CONFIG_PATTERN_OPERATOR_REGEXP = '%1$s REGEXP %2$s';
    const SQL_CONFIG_PATTERN_OPERATOR_IN = '%1$s IN (%2$s)';
    const SQL_CONFIG_PATTERN_OPERATOR_NULL = '%1$s IS NULL';
    const SQL_CONFIG_PATTERN_OPERATOR_LESS = '%1$s < %2$s';
    const SQL_CONFIG_PATTERN_OPERATOR_LESS_EQUAL = '%1$s <= %2$s';
    const SQL_CONFIG_PATTERN_OPERATOR_GREATER = '%1$s > %2$s';
    const SQL_CONFIG_PATTERN_OPERATOR_GREATER_EQUAL = '%1$s >= %2$s';

    const SQL_CONFIG_PATTERN_GROUP_MAIN = '(%1$s)';
    const SQL_CONFIG_PATTERN_GROUP_TYPE_AND = '%1$s AND %2$s';
    const SQL_CONFIG_PATTERN_GROUP_TYPE_OR = '%1$s OR %2$s';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get configuration operator options array.
     *
     * @return array
     */
    static public function getTabConfigOperator()
    {
        // Init var
        $result = array(
            ConstConditionClause::OPERATOR_CONFIG_EQUAL => self::SQL_CONFIG_PATTERN_OPERATOR_EQUAL,
            ConstConditionClause::OPERATOR_CONFIG_LIKE => self::SQL_CONFIG_PATTERN_OPERATOR_LIKE,
            ConstConditionClause::OPERATOR_CONFIG_REGEXP => self::SQL_CONFIG_PATTERN_OPERATOR_REGEXP,
            ConstConditionClause::OPERATOR_CONFIG_IN => self::SQL_CONFIG_PATTERN_OPERATOR_IN,
            ConstConditionClause::OPERATOR_CONFIG_NULL => self::SQL_CONFIG_PATTERN_OPERATOR_NULL,
            ConstConditionClause::OPERATOR_CONFIG_LESS => self::SQL_CONFIG_PATTERN_OPERATOR_LESS,
            ConstConditionClause::OPERATOR_CONFIG_LESS_EQUAL => self::SQL_CONFIG_PATTERN_OPERATOR_LESS_EQUAL,
            ConstConditionClause::OPERATOR_CONFIG_GREATER => self::SQL_CONFIG_PATTERN_OPERATOR_GREATER,
            ConstConditionClause::OPERATOR_CONFIG_GREATER_EQUAL => self::SQL_CONFIG_PATTERN_OPERATOR_GREATER_EQUAL,
        );

        // Return result
        return $result;
    }



    /**
     * Get configuration type options array.
     *
     * @return array
     */
    static public function getTabConfigGroupType()
    {
        // Init var
        $result = array(
            ConstConditionClause::GROUP_TYPE_CONFIG_AND => self::SQL_CONFIG_PATTERN_GROUP_TYPE_AND,
            ConstConditionClause::GROUP_TYPE_CONFIG_OR => self::SQL_CONFIG_PATTERN_GROUP_TYPE_OR
        );

        // Return result
        return $result;
    }



}