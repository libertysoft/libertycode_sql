<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\register\table\exception;

use Exception;

use liberty_code\register\register\exception\KeyInvalidFormatException as BaseKeyInvalidFormatException;
use liberty_code\sql\register\table\library\ConstTableRegister;



class KeyInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $key
     */
	public function __construct($key) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstTableRegister::EXCEPT_MSG_KEY_INVALID_FORMAT, strval($key));
	}





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified key has valid format.
     *
     * @param mixed $key
     * @param array $tabKey = null
     * @return boolean
     * @throws BaseKeyInvalidFormatException
     * @throws static
     */
    public static function setCheck($key, array $tabKey = null)
    {
        // Check key valid
        BaseKeyInvalidFormatException::setCheck($key);

        // Init var
        $result =
            // Check key in scope, if required
            (!is_array($tabKey)) ||
            in_array($key, $tabKey);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($key);
        }

        // Return result
        return $result;
    }



}