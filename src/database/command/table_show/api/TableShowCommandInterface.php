<?php
/**
 * Description :
 * This class allows to describe behavior of table show SQL command class.
 * Table show command allows to design string SQL table show command,
 * from following specified configuration:
 * [
 *     db_name(optional: got selected database if not found):"string database name"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\table_show\api;

use liberty_code\sql\database\command\api\CommandInterface;



interface TableShowCommandInterface extends CommandInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Set string database name config.
     *
     * @param string $strDbNm
     * @return static
     */
    public function setDbName($strDbNm);
}