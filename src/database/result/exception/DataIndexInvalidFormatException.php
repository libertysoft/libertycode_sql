<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\result\exception;

use liberty_code\sql\database\result\library\ConstResult;



class DataIndexInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $index
     */
	public function __construct($index)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
        $this->message = sprintf(ConstResult::EXCEPT_MSG_DATA_INDEX_INVALID_FORMAT, strval($index));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

	/**
	 * Check if specified index has valid format.
	 * 
     * @param mixed $index
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($index)
    {
		// Init var
		$result =
            // Check valid integer
            is_int($index) && ($index >= 0);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($index);
		}
		
		// Return result
		return $result;
    }
	
	
	
}