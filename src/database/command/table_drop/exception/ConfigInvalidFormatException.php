<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\table_drop\exception;

use liberty_code\sql\database\command\clause\library\ToolBoxTableClause;
use liberty_code\sql\database\command\table_drop\library\ConstTableDropCommand;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstTableDropCommand::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid table clause
            (
                (!isset($config[ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE])) ||
                ToolBoxTableClause::checkClauseTableIsValid($config[ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE])
            ) &&

            // Check valid table clause pattern
            (
                (!isset($config[ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE_PATTERN])) ||
                is_string($config[ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE_PATTERN])
            ) &&

            // Check table minimum requirement
            (
                isset($config[ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE])  ||
                isset($config[ConstTableDropCommand::TAB_CONFIG_KEY_CLAUSE_TABLE_PATTERN])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}