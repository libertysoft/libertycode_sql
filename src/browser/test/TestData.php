<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/test/HelpInsertTestTable.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\item_browser\operation\model\OperatorData;
use liberty_code\item_browser\browser\library\ConstBrowser as BaseConstBrowser;
use liberty_code\item_browser\browser\page\library\ConstPageBrowser;
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;
use liberty_code\sql\browser\library\ConstBrowser;
use liberty_code\sql\browser\data\library\ConstDataBrowser;
use liberty_code\sql\browser\data\model\DataBrowser;



// Init var
$tabConfig = array(
    BaseConstBrowser::TAB_CONFIG_KEY_QUERY => [
        'select' => [
            [
                'column_name' => $strColNmCnfType,
            ],
            [
                'column_name' => $strColNmCnfNm,
            ],
            [
                'column_name' => $strColNmCnfVal,
            ]
        ],
        'from' => [
            [
                'table_name' => $strTableNmCnf
            ]
        ],
        'where' => [
            'content' => [
                [
                    'operand' => $strColNmCnfType,
                    'operator' => 'equal',
                    'value' => ['value' => 'test-insert'],
                    'not' => 1
                ]
            ]
        ]
    ],
    ConstPageBrowser::TAB_CONFIG_KEY_ITEM_COUNT_PER_PAGE => 2,
    ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_ID => $strColNmCnfType,
    ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_KEY => $strColNmCnfNm,
    ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_VALUE => $strColNmCnfVal,
    /*
    ConstBrowser::TAB_CONFIG_KEY_QUERY_ITEM_COUNT => [
        'select' => [
            [
                'column_name' => $strColNmCnfType,
                'pattern' => '(COUNT(DISTINCT %1$s) - 1) AS count_item'
            ]
        ],
        'from' => [
            [
                'table_name' => $strTableNmCnf
            ]
        ],
        'where' => [
            'content' => [
                [
                    'operand' => $strColNmCnfType,
                    'operator' => 'equal',
                    'value' => ['value' => 'test-insert'],
                    'not' => 1
                ]
            ]
        ]
    ],
    //*/
    /*
    ConstDataBrowser::TAB_CONFIG_KEY_QUERY_PAGE => [
        'select' => [
            [
                'column_name' => $strColNmCnfType,
                'alias' => 'item'
            ]
        ],
        'from' => [
            [
                'table_name' => $strTableNmCnf
            ]
        ],
        'where' => [
            'content' => [
                [
                    'operand' => $strColNmCnfType,
                    'operator' => 'equal',
                    'value' => ['value' => 'test-insert'],
                    'not' => 1
                ]
            ]
        ],
        'group' => [
            'column_name' => $strColNmCnfType
        ]
    ]
    //*/
);

$objOperatorData = new OperatorData();
$objCommandFacto = new StandardCommandFactory($objConnection);
$objRegister = new MemoryRegister();
$objBrowser = new DataBrowser(
    $objOperatorData,
    $objCommandFacto,
    $objRegister,
    $tabConfig,
    array(),
    array()
);

$tabConfigCriteria = array(
    $strColNmCnfNm . '_1' => [
        ConstBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
            'content' => [
                [
                    'operand' => $strColNmCnfType,
                    'operator' => 'in',
                    'value' => [
                        'pattern' => $objCommandFacto->getObjSelectCommand(array(
                            'select' => [['table_name' => 'cnf_1', 'column_name' => $strColNmCnfType]],
                            'from' => [['table_name' => $strTableNmCnf, 'alias' => 'cnf_1']],
                            'where' => ['content' => [
                                [
                                    'operand' => ['table_name' => 'cnf_1', 'column_name' => $strColNmCnfNm],
                                    'operator' => 'equal',
                                    'value' => ['value' => 'key-1']
                                ],
                                [
                                    'operand' => ['table_name' => 'cnf_1', 'column_name' => $strColNmCnfVal],
                                    'operator' => 'like',
                                    'value' => ['value' => '%' . ConstBrowser::OPERATION_TAG_VALUE . '%']
                                ]
                            ]]
                        ))->getStrCommand()
                    ]
                ]
            ]
        ],
        ConstBrowser::TAB_OPERATION_CONFIG_KEY_DEFAULT_VALUE => '_'
    ],
    $strColNmCnfNm . '_3' => [
        ConstBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
            'content' => [
                [
                    'operand' => $strColNmCnfType,
                    'operator' => 'in',
                    'value' => [
                        'pattern' => $objCommandFacto->getObjSelectCommand(array(
                            'select' => [['table_name' => 'cnf_2', 'column_name' => $strColNmCnfType]],
                            'from' => [['table_name' => $strTableNmCnf, 'alias' => 'cnf_2']],
                            'where' => ['content' => [
                                [
                                    'operand' => ['table_name' => 'cnf_2', 'column_name' => $strColNmCnfNm],
                                    'operator' => 'equal',
                                    'value' => ['value' => 'key-3']
                                ],
                                [
                                    'operand' => ['table_name' => 'cnf_2', 'column_name' => $strColNmCnfVal],
                                    'operator' => 'equal',
                                    'value' => ['value' => ConstBrowser::OPERATION_TAG_VALUE]
                                ]
                            ]]
                        ))->getStrCommand()
                    ]
                ]
            ]
        ]
    ]
);

$tabConfigSort = array(
    $strColNmCnfType => [
        ConstBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
            'operand' => $strColNmCnfType,
            'operator' => ConstBrowser::OPERATION_TAG_VALUE
        ]
    ],
    $strColNmCnfDateCreate => [
        ConstBrowser::TAB_OPERATION_CONFIG_KEY_CLAUSE => [
            'operand' => $strColNmCnfDateCreate,
            'operator' => ConstBrowser::OPERATION_TAG_VALUE
        ]
    ]
);

$objBrowser->hydrateCriteriaConfig($tabConfigCriteria);
$objBrowser->hydrateSortConfig($tabConfigSort);



// Insert example data on config table
$tabData = array(
    1 => [
        'key-1' => 'Value B',
        'key-2' => 1,
        'key-3' => true
    ],
    2 => [
        'key-1' => 'Value B',
        'key-2' => 2,
        'key-3' => true
    ],
    3 => [
        'key-1' => 'Value A',
        'key-2' => 3,
        'key-3' => false
    ],
    4 => [
        'key-1' => 'Value A',
        'key-2' => 4,
        'key-3' => true
    ],
    5 => [
        'key-1' => 'Value B',
        'key-2' => 5,
        'key-3' => false
    ],
    6 => [
        'key-1' => 'Value B',
        'key-2' => 6,
        'key-3' => false
    ],
    7 => [
        'key-1' => 'Value A',
        'key-2' => 7,
        'key-3' => true
    ],
);

$strSql =
    "INSERT INTO " . $objConnection->getStrEscapeName($strTableNmCnf) . " (
        " . $objConnection->getStrEscapeName($strColNmCnfType) . ", 
        " . $objConnection->getStrEscapeName($strColNmCnfNm) . ", 
        " . $objConnection->getStrEscapeName($strColNmCnfVal) . ", 
        " . $objConnection->getStrEscapeName($strColNmCnfDateCreate) . ", 
        " . $objConnection->getStrEscapeName($strColNmCnfDateUpdate) . "
    ) VALUES (
        :Type, 
        :Key, 
        :Value, 
        NOW(), 
        NOW()
    );";
$objStatement = $objConnection->getObjStatement($strSql);

$objConnection->transactionStart();
try
{
    foreach($tabData as $intId => $data)
    {
        foreach($data as $strKey => $value)
        {
            $objStatement->execute(array(
                'Type' => $intId,
                'Key' => $strKey,
                'Value' => $value
            ));
        }
    }
    $objConnection->transactionEndCommit();
}
catch(\Exception $e)
{
    $objConnection->transactionEndRollback();
}



// Test get items
$tabConfig = array(
    [
        [
            $strColNmCnfNm . '_1' => 'A'
        ],
        [
            $strColNmCnfType => 'desc'
        ],
        -1
    ], // Ko: bad format on active page index
    [
        [
            $strColNmCnfNm . '_1' => 'A'
        ],
        [
            $strColNmCnfType => 'desc'
        ],
        0
    ], // Ok
    [
        [],
        [
            $strColNmCnfType => 'desc'
        ],
        1
    ], // Ok
    [
        [
            $strColNmCnfNm . '_3' => '0'
        ],
        [
            $strColNmCnfType => 'desc',
            $strColNmCnfDateCreate => 'asc'
        ],
        1
    ], // Ok
    [
        [
            $strColNmCnfNm . '_1' => 'B',
            $strColNmCnfNm . '_3' => true
        ],
        [
            $strColNmCnfType => 'asc',
            $strColNmCnfDateCreate => 'asc'
        ],
        0
    ], // Ok
    [
        [],
        [],
        2
    ], // Ok
    [
        [],
        [],
        4
    ] // Ko: active page index out of bounds
);

foreach($tabConfig as $config)
{
    echo('Test get items: <br />');
    try{
        $tabValueCriteria = $config[0];
        $tabValueSort = $config[1];
        $intActivePageIndex = $config[2];

        $objBrowser->hydrateCriteriaValue($tabValueCriteria);
        $objBrowser->hydrateSortValue($tabValueSort);
        $objBrowser->setActivePageIndex($intActivePageIndex);

        echo('Get: SQL: <pre>');var_dump($objBrowser->getObjCommand()->getStrCommand());echo('</pre>');
        echo('Get: Item count: <pre>');var_dump($objBrowser->getIntItemCount());echo('</pre>');
        echo('Get: Item count per page: <pre>');var_dump($objBrowser->getIntItemCountPerPage());echo('</pre>');
        echo('Get: Item count on active page: <pre>');var_dump($objBrowser->getIntItemCountOnActivePage());echo('</pre>');
        echo('Get: Page count: <pre>');var_dump($objBrowser->getIntPageCount());echo('</pre>');
        echo('Get: Active page index: <pre>');var_dump($objBrowser->getIntActivePageIndex());echo('</pre>');

        echo('Get: Items: <pre>');var_dump($objBrowser->getTabItem());echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get cache register items
$tabCacheItem = $objBrowser->getObjCacheRegister()->getTabItem();
echo('Cache register items: <pre>');var_dump($tabCacheItem);echo('</pre>');

echo('<br /><br /><br />');



// Remove test database, if required
$_GET['clean'] = '1';
require_once($strRootAppPath . '/src/database/test/HelpRemoveTestDb.php');


