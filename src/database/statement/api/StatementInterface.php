<?php
/**
 * Description :
 * This class allows to describe behavior of SQL database statement class.
 * Statement allows to design prepared query,
 * which execute query from specified query parameters,
 * from specific SQL data storage.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\statement\api;

use liberty_code\sql\database\result\api\ResultInterface;



interface StatementInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command.
     *
     * @return string
     */
    public function getStrCommand();



    /**
     * Get command parameters array.
     *
     * @return array
     */
    public function getTabParam();





    // Methods setters
    // ******************************************************************************

    /**
     * Set command parameters array.
     *
     * @param array $tabParam
     */
    public function setParam(array $tabParam);





    // Methods execute
    // ******************************************************************************

    /**
     * Execute statement,
     * using command parameters already set or provided.
     * Return false if doesn't run, true or the number of row affected else, if possible.
     * Correct select command will return true or 0 (no affected rows).
     *
     * @param array $tabParam = array()
     * @return boolean|integer
     */
    public function execute(array $tabParam = array());



    /**
     * Execute statement with data returns,
     * using command parameters already set or provided.
     * Return object ResultInterface if run correctly, false else.
     * Correct no-select command return empty ResultInterface.
     *
     * @param array $tabParam = array()
     * @return boolean|ResultInterface
     */
    public function executeResult(array $tabParam = array());



}