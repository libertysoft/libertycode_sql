<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;



class ConstLimitClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_COUNT = 'count';
    const TAB_CONFIG_KEY_START = 'start';
    const TAB_CONFIG_KEY_PATTERN = 'pattern';
}