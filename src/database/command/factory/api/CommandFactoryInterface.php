<?php
/**
 * Description :
 * This class allows to describe behavior of SQL database command factory class.
 * Command factory allows to provide new SQL commands,
 * from specified connection.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\factory\api;

use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\database_use\api\DbUseCommandInterface;
use liberty_code\sql\database\command\database_show\api\DbShowCommandInterface;
use liberty_code\sql\database\command\database_create\api\DbCreateCommandInterface;
use liberty_code\sql\database\command\database_alter\api\DbAlterCommandInterface;
use liberty_code\sql\database\command\database_drop\api\DbDropCommandInterface;
use liberty_code\sql\database\command\table_show\api\TableShowCommandInterface;
use liberty_code\sql\database\command\table_drop\api\TableDropCommandInterface;
use liberty_code\sql\database\command\select\api\SelectCommandInterface;
use liberty_code\sql\database\command\insert\api\InsertCommandInterface;
use liberty_code\sql\database\command\update\api\UpdateCommandInterface;
use liberty_code\sql\database\command\delete\api\DeleteCommandInterface;



interface CommandFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get connection object.
     *
     * @return ConnectionInterface
     */
    public function getObjConnection();



    /**
     * Get new object instance use database SQL command.
     *
     * @param array $tabConfig = null
     * @return DbUseCommandInterface
     */
    public function getObjDbUseCommand(array $tabConfig = null);



    /**
     * Get new object instance show database SQL command.
     *
     * @param array $tabConfig = null
     * @return DbShowCommandInterface
     */
    public function getObjDbShowCommand(array $tabConfig = null);



    /**
     * Get new object instance create database SQL command.
     *
     * @param array $tabConfig = null
     * @return DbCreateCommandInterface
     */
    public function getObjDbCreateCommand(array $tabConfig = null);



    /**
     * Get new object instance alter database SQL command.
     *
     * @param array $tabConfig = null
     * @return DbAlterCommandInterface
     */
    public function getObjDbAlterCommand(array $tabConfig = null);



    /**
     * Get new object instance drop database SQL command.
     *
     * @param array $tabConfig = null
     * @return DbDropCommandInterface
     */
    public function getObjDbDropCommand(array $tabConfig = null);



    /**
     * Get new object instance show table SQL command.
     *
     * @param array $tabConfig = null
     * @return TableShowCommandInterface
     */
    public function getObjTableShowCommand(array $tabConfig = null);



    /**
     * Get new object instance drop table SQL command.
     *
     * @param array $tabConfig = null
     * @return TableDropCommandInterface
     */
    public function getObjTableDropCommand(array $tabConfig = null);



    /**
     * Get new object instance select SQL command.
     *
     * @param array $tabConfig = null
     * @return SelectCommandInterface
     */
    public function getObjSelectCommand(array $tabConfig = null);



    /**
     * Get new object instance insert SQL command.
     *
     * @param array $tabConfig = null
     * @return InsertCommandInterface
     */
    public function getObjInsertCommand(array $tabConfig = null);



    /**
     * Get new object instance update SQL command.
     *
     * @param array $tabConfig = null
     * @return UpdateCommandInterface
     */
    public function getObjUpdateCommand(array $tabConfig = null);



    /**
     * Get new object instance delete SQL command.
     *
     * @param array $tabConfig = null
     * @return DeleteCommandInterface
     */
    public function getObjDeleteCommand(array $tabConfig = null);





    // Methods setters
    // ******************************************************************************

    /**
     * Set connection object.
     *
     * @param ConnectionInterface $objConnection
     */
    public function setConnection(ConnectionInterface $objConnection);



}