<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\clause\library\ToolBoxValueClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardExpressionClause;
use liberty_code\sql\database\command\clause\standard\library\ConstStandardValueClause;



class ToolBoxStandardValueClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for value clause.
     * Configuration array format: @see ToolBoxValueClause::checkClauseValueIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param array $tabConfig
     * @return string
     */
    public static function getStrClauseValueCommand(ConnectionInterface $objConnection, array $tabConfig)
    {
        // Init var
        $result = '';

        // Run each sub-config
        foreach($tabConfig as $config)
        {
            $strValueCommand = sprintf(
                ConstStandardValueClause::SQL_CONFIG_PATTERN_MAIN,
                ToolBoxStandardExpressionClause::getStrClauseExpressionCommand($objConnection, $config)
            );

            // Get sub-config string command
            $result =
                (trim($result) == '') ?
                    $strValueCommand :
                    sprintf(
                        ConstStandardValueClause::SQL_CONFIG_PATTERN_LINK,
                        $result,
                        $strValueCommand
                    );
        }

        // Return result
        return $result;
    }



}