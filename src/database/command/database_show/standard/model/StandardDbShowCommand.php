<?php
/**
 * Description :
 * This class allows to define standard database show SQL command class.
 * Standard database show command allows to design string SQL database show command,
 * from default configuration,
 * and uses standard SQL.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_show\standard\model;

use liberty_code\sql\database\command\database_show\model\DefaultDbShowCommand;

use liberty_code\sql\database\command\database_show\exception\ConfigInvalidFormatException;
use liberty_code\sql\database\command\database_show\standard\library\ConstStandardDbShowCommand;



class StandardDbShowCommand extends DefaultDbShowCommand
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrCommand()
    {
        // Return result
        return ConstStandardDbShowCommand::SQL_CONFIG_PATTERN_SHOW;
    }



}