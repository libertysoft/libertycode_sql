<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\command\clause\library\ConstLimitClause;



class ToolBoxLimitClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

    /**
     * Check if limit clause is valid.
     * Limit configuration format:
     * integer row count
     *
     * OR
     *
     * {
     *     count(required: if pattern not found): integer row count,
     *
     *     start(optional): integer row index start,
     *
     *     pattern(required: if count not found):
     *         "string sprintf pattern,
     *         where '%1$s' or '%s' replaced by calculated limit, if found"
     * }
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkClauseLimitIsValid($config)
    {
        // Init var
        $result =
            // Case simple config
            (
                // Check valid row count
                is_int($config) &&
                ($config >= 0)
            ) ||
            // Case complex config
            (
                // Check valid array
                is_array($config) &&

                // Check valid row count
                (
                    (!isset($config[ConstLimitClause::TAB_CONFIG_KEY_COUNT])) ||
                    (
                        is_int($config[ConstLimitClause::TAB_CONFIG_KEY_COUNT]) &&
                        ($config[ConstLimitClause::TAB_CONFIG_KEY_COUNT] >= 0)
                    )
                ) &&

                // Check valid row index start
                (
                    (!isset($config[ConstLimitClause::TAB_CONFIG_KEY_START])) ||
                    (
                        is_int($config[ConstLimitClause::TAB_CONFIG_KEY_START]) &&
                        ($config[ConstLimitClause::TAB_CONFIG_KEY_START] >= 0)
                    )
                ) &&

                // Check valid pattern
                (
                    (!isset($config[ConstLimitClause::TAB_CONFIG_KEY_PATTERN])) ||
                    (
                        is_string($config[ConstLimitClause::TAB_CONFIG_KEY_PATTERN]) &&
                        (trim($config[ConstLimitClause::TAB_CONFIG_KEY_PATTERN]) != '')
                    )
                ) &&

                // Check minimum requirement
                (
                    isset($config[ConstLimitClause::TAB_CONFIG_KEY_COUNT]) ||
                    isset($config[ConstLimitClause::TAB_CONFIG_KEY_PATTERN])
                )
            );

        // Return result
        return $result;
    }



}