<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\delete\exception;

use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxConditionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxOrderClause;
use liberty_code\sql\database\command\clause\library\ToolBoxLimitClause;
use liberty_code\sql\database\command\delete\library\ConstDeleteCommand;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDeleteCommand::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid delete clause
            (
                (!isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_DELETE])) ||
                ToolBoxExpressionClause::checkClauseExpressionIsValid($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_DELETE])
            ) &&

            // Check valid delete clause pattern
            (
                (!isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_DELETE_PATTERN])) ||
                is_string($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_DELETE_PATTERN])
            ) &&

            // Check valid from clause
            (
                (!isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM])) ||
                ToolBoxFromClause::checkClauseFromIsValid($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM])
            ) &&

            // Check valid from clause pattern
            (
                (!isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN])) ||
                is_string($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN])
            ) &&

            // Check from minimum requirement
            (
                isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM])  ||
                isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN])
            ) &&

            // Check valid where clause
            (
                (!isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE])) ||
                ToolBoxConditionClause::checkClauseConditionIsValid($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE])
            ) &&

            // Check valid where clause pattern
            (
                (!isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN])) ||
                is_string($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN])
            ) &&

            // Check valid order clause
            (
                (!isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_ORDER])) ||
                ToolBoxOrderClause::checkClauseOrderIsValid($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_ORDER])
            ) &&

            // Check valid order clause pattern
            (
                (!isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_ORDER_PATTERN])) ||
                is_string($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_ORDER_PATTERN])
            ) &&

            // Check valid limit clause
            (
                (!isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT])) ||
                ToolBoxLimitClause::checkClauseLimitIsValid($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT])
            ) &&

            // Check valid limit clause pattern
            (
                (!isset($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN])) ||
                is_string($config[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}