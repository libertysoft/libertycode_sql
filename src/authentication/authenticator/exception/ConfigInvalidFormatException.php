<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\authentication\authenticator\exception;

use liberty_code\sql\authentication\authenticator\library\ConstSqlAuthenticator;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstSqlAuthenticator::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function($tabStr)
        {
            $result = is_array($tabStr) && (count($tabStr) > 0);

            // Check each value is valid
            for($intCpt = 0; $result && ($intCpt < count($tabStr)); $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init array of mapping data check function
        $checkTabConfigMapDataIsValid = function($tabConfigMapData, $boolMapId) use ($checkTabStrIsValid)
        {
            $result = is_array($tabConfigMapData);

            // Check each mapping data is valid, if required
            if($result)
            {
                $tabConfigMapData = array_values($tabConfigMapData);
                for($intCpt = 0; ($intCpt < count($tabConfigMapData)) && $result; $intCpt++)
                {
                    $configMapData = $tabConfigMapData[$intCpt];
                    $result =
                        // Check valid data key
                        isset($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_DATA_KEY]) &&
                        is_string($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_DATA_KEY]) &&
                        (trim($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_DATA_KEY]) != '') &&

                        // Check valid data format callable
                        (
                            (!isset($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE])) ||
                            is_callable($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE])
                        ) &&

                        // Check valid column name
                        (
                            (!isset($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME])) ||
                            (
                                (
                                    is_string($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) &&
                                    (trim($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) != '')
                                ) ||
                                (
                                    is_array($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME]) &&
                                    $checkTabStrIsValid($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_COLUMN_NAME])
                                )
                            )
                        );

                    $result =
                        $result &&
                        (
                            $boolMapId ?
                                // Case mapping identification data: additional check:
                                // Check valid operator like required option
                                (
                                    (!isset($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_OPERATOR_LIKE_REQUIRE])) ||
                                    (
                                        // Check is boolean or numeric (0 = false, 1 = true)
                                        is_bool($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_OPERATOR_LIKE_REQUIRE]) ||
                                        is_int($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_OPERATOR_LIKE_REQUIRE]) ||
                                        (
                                            is_string($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_OPERATOR_LIKE_REQUIRE]) &&
                                            ctype_digit($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_OPERATOR_LIKE_REQUIRE])
                                        )
                                    )
                                ) :
                                // Case else: mapping authentication data: additional check:
                                // Check valid check data callable
                                (
                                    (!isset($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_CHECK_DATA_CALLABLE])) ||
                                    is_callable($configMapData[ConstSqlAuthenticator::TAB_CONFIG_KEY_CHECK_DATA_CALLABLE])
                                )
                        );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid select command query
            isset($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_QUERY]) &&
            is_array($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_QUERY]) &&

            // Check valid mapping identification data
            (
                (!isset($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_MAPPING_ID_DATA])) ||
                $checkTabConfigMapDataIsValid($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_MAPPING_ID_DATA], true)
            ) &&

            // Check valid mapping authentication data
            (
                (!isset($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_MAPPING_AUTH_DATA])) ||
                $checkTabConfigMapDataIsValid($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_MAPPING_AUTH_DATA], false)
            ) &&

            // Check valid default identification data column name
            (
                (!isset($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_ID_DATA_COLUMN_NAME])) ||
                (
                    is_string($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_ID_DATA_COLUMN_NAME]) &&
                    (trim($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_ID_DATA_COLUMN_NAME]) != '')
                )
            ) &&

            // Check valid default authentication data column name
            (
                (!isset($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_AUTH_DATA_COLUMN_NAME])) ||
                (
                    is_string($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_AUTH_DATA_COLUMN_NAME]) &&
                    (trim($config[ConstSqlAuthenticator::TAB_CONFIG_KEY_DEFAULT_AUTH_DATA_COLUMN_NAME]) != '')
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}