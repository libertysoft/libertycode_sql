<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/clause/test/Include.php');

// Use
use liberty_code\sql\database\command\clause\library\ToolBoxTableClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardTableClause;



// Init var
$config_1 = array(
    'db_name' => 'db',
    'table_name' => 'table_1'
);

$config_2 = array(
    'db_name' => 'db'
);

$config_3 = array(
    'table_name' => 'table_2'
);

$config_4 = array(
    'table_name' => 7
);

$config_5 = 'table_3';

$config_6 = array(
    'db_name' => 'db',
    'pattern' => '%s`table_4`'
);

$config_7 = array(
    'pattern' => '`table_5`'
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3, // Ok
    $config_4, // Ko
    $config_5, // Ok
    $config_6, // Ok
    $config_7 // Ok
);



// Test config
process(
    $tabConfig, 
    function($config)
    {
        return ToolBoxTableClause::checkClauseTableIsValid($config);
    },
    function($config) use ($objConnection)
    {
        return ToolBoxStandardTableClause::getStrClauseTableCommand($objConnection, $config);
    }
);


