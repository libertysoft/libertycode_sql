<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\clause\library\ConstExpressionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardClause;
use liberty_code\sql\database\command\clause\standard\library\ConstStandardExpressionClause;



class ToolBoxStandardExpressionClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for expression.
     * Expression configuration format: @see ToolBoxExpressionClause::checkExpressionIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param string|array $config
     * @return string
     */
    public static function getStrExpressionCommand(ConnectionInterface $objConnection, $config)
    {
        // Init var
        $result = '';

        // Get column name or value
        if(is_string($config))
        {
            $result = $objConnection->getStrEscapeName($config);
        }
        else if(isset($config[ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME]))
        {
            $result = $objConnection->getStrEscapeName($config[ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME]);
        }
        else if(array_key_exists(ConstExpressionClause::TAB_CONFIG_KEY_VALUE, $config))
        {
            $result = $objConnection->getStrEscapeValue($config[ConstExpressionClause::TAB_CONFIG_KEY_VALUE]);
        }

        // Get table, if required
        if(isset($config[ConstExpressionClause::TAB_CONFIG_KEY_TABLE_NAME]))
        {
            $result = (
                (trim($result) != '') ?
                    sprintf(
                        ConstStandardExpressionClause::SQL_CONFIG_PATTERN_TABLE,
                        $objConnection->getStrEscapeName($config[ConstExpressionClause::TAB_CONFIG_KEY_TABLE_NAME]),
                        $result
                    ) :
                    $objConnection->getStrEscapeName($config[ConstExpressionClause::TAB_CONFIG_KEY_TABLE_NAME])
            );
        }

        // Get database, if required
        if(isset($config[ConstExpressionClause::TAB_CONFIG_KEY_DB_NAME]))
        {
            $result = (
                (trim($result) != '') ?
                    sprintf(
                        ConstStandardExpressionClause::SQL_CONFIG_PATTERN_DB,
                        $objConnection->getStrEscapeName($config[ConstExpressionClause::TAB_CONFIG_KEY_DB_NAME]),
                        $result
                    ) :
                    $objConnection->getStrEscapeName($config[ConstExpressionClause::TAB_CONFIG_KEY_DB_NAME])
            );
        }

        // Get alias, if required
        if(isset($config[ConstExpressionClause::TAB_CONFIG_KEY_ALIAS]))
        {
            $result = (
                (trim($result) != '') ?
                    sprintf(
                        ConstStandardExpressionClause::SQL_CONFIG_PATTERN_ALIAS,
                        $result,
                        $objConnection->getStrEscapeName($config[ConstExpressionClause::TAB_CONFIG_KEY_ALIAS])
                    ) :
                    $objConnection->getStrEscapeName($config[ConstExpressionClause::TAB_CONFIG_KEY_ALIAS])
            );
        }

        // Get pattern value, if required
        if(isset($config[ConstExpressionClause::TAB_CONFIG_KEY_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue($config[ConstExpressionClause::TAB_CONFIG_KEY_PATTERN], $result);
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for expression clause.
     * Configuration array format: @see ToolBoxExpressionClause::checkClauseExpressionIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param array $tabConfig
     * @return string
     */
    public static function getStrClauseExpressionCommand(ConnectionInterface $objConnection, array $tabConfig)
    {
        // Init var
        $result = '';

        // Run each sub-config
        foreach($tabConfig as $config)
        {
            // Get sub-config string command
            $result =
                (trim($result) == '') ?
                    static::getStrExpressionCommand($objConnection, $config) :
                    sprintf(
                        ConstStandardExpressionClause::SQL_CONFIG_PATTERN_LINK,
                        $result,
                        static::getStrExpressionCommand($objConnection, $config)
                    );
        }

        // Return result
        return $result;
    }



}