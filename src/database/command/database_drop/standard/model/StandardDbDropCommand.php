<?php
/**
 * Description :
 * This class allows to define standard database drop SQL command class.
 * Standard database drop command allows to design string SQL database drop command,
 * from default configuration,
 * and uses standard SQL.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_drop\standard\model;

use liberty_code\sql\database\command\database_drop\model\DefaultDbDropCommand;

use liberty_code\sql\database\command\database_drop\library\ConstDbDropCommand;
use liberty_code\sql\database\command\database_drop\exception\ConfigInvalidFormatException;
use liberty_code\sql\database\command\database_drop\standard\library\ConstStandardDbDropCommand;



class StandardDbDropCommand extends DefaultDbDropCommand
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Build SQL command
        $result = sprintf(
            ConstStandardDbDropCommand::SQL_CONFIG_PATTERN_DROP,
            $objConnection->getStrEscapeName($tabConfig[ConstDbDropCommand::TAB_CONFIG_KEY_DB_NAME])
        );

        // Return result
        return $result;
    }



}