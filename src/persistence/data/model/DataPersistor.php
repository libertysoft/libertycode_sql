<?php
/**
 * Description :
 * This class allows to define data SQL persistor class.
 * Attribute values are stored in table like:
 * Specific column name = list of attribute names - Specific column name = list of attribute values
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\persistence\data\model;

use liberty_code\sql\persistence\model\DefaultPersistor;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\library\datetime\library\ToolBoxDateTime;
use liberty_code\sql\database\command\clause\library\ConstExpressionClause;
use liberty_code\sql\database\command\clause\library\ConstConditionClause;
use liberty_code\sql\database\command\clause\library\ConstFromClause;
use liberty_code\sql\database\command\clause\library\ConstOrderClause;
use liberty_code\sql\database\command\clause\library\ConstSetClause;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;
use liberty_code\sql\database\command\insert\library\ConstInsertCommand;
use liberty_code\sql\database\command\update\library\ConstUpdateCommand;
use liberty_code\sql\database\command\delete\library\ConstDeleteCommand;
use liberty_code\sql\persistence\library\ConstPersistor;
use liberty_code\sql\persistence\data\library\ConstDataPersistor;
use liberty_code\sql\persistence\data\library\ToolBoxDataPersistor;
use liberty_code\sql\persistence\data\exception\ActionConfigInvalidFormatException;



class DataPersistor extends DefaultPersistor
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Last new id calculated
     * @var null|string
     */
    static protected $__lastIdNew = null;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getIdNew(array $tabConfig = null)
    {
        // Get new id
        do
        {
            $result = ToolBoxDateTime::getStrTimeNow();
        }
        while(
            (!is_null(static::$__lastIdNew)) &&
            (static::$__lastIdNew == $result)
        );

        // Register new id
        static::$__lastIdNew = $result;

        // Return result
        return $result;
    }





    // Methods getters (Persistor interface implementation)
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     @see getData() configuration array format,
     *
     *     column_name_attribute_name(required): "string column name where attribute names stored",
     *
     *     column_name_attribute_value(required): "string column name where attribute values stored",
     *
     *     attribute_name_select(optional: considered select all attributes table, if attribute_name_select not found)): [
     *         'string attribute name 1 to select',
     *         ...,
     *         'string attribute name N to select'
     *     ]
     * ]
     *
     * @inheritdoc
     */
    public function getTabData(
        array $tabId = array(),
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Check arguments
        ActionConfigInvalidFormatException::setCheck(ConstPersistor::TAB_ACTION_KEY_GET, $tabConfig);

        // Init var
        $result = false;
        $objCommandFactory = $this->getObjCommandFactory();

        // Check empty data return required
        if(
            array_key_exists(ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID, $tabConfig) &&
            (count($tabId) == 0)
        )
        {
            $result = array();
        }
        // Else: process selection if command factory valid
        else if(!is_null($objCommandFactory))
        {
            // Get info
            $tabCmdConfig = array();
            $objConnection = $this->getObjCommandFactory()->getObjConnection();
            $strColNmAttrNm = $tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_NAME];
            $strColNmAttrValue = $tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_VALUE];
            $strColNmId = ToolBoxTable::getItem(
                $tabConfig,
                ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID,
                null
            );

            // Set select clause
            $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT] = array();
            if(!is_null($strColNmId))
            {
                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT][] = array(
                    ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME => $strColNmId
                );
            }
            $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT][] = array(
                ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME => $strColNmAttrNm
            );
            $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT][] = array(
                ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME => $strColNmAttrValue
            );

            // Set from clause
            $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME]]
            );

            // Set attribute selection scope, if required
            if(isset($tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SELECT]))
            {
                $tabEscapeAttrNm = array_map(
                    function($value) use($objConnection) {
                        return $objConnection->getStrEscapeValue($value);
                    },
                    $tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SELECT]
                );
                $strListAttrNm = implode(', ', $tabEscapeAttrNm);

                // Set where clause
                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                        [
                            ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmAttrNm,
                            ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_IN,
                            ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                                ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $strListAttrNm
                            ]
                        ]
                    ]
                );
            }

            // Set id info, if required
            if(
                (!is_null($strColNmId)) &&
                (count($tabId) > 0)
            )
            {
                $tabEscapeId = array_map(
                    function($value) use($objConnection) {
                        return $objConnection->getStrEscapeValue($value);
                    },
                    $tabId
                );
                $strListId = implode(', ', $tabEscapeId);

                // Init where clause, if required
                if(!isset(
                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                    [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]
                ))
                {
                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => []
                    );
                }

                // Set where clause
                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT][] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmId,
                    ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_IN,
                    ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                        ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $strListId
                    ]
                );

                // Set order clause
                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER] = array(
                    [
                        ConstOrderClause::TAB_CONFIG_KEY_OPERAND => [
                            ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => sprintf(
                                'FIELD(%1$s, %2$s)',
                                $objConnection->getStrEscapeName($strColNmId),
                                $strListId
                            )
                        ],
                        ConstOrderClause::TAB_CONFIG_KEY_OPERATOR => ConstOrderClause::OPERATOR_CONFIG_ASC
                    ]
                );
            }

            // Set pattern
            if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE, $tabConfig))
            {
                // Init where clause, if required
                if(!isset(
                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                    [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]
                ))
                {
                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => []
                    );
                }

                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT][] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_PATTERN =>
                        $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE]
                );
            }

            if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_ORDER, $tabConfig))
            {
                // Init where clause, if required
                if(!isset($tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER]))
                {
                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER] = array();
                }

                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER][] = array(
                    ConstOrderClause::TAB_CONFIG_KEY_PATTERN =>
                        $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_ORDER]
                );
            }

            // Get command
            $objCommand = $objCommandFactory->getObjSelectCommand($tabCmdConfig);

            // Run query result
            if(($resultQuery = $objCommand->executeResult()) !== false)
            {
                $result = array();
                while(($data = $resultQuery->getFetchData()) !== false)
                {
                    $result[] = $data;
                }
                $resultQuery->close();

                // Format data
                if(count($result) > 0)
                {
                    $result = ToolBoxDataPersistor::getTabDataFormatResult(
                        $result,
                        $strColNmAttrNm,
                        $strColNmAttrValue,
                        $strColNmId
                    );
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     column_name_attribute_name(required): "string column name where attribute names stored",
     *
     *     column_name_attribute_value(required): "string column name where attribute values stored"
     * ]
     *
     * @inheritdoc
     */
    public function getTabSearchData(
        $query,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Check arguments
        ActionConfigInvalidFormatException::setCheck(ConstPersistor::TAB_ACTION_KEY_SEARCH, $tabConfig);

        // Init var
        $result = parent::getTabSearchData($query, $tabConfig);

        // Get info
        $strColNmAttrNm = $tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_NAME];
        $strColNmAttrValue = $tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_VALUE];
        $strColNmId = ToolBoxTable::getItem(
            $tabConfig,
            ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID,
            null
        );

        // Format data, if required
        if(
            (is_array($result)) && (count($result) > 0) &&
            isset($result[0][$strColNmAttrNm]) &&
            isset($result[0][$strColNmAttrValue]) &&
            (
                is_null($strColNmId) ||
                isset($result[0][$strColNmId])
            )
        )
        {
            $result = ToolBoxDataPersistor::getTabDataFormatResult(
                $result,
                $strColNmAttrNm,
                $strColNmAttrValue,
                $strColNmId
            );
        }

        // Return result
        return $result;
    }





    // Methods setters (Persistor interface implementation)
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     @see createData() configuration array format,
     *
     *     column_name_attribute_name(required): "string column name where attribute names stored",
     *
     *     column_name_attribute_value(required): "string column name where attribute values stored",
     *
     *     opt_id_new(optional: got 'persistor' if opt_id_new not found): "string option ID new calculation: persistor | connection"
     * ]
     *
     *  Note:
     * -> Configuration opt_id_new (option ID new calculation):
     *     @see createData() .
     *     If opt_id_new = persistor and @see getIdNew() return null,
     *     try to use connection feature to get new id.
     *
     * @inheritdoc
     */
    public function createTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabId = null,
        array &$tabInfo = null
    )
    {
        // Check arguments
        ActionConfigInvalidFormatException::setCheck(ConstPersistor::TAB_ACTION_KEY_CREATE, $tabConfig);

        // Init var
        $result = true;
        $tabCreateData = $tabData;

        // Check data found
        if(count($tabCreateData) > 0)
        {
            // Init var
            $result = false;
            $objCommandFactory = $this->getObjCommandFactory();

            // Check command factory valid
            if(!is_null($objCommandFactory))
            {
                // Get info
                $tabCmdConfig = array();
                $objConnection = $this->getObjCommandFactory()->getObjConnection();
                $strColNmAttrNm = $tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_NAME];
                $strColNmAttrValue = $tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_VALUE];
                $strColNmId = ToolBoxTable::getItem(
                    $tabConfig,
                    ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID,
                    null
                );
                $strOptIdNew = ToolBoxTable::getItem(
                    $tabConfig,
                    ConstPersistor::TAB_CONFIG_KEY_OPT_ID_NEW,
                    ConstPersistor::TAB_CONFIG_OPT_ID_NEW_PERSISTOR
                );

                // Init set index array of command config value function
                $setTabCmdConfigValue = function(array $data, array &$tabCmdConfig) use ($tabConfig, $strColNmId)
                {
                    foreach($data as $strKey => $value)
                    {
                        if(is_null($strColNmId) || ($strColNmId != $strKey))
                        {
                            $tabCmdConfigValue = array();

                            // Set value clause
                            if((!is_null($strColNmId)) && array_key_exists($strColNmId, $data))
                            {
                                $tabCmdConfigValue[] = array(
                                    ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $data[$strColNmId]
                                );
                            }
                            $tabCmdConfigValue[] = array(ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $strKey);
                            $tabCmdConfigValue[] = array(ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $value);

                            // Set pattern
                            if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_VALUE, $tabConfig))
                            {
                                $tabCmdConfigValue[] = array(
                                    ConstExpressionClause::TAB_CONFIG_KEY_PATTERN =>
                                        $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_VALUE]
                                );
                            }

                            $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE][] = $tabCmdConfigValue;
                        }
                    }
                };

                // Set from clause
                $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                    [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME]]
                );

                // Set column clause
                $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN] = array();
                if(!is_null($strColNmId)) $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN][] = $strColNmId;
                $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN][] = $strColNmAttrNm;
                $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN][] = $strColNmAttrValue;

                // Set pattern
                if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_COLUMN, $tabConfig))
                {
                    $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN][] = array(
                        ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_COLUMN]
                    );
                }

                // Set id from persistor, if required
                if(
                    (!is_null($strColNmId)) &&
                    ($strOptIdNew == ConstPersistor::TAB_CONFIG_OPT_ID_NEW_PERSISTOR)
                )
                {
                    // Run all data
                    for($intCpt = 0; $intCpt < count($tabCreateData); $intCpt++)
                    {
                        $idNew = $this->getIdNew();

                        // Register new id, if required
                        if(!is_null($idNew))
                        {
                            $tabCreateData[$intCpt][$strColNmId] = $idNew;
                            if(!is_null($tabId)) $tabId[] = $idNew;
                        }
                    }
                }

                // Case: single insert: return id required and not already provided
                if(
                    // ($strOptIdNew == ConstPersistor::TAB_CONFIG_OPT_ID_NEW_CONNECTION) &&
                    (is_array($tabId)) && (count($tabId) == 0)
                )
                {
                    if(count($tabCreateData) > 0)
                    {
                        // Run each data
                        $result = true;
                        $tabDataKey = array_keys($tabCreateData);
                        for($intCpt = 0; ($intCpt < count($tabDataKey)) && $result; $intCpt++)
                        {
                            $data = $tabCreateData[$tabDataKey[$intCpt]];

                            // Set value clause
                            $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE] = array();
                            $setTabCmdConfigValue($data, $tabCmdConfig);

                            // Get command
                            $objCommand = $objCommandFactory->getObjInsertCommand($tabCmdConfig);

                            // Run query
                            $result = ($objCommand->execute() !== false);

                            // Register new id, from connection, if required
                            if($result)
                            {
                                $idNew = $objConnection->getStrLastInsertId();
                                if(!is_null($idNew)) $tabId[] = $idNew;
                            }
                        }
                    }
                }
                // Case else: bulk insert
                else
                {
                    // Set value clause
                    $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE] = array();
                    foreach($tabCreateData as $data)
                    {
                        // Set value clause
                        $setTabCmdConfigValue($data, $tabCmdConfig);
                    }

                    // Get command
                    $objCommand = $objCommandFactory->getObjInsertCommand($tabCmdConfig);

                    // Run query
                    $result = ($objCommand->execute() !== false);
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see updateData() configuration array format,
     *
     *     column_name_attribute_name(required): "string column name where attribute names stored",
     *
     *     column_name_attribute_value(required): "string column name where attribute values stored"
     * ]
     *
     * @inheritdoc
     */
    public function updateTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Check arguments
        ActionConfigInvalidFormatException::setCheck(ConstPersistor::TAB_ACTION_KEY_UPDATE, $tabConfig);

        // Init var
        $result = true;

        // Check data found
        if(count($tabData) > 0)
        {
            // Init var
            $result = false;
            $objCommandFactory = $this->getObjCommandFactory();

            // Check command factory valid
            if(!is_null($objCommandFactory))
            {
                // Get info
                $tabCmdConfig = array();
                $strColNmAttrNm = $tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_NAME];
                $strColNmAttrValue = $tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_VALUE];
                $strColNmId = ToolBoxTable::getItem(
                    $tabConfig,
                    ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID,
                    null
                );

                // Set from clause
                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                    [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME]]
                );

                if(count($tabData) > 0)
                {
                    // Run each data
                    $result = true;
                    $tabDataKey = array_keys($tabData);
                    for($intCpt = 0; ($intCpt < count($tabDataKey)) && $result; $intCpt++)
                    {
                        $data = $tabData[$tabDataKey[$intCpt]];
                        $tabKey = array_keys($data);
                        for($intCpt2 = 0; ($intCpt2 < count($tabKey)) && $result; $intCpt2++)
                        {
                            $strKey = $tabKey[$intCpt2];
                            $value = $data[$strKey];

                            // Set set clause
                            $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET] = array(
                                [
                                    ConstSetClause::TAB_CONFIG_KEY_OPERAND => $strColNmAttrValue,
                                    ConstSetClause::TAB_CONFIG_KEY_VALUE => [
                                        ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $value
                                    ]
                                ]
                            );

                            // Set pattern
                            if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_SET, $tabConfig))
                            {
                                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET][] = array(
                                    ConstSetClause::TAB_CONFIG_KEY_PATTERN =>
                                        $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_SET]
                                );
                            }

                            // Set where clause
                            $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                                ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                                ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                                    [
                                        ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmAttrNm,
                                        ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_EQUAL,
                                        ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                                            ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $strKey
                                        ]
                                    ]
                                ]
                            );

                            // Set id info, if required
                            if((!is_null($strColNmId)) && array_key_exists($strColNmId, $data))
                            {
                                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                                [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT][] = array(
                                    ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmId,
                                    ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_EQUAL,
                                    ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                                        ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $data[$strColNmId]
                                    ]
                                );
                            }

                            // Set pattern
                            if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE, $tabConfig))
                            {
                                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                                [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT][] = array(
                                    ConstConditionClause::TAB_CONFIG_KEY_PATTERN =>
                                        $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE]
                                );
                            }

                            // Get command
                            $objCommand = $objCommandFactory->getObjUpdateCommand($tabCmdConfig);

                            // Run query
                            $result = ($objCommand->execute() !== false);
                        }
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see deleteData() configuration array format,
     *
     *     column_name_attribute_name(required only if attribute_name_select provided): "string column name where attribute names stored",
     *
     *     attribute_name_select(optional: considered select all attributes table, if attribute_name_select not found)): [
     *         'string attribute name 1 to select',
     *         ...,
     *         'string attribute name N to select'
     *     ]
     * ]
     *
     * @inheritdoc
     */
    public function deleteTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Check arguments
        ActionConfigInvalidFormatException::setCheck(ConstPersistor::TAB_ACTION_KEY_DELETE, $tabConfig);

        // Init var
        $result = true;

        // Check data found
        if(count($tabData) > 0)
        {
            // Init var
            $result = false;
            $objCommandFactory = $this->getObjCommandFactory();

            // Check command factory valid
            if(!is_null($objCommandFactory))
            {
                // Get info
                $tabCmdConfig = array();
                $objConnection = $this->getObjCommandFactory()->getObjConnection();

                // Set from clause
                $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                    [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstPersistor::TAB_CONFIG_KEY_TABLE_NAME]]
                );

                // Set attribute selection scope, if required
                if(isset($tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SELECT]))
                {
                    $strColNmAttrNm = $tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_NAME];
                    $tabEscapeAttrNm = array_map(
                        function($value) use($objConnection) {
                            return $objConnection->getStrEscapeValue($value);
                        },
                        $tabConfig[ConstDataPersistor::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SELECT]
                    );
                    $strListAttrNm = implode(', ', $tabEscapeAttrNm);

                    // Set where clause
                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                        ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                            [
                                ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmAttrNm,
                                ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_IN,
                                ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                                    ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $strListAttrNm
                                ]
                            ]
                        ]
                    );
                }

                // Set id info, if required
                if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID, $tabConfig))
                {
                    $strColNmId = $tabConfig[ConstPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ID];
                    $tabId = array();
                    foreach($tabData as $data)
                    {
                        if(array_key_exists($strColNmId, $data))
                        {
                            $tabId[] = $data[$strColNmId];
                        }
                    }
                    $tabEscapeId = array_map(
                        function($value) use($objConnection) {
                            return $objConnection->getStrEscapeValue($value);
                        },
                        $tabId
                    );
                    $strListId = implode(', ', $tabEscapeId);

                    // Init where clause, if required
                    if(!isset(
                        $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                        [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]
                    ))
                    {
                        $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                            ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                            ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => []
                        );
                    }

                    // Set where clause
                    $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                    [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT][] = array(
                        ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmId,
                        ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_IN,
                        ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                            ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $strListId
                        ]
                    );
                }

                // Set pattern
                if(array_key_exists(ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE, $tabConfig))
                {
                    // Init where clause, if required
                    if(!isset(
                        $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                        [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT]
                    ))
                    {
                        $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                            ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                            ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => []
                        );
                    }

                    $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                    [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT][] = array(
                        ConstConditionClause::TAB_CONFIG_KEY_PATTERN =>
                            $tabConfig[ConstPersistor::TAB_CONFIG_KEY_PATTERN_WHERE]
                    );
                }

                // Get command
                $objCommand = $objCommandFactory->getObjDeleteCommand($tabCmdConfig);

                // Run query
                $result = ($objCommand->execute() !== false);
            }
        }

        // Return result
        return $result;
    }



}