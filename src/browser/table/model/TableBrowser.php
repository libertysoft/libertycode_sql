<?php
/**
 * Description :
 * This class allows to define table SQL browser class.
 * Table SQL browser is default SQL browser,
 * where items are stored like:
 * Each column name = item key - Row = item value
 *
 * Table SQL browser uses the following specified browsing configuration:
 * [
 *     Default SQL browser configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\browser\table\model;

use liberty_code\sql\browser\model\DefaultBrowser;

use liberty_code\sql\database\command\clause\library\ConstExpressionClause;
use liberty_code\sql\database\command\clause\library\ConstLimitClause;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;
use liberty_code\sql\browser\table\library\ConstTableBrowser;



class TableBrowser extends DefaultBrowser
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getQueryItemCountEngine()
    {
        // Init var
        $result = $this->getQuery();

        // Determine query for total count of item
        if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT, $result))
        {
            unset($result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT]);
        }
        if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN, $result))
        {
            unset($result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN]);
        }
        if(array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT, $result))
        {
            unset($result[ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT]);
        }
        $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT] = array(
            [
                ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => ConstTableBrowser::ITEM_COUNT_EXPRESSION_PATTERN
            ]
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getQueryCalculatePage(array $query)
    {
        // Init var
        $result = $query;

        // Set limit clause
        $result[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT] = array(
            ConstLimitClause::TAB_CONFIG_KEY_COUNT => $this->getIntItemCountOnActivePage(),
            ConstLimitClause::TAB_CONFIG_KEY_START => ($this->getIntActivePageIndex() * $this->getIntItemCountPerPage())
        );

        // Return result
        return $result;
    }



}