<?php
/**
 * Description :
 * This class allows to define standard SQL command factory class.
 * Standard command factory allows to provide new standard SQL commands,
 * from specified connection.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\factory\standard\model;

use liberty_code\sql\database\command\factory\model\DefaultCommandFactory;

use liberty_code\sql\database\command\database_use\standard\model\StandardDbUseCommand;
use liberty_code\sql\database\command\database_show\standard\model\StandardDbShowCommand;
use liberty_code\sql\database\command\database_create\standard\model\StandardDbCreateCommand;
use liberty_code\sql\database\command\database_alter\standard\model\StandardDbAlterCommand;
use liberty_code\sql\database\command\database_drop\standard\model\StandardDbDropCommand;
use liberty_code\sql\database\command\table_show\standard\model\StandardTableShowCommand;
use liberty_code\sql\database\command\table_drop\standard\model\StandardTableDropCommand;
use liberty_code\sql\database\command\select\standard\model\StandardSelectCommand;
use liberty_code\sql\database\command\insert\standard\model\StandardInsertCommand;
use liberty_code\sql\database\command\update\standard\model\StandardUpdateCommand;
use liberty_code\sql\database\command\delete\standard\model\StandardDeleteCommand;



class StandardCommandFactory extends DefaultCommandFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @return StandardDbUseCommand
     */
    public function getObjDbUseCommand(array $tabConfig = null)
    {
        // Init var
        /** @var StandardDbUseCommand $result */
        $result = $this->getObjCommandEngine(
            StandardDbUseCommand::class,
            function () {return new StandardDbUseCommand($this->getObjConnection());},
            $tabConfig,
            true
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return StandardDbShowCommand
     */
    public function getObjDbShowCommand(array $tabConfig = null)
    {
        // Init var
        /** @var StandardDbShowCommand $result */
        $result = $this->getObjCommandEngine(
            StandardDbShowCommand::class,
            function () {return new StandardDbShowCommand($this->getObjConnection());},
            $tabConfig,
            true
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return StandardDbCreateCommand
     */
    public function getObjDbCreateCommand(array $tabConfig = null)
    {
        // Init var
        /** @var StandardDbCreateCommand $result */
        $result = $this->getObjCommandEngine(
            StandardDbCreateCommand::class,
            function () {return new StandardDbCreateCommand($this->getObjConnection());},
            $tabConfig,
            true
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return StandardDbAlterCommand
     */
    public function getObjDbAlterCommand(array $tabConfig = null)
    {
        // Init var
        /** @var StandardDbAlterCommand $result */
        $result = $this->getObjCommandEngine(
            StandardDbAlterCommand::class,
            function () {return new StandardDbAlterCommand($this->getObjConnection());},
            $tabConfig,
            true
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return StandardDbDropCommand
     */
    public function getObjDbDropCommand(array $tabConfig = null)
    {
        // Init var
        /** @var StandardDbDropCommand $result */
        $result = $this->getObjCommandEngine(
            StandardDbDropCommand::class,
            function () {return new StandardDbDropCommand($this->getObjConnection());},
            $tabConfig,
            true
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return StandardTableShowCommand
     */
    public function getObjTableShowCommand(array $tabConfig = null)
    {
        // Init var
        /** @var StandardTableShowCommand $result */
        $result = $this->getObjCommandEngine(
            StandardTableShowCommand::class,
            function () {return new StandardTableShowCommand($this->getObjConnection());},
            $tabConfig,
            true
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return StandardTableDropCommand
     */
    public function getObjTableDropCommand(array $tabConfig = null)
    {
        // Init var
        /** @var StandardTableDropCommand $result */
        $result = $this->getObjCommandEngine(
            StandardTableDropCommand::class,
            function () {return new StandardTableDropCommand($this->getObjConnection());},
            $tabConfig,
            true
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return StandardSelectCommand
     */
    public function getObjSelectCommand(array $tabConfig = null)
    {
        // Init var
        /** @var StandardSelectCommand $result */
        $result = $this->getObjCommandEngine(
            StandardSelectCommand::class,
            function () {return new StandardSelectCommand($this->getObjConnection());},
            $tabConfig,
            true
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return StandardInsertCommand
     */
    public function getObjInsertCommand(array $tabConfig = null)
    {
        // Init var
        /** @var StandardInsertCommand $result */
        $result = $this->getObjCommandEngine(
            StandardInsertCommand::class,
            function () {return new StandardInsertCommand($this->getObjConnection());},
            $tabConfig,
            true
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return StandardUpdateCommand
     */
    public function getObjUpdateCommand(array $tabConfig = null)
    {
        // Init var
        /** @var StandardUpdateCommand $result */
        $result = $this->getObjCommandEngine(
            StandardUpdateCommand::class,
            function () {return new StandardUpdateCommand($this->getObjConnection());},
            $tabConfig,
            true
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return StandardDeleteCommand
     */
    public function getObjDeleteCommand(array $tabConfig = null)
    {
        // Init var
        /** @var StandardDeleteCommand $result */
        $result = $this->getObjCommandEngine(
            StandardDeleteCommand::class,
            function () {return new StandardDeleteCommand($this->getObjConnection());},
            $tabConfig,
            true
        );

        // Return result
        return $result;
    }



}