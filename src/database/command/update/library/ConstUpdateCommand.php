<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\update\library;



class ConstUpdateCommand
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_CLAUSE_FROM = 'from';
    const TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN = 'from_pattern';
    const TAB_CONFIG_KEY_CLAUSE_SET = 'set';
    const TAB_CONFIG_KEY_CLAUSE_SET_PATTERN = 'set_pattern';
    const TAB_CONFIG_KEY_CLAUSE_WHERE = 'where';
    const TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN = 'where_pattern';
    const TAB_CONFIG_KEY_CLAUSE_LIMIT = 'limit';
    const TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN = 'limit_pattern';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the default update command configuration standard.';



}