<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\library\instance\model\Multiton;



class ToolBoxStandardClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string pattern formatted value,
     * from specified pattern,
     * and specified value.
     *
     * @param string $strPattern
     * @param string $strValue
     * @return string
     */
    public static function getStrPatternValue($strPattern, $strValue)
    {
        // Init var
        $result = $strPattern;

        // Get formatted value, if required
        if(
            (strpos($result,'%1$s') !== false) ||
            (strpos($result,'%s') !== false)
        )
        {
            $result = sprintf($result, $strValue);
        }

        // Return result
        return $result;
    }



}