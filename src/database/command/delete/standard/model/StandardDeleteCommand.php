<?php
/**
 * Description :
 * This class allows to define standard delete SQL command class.
 * Standard delete command allows to design string SQL delete command,
 * from default configuration,
 * and uses standard SQL.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\delete\standard\model;

use liberty_code\sql\database\command\delete\model\DefaultDeleteCommand;

use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardExpressionClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardFromClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardConditionClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardOrderClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardLimitClause;
use liberty_code\sql\database\command\delete\library\ConstDeleteCommand;
use liberty_code\sql\database\command\delete\exception\ConfigInvalidFormatException;
use liberty_code\sql\database\command\delete\standard\library\ConstStandardDeleteCommand;



class StandardDeleteCommand extends DefaultDeleteCommand
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for delete clause.
     *
     * @return string
     */
    protected function getStrClauseDeleteCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
            isset($tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_DELETE]) ?
                ToolBoxStandardExpressionClause::getStrClauseExpressionCommand(
                    $objConnection,
                    $tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_DELETE]
                ) :
                ''
        );
        $result = trim(sprintf(
            ConstStandardDeleteCommand::SQL_CONFIG_PATTERN_DELETE,
            $result
        ));

        // Get pattern value, if required
        if(isset($tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_DELETE_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_DELETE_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for from clause.
     *
     * @return string
     */
    protected function getStrClauseFromCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
            isset($tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM]) ?
                sprintf(
                    ConstStandardDeleteCommand::SQL_CONFIG_PATTERN_FROM,
                    ToolBoxStandardFromClause::getStrClauseFromCommand(
                        $objConnection,
                        $tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM]
                    )
                ) :
                ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for where clause.
     *
     * @return string
     */
    protected function getStrClauseWhereCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]) ?
            sprintf(
                ConstStandardDeleteCommand::SQL_CONFIG_PATTERN_WHERE,
                ToolBoxStandardConditionClause::getStrClauseConditionCommand(
                    $objConnection,
                    $tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for order clause.
     *
     * @return string
     */
    protected function getStrClauseOrderCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_ORDER]) ?
            sprintf(
                ConstStandardDeleteCommand::SQL_CONFIG_PATTERN_ORDER,
                ToolBoxStandardOrderClause::getStrClauseOrderCommand(
                    $objConnection,
                    $tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_ORDER]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_ORDER_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_ORDER_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for limit clause.
     *
     * @return string
     */
    protected function getStrClauseLimitCommand()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT]) ?
            sprintf(
                ConstStandardDeleteCommand::SQL_CONFIG_PATTERN_LIMIT,
                ToolBoxStandardLimitClause::getStrClauseLimitCommand(
                    $tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrCommand()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Build SQL command
        $strDelete = $this->getStrClauseDeleteCommand();
        $strFrom = $this->getStrClauseFromCommand();
        $strWhere = $this->getStrClauseWhereCommand();
        $strOrder = $this->getStrClauseOrderCommand();
        $strLimit = $this->getStrClauseLimitCommand();
        $result =
            $strDelete . ((trim($strFrom) != '') ? ' ' : '') .
            $strFrom . ((trim($strWhere) != '') ? ' ' : '') .
            $strWhere . ((trim($strOrder) != '') ? ' ' : '') .
            $strOrder . ((trim($strLimit) != '') ? ' ' : '') .
            $strLimit;

        // Return result
        return $result;
    }



}