<?php
// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/database/test/HelpCreateTestDb.php');



// Init var
$strTableNmUsr = 'user';
$strColNmUsrId = 'usr_id';
$strColNmUsrLg = 'usr_login';
$strColNmUsrPw = 'usr_password';
$strColNmUsrNm = 'usr_name';
$strColNmUsrFnm = 'usr_first_name';
$strColNmUsrEmail = 'usr_email';
$strColNmUsrDateCreate = 'usr_dt_create';
$strColNmUsrDateUpdate = 'usr_dt_update';

$strTableNmAdmin = 'admin';
$strColNmAdmId = 'adm_id';
$strColNmAdmNm = 'adm_name';
$strColNmAdmRight1 = 'adm_right_1';
$strColNmAdmRight2 = 'adm_right_2';
$strColNmAdmRight3 = 'adm_right_3';
$strColNmAdmDateCreate = 'adm_dt_create';
$strColNmAdmDateUpdate = 'adm_dt_update';

$strTableNmMsg = 'message';
$strColNmMsgId = 'msg_id';
$strColNmMsgDateCreate = 'msg_dt_create';
$strColNmMsgTxt = 'msg_txt';
$strColNmMsgUsr = 'msg_usr_id';

$strTableNmCnf = 'config';
$strColNmCnfType = 'cnf_type';
$strColNmCnfNm = 'cnf_name';
$strColNmCnfVal = 'cnf_value';
$strColNmCnfDateCreate = 'cnf_dt_create';
$strColNmCnfDateUpdate = 'cnf_dt_update';
$strColNmCnfDateExpire = 'cnf_dt_expire';

$strTableNmPrm = 'param';
$strColNmPrmType = 'prm_type';
$strColNmPrmNm1 = 'prm_1';
$strColNmPrmNm2 = 'prm_2';
$strColNmPrmNm3 = 'prm_3';
$strColNmPrmNm4 = 'prm_4';
$strColNmPrmDateCreate = 'prm_dt_create';
$strColNmPrmDateUpdate = 'prm_dt_update';



// Create tables, if required
$objConnection->transactionStart();
try
{
    // Create user table
    $strSql =
        "CREATE TABLE IF NOT EXISTS " . $objConnection->getStrEscapeName($strTableNmUsr) . " (
            " . $objConnection->getStrEscapeName($strColNmUsrId) . " int(10) NOT NULL AUTO_INCREMENT,
            " . $objConnection->getStrEscapeName($strColNmUsrLg) . " varchar(100) COLLATE utf8_bin NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmUsrPw) . " varchar(200) COLLATE utf8_bin NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmUsrNm) . " varchar(200) COLLATE utf8_bin NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmUsrFnm) . " varchar(200) COLLATE utf8_bin NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmUsrEmail) . " varchar(200) COLLATE utf8_bin,
            " . $objConnection->getStrEscapeName($strColNmUsrDateCreate) . " datetime NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmUsrDateUpdate) . " datetime NOT NULL,
        UNIQUE KEY " . $objConnection->getStrEscapeName($strColNmUsrId) . " (" . $objConnection->getStrEscapeName($strColNmUsrId) . ")
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
    $objConnection->execute($strSql);

    echo(sprintf('Table "%s" created, if required.', $strTableNmUsr));
    echo('<br />' . PHP_EOL);



    // Create admin table
    $strSql =
        "CREATE TABLE IF NOT EXISTS " . $objConnection->getStrEscapeName($strTableNmAdmin) . " (
            " . $objConnection->getStrEscapeName($strColNmAdmId) . " int(10) NOT NULL AUTO_INCREMENT,
            " . $objConnection->getStrEscapeName($strColNmAdmNm) . " varchar(100) COLLATE utf8_bin NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmAdmRight1) . " varchar(200) COLLATE utf8_bin,
            " . $objConnection->getStrEscapeName($strColNmAdmRight2) . " int(10) COLLATE utf8_bin,
            " . $objConnection->getStrEscapeName($strColNmAdmRight3) . " varchar(300) COLLATE utf8_bin,
            " . $objConnection->getStrEscapeName($strColNmAdmDateCreate) . " datetime NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmAdmDateUpdate) . " datetime NOT NULL,
        UNIQUE KEY " . $objConnection->getStrEscapeName($strColNmAdmId) . " (" . $objConnection->getStrEscapeName($strColNmAdmId) . ")
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
    $objConnection->execute($strSql);

    echo(sprintf('Table "%s" created, if required.', $strTableNmAdmin));
    echo('<br />' . PHP_EOL);



    // Create message table
    $strSql =
        "CREATE TABLE IF NOT EXISTS " . $objConnection->getStrEscapeName($strTableNmMsg) . " (
            " . $objConnection->getStrEscapeName($strColNmMsgId) . " int(10) NOT NULL AUTO_INCREMENT,
            " . $objConnection->getStrEscapeName($strColNmMsgDateCreate) . " datetime NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmMsgTxt) . " text,
            " . $objConnection->getStrEscapeName($strColNmMsgUsr) . " int(10) NOT NULL,
        UNIQUE KEY " . $objConnection->getStrEscapeName($strColNmMsgId) . " (" . $objConnection->getStrEscapeName($strColNmMsgId) . ")
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
    $objConnection->execute($strSql);

    echo(sprintf('Table "%s" created, if required.', $strTableNmMsg));
    echo('<br />' . PHP_EOL);



    // Create config table
    $strSql =
        "CREATE TABLE IF NOT EXISTS " . $objConnection->getStrEscapeName($strTableNmCnf) . " (
            " . $objConnection->getStrEscapeName($strColNmCnfType) . " varchar(100) COLLATE utf8_bin NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmCnfNm) . " varchar(100) COLLATE utf8_bin NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmCnfVal) . " text COLLATE utf8_bin,
            " . $objConnection->getStrEscapeName($strColNmCnfDateCreate) . " datetime NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmCnfDateUpdate) . " datetime NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmCnfDateExpire) . " datetime DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
    $objConnection->execute($strSql);

    echo(sprintf('Table "%s" created, if required.', $strTableNmCnf));
    echo('<br />' . PHP_EOL);



    // Create param table
    $strSql =
        "CREATE TABLE IF NOT EXISTS " . $objConnection->getStrEscapeName($strTableNmPrm) . " (
            " . $objConnection->getStrEscapeName($strColNmPrmType) . " varchar(100) COLLATE utf8_bin NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmPrmNm1) . " text COLLATE utf8_bin,
            " . $objConnection->getStrEscapeName($strColNmPrmNm2) . " text COLLATE utf8_bin,
            " . $objConnection->getStrEscapeName($strColNmPrmNm3) . " text COLLATE utf8_bin,
            " . $objConnection->getStrEscapeName($strColNmPrmNm4) . " text COLLATE utf8_bin,
            " . $objConnection->getStrEscapeName($strColNmPrmDateCreate) . " datetime NOT NULL,
            " . $objConnection->getStrEscapeName($strColNmPrmDateUpdate) . " datetime NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
    $objConnection->execute($strSql);

    echo(sprintf('Table "%s" created, if required.', $strTableNmPrm));
    echo('<br />' . PHP_EOL);

    $objConnection->transactionEndCommit();
}
catch(\Throwable $e)
{
    $objConnection->transactionEndRollback();

    echo(sprintf('Transaction failed: Table(s) not created.'));
    echo('<br />' . PHP_EOL);
}



echo('<br /><br /><br />' . PHP_EOL . PHP_EOL . PHP_EOL);


