<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/test/HelpGetCommandFacto.php');



// Init var
$objCommand = $objCommandFacto->getObjInsertCommand();

$config_1 = array(
    'from' => [
        [
            'db_name' => 'libertycode_sql_test',
            'table_name' => 'user'
        ]
    ],
    'column' => [
        [
            'table_name' => 'user',
            'column_name' => 'usr_login',
        ],
        [
            'table_name' => 'user',
            'column_name' => 'usr_name',
        ],
        [
            'table_name' => 'user',
            'column_name' => 'usr_first_name',
        ],
        [
            'table_name' => 'user',
            'column_name' => 'usr_email',
        ],
        [
            'table_name' => 'user',
            'column_name' => 'usr_dt_create',
        ],
        [
            'table_name' => 'user',
            'column_name' => 'usr_dt_update',
        ]
    ],
    'value' => [
        [
            ['value' => 'Login inserted 1'],
            ['value' => 'Name inserted 1'],
            ['value' => 'First name inserted 1'],
            ['value' => 'Email inserted 1'],
            ['pattern' => 'NOW()'],
            ['pattern' => 'NOW()']
        ],
        [
            ['value' => 'Login inserted 2'],
            ['value' => 'Name inserted 2'],
            ['value' => 'First name inserted 2'],
            ['value' => 'Email inserted 2'],
            ['pattern' => 'NOW()'],
            ['pattern' => 'NOW()']
        ],
        [
            ['value' => 'Login inserted 3'],
            ['value' => 'Name inserted 3'],
            ['value' => 'First name inserted 3'],
            ['value' => 'Email inserted 3'],
            ['pattern' => 'NOW()'],
            ['pattern' => 'NOW()']
        ]
    ]
);

$config_2 = array(
    'column' => [
        [
            'table_name' => 'user',
            'column_name' => 'usr_login',
        ],
        [
            'table_name' => 'user',
            'column_name' => 'usr_name',
        ],
        [
            'table_name' => 'user',
            'column_name' => 'usr_first_name',
        ],
        [
            'table_name' => 'user',
            'column_name' => 'usr_email',
        ],
        [
            'table_name' => 'user',
            'column_name' => 'usr_dt_create',
        ],
        [
            'table_name' => 'user',
            'column_name' => 'usr_dt_update',
        ]
    ],
    'value' => [
        [
            ['value' => 'Login inserted 1'],
            ['value' => 'Name inserted 1'],
            ['value' => 'First name inserted 1'],
            ['value' => 'Email inserted 1'],
            ['pattern' => 'NOW()'],
            ['pattern' => 'NOW()']
        ]
    ]
);

$config_3 = array(
    'from' => [
        'message'
    ],
    'column' => [
        'msg_dt_create',
        'msg_txt',
        'msg_usr_id'
    ],
    'value' => [
        [
            ['pattern' => 'NOW()'],
            ['value' => 'Txt inserted 1'],
            ['value' => 1]
        ],
        [
            ['pattern' => 'NOW()'],
            ['value' => 'Txt inserted 2'],
            ['value' => 1]
        ],
        [
            ['pattern' => 'NOW()'],
            ['value' => 'Txt inserted 3'],
            ['value' => 3]
        ]
    ]
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3 // Ok
);



// Test command
require_once($strRootAppPath . '/src/database/command/test/HelpRunCommand.php');



// Reconnection, if required
if(!$objConnection->checkIsConnect())$objConnection->connect();



// Test build clause: Ko
$tabConfig = array(
    'From' => array(['test' => 'test']),
    'Column' => array(['test' => 'test']),
    'Value' => array('test' => 'test')
);

foreach($tabConfig as $strClause => $config)
{
    // Get info
    $strClauseCall = sprintf('setClause%s', $strClause);

    echo('Test build clause: Ko ' . $strClause . ':<br />');

    try
    {
        $objCommand = $objCommandFacto->getObjInsertCommand();
        $objCommand->$strClauseCall($config);

        echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
        echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
    }
    catch(\Exception $e)
    {
        echo('Config failed: ' . $e->getMessage());
    }

    echo('<br /><br /><br />');
}



// Test build clause: Ok
echo('Test build clause: <br />');

try
{
    $objCommand = $objCommandFacto->getObjInsertCommand(
        array(
            'from_pattern' => '[%s]',
            'column_pattern' => '{%s}',
            'value_pattern' => '[%s]'
        )
    );
    $objCommand->setClauseFrom(
        array(
            [
                'db_name' => 'libertycode_sql_test',
                'table_name' => 'user'
            ]
        )
    )->setClauseColumn(
        array(
            [
                'table_name' => 'user',
                'column_name' => 'usr_login',
            ],
            [
                'table_name' => 'user',
                'column_name' => 'usr_name',
            ],
            [
                'table_name' => 'user',
                'column_name' => 'usr_first_name',
            ],
            [
                'table_name' => 'user',
                'column_name' => 'usr_email',
            ],
            [
                'table_name' => 'user',
                'column_name' => 'usr_dt_create',
            ],
            [
                'table_name' => 'user',
                'column_name' => 'usr_dt_update',
            ]
        )
    )->setClauseValue(
        array(
            [
                ['value' => 'Login inserted 1'],
                ['value' => 'Name inserted 1'],
                ['value' => 'First name inserted 1'],
                ['value' => 'Email inserted 1'],
                ['pattern' => 'NOW()'],
                ['pattern' => 'NOW()']
            ],
            [
                ['value' => 'Login inserted 2'],
                ['value' => 'Name inserted 2'],
                ['value' => 'First name inserted 2'],
                ['value' => 'Email inserted 2'],
                ['pattern' => 'NOW()'],
                ['pattern' => 'NOW()']
            ],
            [
                ['value' => 'Login inserted 3'],
                ['value' => 'Name inserted 3'],
                ['value' => 'First name inserted 3'],
                ['value' => 'Email inserted 3'],
                ['pattern' => 'NOW()'],
                ['pattern' => 'NOW()']
            ]
        )
    );

    echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
    echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
}
catch(\Exception $e)
{
    echo('Config failed: ' . $e->getMessage());
}

echo('<br /><br /><br />');



// Test build clause: Ok
echo('Test build clause: <br />');

try
{
    $objCommand = $objCommandFacto->getObjInsertCommand(
        array(
            'from_pattern' => '[INSERT INTO ...]',
            'column_pattern' => '{...}',
            'value_pattern' => '[VALUES ...]'
        )
    );

    echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
    echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
}
catch(\Exception $e)
{
    echo('Config failed: ' . $e->getMessage());
}

echo('<br /><br /><br />');


