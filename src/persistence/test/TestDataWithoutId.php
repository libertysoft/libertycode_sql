<?php
/**
 * GET arguments:
 * - delete: = 1: Remove data.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/test/HelpCreateTestTable.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\persistence\data\model\DataPersistor;
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;



// Init var
$objCommandFacto = new StandardCommandFactory($objConnection);
$objPersistor = new DataPersistor($objCommandFacto);
$tabConfig = array(
    'table_name' => $strTableNmCnf,
    'column_name_attribute_name' => $strColNmCnfNm,
    'column_name_attribute_value' => $strColNmCnfVal,
    //*
    'attribute_name_select' => [
        'key_1',
        'key_2'
    ],
    //*/
    //'pattern_select' => '',
    'pattern_where' => sprintf(
        '(%1$s = %2$s)',
        $objConnection->getStrEscapeName($strColNmCnfType),
        $objConnection->getStrEscapeValue('test_data')
    ),
    'pattern_order' => sprintf(
        '%1$s ASC',
        $objConnection->getStrEscapeName($strColNmCnfNm)
    ),
    'pattern_column' => sprintf(
        '%1$s, %2$s, %3$s',
        $objConnection->getStrEscapeName($strColNmCnfType),
        $objConnection->getStrEscapeName($strColNmCnfDateCreate),
        $objConnection->getStrEscapeName($strColNmCnfDateUpdate)
    ),
    'pattern_value' => sprintf(
        '%1$s, NOW(), NOW()',
        $objConnection->getStrEscapeValue('test_data')
    ),
    'pattern_set' => sprintf(
        '%1$s = NOW()',
        $objConnection->getStrEscapeName($strColNmCnfDateUpdate)
    )
);



// Test insert 1 config
echo('Test insert 1 config: <br />');
$data = array(
    'key_1' => 'Value test without id',
    'key_2' => 2.5,
    'key_3' => null
);

// echo('Execute: <pre>');var_dump($objPersistor->createTabData(array($data), $tabConfig));echo('</pre>');
echo('Execute: <pre>');var_dump($objPersistor->createData($data, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test select 1 config
echo('Test select 1 config: <br />');

echo('Get: <pre>');print_r($objPersistor->getData(null, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test search several configs
echo('Test search multi configs: <br />');

$tabConfigRequest = array(
    'select' => [
        ['column_name' => 'cnf_name'],
        ['column_name' => 'cnf_value']
    ],
    'from' => [
        ['table_name' => 'config']
    ],
    'where' => [
        'content' => [
            [
                'operand' => ['column_name' => 'cnf_name'],
                'operator' => 'equal',
                'value' => ['value' => 'key_3'],
                'not' => true
            ]
        ]
    ],
    'order' => [
        [
            'operand' => ['column_name' => 'cnf_name']
        ]
    ]
);

echo('Search: <pre>');var_dump($objPersistor->getTabSearchData($tabConfigRequest, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test update 1 config
echo('Test update 1 config: <br />');
$data = array(
    'key_1' => 'Value test without id updated',
    'key_2' => 7.5,
    'key_3' => null
);

echo('Execute: <pre>');var_dump($objPersistor->updateData($data, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test select 1 config
echo('Test select 1 config: <br />');

echo('Get: <pre>');print_r($objPersistor->getData(null, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Delete configs, if required
if(trim(ToolBoxTable::getItem($_GET, 'delete', '0')) == '1')
{
    // Test delete 1 config
    echo('Test delete 1 config: <br />');

    echo('Execute: <pre>');var_dump($objPersistor->deleteData($data, $tabConfig));echo('</pre>');

    echo('<br /><br /><br />');



    // Test select 1 config
    echo('Test select 1 config: <br />');

    echo('Get: <pre>');print_r($objPersistor->getData(null, $tabConfig));echo('</pre>');

    echo('<br /><br /><br />');
}



// Remove test database, if required
require_once($strRootAppPath . '/src/database/test/HelpRemoveTestDb.php');


