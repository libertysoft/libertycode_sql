<?php
/**
 * Description :
 * This class allows to describe behavior of database create SQL command class.
 * Database create command allows to design string SQL database create command,
 * from following specified configuration:
 * [
 *     db_name(required):"string database name",
 *
 *     charset(optional):"string charset",
 *
 *     collation(optional):"string collation"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_create\api;

use liberty_code\sql\database\command\api\CommandInterface;



interface DbCreateCommandInterface extends CommandInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Set string database name config.
     *
     * @param string $strDbNm
     * @return static
     */
    public function setDbName($strDbNm);



    /**
     * Set string charset config.
     *
     * @param string $strCharset
     * @return static
     */
    public function setCharset($strCharset);



    /**
     * Set string collation config.
     *
     * @param string $strCollation
     * @return static
     */
    public function setCollation($strCollation);
}