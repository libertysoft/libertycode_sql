<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/test/HelpGetCommandFacto.php');



// Init var
$objCommand = $objCommandFacto->getObjTableShowCommand();

$config_1 = array();

$config_2 = array(
    'db_name' => 7
);

$config_3 = array(
    'db_name' => $strDbNm
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3, // Ok
);



// Select database
$strSql = "USE " . $objConnection->getStrEscapeName($strDbNm) . ";";
$objConnection->execute($strSql);



// Test command
$_GET['exec'] = '1';
$_GET['exec-result'] = '1';
$_GET['exec-stmt'] = '0';
$_GET['exec-stmt-result'] = '0';
//$_GET['clean'] = '1';
require($strRootAppPath . '/src/database/command/test/HelpRunCommand.php');




// Reconnection, if required
if(!$objConnection->checkIsConnect())$objConnection->connect();



// Test build config: Ko
$tabConfig = array(
    'DbName' => 7
);

$objCommand = $objCommandFacto->getObjTableShowCommand();
foreach($tabConfig as $strConfig => $config)
{
    // Get info
    $strConfigCall = sprintf('set%s', $strConfig);

    echo('Test build config: Ko ' . $strConfig . ':<br />');

    try
    {
        $objCommand->$strConfigCall($config);

        echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
        echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
    }
    catch(\Exception $e)
    {
        echo('Config failed: ' . $e->getMessage());
    }

    echo('<br /><br /><br />');
}



// Test build config: Ok
$tabConfig = array(
    'DbName' => 'test_nm'
);

$objCommand = $objCommandFacto->getObjTableShowCommand();
foreach($tabConfig as $strConfig => $config)
{
    // Get info
    $strConfigCall = sprintf('set%s', $strConfig);

    echo('Test build config: Ok ' . $strConfig . ':<br />');

    try
    {
        $objCommand->$strConfigCall($config);

        echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
        echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
    }
    catch(\Exception $e)
    {
        echo('Config failed: ' . $e->getMessage());
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


