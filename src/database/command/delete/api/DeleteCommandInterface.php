<?php
/**
 * Description :
 * This class allows to describe behavior of delete SQL command class.
 * Delete command allows to design string SQL delete command,
 * from following specified configuration:
 * [
 *     from(required: if from_pattern not found)): array (@see ToolBoxFromClause::checkClauseFromIsValid() ),
 *
 *     from_pattern(required: if from not found):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated from clause, if found",
 *
 *     where(optional): array (@see ToolBoxConditionClause::checkClauseConditionIsValid() ),
 *
 *     where_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated where clause, if found",
 *
 *     order(optional): array (@see ToolBoxOrderClause::checkClauseOrderIsValid() ),
 *
 *     order_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated order clause, if found",
 *
 *     limit(optional): array (@see ToolBoxLimitClause::checkClauseLimitIsValid() ),
 *
 *     limit_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated limit clause, if found"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\delete\api;

use liberty_code\sql\database\command\api\CommandInterface;

use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxConditionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxOrderClause;
use liberty_code\sql\database\command\clause\library\ToolBoxLimitClause;



interface DeleteCommandInterface extends CommandInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Set delete clause config array.
     * Configuration format follows @see ToolBoxExpressionClause::checkClauseExpressionIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseDelete(array $tabConfig);



    /**
     * Set from clause config array.
     * Configuration format follows @see ToolBoxFromClause::checkClauseFromIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseFrom(array $tabConfig);



    /**
     * Set where clause config array.
     * Configuration format follows @see ToolBoxConditionClause::checkClauseConditionIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseWhere(array $tabConfig);



    /**
     * Set order clause config array.
     * Configuration format follows @see ToolBoxOrderClause::checkClauseOrderIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseOrder(array $tabConfig);



    /**
     * Set limit clause config array.
     * Configuration format follows @see ToolBoxLimitClause::checkClauseLimitIsValid().
     *
     * @param integer|array $config
     * @return static
     */
    public function setClauseLimit($config);
}