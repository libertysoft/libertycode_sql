<?php

namespace liberty_code\sql\database\test;

use liberty_code\sql\database\connection\pdo\model\PdoConnection;

use PDO;
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\database\connection\library\ConstConnection;
use liberty_code\sql\database\connection\pdo\library\ConstPdoConnection;



class MysqlPdoConnection extends PdoConnection
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrConfigDsn()
    {
        // Init var
        $result = '';
        $tabInfo = array(
            [
                ConstConnection::TAB_CONFIG_KEY_HOST,
                'host=%s'
            ],
            [
                ConstConnection::TAB_CONFIG_KEY_PORT,
                'port=%s'
            ],
            [
                ConstConnection::TAB_CONFIG_KEY_DB_NAME,
                'dbname=%s'
            ],
            [
                ConstConnection::TAB_CONFIG_KEY_CHARSET,
                'charset=%s'
            ]
        );

        // Run all info
        foreach($tabInfo as $info)
        {
            // Get info
            $strConfigKey = $info[0];
            $strPattern = $info[1];

            // Get value
            $strValue = strval(ToolBoxTable::getItem(
                $this->getTabConfig(),
                $strConfigKey,
                ''
            ));

            // Format value
            $strValue = (
            (trim($strValue) != '') ?
                sprintf($strPattern, $strValue) :
                ''
            );

            // Register in result, if required
            if(trim($strValue) != '')
            {
                if(trim($result) == '')
                {
                    $result = $strValue;
                }
                else
                {
                    $result = sprintf(
                        '%1$s;%2$s',
                        $result,
                        $strValue
                    );
                }
            }
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig, $boolConnect = true)
    {
        // Set MySQL driver
        $tabConfig[ConstPdoConnection::TAB_CONFIG_KEY_DRIVER] = 'mysql';

        // Call parent method
        parent::setConfig($tabConfig, $boolConnect);
    }



}