<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\register\table\exception;

use Exception;

use liberty_code\sql\register\table\library\ConstTableRegister;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstTableRegister::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function($tabStr)
        {
            $result = is_array($tabStr) && (count($tabStr) > 0);

            // Check each column name is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = is_string($strValue) && (trim($strValue) != '');
            }

            return $result;
        };

        // Init var
        $tabTzNm = timezone_identifiers_list();
        $result =
            // Check valid column name selection
            (
                (!isset($config[ConstTableRegister::TAB_CONFIG_KEY_COLUMN_NAME_SELECT])) ||
                $checkTabStrIsValid($config[ConstTableRegister::TAB_CONFIG_KEY_COLUMN_NAME_SELECT])
            ) &&

            // Check valid get timezone name
            (
                (!isset($config[ConstTableRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME])) ||
                (
                    is_string($config[ConstTableRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME]) &&
                    (in_array($config[ConstTableRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME], $tabTzNm))
                )
            ) &&

            // Check valid set timezone name
            (
                (!isset($config[ConstTableRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME])) ||
                (
                    is_string($config[ConstTableRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME]) &&
                    (in_array($config[ConstTableRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME], $tabTzNm))
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}