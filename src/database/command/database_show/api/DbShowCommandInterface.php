<?php
/**
 * Description :
 * This class allows to describe behavior of database show SQL command class.
 * Database show command allows to design string SQL database show command,
 * without specific configuration.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_show\api;

use liberty_code\sql\database\command\api\CommandInterface;



interface DbShowCommandInterface extends CommandInterface
{

}