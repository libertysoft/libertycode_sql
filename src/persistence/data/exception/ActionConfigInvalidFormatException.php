<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\persistence\data\exception;

use liberty_code\sql\persistence\library\ConstPersistor;
use liberty_code\sql\persistence\exception\ActionConfigInvalidFormatException as BaseActionConfigInvalidFormatException;
use liberty_code\sql\persistence\data\library\ConstDataPersistor;



class ActionConfigInvalidFormatException extends \Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param string $strAction
     * @param mixed $config
     */
    public function __construct($strAction, $config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstDataPersistor::EXCEPT_MSG_ACTION_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "..."),
            mb_strimwidth(strval($strAction), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format, for specified action.
     *
     * @param string $strAction
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($strAction, $config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function($tabStr)
        {
            $result = is_array($tabStr) && (count($tabStr) > 0);

            // Check each column name is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = is_string($strValue) && (trim($strValue) != '');
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid column name attribute name
            (
                (!isset($config[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_NAME])) ||
                (
                    is_string($config[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_NAME]) &&
                    (trim($config[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_NAME]) != '')
                )
            ) &&

            // Check valid column name attribute value
            (
                (!isset($config[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_VALUE])) ||
                (
                    is_string($config[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_VALUE]) &&
                    (trim($config[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_VALUE]) != '')
                )
            ) &&

            // Check valid selection
            (
                (!isset($config[ConstDataPersistor::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SELECT])) ||
                $checkTabStrIsValid($config[ConstDataPersistor::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SELECT])
            );

        // Check by action
        switch($strAction)
        {
            case ConstPersistor::TAB_ACTION_KEY_GET:
            case ConstPersistor::TAB_ACTION_KEY_SEARCH:
            case ConstPersistor::TAB_ACTION_KEY_CREATE:
            case ConstPersistor::TAB_ACTION_KEY_UPDATE:
                $result = $result &&
                    // Column name attribute name, required
                    isset($config[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_NAME]) &&

                    // Column name attribute value, required
                    isset($config[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_VALUE]);

                break;

            case ConstPersistor::TAB_ACTION_KEY_DELETE:
                $result = $result &&
                    // Column name attribute name, required only if selection provided
                    (!isset($config[ConstDataPersistor::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SELECT])) ||
                    (
                        isset($config[ConstDataPersistor::TAB_CONFIG_KEY_COLUMN_NAME_ATTRIBUTE_NAME]) &&
                        isset($config[ConstDataPersistor::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SELECT])
                    );

                break;
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format, for specified action.
     *
     * @param string $strAction
     * @param mixed $config
     * @return boolean
     * @throws static
     * @throws BaseActionConfigInvalidFormatException
     */
    static public function setCheck($strAction, $config)
    {
        // Check basic
        BaseActionConfigInvalidFormatException::setCheck($strAction, $config);

        // Init var
        $result =
            // Check valid array
            static::checkConfigIsValid($strAction, $config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($strAction, (is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}