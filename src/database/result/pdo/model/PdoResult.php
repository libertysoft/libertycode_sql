<?php
/**
 * Description :
 * This class allows to define PDO SQL database result class.
 * Based on PDO statement, to read and get query results.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\result\pdo\model;

use liberty_code\sql\database\result\model\DefaultResult;

use liberty_code\sql\database\statement\pdo\exception\PdoStatementInvalidFormatException;
use liberty_code\sql\database\result\exception\DataIndexInvalidFormatException;
use liberty_code\sql\database\result\exception\DataNameInvalidFormatException;
use liberty_code\sql\database\result\exception\ColNotFoundException;
use liberty_code\sql\database\result\pdo\library\ConstPdoResult;



/**
 * @method \PDOStatement getObjPdoStatement() Get PDO statement object.
 */
class PdoResult extends DefaultResult
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Index array of cached string column names
     * @var array
     */
    protected $tabColNm;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param \PDOStatement $objStatement
     * @param boolean $boolInitialize = true
     */
    public function __construct(\PDOStatement $objStatement, $boolInitialize = true)
    {
        // Init var
        $boolInitialize = (is_bool($boolInitialize) ? $boolInitialize : true);

        // Init properties
        $this->tabColNm = array();

        // Call parent constructor
        parent::__construct(false);

        // Init PDO statement
        $this->setPdoStatement($objStatement);

        // Init reading data, if required
        if($boolInitialize)
        {
            $this->initialize();
        }
        else
        {
            // Call parent initialization
            parent::initialize();
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstPdoResult::DATA_KEY_DEFAULT_PDO_STATEMENT))
        {
            $this->__beanTabData[ConstPdoResult::DATA_KEY_DEFAULT_PDO_STATEMENT] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * @inheritdoc
     */
    public function initialize()
    {
        // Init var
        $this->tabColNm = array();

        // Call parent method
        parent::initialize();

        // Reset query cursor position.
        $this->getObjPdoStatement()->execute();
    }



    /**
     * @inheritdoc
     */
    public function close()
    {
        $this->getObjPdoStatement()->closeCursor();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstPdoResult::DATA_KEY_DEFAULT_PDO_STATEMENT
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstPdoResult::DATA_KEY_DEFAULT_PDO_STATEMENT:
                    PdoStatementInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set PDO statement object.
     *
     * @param \PDOStatement $objStatement
     */
    public function setPdoStatement(\PDOStatement $objStatement)
    {
        $this->beanSet(ConstPdoResult::DATA_KEY_DEFAULT_PDO_STATEMENT, $objStatement);
    }





    // Methods fetching
    // ******************************************************************************

    /**
     * Get data from cursor current position.
     * Return data if found, false else
     * Data format:
     * {
     *     column_name_1 => value 1,
     *     ...,
     *     column_name_N => value N
     * }
     *
     * @return boolean|array
     *
     * @inheritdoc
     */
    public function getFetchData()
    {
        // Return result
        return $this->getObjPdoStatement()->fetch(
            \PDO::FETCH_ASSOC,
            \PDO::FETCH_ORI_NEXT,
            0
        );
    }





    // Methods count
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getIntCountCol()
    {
        // Check search count required
        if($this->intCountCol == 0)
        {
            // Get count
            $this->intCountCol = $this->getObjPdoStatement()->columnCount();
        }

        // Return result
        return $this->intCountCol;
    }



    /**
     * @inheritdoc
     */
    public function getIntCountRow()
    {
        // Check search count required
        if($this->intCountRow == 0)
        {
            // Get count
            $this->intCountRow = $this->getObjPdoStatement()->rowCount();
        }

        // Return result
        return $this->intCountRow;
    }





    // Methods data
    // ******************************************************************************

    /**
     * Get index array of string column names.
     *
     * @return array
     */
    protected function getTabColName()
    {
        // Get column names, if required
        if(count($this->tabColNm) == 0)
        {
            // Run each columns
            for($cpt = 0; $cpt < $this->getIntCountCol(); $cpt++)
            {
                // Register column name, if required (if found)
                $tabColInfo = $this->getObjPdoStatement()->getColumnMeta($cpt);
                if(array_key_exists('name', $tabColInfo))
                {
                    $this->tabColNm[] = $tabColInfo['name'];
                }
            }
        }

        // Return result
        return $this->tabColNm;
    }



    /**
     * @inheritdoc
     * @throws DataIndexInvalidFormatException
     * @throws ColNotFoundException
     */
    public function getStrColName($intColIndex)
    {
        // Check arguments
        DataIndexInvalidFormatException::setCheck($intColIndex);

        // Init var
        $tabColNm = $this->getTabColName();

        // Check column index found
        if(!array_key_exists($intColIndex, $tabColNm)) throw new ColNotFoundException($intColIndex);

        // Return result
        return $tabColNm[$intColIndex];
    }



    /**
     * @inheritdoc
     * @throws DataNameInvalidFormatException
     * @throws ColNotFoundException
     */
    public function getIntColIndex($strColNm)
    {
        // Check arguments
        DataNameInvalidFormatException::setCheck($strColNm);

        // Init var
        $tabColNm = $this->getTabColName();
        $result = array_search($strColNm, $tabColNm);

        // Check column index valid
        if(($result === false) || (!is_int($result))) throw new ColNotFoundException($strColNm);

        // Return result
        return $result;
    }



}