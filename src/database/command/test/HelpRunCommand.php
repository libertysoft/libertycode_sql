<?php
/**
 * GET arguments:
 * - clean: = 1: Drop test database at the end.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/command/test/HelpGetCommandFacto.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\database\command\model\DefaultCommand;



/** @var DefaultCommand $objCommand */
if(isset($objCommand) && isset($tabConfig))
{
    foreach($tabConfig as $config)
    {
        // Test config
        echo('Test config: <br />');
        echo('Config: <pre>');var_dump($config);echo('</pre>');

        try{
            $objCommand->setConfig($config);

            // Test config
            echo('Test get config: <br />');
            echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');

            // Test command
            echo('Test get command: <br />');
            echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');

            // Test execute
            if(trim(ToolBoxTable::getItem($_GET, 'exec', '1')) == '1')
            {
                echo('Test execute: <br />');
                echo('Execute: <pre>');var_dump($objCommand->execute());echo('</pre>');
            }

            // Test execute result, if required
            if(trim(ToolBoxTable::getItem($_GET, 'exec-result', '1')) == '1')
            {
                echo('Test execute result: <br />');
                $objResult = $objCommand->executeResult();
                echo('Execute result: <pre>');
                if(is_bool($objResult))
                {
                    var_dump($objResult);
                }
                else
                {
                    print_r($objResult->getTabData());
                    $objResult->close();
                }
                echo('</pre>');
            }

            // Test execute statement, if required
            if(trim(ToolBoxTable::getItem($_GET, 'exec-stmt', '1')) == '1')
            {
                echo('Test execute statement: <br />');
                $objStatement = $objCommand->getObjStatement();
                echo('Execute statement: <pre>');
                var_dump($objStatement->execute());
                echo('</pre>');
            }


            // Test execute statement result
            if(trim(ToolBoxTable::getItem($_GET, 'exec-stmt-result', '1')) == '1')
            {
                echo('Test execute statement result: <br />');
                $objResult = $objStatement->executeResult();
                echo('Execute statement result: <pre>');
                print_r($objResult->getTabData());
                echo('</pre>');
                $objResult->close();
            }
        }
        catch(\Exception $e)
        {
            echo('Config failed: ' . $e->getMessage());
        }

        echo('<br /><br /><br />');
    }
}



// Remove test database, if required
require_once($strRootAppPath . '/src/database/test/HelpRemoveTestDb.php');


