<?php
// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/database/test/HelpCreateTestTable.php');



// Insert in tables
$objConnection->transactionStart();
try
{
    // Insert user table
    $strSql =
        "INSERT INTO " . $objConnection->getStrEscapeName($strTableNmUsr) . " (
            " . $objConnection->getStrEscapeName($strColNmUsrLg) . ", 
            " . $objConnection->getStrEscapeName($strColNmUsrPw) . ",
            " . $objConnection->getStrEscapeName($strColNmUsrNm) . ", 
            " . $objConnection->getStrEscapeName($strColNmUsrFnm) . ", 
            " . $objConnection->getStrEscapeName($strColNmUsrEmail) . ",
            " . $objConnection->getStrEscapeName($strColNmUsrDateCreate) . ",
            " . $objConnection->getStrEscapeName($strColNmUsrDateUpdate) . "
        ) VALUES (
            :Lg,
            :Pw,
            :Nm,
            :Fnm,
            :Email,
            NOW(), 
            NOW()
        );";
    $objStatement = $objConnection->getObjStatement($strSql);

    $objStatement->execute(array(
        'Lg' => 'lg_1',
        'Pw' => 'pw-1',
        'Nm' => 'NM 1',
        'Fnm' => 'Fnm 1',
        'Email' => null
    ));
    $strUsrId1 = $objConnection->getStrLastInsertId();

    $objStatement->execute(array(
        'Lg' => 'lg_2',
        'Pw' => 'pw-2',
        'Nm' => 'NM 2',
        'Fnm' => 'Fnm 2',
        'Email' => 2
    ));
    $strUsrId2 = $objConnection->getStrLastInsertId();

    $objStatement->execute(array(
        'Lg' => 'lg_3',
        'Pw' => 'pw-3',
        'Nm' => 'NM 3',
        'Fnm' => 'Fnm 3',
        'Email' => 'fnm3.nm3@test.com'
    ));
    $strUsrId3 = $objConnection->getStrLastInsertId();

    echo(sprintf('Insert in table "%s".', $strTableNmUsr));
    echo('<br />' . PHP_EOL);



    // Insert admin table
    $strSql =
        "INSERT INTO " . $objConnection->getStrEscapeName($strTableNmAdmin) . " (
            " . $objConnection->getStrEscapeName($strColNmAdmNm) . ", 
            " . $objConnection->getStrEscapeName($strColNmAdmRight1) . ", 
            " . $objConnection->getStrEscapeName($strColNmAdmRight2) . ", 
            " . $objConnection->getStrEscapeName($strColNmAdmRight3) . ",
            " . $objConnection->getStrEscapeName($strColNmAdmDateCreate) . ",
            " . $objConnection->getStrEscapeName($strColNmAdmDateUpdate) . "
        ) VALUES (
            :Nm,
            :Right1,
            :Right2,
            :Right3,
            NOW(), 
            NOW()
        );";
    $objStatement = $objConnection->getObjStatement($strSql);

    $objStatement->execute(array(
        'Nm' => 'adm_1',
        'Right1' => 'Value 1',
        'Right2' => 1,
        'Right3' => 'Text 1'
    ));
    $strUsrId1 = $objConnection->getStrLastInsertId();

    $objStatement->execute(array(
        'Nm' => 'adm_2',
        'Right1' => 'Value 2',
        'Right2' => 2,
        'Right3' => 'Text 2'
    ));
    $strUsrId2 = $objConnection->getStrLastInsertId();

    $objStatement->execute(array(
        'Nm' => 'adm_test',
        'Right1' => null,
        'Right2' => null,
        'Right3' => null
    ));
    $strUsrId3 = $objConnection->getStrLastInsertId();

    echo(sprintf('Insert in table "%s".', $strTableNmAdmin));
    echo('<br />' . PHP_EOL);



    // Insert message table
    $strSql =
        "INSERT INTO " . $objConnection->getStrEscapeName($strTableNmMsg) . " (
            " . $objConnection->getStrEscapeName($strColNmMsgDateCreate) . ", 
            " . $objConnection->getStrEscapeName($strColNmMsgTxt) . ", 
            " . $objConnection->getStrEscapeName($strColNmMsgUsr) . "
        ) VALUES (
            NOW(), 
            :Txt, 
            :UsrId
        );";
    $objStatement = $objConnection->getObjStatement($strSql);

    $objStatement->execute(array(
        'Txt' => "Txt' 1",
        'UsrId' => intval($strUsrId1)
    ));

    $objStatement->execute(array(
        'Txt' => 'Txt 2',
        'UsrId' => intval($strUsrId1)
    ));

    $objStatement->execute(array(
        'Txt' => "Txt' 3",
        'UsrId' => strval($strUsrId1)
    ));

    $objStatement->execute(array(
        'Txt' => null,
        'UsrId' => intval($strUsrId3)
    ));

    $objStatement->execute(array(
        'Txt' => "Txt' 5",
        'UsrId' => strval($strUsrId3)
    ));

    echo(sprintf('Insert in "%s".', $strTableNmMsg));
    echo('<br />' . PHP_EOL);



    // Insert config table
    $strSql =
        "INSERT INTO " . $objConnection->getStrEscapeName($strTableNmCnf) . " (
            " . $objConnection->getStrEscapeName($strColNmCnfType) . ", 
            " . $objConnection->getStrEscapeName($strColNmCnfNm) . ", 
            " . $objConnection->getStrEscapeName($strColNmCnfVal) . ", 
            " . $objConnection->getStrEscapeName($strColNmCnfDateCreate) . ", 
            " . $objConnection->getStrEscapeName($strColNmCnfDateUpdate) . "
        ) VALUES (
            " . $objConnection->getStrEscapeValue('test-insert') . ", 
            :Key, 
            :Value, 
            NOW(), 
            NOW()
        );";
    $objStatement = $objConnection->getObjStatement($strSql);

    $objStatement->execute(array(
        'Key' => "key-1",
        'Value' => 1
    ));

    $objStatement->execute(array(
        'Key' => 'key-2',
        'Value' => 'Value 2'
    ));

    $objStatement->execute(array(
        'Key' => 'key-3',
        'Value' => 7.5
    ));

    echo(sprintf('Insert in "%s".', $strTableNmCnf));
    echo('<br />' . PHP_EOL);



    // Insert param table
    $strSql =
        "INSERT INTO " . $objConnection->getStrEscapeName($strTableNmPrm) . " (
            " . $objConnection->getStrEscapeName($strColNmPrmType) . ", 
            " . $objConnection->getStrEscapeName($strColNmPrmNm1) . ", 
            " . $objConnection->getStrEscapeName($strColNmPrmNm2) . ", 
            " . $objConnection->getStrEscapeName($strColNmPrmNm3) . ", 
            " . $objConnection->getStrEscapeName($strColNmPrmNm4) . ", 
            " . $objConnection->getStrEscapeName($strColNmPrmDateCreate) . ", 
            " . $objConnection->getStrEscapeName($strColNmPrmDateUpdate) . "
        ) VALUES (
            :Type, 
            :Param1, 
            :Param2, 
            :Param3, 
            :Param4, 
            NOW(), 
            NOW()
        );";
    $objStatement = $objConnection->getObjStatement($strSql);

    $objStatement->execute(array(
        'Type' => 'param_1',
        'Param1' => 'Value 1',
        'Param2' => 'Val 1',
        'Param3' => 'Text 1',
        'Param4' => 'Text value 1'
    ));

    $objStatement->execute(array(
        'Type' => 'param_2',
        'Param1' => 'Value 2',
        'Param2' => 'Val 2',
        'Param3' => 'Text 2',
        'Param4' => 'Text value 2'
    ));

    $objStatement->execute(array(
        'Type' => 'param_test',
        'Param1' => serialize(null),
        'Param2' => serialize(null),
        'Param3' => serialize(null),
        'Param4' => serialize(null)
    ));

    echo(sprintf('Insert in table "%s".', $strTableNmPrm));
    echo('<br />' . PHP_EOL);



    $objConnection->transactionEndCommit();
}
catch(\Throwable $e)
{
    $objConnection->transactionEndRollback();

    echo(sprintf('Transaction failed: Table(s) not created.'));
    echo('<br />' . PHP_EOL);
}



echo('<br /><br /><br />' . PHP_EOL . PHP_EOL . PHP_EOL);


