<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/clause/test/Include.php');

// Use
use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardExpressionClause;



// Init var
$config_1 = array(
    'col_1',
    [
        'db_name' => 'db',
        'table_name' => 'table',
        'column_name' => 'col_2',
        'alias' => 'alias 2'
    ],
    [
        'db_name' => 'db',
        'table_name' => 'table',
        'value' => 3,
        'alias' => 'alias 3'
    ],
    [
        'db_name' => 'db',
        'table_name' => 'table',
        'pattern' => '%s\'col 4\''
    ]
);

$config_2 = array(
    1,
    [
        'db_name' => 'db',
        'table_name' => 'table',
        'column_name' => 'col_2',
        'alias' => 'alias 2'
    ]
);

$config_3 = array(
    [
        'db_name' => 'db',
        'table_name' => 'table',
        'column_name' => 'col_1',
        'alias' => 'alias 1',
        'pattern' => '[%1$s]'
    ],
    [
        'table_name' => 'table',
        'column_name' => 'col_2'
    ],
    [
        'value' => true,
    ],
    [
        'column_name' => 'col_4',
        'pattern' => 'pattern 4'
    ]
);

$config_4 = array(
    [
        'db_name' => 'db',
        'table_name' => 'table',
        'alias' => 'alias 1'
    ]
);

$config_5 = array(
    [
        'table_name' => 'table',
        'value' => 'val 1'
    ],
    [
        'db_name' => 'db',
        'value' => 2.2
    ],
    [
        'value' => true,
    ],
    [
        'value' => false,
        'pattern' => '[%s]'
    ],
    [
        'value' => null,
        'pattern' => '[%1$s]'
    ]
);

$config_6 = array(
    [
        'db_name' => 'db',
        'value' => array()
    ]
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3, // Ok
    $config_4, // Ok
    $config_5, // Ok
    $config_6 // Ko
);



// Test config
process(
    $tabConfig,
    function($config)
    {
        return ToolBoxExpressionClause::checkClauseExpressionIsValid($config);
    },
    function($config) use ($objConnection)
    {
        return ToolBoxStandardExpressionClause::getStrClauseExpressionCommand($objConnection, $config);
    }
);


