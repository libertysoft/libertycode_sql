<?php
/**
 * Description :
 * This class allows to describe behavior of insert SQL command class.
 * Insert command allows to design string SQL insert command,
 * from following specified configuration:
 * [
 *     from(required: if from_pattern not found): array (@see ToolBoxFromClause::checkClauseFromIsValid() ),
 *
 *     from_pattern(required: if from not found):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated from clause, if found",
 *
 *     column(optional): array (@see ToolBoxExpressionClause::checkClauseExpressionIsValid() ),
 *
 *     column_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated column clause, if found",
 *
 *     value(required: if value_pattern not found): array (@see ToolBoxValueClause::checkClauseValueIsValid() ),
 *
 *     value_pattern(required: if value not found):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated value clause, if found"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\insert\api;

use liberty_code\sql\database\command\api\CommandInterface;

use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxValueClause;



interface InsertCommandInterface extends CommandInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Set from clause config array.
     * Configuration format follows @see ToolBoxFromClause::checkClauseFromIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseFrom(array $tabConfig);



    /**
     * Set column clause config array.
     * Configuration format follows @see ToolBoxExpressionClause::checkClauseExpressionIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseColumn(array $tabConfig);



    /**
     * Set value clause config array.
     * Configuration format follows @see ToolBoxValueClause::checkClauseValueIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseValue(array $tabConfig);
}