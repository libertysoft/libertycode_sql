<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/clause/test/Include.php');

// Use
use liberty_code\sql\database\command\clause\library\ToolBoxValueClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardValueClause;



// Init var
$config_1 = array(
    [
        'col_1',
        [
            'db_name' => 'db',
            'table_name' => 'table',
            'column_name' => 'col_2'
        ],
        [
            'value' => 1
        ],
        [
            'value' => true
        ]
    ],
    [
        'col_1',
        [
            'db_name' => 'db',
            'table_name' => 'table',
            'column_name' => 'col_2'
        ],
        [
            'value' => 2
        ],
        [
            'value' => false
        ]
    ],
    [
        'col_1',
        [
            'db_name' => 'db',
            'table_name' => 'table',
            'column_name' => 'col_2'
        ],
        [
            'value' => 3
        ],
        [
            'value' => 'test'
        ]
    ]
);

$config_2 = array(
    'col_1',
    [
        'db_name' => 'db',
        'table_name' => 'table',
        'column_name' => 'col_2'
    ],
    [
        'value' => 1
    ],
    [
        'value' => true
    ]
);

$tabConfig = array(
    $config_1, // Ok
    $config_2 // Ko
);



// Test config
process(
    $tabConfig, 
    function($config)
    {
        return ToolBoxValueClause::checkClauseValueIsValid($config);
    },
    function($config) use ($objConnection)
    {
        return ToolBoxStandardValueClause::getStrClauseValueCommand($objConnection, $config);
    }
);


