<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\connection\library;



class ConstConnection
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_HOST = 'host';
    const TAB_CONFIG_KEY_PORT = 'port';
    const TAB_CONFIG_KEY_DB_NAME = 'db_name';
    const TAB_CONFIG_KEY_CHARSET = 'charset';
    const TAB_CONFIG_KEY_LOGIN = 'login';
    const TAB_CONFIG_KEY_PASSWORD = 'password';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the default connection configuration standard.';
    const EXCEPT_MSG_CONNECTION_REQUIRE = 'Connection must be connected!';
    const EXCEPT_MSG_DISCONNECTION_REQUIRE = 'Connection must be disconnected!';



}