<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\persistence\library;



class ConstPersistor
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_COMMAND_FACTORY = 'objCommandFactory';



    // Configuration
    const TAB_CONFIG_KEY_TABLE_NAME = 'table_name';
    const TAB_CONFIG_KEY_COLUMN_NAME_ID = 'column_name_id';
    const TAB_CONFIG_KEY_OPT_ID_NEW = 'opt_id_new';
    const TAB_CONFIG_KEY_PATTERN_SELECT = 'pattern_select';
    const TAB_CONFIG_KEY_PATTERN_WHERE = 'pattern_where';
    const TAB_CONFIG_KEY_PATTERN_ORDER = 'pattern_order';
    const TAB_CONFIG_KEY_PATTERN_COLUMN = 'pattern_column';
    const TAB_CONFIG_KEY_PATTERN_VALUE = 'pattern_value';
    const TAB_CONFIG_KEY_PATTERN_SET = 'pattern_set';

    // Configuration ID new
    const TAB_CONFIG_OPT_ID_NEW_PERSISTOR = 'persistor'; // ID new get from persistor
    const TAB_CONFIG_OPT_ID_NEW_CONNECTION = 'connection'; // ID new get from connection

    // Configuration action
    const TAB_ACTION_KEY_GET = 'get';
    const TAB_ACTION_KEY_SEARCH = 'search';
    const TAB_ACTION_KEY_CREATE = 'create';
    const TAB_ACTION_KEY_UPDATE = 'update';
    const TAB_ACTION_KEY_DELETE = 'delete';


	
    // Exception message constants
    const EXCEPT_MSG_COMMAND_FACTORY_INVALID_FORMAT = 'Following factory "%1$s" invalid! It must be a command factory object.';
    const EXCEPT_MSG_ACTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid for action "%2$s"! 
        The config must be an array, not empty and following the adequat default persistor action configuration standard.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get ID new options index array.
     *
     * @return array
     */
    static public function getTabConfigOptIdNew()
    {
        // Init var
        $result = array(
            self::TAB_CONFIG_OPT_ID_NEW_PERSISTOR,
            self::TAB_CONFIG_OPT_ID_NEW_CONNECTION
        );

        // Return result
        return $result;
    }



    /**
     * Get action index array.
     *
     * @return array
     */
    static public function getTabAction()
    {
        // Init var
        $result = array(
            self::TAB_ACTION_KEY_GET,
            self::TAB_ACTION_KEY_SEARCH,
            self::TAB_ACTION_KEY_CREATE,
            self::TAB_ACTION_KEY_UPDATE,
            self::TAB_ACTION_KEY_DELETE
        );

        // Return result
        return $result;
    }



}