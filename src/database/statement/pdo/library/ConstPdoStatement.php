<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\statement\pdo\library;



class ConstPdoStatement
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PDO_STATEMENT = 'objPdoStatement';



    // Exception message constants
    const EXCEPT_MSG_PARAM_INVALID_FORMAT =
        'Following parameters config "%1$s" invalid! 
        The config must be an array, following the PDO statement execution parameters configuration standard.';
    const EXCEPT_MSG_PDO_STATEMENT_INVALID_FORMAT = 'Following statement "%1$s" invalid! It must be a PDO statement object.';



}