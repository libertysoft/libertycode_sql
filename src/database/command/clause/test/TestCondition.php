<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/clause/test/Include.php');

// Use
use liberty_code\sql\database\command\clause\library\ToolBoxConditionClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardConditionClause;



// Init var
$config_1 = array(
    'type' => 'or',
    'content' => [
        [
            'operand' => [
                'db_name' => 'db',
                'table_name' => 'table',
                'column_name' => 'col_1'
            ],
            'operator' => 'equal',
            'value' => 'col_value_1'
        ],
        [
            'content' => [
                [
                    'operand' => [
                        'db_name' => 'db',
                        'table_name' => 'table',
                        'column_name' => 'col_2'
                    ],
                    'operator' => 'equal',
                    'value' => ['value' => 2],
                    'not' => 1,
                    'pattern' => '[%s]'
                ],
                [
                    'operand' => ['pattern' => '`col_2`'],
                    'operator' => 'null',
                    'not' => 1
                ],
                [
                    'pattern' => 'test'
                ]
            ]
        ],
        [
            'operand' => [
                'db_name' => 'db',
                'table_name' => 'table',
                'column_name' => 'col_3'
            ],
            'operator' => 'operator:FUNCTION(%1$s, %2$s)',
            'value' => 'col_value_1',
            'pattern' => '%s AS TEST_FUNCTION'
        ]
    ]
);

$config_2 = array(
    'type' => 'test',
    'content' => [
        [
            'operand' => [
                'db_name' => 'db',
                'table_name' => 'table',
                'column_name' => 'col_1'
            ],
            'operator' => 'equal',
            'value' => 'col_value_1'
        ]
    ]
);

$config_3 = array(
    'type' => 'and',
    'content' => [
        [
            'pattern' => 'test'
        ],
        [
            'operand' => 'col_value_1',
            'operator' => 'less_equal',
            'value' => ['value' => 5]
        ],
        [
            'operand' => 'col_value_1',
            'operator' => 'greater',
            'value' => ['value' => 6]
        ],
        [
            'operand' => 'col_value_1',
            'operator' => 'operator:FUNCTION(%s, %s)',
            'value' => ['value' => 7]
        ]
    ],
    'pattern' => 'IF(%1$s, 1, 0) = 1'
);

$config_4 = array(
    'type' => 'and',
    'content' => [
        [
            'operand' => [
                'db_name' => 'db',
                'table_name' => 'table',
                'column_name' => 'col_1'
            ],
            'operator' => 'test',
            'value' => 'col_value_1'
        ]
    ]
);

$config_5 = array(
    'type' => 'and',
    'content' => [
        [
            'operand' => [
                'db_name' => 'db',
                'table_name' => 'table',
                'column_name' => 'col_1'
            ],
            'pattern' => '%s IS Null'
        ],
        [
            'operand' => [
                'db_name' => 'db',
                'table_name' => 'table',
                'column_name' => 'col_1'
            ],
            'operator' => 'operator:FUNCTION(%s)'
        ]
    ]
);

$config_6 = array(
    'type' => 'and',
    'content' => [
        [
            'operand' => [
                'db_name' => 'db',
                'table_name' => 'table',
                'column_name' => 'col_1'
            ]
        ]
    ],
    'pattern' => 'test_pattern(%1$s)'
);

$config_7 = array(
    'pattern' => 'test_pattern'
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3, // Ok
    $config_4, // Ko
    $config_5, // Ok
    $config_6, // Ko
    $config_7 // Ko
);



// Test config
process(
    $tabConfig, 
    function($config)
    {
        return ToolBoxConditionClause::checkClauseConditionIsValid($config);
    },
    function($config) use ($objConnection)
    {
        return ToolBoxStandardConditionClause::getStrClauseConditionCommand($objConnection, $config);
    }
);


