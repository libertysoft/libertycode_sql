<?php
/**
 * Description :
 *
 * This class allows to define default database create SQL command class.
 * Can be consider is base of all database create SQL command type.
 *
 * Default database create command allows to design string database create SQL command,
 * from following specified configuration: @see DbCreateCommandInterface.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_create\model;

use liberty_code\sql\database\command\model\DefaultCommand;
use liberty_code\sql\database\command\database_create\api\DbCreateCommandInterface;

use liberty_code\sql\database\command\library\ConstCommand;
use liberty_code\sql\database\command\database_create\library\ConstDbCreateCommand;
use liberty_code\sql\database\command\database_create\exception\ConfigInvalidFormatException;



abstract class DefaultDbCreateCommand extends DefaultCommand implements DbCreateCommandInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		// $result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstCommand::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function setDbName($strDbNm)
    {
        // Check config valid
        if(
            (!is_string($strDbNm)) ||
            (trim($strDbNm) == ''))
        {
            throw new ConfigInvalidFormatException($strDbNm);
        }

        // Set config
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstDbCreateCommand::TAB_CONFIG_KEY_DB_NAME] = $strDbNm;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function setCharset($strCharset)
    {
        // Check config valid
        if(
            (!is_string($strCharset)) ||
            (trim($strCharset) == ''))
        {
            throw new ConfigInvalidFormatException($strCharset);
        }

        // Set config
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstDbCreateCommand::TAB_CONFIG_KEY_CHARSET] = $strCharset;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function setCollation($strCollation)
    {
        // Check config valid
        if(
            (!is_string($strCollation)) ||
            (trim($strCollation) == ''))
        {
            throw new ConfigInvalidFormatException($strCollation);
        }

        // Set config
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstDbCreateCommand::TAB_CONFIG_KEY_COLLATION] = $strCollation;

        // Return result
        return $this;
    }



}