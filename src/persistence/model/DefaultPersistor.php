<?php
/**
 * Description :
 * This class allows to define default SQL persistor class.
 * Can be consider is base of all SQL persistor type.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\persistence\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\model\persistence\api\PersistorInterface;

use liberty_code\sql\database\command\select\api\SelectCommandInterface;
use liberty_code\sql\database\command\factory\api\CommandFactoryInterface;
use liberty_code\sql\persistence\library\ConstPersistor;
use liberty_code\sql\persistence\exception\CommandFactoryInvalidFormatException;
use liberty_code\sql\persistence\exception\ActionConfigInvalidFormatException;



/**
 * @method null|CommandFactoryInterface getObjCommandFactory() Get command factory object.
 */
abstract class DefaultPersistor extends FixBean implements PersistorInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param CommandFactoryInterface $objCommandFactory = null
     */
    public function __construct(CommandFactoryInterface $objCommandFactory = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init command factory if required
        if(!is_null($objCommandFactory))
        {
            $this->setCommandFactory($objCommandFactory);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstPersistor::DATA_KEY_DEFAULT_COMMAND_FACTORY))
        {
            $this->__beanTabData[ConstPersistor::DATA_KEY_DEFAULT_COMMAND_FACTORY] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstPersistor::DATA_KEY_DEFAULT_COMMAND_FACTORY
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstPersistor::DATA_KEY_DEFAULT_COMMAND_FACTORY:
                    CommandFactoryInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get mixed new id,
     * from configuration send during action.
     * If no specific new id required,
     * or no specific new id rule,
     * return null.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig = null
     * @return null|mixed
     */
    protected function getIdNew(array $tabConfig = null)
    {
        // Init var
        $result = null;

        // Process by configuration case
        if(is_array($tabConfig))
        {
            $result = null;
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set command factory object.
     *
     * @param CommandFactoryInterface $objCommandFactory
     */
    public function setCommandFactory(CommandFactoryInterface $objCommandFactory)
    {
        $this->beanSet(ConstPersistor::DATA_KEY_DEFAULT_COMMAND_FACTORY, $objCommandFactory);
    }





    // Methods getters (Persistor interface implementation)
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     table_name(required): "string table name",
     *
     *     column_name_id(optional: considered without id system, if column_name_id not found): "string column id name",
     *
     *     pattern_select(optional: got null if pattern not found): "string select pattern",
     *
     *     pattern_where(optional: got null if pattern not found): "string where pattern",
     *
     *     pattern_order(optional: got null if pattern not found): "string order pattern"
     * ]
     *
     * Note:
     * -> Configuration patterns format:
     *     Patterns are strings added at the end of the calculated SQL clause.
     *     It transforms as sprintf format during operation.
     *     That's reason why sprintf syntax must be excluded from patterns.
     *
     * @inheritdoc
     */
    public function getData(
        $id = null,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $tabId = is_null($id) ? array() : array($id);
        $result = $this->getTabData($tabId, $tabConfig, $tabInfo);

        // Get result
        if(is_array($result))
        {
            if(count($result) > 0)
            {
                $result = $result[0];
            }
            else
            {
                $result = null;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Selection query format:
     * [
     *     @see SelectCommandInterface configuration array format
     * ]
     * OR
     * "String SQL select command"
     *
     * @inheritdoc
     */
    public function getTabSearchData(
        $query,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Check arguments
        ActionConfigInvalidFormatException::setCheck(ConstPersistor::TAB_ACTION_KEY_SEARCH, $tabConfig);

        // Init var
        $result = false;
        $objCommandFactory = $this->getObjCommandFactory();

        // Check command factory valid
        if(!is_null($objCommandFactory))
        {
            // Get query result
            $resultQuery = false;
            if(is_array($query))
            {
                // Get from command, if configured query detected
                $objCommand = $objCommandFactory->getObjSelectCommand($query);
                $resultQuery = $objCommand->executeResult();
            }
            else if(is_string($query))
            {
                // Get from connection request, if string query detected
                $objConnection = $objCommandFactory->getObjConnection();
                $resultQuery = $objConnection->executeResult($query);
            }

            // Run query result
            if($resultQuery !== false)
            {
                $result = array();
                while(($data = $resultQuery->getFetchData()) !== false)
                {
                    $result[] = $data;
                }
                $resultQuery->close();
            }
        }

        // Return result
        return $result;
    }





    // Methods setters (Persistor interface implementation)
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     table_name(required): "string table name",
     *
     *     column_name_id(optional: considered without id system, if column_name_id not found): "string column id name",
     *
     *     opt_id_new(optional): "string option ID new calculation: persistor | connection",
     *
     *     pattern_column(optional: got null if pattern not found): "string column pattern",
     *
     *     pattern_value(optional: got null if pattern not found): "string value pattern"
     * ]
     *
     * Note:
     * -> Configuration patterns format:
     *     @see getData() note configuration patterns format.
     * -> Configuration opt_id_new (option ID new calculation):
     *     - If opt_id_new = persistor: means @see getIdNew() used to get new id.
     *     - If opt_id_new = connection: connection feature used to get new id.
     *
     * @inheritdoc
     */
    public function createData(
        array &$data,
        array $tabConfig = null,
        &$id = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $tabId = array();
        $tabData = array($data);
        $result = $this->createTabData($tabData, $tabConfig, $tabId, $tabInfo);

        // Register id, if required
        if(is_array($tabId) && (count($tabId) > 0))
        {
            $id = $tabId[(count($tabId) - 1)];
        }

        // Register data, if required
        if(count($tabData) > 0)
        {
            $data = $tabData[(count($tabData) - 1)];
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     table_name(required): "string table name",
     *
     *     column_name_id(optional: considered without id system, if column_name_id not found): "string column id name",
     *
     *     pattern_set(optional: got null if pattern not found): "string set pattern",
     *
     *     pattern_where(optional: got null if pattern not found): "string where pattern"
     * ]
     *
     * Note:
     * -> Configuration patterns format:
     *     @see getData() note configuration patterns format.
     *
     * @inheritdoc
     */
    public function updateData(
        array &$data,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $tabData = array($data);
        $result = $this->updateTabData($tabData, $tabConfig, $tabInfo);

        // Register data, if required
        if(count($tabData) > 0)
        {
            $data = $tabData[(count($tabData) - 1)];
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     table_name(required): "string table name",
     *
     *     column_name_id(optional: considered without id system, if column_name_id not found): "string column id name",
     *
     *     pattern_where(optional: got null if pattern not found): "string where pattern"
     * ]
     *
     * Note:
     * -> Configuration patterns format:
     *     @see getData() note configuration patterns format.
     *
     * @inheritdoc
     */
    public function deleteData(
        array &$data,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $tabData = array($data);
        $result = $this->deleteTabData($tabData, $tabConfig, $tabInfo);

        // Register data, if required
        if(count($tabData) > 0)
        {
            $data = $tabData[(count($tabData) - 1)];
        }

        // Return result
        return $result;
    }





    // Methods transaction (Persistor interface implementation)
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkInTransaction()
    {
        // Init var
        $result = false;
        $objConnection = $this->getObjCommandFactory()->getObjConnection();

        // Check connection valid
        if(!is_null($objConnection))
        {
            $result = $objConnection->checkInTransaction();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function transactionStart()
    {
        // Init var
        $result = false;
        $objConnection = $this->getObjCommandFactory()->getObjConnection();

        // Check connection valid
        if(!is_null($objConnection))
        {
            $result = $objConnection->transactionStart();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function transactionEndCommit()
    {
        // Init var
        $result = false;
        $objConnection = $this->getObjCommandFactory()->getObjConnection();

        // Check connection valid
        if(!is_null($objConnection))
        {
            $result = $objConnection->transactionEndCommit();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function transactionEndRollback()
    {
        // Init var
        $result = false;
        $objConnection = $this->getObjCommandFactory()->getObjConnection();

        // Check connection valid
        if(!is_null($objConnection))
        {
            $result = $objConnection->transactionEndRollback();
        }

        // Return result
        return $result;
    }



}