<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\authentication\authenticator\library;



class ConstSqlAuthenticator
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_COMMAND_FACTORY = 'objCommandFactory';



    // Configuration
    const TAB_CONFIG_KEY_QUERY = 'query';
    const TAB_CONFIG_KEY_MAPPING_ID_DATA = 'mapping_identification_data';
    const TAB_CONFIG_KEY_MAPPING_AUTH_DATA = 'mapping_authentication_data';
    const TAB_CONFIG_KEY_DATA_KEY = 'data_key';
    const TAB_CONFIG_KEY_DATA_FORMAT_CALLABLE = 'data_format_callable';
    const TAB_CONFIG_KEY_COLUMN_NAME = 'column_name';
    const TAB_CONFIG_KEY_OPERATOR_LIKE_REQUIRE = 'operator_like_require';
    const TAB_CONFIG_KEY_CHECK_DATA_CALLABLE = 'check_data_callable';
    const TAB_CONFIG_KEY_DEFAULT_ID_DATA_COLUMN_NAME = 'default_identification_data_column_name';
    const TAB_CONFIG_KEY_DEFAULT_AUTH_DATA_COLUMN_NAME = 'default_authentication_data_column_name';



    // Exception message constants
    const EXCEPT_MSG_COMMAND_FACTORY_INVALID_FORMAT = 'Following factory "%1$s" invalid! It must be a command factory object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the SQL authenticator configuration standard.';



}