<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\browser\data\exception;

use liberty_code\sql\database\command\select\exception\ConfigInvalidFormatException as CommandConfigInvalidFormatException;
use liberty_code\sql\browser\data\library\ConstDataBrowser;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDataBrowser::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid select command query, for page calculation
            (
                (!isset($config[ConstDataBrowser::TAB_CONFIG_KEY_QUERY_PAGE])) ||
                (
                    is_array($config[ConstDataBrowser::TAB_CONFIG_KEY_QUERY_PAGE]) &&
                    CommandConfigInvalidFormatException::checkConfigIsValid($config[ConstDataBrowser::TAB_CONFIG_KEY_QUERY_PAGE])
                )
            ) &&

            // Check valid column name item id
            isset($config[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_ID]) &&
            is_string($config[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_ID]) &&
            (trim($config[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_ID]) != '') &&

            // Check valid column name item name
            isset($config[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_KEY]) &&
            is_string($config[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_KEY]) &&
            (trim($config[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_KEY]) != '') &&

            // Check valid column name item value
            isset($config[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_VALUE]) &&
            is_string($config[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_VALUE]) &&
            (trim($config[ConstDataBrowser::TAB_CONFIG_KEY_COLUMN_NAME_ITEM_VALUE]) != '');

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}