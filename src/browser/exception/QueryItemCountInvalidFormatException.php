<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\browser\exception;

use liberty_code\sql\browser\library\ConstBrowser;



class QueryItemCountInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $query
     */
	public function __construct($query)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstBrowser::EXCEPT_MSG_QUERY_ITEM_COUNT_INVALID_FORMAT,
            mb_strimwidth(strval($query), 0, 50, "...")
        );
	}
	
	
	
}