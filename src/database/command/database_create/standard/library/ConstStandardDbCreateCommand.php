<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_create\standard\library;



class ConstStandardDbCreateCommand
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_CREATE = 'CREATE DATABASE %1$s';
    const SQL_CONFIG_PATTERN_CHARSET = 'CHARACTER SET %1$s';
    const SQL_CONFIG_PATTERN_COLLATION = 'COLLATE %1$s';
}