<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\command\clause\library\ToolBoxConditionClause;
use liberty_code\sql\database\command\clause\library\ConstFromClause;



class ToolBoxFromClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

    /**
     * Check if table configuration is valid.
     * Table configuration format:
     * "string table name" (table_name)
     *
     * OR
     *
     * {
     *     db_name(optional): "string database name",
     *
     *     table_name(required: if pattern not found): "string table name",
     *
     *     alias(optional): "string alias",
     *
     *     pattern(required: if table_name not found):
     *         "string sprintf pattern,
     *         where '%1$s' or '%s' replaced by calculated table, if found"
     * }
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkTableIsValid($config)
    {
        // Init var
        $result =
            // Case simple config
            (
                // Check valid table name
                is_string($config) &&
                (trim($config) != '')
            ) ||
            // Case complex config
            (
                // Check valid array
                is_array($config) &&

                // Check valid db name
                (
                    (!isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_DB_NAME])) ||
                    (
                        is_string($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_DB_NAME]) &&
                        (trim($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_DB_NAME]) != '')
                    )
                ) &&

                // Check valid table name
                (
                    (!isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME])) ||
                    (
                        is_string($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME]) &&
                        (trim($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME]) != '')
                    )
                ) &&

                // Check valid alias
                (
                    (!isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_ALIAS])) ||
                    (
                        is_string($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_ALIAS]) &&
                        (trim($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_ALIAS]) != '')
                    )
                ) &&

                // Check valid pattern
                (
                    (!isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_PATTERN])) ||
                    (
                        is_string($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_PATTERN]) &&
                        (trim($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_PATTERN]) != '')
                    )
                ) &&

                // Check minimum requirement
                (
                    isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME]) ||
                    isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_PATTERN])
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if join configuration is valid.
     * Join configuration format:
     * {
     *     type(optional: got 'inner' if type not found)): "string join type" (@see ConstFromClause::getTabConfigJoinType() ),
     *
     *     left(required): 'element (@see ToolBoxFromClause::checkElementIsValid() )',
     *
     *     right(required): 'element',
     *
     *     on(required): condition clause (@see ToolBoxConditionClause::checkClauseConditionIsValid() )
     * }
     *
     * @param array $tabConfig
     * @return boolean
     */
    public static function checkJoinIsValid(array $tabConfig)
    {
        // Init var
        $result =
            // Check valid join type
            (
                (!isset($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_TYPE])) ||
                (
                    is_string($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_TYPE]) &&
                    in_array($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_TYPE], ConstFromClause::getTabConfigJoinType())
                )
            ) &&

            // Check valid left
            isset($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_LEFT]) &&
            static::checkElementIsValid($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_LEFT]) &&

            // Check valid right
            isset($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_RIGHT]) &&
            static::checkElementIsValid($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_RIGHT]) &&

            // Check valid on condition
            isset($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_ON]) &&
            is_array($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_ON]) &&
            ToolBoxConditionClause::checkClauseConditionIsValid($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_ON]);

        // Return result
        return $result;
    }



    /**
     * Check if element configuration is valid.
     * Element configuration format:
     * join (@see ToolBoxFromClause::checkJoinIsValid() )
     *
     * OR
     *
     * table (@see ToolBoxFromClause::checkTableIsValid() )
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkElementIsValid($config)
    {
        // Init var
        $result =
            static::checkTableIsValid($config) ||
            (
                is_array($config) &&
                static::checkJoinIsValid($config)
            );

        // Return result
        return $result;
    }



    /**
     *
     * Check if from clause is valid.
     * Configuration is an index array of elements,
     * where each element uses following format:
     * @see ToolBoxFromClause::checkElementIsValid().
     *
     * @param array $tabConfig
     * @return boolean
     */
    public static function checkClauseFromIsValid(array $tabConfig)
    {
        // Init var
        $result = true;

        // Run each element
        $tabKey = array_keys($tabConfig);
        for($intCpt = 0; ($intCpt < count($tabKey)) && $result; $intCpt++)
        {
            // Check element valid
            $result = static::checkElementIsValid($tabConfig[$tabKey[$intCpt]]);
        }

        // Return result
        return $result;
    }



}