<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\browser\data\library;



class ConstDataBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_QUERY_PAGE = 'query_page';
    const TAB_CONFIG_KEY_COLUMN_NAME_ITEM_ID = 'column_name_item_id';
    const TAB_CONFIG_KEY_COLUMN_NAME_ITEM_KEY = 'column_name_item_key';
    const TAB_CONFIG_KEY_COLUMN_NAME_ITEM_VALUE = 'column_name_item_value';

    // Item count configuration
    const ITEM_COUNT_EXPRESSION_PATTERN = 'COUNT(DISTINCT %1$s)';
    

	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the SQL page browser configuration standard.';
    const EXCEPT_MSG_QUERY_PAGE_INVALID_FORMAT =
        'Following query "%1$s" invalid! 
        The query used for page calculation, must have the list of item ids, in first column.';



}