<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ConstOrderClause;
use liberty_code\sql\database\command\clause\library\ToolBoxOrderClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardExpressionClause;
use liberty_code\sql\database\command\clause\standard\library\ConstStandardOrderClause;



class ToolBoxStandardOrderClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for order.
     * Order configuration format: @see ToolBoxOrderClause::checkOrderIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param string|array $config
     * @return string
     */
    public static function getStrOrderCommand(ConnectionInterface $objConnection, $config)
    {
        // Init var
        $tabOperator = ConstStandardOrderClause::getTabConfigOperator();

        // Get operand expression
        $strOperand = '';
        if(isset($config[ConstOrderClause::TAB_CONFIG_KEY_OPERAND]))
        {
            $strOperand = ToolBoxStandardExpressionClause::getStrExpressionCommand(
                $objConnection,
                $config[ConstOrderClause::TAB_CONFIG_KEY_OPERAND]
            );
        }
        else if(
            ToolBoxExpressionClause::checkExpressionIsValid($config) &&
            (!(
                is_array($config) &&
                (count($config) == 1) &&
                array_key_exists(ConstOrderClause::TAB_CONFIG_KEY_PATTERN, $config)
            ))
        )
        {
            $strOperand = ToolBoxStandardExpressionClause::getStrExpressionCommand(
                $objConnection,
                $config
            );
        }

        // Get operator
        $strOperator = ConstOrderClause::OPERATOR_CONFIG_ASC;
        if(isset($config[ConstOrderClause::TAB_CONFIG_KEY_OPERATOR]))
        {
            $strOperator = $config[ConstOrderClause::TAB_CONFIG_KEY_OPERATOR];
        }

        // Get order
        $result = sprintf($tabOperator[$strOperator], $strOperand);

        // Get pattern value, if required
        if(isset($config[ConstOrderClause::TAB_CONFIG_KEY_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue($config[ConstOrderClause::TAB_CONFIG_KEY_PATTERN], $result);
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for order clause.
     * Configuration array format: @see ToolBoxOrderClause::checkClauseOrderIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param array $tabConfig
     * @return string
     */
    public static function getStrClauseOrderCommand(ConnectionInterface $objConnection, array $tabConfig)
    {
        // Init var
        $result = '';

        // Run each sub-config
        foreach($tabConfig as $config)
        {
            // Get sub-config string command
            $result =
                (trim($result) == '') ?
                    static::getStrOrderCommand($objConnection, $config) :
                    sprintf(
                        ConstStandardOrderClause::SQL_CONFIG_PATTERN_LINK,
                        $result,
                        static::getStrOrderCommand($objConnection, $config)
                    );
        }

        // Return result
        return $result;
    }



}