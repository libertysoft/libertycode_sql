<?php
/**
 * Description :
 * This class allows to define table SQL register class.
 *
 * Items are stored in table like:
 * - Each column name = key
 * - Row = serialization of following item data array:
 * [
 *     item(required): mixed item,
 *     expire_timeout_datetime(optional): DateTime object
 * ]
 * Deletion means row (item data) = null for specified key.
 * Non exists key means no column named as key found, or its row (item data) = null.
 *
 * Table SQL register uses the following specified configuration:
 * [
 *     Default SQL register configuration,
 *
 *     column_name_select(optional: got all columns from table, if not found)): [
 *         'string column name 1 to select',
 *         ...,
 *         'string column name N to select',
 *     ],
 *
 *     get_timezone_name(optional: got date_default_timezone_get() if not found):
 *         "string timezone name, used to render item expiration datetime",
 *
 *     set_timezone_name(optional: got 'UTC' if not found):
 *         "string timezone name, used to register item expiration datetime"
 * ]
 *
 * Column name selection (column_name_select):
 * Selection allows to define keys list scope for selection and modification.
 *
 * Templates used:
 * template_where, template_order, template_update
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\register\table\model;

use liberty_code\sql\register\model\DefaultRegister;

use DateTime;
use DateTimeZone;
use DateInterval;
use liberty_code\register\item\exception\ItemInvalidFormatException;
use liberty_code\register\register\exception\KeyFoundException;
use liberty_code\register\register\exception\KeyNotFoundException;
use liberty_code\sql\database\command\clause\library\ConstExpressionClause;
use liberty_code\sql\database\command\clause\library\ConstConditionClause;
use liberty_code\sql\database\command\clause\library\ConstFromClause;
use liberty_code\sql\database\command\clause\library\ConstOrderClause;
use liberty_code\sql\database\command\clause\library\ConstLimitClause;
use liberty_code\sql\database\command\clause\library\ConstSetClause;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;
use liberty_code\sql\database\command\update\library\ConstUpdateCommand;
use liberty_code\sql\database\command\factory\api\CommandFactoryInterface;
use liberty_code\sql\register\library\ConstRegister;
use liberty_code\sql\register\table\library\ConstTableRegister;
use liberty_code\sql\register\table\exception\ConfigInvalidFormatException;
use liberty_code\sql\register\table\exception\KeyInvalidFormatException;
use liberty_code\sql\register\table\exception\GetConfigInvalidFormatException;
use liberty_code\sql\register\table\exception\SetConfigInvalidFormatException;



class TableRegister extends DefaultRegister
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Index array of cached all string column names
     * @var null|array
     */
    protected $tabCacheColName;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(
        array $tabConfig = null,
        CommandFactoryInterface $objCommandFactory = null
    )
    {
        // Init var
        $this->tabCacheColName = null;

        // Call parent constructor
        parent::__construct($tabConfig, $objCommandFactory);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRegister::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if specified item data is valid.
     *
     * Item data array format:
     * @see TableRegister item data array format.
     *
     * @param array $itemData
     * @return boolean
     */
    protected function checkItemDataValid(array $itemData)
    {
        // Init var
        $objExpireDt = $this->getObjExpireTimeoutDtFromItemData($itemData);
        $objDtNow = (new DateTime())
            ->setTimezone(
                new DateTimeZone($this->getStrSetTimezoneName())
            );
        $result = (
            // Check item found
            isset($itemData[ConstTableRegister::TAB_ITEM_DATA_KEY_ITEM]) &&

            // Check expiration
            (
                is_null($objExpireDt) ||
                ($objExpireDt > $objDtNow)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of all string column names.
     *
     * @return array
     * @throws ConfigInvalidFormatException
     */
    protected function getTabColName()
    {
        // Get column names, if required
        if(!is_array($this->tabCacheColName))
        {
            // Init var
            $objCommandFactory = $this->getObjCommandFactory();
            $tabConfig = $this->getTabConfig();

            // Check configuration
            $this->beanSetValidValue(ConstRegister::DATA_KEY_DEFAULT_CONFIG, $tabConfig);

            // Check command factory valid
            $this->tabCacheColName = array();
            if(!is_null($objCommandFactory))
            {
                $tabCmdConfig = array(
                    ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT => [
                        [ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => '*']
                    ],
                    ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM => [
                        [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstRegister::TAB_CONFIG_KEY_TABLE_NAME]]
                    ],
                    ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT => [
                        ConstLimitClause::TAB_CONFIG_KEY_COUNT => 0
                    ]
                );

                // Get command
                $objCommand = $objCommandFactory->getObjSelectCommand($tabCmdConfig);

                // Run query result
                if(
                    (($resultQuery = $objCommand->executeResult()) !== false) &&
                    (($intCountCol = $resultQuery->getIntCountCol()) > 0)
                )
                {
                    // Get column names
                    for($cpt = 0; $cpt < $intCountCol; $cpt++)
                    {
                        $this->tabCacheColName[] = $resultQuery->getStrColName($cpt);
                    }
                }
            }
        }

        // Return result
        return $this->tabCacheColName;
    }



    /**
     * Get index array of string column names selected.
     *
     * @return array
     */
    protected function getTabColNameSelect()
    {
        // Init var
        $tabColNm = $this->getTabColName();
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstTableRegister::TAB_CONFIG_KEY_COLUMN_NAME_SELECT, $tabConfig) ?
                array_filter(
                    array_values($tabConfig[ConstTableRegister::TAB_CONFIG_KEY_COLUMN_NAME_SELECT]),
                    function($strKey) use ($tabColNm) {
                        return in_array($strKey, $tabColNm);
                    }
                ) :
                $tabColNm
        );

        // Return result
        return $result;
    }



    /**
     * Get timezone name,
     * used to render item expiration timeout datetime.
     *
     * @return string
     */
    public function getStrGetTimezoneName()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstTableRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME, $tabConfig) ?
                $tabConfig[ConstTableRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME] :
                date_default_timezone_get()
        );

        // Return result
        return $result;
    }



    /**
     * Get timezone name,
     * used to register item expiration timeout datetime.
     *
     * @return string
     */
    public function getStrSetTimezoneName()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstTableRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME, $tabConfig) ?
                $tabConfig[ConstTableRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME] :
                'UTC'
        );

        // Return result
        return $result;
    }



    /**
     * Get item data,
     * from specified item.
     * Overwrite it to implement specific values formatting.
     *
     * Configuration array format:
     * @see setItem() configuration array format.
     *
     * Return array format:
     * @see TableRegister item data array format.
     *
     * @param mixed $item
     * @param array $tabConfig = null
     * @return array
     */
    protected function getItemDataFromItem($item, array $tabConfig = null)
    {
        // Init var
        $result = array(
            ConstTableRegister::TAB_ITEM_DATA_KEY_ITEM => $item,
            ConstTableRegister::TAB_ITEM_DATA_KEY_EXPIRE_TIMEOUT_DATETIME => $this->getObjExpireTimeoutDtFromConfig($tabConfig)
        );

        // Return result
        return $result;
    }



    /**
     * Get array of item data,
     * from specified command configuration.
     *
     * Return associative array format:
     * Key => Value: Item data: @see TableRegister item data array format.
     *
     * Remove success return true if all items success,
     * false if an error occurs on at least one item.
     *
     * @param array $tabCmdConfig
     * @param boolean &$boolRemoveSuccess = true
     * @return array
     * @throws ConfigInvalidFormatException
     */
    protected function getTabItemDataFromCmdConfig(array $tabCmdConfig, &$boolRemoveSuccess = true)
    {
        // Init var
        $result = array();
        $boolRemoveSuccess = true;
        $objCommandFactory = $this->getObjCommandFactory();
        $tabConfig = $this->getTabConfig();

        // Check configuration
        $this->beanSetValidValue(ConstRegister::DATA_KEY_DEFAULT_CONFIG, $tabConfig);

        // Check command factory valid
        if(!is_null($objCommandFactory))
        {
            // Init var
            $tabConfig = $this->getTabConfig();

            // Set from clause
            $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstRegister::TAB_CONFIG_KEY_TABLE_NAME]]
            );

            // Set where clause: Set pattern, if required
            if(array_key_exists(ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE, $tabConfig))
            {
                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => array_merge(
                        [
                            [
                                ConstConditionClause::TAB_CONFIG_KEY_PATTERN =>
                                    $tabConfig[ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE]
                            ]
                        ],
                        (
                        array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE, $tabCmdConfig) ?
                            [
                                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                            ] :
                            []
                        )
                    )
                );
            }

            // Set order clause: Set pattern
            if(array_key_exists(ConstRegister::TAB_CONFIG_KEY_PATTERN_ORDER, $tabConfig))
            {
                // Init order clause, if required
                if(!isset($tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER]))
                {
                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER] = array();
                }

                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER][] = array(
                    ConstOrderClause::TAB_CONFIG_KEY_PATTERN =>
                        $tabConfig[ConstRegister::TAB_CONFIG_KEY_PATTERN_ORDER]
                );
            }

            // Get command
            $objCommand = $objCommandFactory->getObjSelectCommand($tabCmdConfig);

            // Run query result
            if(
                (($resultQuery = $objCommand->executeResult()) !== false) &&
                (($data = $resultQuery->getFetchData()) !== false)
            )
            {
                $resultQuery->close();
                $tabKeyNotValid = array();
                foreach($data as $strKey => $itemData)
                {
                    // Get item data
                    $itemData = $this->getItemDataFromItemDataSet($itemData);

                    // Register item, if required (item is valid)
                    if(!is_null($itemData))
                    {
                        if(is_array($itemData) && $this->checkItemDataValid($itemData))
                        {
                            $result[$strKey] = $itemData;
                        }
                        else
                        {
                            $tabKeyNotValid[] = $strKey;
                        }
                    }
                }

                // Remove items, if required
                if(count($tabKeyNotValid) > 0)
                {
                    // Remove items
                    $tabItemDataNotValid = array_combine(
                        $tabKeyNotValid,
                        array_fill(0, count($tabKeyNotValid), null)
                    );
                    $boolRemoveSuccess = $this->setTabItemData($tabItemDataNotValid);
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get item data,
     * from specified key.
     *
     * Return array format:
     * @see TableRegister item data array format.
     *
     * Remove success return true if all items success,
     * false if an error occurs on at least one item.
     *
     * @param string $strKey
     * @param boolean &$boolRemoveSuccess = true
     * @return null|array
     * @throws KeyInvalidFormatException
     */
    protected function getItemDataFromKey(
        $strKey,
        &$boolRemoveSuccess = true
    )
    {
        // Check arguments
        KeyInvalidFormatException::setCheck($strKey, $this->getTabColNameSelect());

        // Init var
        $tabCmdConfig = array(
            ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT => [
                [ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME => $strKey]
            ]
        );

        // Get item data, if required
        $tabItemData = $this->getTabItemDataFromCmdConfig($tabCmdConfig, $boolRemoveSuccess);
        $result = (
            array_key_exists($strKey, $tabItemData) ?
                $tabItemData[$strKey] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get item data,
     * from specified item data, used in support, to be set.
     * Overwrite it to implement specific feature.
     *
     * Return array format:
     * @see TableRegister item data array format.
     *
     * @param string $strItemData
     * @return null|array
     */
    protected function getItemDataFromItemDataSet($strItemData)
    {
        // Return result
        return unserialize($strItemData);
    }



    /**
     * Get string item data, used in support, to be set,
     * from specified item data.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see TableRegister item data array format.
     *
     * @param null|array $itemData
     * @return string
     */
    protected function getStrItemDataSetFromItemData($itemData)
    {
        // Return result
        return serialize($itemData);
    }



    /**
     * Get expiration timeout datetime,
     * from specified configuration.
     *
     * Configuration array format:
     * @see setItem() configuration array format.
     *
     * @param array $tabConfig = null
     * @return null|DateTime
     */
    protected function getObjExpireTimeoutDtFromConfig(array $tabConfig = null)
    {
        // Init var
        $result = (
            (
                (!is_null($tabConfig)) &&
                array_key_exists(ConstTableRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME, $tabConfig)
            ) ?
                // Set specific time
                (
                    ($tabConfig[ConstTableRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME] instanceof DateTime) ?
                        $tabConfig[ConstTableRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME] :
                        (new DateTime())->setTimestamp($tabConfig[ConstTableRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME])
                ) :
                (
                    (
                        (!is_null($tabConfig)) &&
                        array_key_exists(ConstTableRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND, $tabConfig)
                    ) ?
                        // Add specific second, from datetime now
                        (new DateTime())->add(new DateInterval(sprintf(
                            'PT%1$dS',
                            $tabConfig[ConstTableRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND]
                        ))) :
                        null
                )
        );
        $result = (
            (!is_null($result)) ?
                $result->setTimezone(new DateTimeZone($this->getStrSetTimezoneName())) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get expiration timeout datetime,
     * from specified item data.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see TableRegister item data array format.
     *
     * @param array $itemData
     * @return null|DateTime
     */
    protected function getObjExpireTimeoutDtFromItemData(array $itemData)
    {
        // Init var
        $result = (
            (
                array_key_exists(ConstTableRegister::TAB_ITEM_DATA_KEY_EXPIRE_TIMEOUT_DATETIME, $itemData) &&
                ($itemData[ConstTableRegister::TAB_ITEM_DATA_KEY_EXPIRE_TIMEOUT_DATETIME] instanceof DateTime)
            ) ?
                $itemData[ConstTableRegister::TAB_ITEM_DATA_KEY_EXPIRE_TIMEOUT_DATETIME] :
                null
        );
        $result = (
            (!is_null($result)) ?
                $result->setTimezone(new DateTimeZone($this->getStrSetTimezoneName())) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get expiration timeout datetime,
     * from specified key.
     *
     * @param string $strKey
     * @return null|DateTime
     */
    public function getObjExpireTimeoutDt($strKey)
    {
        // Init var
        $itemData = $this->getItemDataFromKey($strKey);
        $result = (
            is_null($itemData) ?
                null :
                $this->getObjExpireTimeoutDtFromItemData($itemData)
        );
        $result = (
            (!is_null($result)) ?
                $result->setTimezone(new DateTimeZone($this->getStrGetTimezoneName())) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get item,
     * from specified item data.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see TableRegister item data array format.
     *
     * @param array $itemData
     * @return null|mixed
     */
    protected function getItemFromItemData(array $itemData)
    {
        // Init var
        $result = (
            array_key_exists(ConstTableRegister::TAB_ITEM_DATA_KEY_ITEM, $itemData) ?
                $itemData[ConstTableRegister::TAB_ITEM_DATA_KEY_ITEM] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getItem($strKey)
    {
        // Init var
        $itemData = $this->getItemDataFromKey($strKey);
        $result = (
            is_null($itemData) ?
                null :
                $this->getItemFromItemData($itemData)
        );

        // Return result
        return $result;
    }



    /**
     * Get command select configuration,
     * with specified get configuration.
     *
     * Selection configuration format:
     * @see getTabItem() configuration array format.
     *
     * @param array $tabConfig = null
     * @return array
     * @throws GetConfigInvalidFormatException
     */
    protected function getTabCommandConfigSelect(array $tabConfig = null)
    {
        // Check arguments
        GetConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabColNm = $this->getTabColNameSelect();
        $result = (
            isset($tabConfig[ConstTableRegister::TAB_GET_CONFIG_KEY_LIST_KEY]) ?
                array_filter(
                    array_values($tabConfig[ConstTableRegister::TAB_GET_CONFIG_KEY_LIST_KEY]),
                    function($strKey) use ($tabColNm) {
                        return in_array($strKey, $tabColNm);
                    }
                ) :
                array()
        );
        $result = ((count($result) > 0) ? $result : $tabColNm);

        // Return result
        return $result;
    }



    /**
     * Selection configuration format:
     * [
     *     list_key(optional): [
     *         "string key 1",
     *         ...,
     *         "string key N"
     *     ]
     * ]
     *
     * @inheritdoc
     */
    public function getTabItem(array $tabConfig = null)
    {
        // Init var
        $tabGetConfig = $tabConfig;
        $tabCmdConfig = array(
            ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT =>
                $this->getTabCommandConfigSelect($tabGetConfig)
        );

        // Register items
        $tabItemData = $this->getTabItemDataFromCmdConfig($tabCmdConfig);
        $result = array_map(
            function($itemData) {
                return $this->getItemFromItemData($itemData);
            },
            $tabItemData
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Remove cache.
     */
    public function removeCache()
    {
        $this->tabCacheColName = null;
    }



    /**
     * Set specified associative array of item data.
     * Return true if all items success,
     * false if an error occurs on at least one item.
     *
     * Associative array of item data format:
     * Key => Value: null|Item data: @see TableRegister item data array format.
     *
     * @param array $tabItemData
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function setTabItemData(array $tabItemData)
    {
        // Init var
        $result = false;
        $objCommandFactory = $this->getObjCommandFactory();
        $tabConfig = $this->getTabConfig();

        // Check configuration
        $this->beanSetValidValue(ConstRegister::DATA_KEY_DEFAULT_CONFIG, $tabConfig);

        // Check command factory valid
        if(!is_null($objCommandFactory))
        {
            // Get info
            $tabCmdConfig = array();

            // Set from clause
            $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstRegister::TAB_CONFIG_KEY_TABLE_NAME]]
            );

            // Set set clause
            $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET] = array_values(array_map(
                function($strKey, $itemData)  {
                    return array(
                        ConstSetClause::TAB_CONFIG_KEY_OPERAND => $strKey,
                        ConstSetClause::TAB_CONFIG_KEY_VALUE => [
                            ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $this->getStrItemDataSetFromItemData($itemData)
                        ]
                    );
                },
                array_keys($tabItemData),
                $tabItemData
            ));

            // Set pattern
            if(array_key_exists(ConstRegister::TAB_CONFIG_KEY_PATTERN_SET, $tabConfig))
            {
                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET][] = array(
                    ConstSetClause::TAB_CONFIG_KEY_PATTERN =>
                        $tabConfig[ConstRegister::TAB_CONFIG_KEY_PATTERN_SET]
                );
            }

            // Set where clause: Set pattern
            if(array_key_exists(ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE, $tabConfig))
            {
                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                        [
                            ConstConditionClause::TAB_CONFIG_KEY_PATTERN =>
                                $tabConfig[ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE]
                        ]
                    ]
                );
            }

            // Get command
            $objCommand = $objCommandFactory->getObjUpdateCommand($tabCmdConfig);

            // Run query
            $result = ($objCommand->execute() !== false);
        }

        // Return result
        return $result;
    }



    /**
     * Set specified item.
     * Return true if success, false if an error occurs.
     *
     * Setting configuration array format:
     * @see setItem() configuration array format.
     *
     * @param string $strKey
     * @param mixed $item
     * @param array $tabConfig = null
     * @return boolean
     * @throws KeyInvalidFormatException
     * @throws ItemInvalidFormatException
     * @throws SetConfigInvalidFormatException
     */
    protected function setItemEngine($strKey, $item, array $tabConfig = null)
    {
        // Check arguments
        KeyInvalidFormatException::setCheck($strKey, $this->getTabColNameSelect());
        ItemInvalidFormatException::setCheck($item);
        SetConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabSetConfig = $tabConfig;
        $itemData = $this->getItemDataFromItem($item, $tabSetConfig);
        $result = $this->setTabItemData(array($strKey => $itemData));

        // Return result
        return $result;
    }



    /**
     * Setting configuration array format:
     * [
     *     expire_timeout_time(optional): integer timestamp / DateTime,
     *
     *     expire_timeout_second(optional): integer number of seconds, from datetime now
     * ]
     *
     * @inheritdoc
     * @throws KeyNotFoundException
     */
    public function setItem($strKey, $item, array $tabConfig = null)
    {
        // Add item, if not found
        if($this->checkItemExists($strKey))
        {
            $result = $this->setItemEngine($strKey, $item, $tabConfig);
        }
        // If item not found, throw exception
        else
        {
            throw new KeyNotFoundException($strKey);
        }

        // Return result
        return $result;
    }



    /**
     * Setting configuration array format:
     * @see setItem() configuration array format.
     *
     * @inheritdoc
     * @throws KeyFoundException
     */
    public function addItem($strKey, $item, array $tabConfig = null)
    {
        // Add item, if not found
        if(!$this->checkItemExists($strKey))
        {
            $result = $this->setItemEngine($strKey, $item, $tabConfig);
        }
        // If item found, throw exception
        else
        {
            throw new KeyFoundException($strKey);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws KeyNotFoundException
     */
    public function removeItem($strKey)
    {
        // Remove item, if found
        if($this->checkItemExists($strKey))
        {
            $result = $this->setTabItemData(array($strKey => null));
        }
        // If item not found, throw exception
        else
        {
            throw new KeyNotFoundException($strKey);
        }

        // Return result
        return $result;
    }



    /**
     * Selection configuration format:
     * @see getTabItem() configuration array format.
     *
     * @inheritdoc
     */
    public function removeItemAll(array $tabConfig = null)
    {
        // Init var
        $tabGetConfig = $tabConfig;
        $tabKey = $this->getTabKey($tabGetConfig);
        $tabItemData = array_combine(
            $tabKey,
            array_fill(0, count($tabKey), null)
        );
        $result = $this->setTabItemData($tabItemData);

        // Return result
        return $result;
    }



    /**
     * Remove all items not valid.
     * Return true if all items success,
     * false if an error occurs on at least one item.
     *
     * @return boolean
     */
    public function removeItemAllNotValid()
    {
        //Init var
        $result = true;
        $tabCmdConfig = array(
            ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT =>
                $this->getTabCommandConfigSelect()
        );

        // Remove items, if required (try getting valid item data, remove else)
        $this->getTabItemDataFromCmdConfig($tabCmdConfig, $result);

        // Return result
        return $result;
    }



}