<?php
/**
 * Description :
 * This class allows to define default SQL database statement class.
 * Can be consider is base of all SQL database statement type.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\statement\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\sql\database\statement\api\StatementInterface;

use liberty_code\sql\database\statement\library\ConstStatement;
use liberty_code\sql\database\statement\exception\ParamInvalidFormatException;



abstract class DefaultStatement extends FixBean implements StatementInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabParam = null
     */
    public function __construct(array $tabParam = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init parameters config, if required
        if(!is_null($tabParam))
        {
            $this->setParam($tabParam);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstStatement::DATA_KEY_DEFAULT_PARAM))
        {
            $this->beanAdd(ConstStatement::DATA_KEY_DEFAULT_PARAM, array());
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstStatement::DATA_KEY_DEFAULT_PARAM
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstStatement::DATA_KEY_DEFAULT_PARAM:
                    ParamInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabParam()
    {
        // Return result
        return $this->beanGet(ConstStatement::DATA_KEY_DEFAULT_PARAM);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setParam(array $tabParam)
    {
        $this->beanSet(ConstStatement::DATA_KEY_DEFAULT_PARAM, $tabParam);
    }



}