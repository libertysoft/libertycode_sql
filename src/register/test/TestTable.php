<?php
/**
 * GET arguments:
 * - delete: = 1: Remove data.
 * - hydrate: = 1: Hydrate data.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/test/HelpInsertTestTable.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;
use liberty_code\sql\register\table\model\TableRegister;



// Init var
$objDtNow = new DateTime();
$objCommandFacto = new StandardCommandFactory($objConnection);
$tabConfig = array(
    'table_name' => $strTableNmPrm,
    'column_name_select' => [
        $strColNmPrmNm1,
        $strColNmPrmNm2,
        $strColNmPrmNm3

    ],
    'pattern_where' => sprintf(
        '%1$s = %2$s',
        $objConnection->getStrEscapeName($strColNmPrmType),
        $objConnection->getStrEscapeValue('param_test')
    ),
    'pattern_order' => sprintf(
        '%1$s ASC',
        $objConnection->getStrEscapeName($strColNmPrmNm1)
    ),
    'pattern_column' => sprintf(
        '%1$s, %2$s, %3$s',
        $objConnection->getStrEscapeName($strColNmPrmType),
        $objConnection->getStrEscapeName($strColNmPrmDateCreate),
        $objConnection->getStrEscapeName($strColNmPrmDateUpdate)
    ),
    'pattern_value' => sprintf(
        '%1$s, NOW(), NOW()',
        $objConnection->getStrEscapeValue('param_test')
    ),
    'pattern_set' => sprintf(
        '%1$s = NOW()',
        $objConnection->getStrEscapeName($strColNmPrmDateUpdate)
    )
);
$objRegister = new TableRegister($tabConfig, $objCommandFacto);



// Datetime now
echo('Datetime now: <pre>');
var_dump((clone $objDtNow)->setTimezone(new DateTimeZone($objRegister->getStrSetTimezoneName())));
echo('</pre>');

echo('<br /><br /><br />');



// Test item not valid remove
echo('Test remove all items, not valid: <br />');
echo('<pre>');var_dump($objRegister->removeItemAllNotValid());echo('</pre>');
echo('<br />');
//die;

echo('<br /><br /><br />');



// Test item add
$test1 = 'Test 1';
$test3 = true;
$tabData = array(
    $strColNmPrmNm1 => [
        'Name test',
        ['expire_timeout_time' => (clone $objDtNow)->add(new DateInterval('PT5S'))]
    ], // Ok
    $strColNmPrmNm2 => [
        7,
        ['expire_timeout_second' => 20]
    ], // Ok
    'key_1' => [
        $test3
    ], // Ko: bad key format: out of scope
    3 => [
        3.7
    ], // Ko: bad key format
    $strColNmPrmNm3 => [
        null
    ] // Ko: bad value format
);

foreach($tabData as $strKey => $tabInfo)
{
    $value = $tabInfo[0];
    $tabConfig = (isset($tabInfo[1]) ? $tabInfo[1] : null);

    echo('Test add item "'.$strKey.'": <br />');
    try{
        echo('<pre>');var_dump($objRegister->addItem($strKey, $value, $tabConfig));echo('</pre>');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test item check/get
$tabKey = array(
    $strColNmPrmNm1, // Found
    $strColNmPrmNm3, // Not found
    $strColNmPrmNm2, // Found
    'key_1', // Bad key format: out of scope
    3 // Bad key format
);

foreach($tabKey as $strKey)
{
    echo('Test check, get item "'.$strKey.'": <br />');
    try{
        echo('Exists: <pre>');var_dump($objRegister->checkItemExists($strKey));echo('</pre>');
        echo('Get: <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test item set
$tabData = array(
    $strColNmPrmNm1 =>
        [
            'Name test updated'
        ], // Ok
    'key_1' =>
        [
            'Test 1 update'
        ], // Ko: bad key format: out of scope
    $strColNmPrmNm2 => [
        null
    ], // Ko: bad value format
    $strColNmPrmNm3 => [
        'test'
    ] // Ko: Not found
);

foreach($tabData as $strKey => $tabInfo)
{
    $value = $tabInfo[0];
    $tabConfig = (isset($tabInfo[1]) ? $tabInfo[1] : null);

    echo('Test set item "'.$strKey.'": <br />');
    try{
        echo('<pre>');var_dump($objRegister->setItem($strKey, $value, $tabConfig));echo('</pre>');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test setting item: <br />');
echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test item put
$tabData = array(
    $strColNmPrmNm2 => [
        8,
        ['expire_timeout_time' => (clone $objDtNow)->add(new DateInterval('PT5S'))]
    ], // Ok: update
    $strColNmPrmNm3 => [
        'fnm.nm@test.com'
    ], // Ok: create
    'key_1' => [
        'Test 1 update'
    ], // Ko: bad key format: out of scope
    true => [
        'Test N'
    ] // Ko: bad key format
);

foreach($tabData as $strKey => $tabInfo)
{
    $value = $tabInfo[0];
    $tabConfig = (isset($tabInfo[1]) ? $tabInfo[1] : null);

    echo('Test put item "'.$strKey.'": <br />');
    try{
        echo('<pre>');var_dump($objRegister->putItem($strKey, $value, $tabConfig));echo('</pre>');
    } catch(\Exception $e) {
        echo(get_class($e) . ' - ' . htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('Test putting item: <br />');
echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test item get, with list key option
echo('Test item get, with list key option: <br />');
$tabGetConfig = array(
    'list_key' => [
        $strColNmPrmNm1, // Found
        'test', // Not found
        $strColNmPrmNm2 // Found
    ]
);
foreach($objRegister->getTabKey($tabGetConfig) as $strKey)
{
    echo('Get "'.$strKey.'": <pre>');var_dump($objRegister->getItem($strKey));echo('</pre>');
}

echo('<br /><br /><br />');



// Test item remove, if required
if(trim(ToolBoxTable::getItem($_GET, 'delete', '0')) == '1')
{
    $tabKey = array(
        $strColNmPrmNm2, // Ok
        'key_1', // Ko: bad key format: out of scope
        3 // Ko: bad key format
    );

    foreach($tabKey as $strKey)
    {
        echo('Test remove item "'.$strKey.'": <br />');
        try{
            echo('<pre>');var_dump($objRegister->removeItem($strKey));echo('</pre>');
        } catch(\Exception $e) {
            echo(htmlentities($e->getMessage()));
            echo('<br />');
        }
        echo('<br />');
    }

    echo('Test removing item: <br />');
    echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
    echo('<br />');

    echo('<br /><br /><br />');



    // Test item remove, with list key option
    $tabGetConfig = array(
        'list_key' => [
            $strColNmPrmNm1, // Found
            'test', // Not found
            $strColNmPrmNm3 // Found
        ]
    );
    $objRegister->removeItemAll($tabGetConfig);

    echo('Test removing item, with list key option: <br />');
    echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
    echo('<br />');

    echo('<br /><br /><br />');
}



// Test item hydrate, if required
if(trim(ToolBoxTable::getItem($_GET, 'hydrate', '0')) == '1')
{
    $tabItem = array(
        $strColNmPrmNm1 => 'Name test hydrated', // Ok: update
        $strColNmPrmNm3 => 'fnm.nm@test-hydrated.com' // Ok: update
    );
    $objRegister->hydrateItem($tabItem);

    echo('Test hydrate item: <br />');
    echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
    echo('<br />');

    echo('<br /><br /><br />');



    // Test item hydrate flush, if required
    if(trim(ToolBoxTable::getItem($_GET, 'delete', '0')) == '1')
    {
        $objRegister->hydrateItem();

        echo('Test hydrate flush: <br />');
        echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
        echo('<br />');

        echo('<br /><br /><br />');
    }
}



// Remove test database, if required
require_once($strRootAppPath . '/src/database/test/HelpRemoveTestDb.php');


