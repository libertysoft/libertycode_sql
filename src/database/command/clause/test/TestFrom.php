<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/clause/test/Include.php');

// Use
use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardFromClause;



// Init var
$config_1 = array(
    [
        'db_name' => 'db',
        'table_name' => 'table_1',
        'alias' => 'Tab 1'
    ],
    [
        'db_name' => 'db',
        'table_name' => 'table_2'
    ],
    'table_3',
    [
        'alias' => 'Tab 4',
        'pattern' => '`table_4`%s'
    ],
    [
        'pattern' => '`table_5`'
    ]
);

$config_2 = array(
    [
        'db_name' => 'db',
        'alias' => 'Tab 1'
    ]
);

$config_3 = array(
    [
        'db_name' => 'db',
        'table_name' => 'table_1',
        'alias' => 'Tab 1'
    ],
    [
        'left' => [
            'db_name' => 'db',
            'table_name' => 'table_2',
            'alias' => 'Tab_2'
        ],
        'right' => 'table_3',
        'on' => [
            'content' => [
                [
                    'operand' => [
                        'table_name' => 'Tab_2',
                        'column_name' => 'col_1'
                    ],
                    'value' => [
                        'table_name' => 'table_3',
                        'column_name' => 'col_1'
                    ]
                ]
            ]
        ]
    ],
    'table_4',
    [
        'type' => 'right_outer',
        'left' => 'table_5',
        'right' => 'table_6',
        'on' => [
            'type' => 'or',
            'content' => [
                [
                    'operand' => [
                        'table_name' => 'table_5',
                        'column_name' => 'col_2'
                    ],
                    'value' => [
                        'table_name' => 'table_6',
                        'column_name' => 'col_2'
                    ]
                ],
                [
                    'operand' => [
                        'table_name' => 'table_5',
                        'column_name' => 'col_3'
                    ],
                    'value' => [
                        'table_name' => 'table_6',
                        'column_name' => 'col_3'
                    ]
                ]
            ]
        ]
    ]
);

$config_4 = array(
    [
        'right' => 'table_2',
        'on' => [
            'content' => [
                [
                    'operand' => [
                        'table_name' => 'table_1',
                        'column_name' => 'col_1'
                    ],
                    'value' => [
                        'table_name' => 'table_2',
                        'column_name' => 'col_1'
                    ]
                ]
            ]
        ]
    ]
);


$config_5 = array(
    [
        'left' => 'table_1',
        'on' => [
            'content' => [
                [
                    'operand' => [
                        'table_name' => 'table_1',
                        'column_name' => 'col_1'
                    ],
                    'value' => [
                        'table_name' => 'table_2',
                        'column_name' => 'col_1'
                    ]
                ]
            ]
        ]
    ]
);

$config_6 = array(
    [
        'left' => 'table_1',
        'right' => 'table_2',
        'on' => [
            'content' => [
                [
                    'operand' => [
                        'table_name' => 'table_1',
                        'column_name' => 'col_1'
                    ],
                    'value' => [
                        'table_name' => 'table_2',
                        'column_name' => 'col_1'
                    ]
                ]
            ]
        ]
    ]
);

$config_7 = array(
    [
        'left' => 'table_1',
        'right' => 'table_2'
    ]
);

$config_8 = array(
    [
        'type' => 'inner',
        'left' => 'table_1',
        'right' => [
            'type' => 'right_outer',
            'left' => [
                'type' => 'left_outer',
                'left' => 'table_3',
                'right' => 'table_4',
                'on' => [
                    'content' => [
                        [
                            'operand' => [
                                'table_name' => 'table_3',
                                'column_name' => 'col_1'
                            ],
                            'value' => [
                                'table_name' => 'table_4',
                                'column_name' => 'col_1'
                            ]
                        ]
                    ]
                ]
            ],
            'right' => 'table_2',
            'on' => [
                'content' => [
                    [
                        'operand' => [
                            'table_name' => 'table_2',
                            'column_name' => 'col_1'
                        ],
                        'value' => [
                            'table_name' => 'table_3',
                            'column_name' => 'col_1'
                        ]
                    ]
                ]
            ]
        ],
        'on' => [
            'content' => [
                [
                    'operand' => [
                        'table_name' => 'table_1',
                        'column_name' => 'col_1'
                    ],
                    'value' => [
                        'table_name' => 'table_2',
                        'column_name' => 'col_1'
                    ]
                ]
            ]
        ]
    ]
);

$tabConfig = array(
    $config_1, // Ok
    $config_2, // Ko
    $config_3, // Ok
    $config_4, // Ko
    $config_5, // Ko
    $config_6, // Ok
    $config_7, // Ko
    $config_8 // Ok
);



// Test config
process(
    $tabConfig, 
    function($config)
    {
        return ToolBoxFromClause::checkClauseFromIsValid($config);
    },
    function($config) use ($objConnection)
    {
        return ToolBoxStandardFromClause::getStrClauseFromCommand($objConnection, $config);
    }
);


