<?php
/**
 * GET arguments:
 * - delete: = 1: Remove data.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/test/HelpCreateTestTable.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\persistence\data\model\DataPersistor;
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;



// Init var
$objCommandFacto = new StandardCommandFactory($objConnection);
$objPersistor = new DataPersistor($objCommandFacto);
$tabConfig = array(
    'table_name' => $strTableNmCnf,
    'column_name_id' => $strColNmCnfType,
    'column_name_attribute_name' => $strColNmCnfNm,
    'column_name_attribute_value' => $strColNmCnfVal,
    'opt_id_new' => 'persistor',
    //*
    'attribute_name_select' => [
        'key_1',
        'key_2'
    ],
    //*/
    //'pattern_select' => '',
    //'pattern_where' => '',
    //'pattern_order' => '',
    'pattern_column' => sprintf(
        '%1$s, %2$s',
        $objConnection->getStrEscapeName($strColNmCnfDateCreate),
        $objConnection->getStrEscapeName($strColNmCnfDateUpdate)
    ),
    'pattern_value' => 'NOW(), NOW()',
    'pattern_set' => sprintf(
        '%1$s = NOW()',
        $objConnection->getStrEscapeName($strColNmCnfDateUpdate)
    )
);



// Test insert 1 config
echo('Test insert 1 config: <br />');
$data = array(
    $strColNmCnfType => null,
    'key_1' => 'Value 1',
    'key_2' => 2,
    'key_3' => null
);
$id = null;

echo('Execute: <pre>');var_dump($objPersistor->createData($data, $tabConfig, $id));echo('</pre>');
echo('Id: <pre>');var_dump($id);echo('</pre>');

echo('<br /><br /><br />');



// Test insert several configs
echo('Test insert multi configs: <br />');
$data1 = array(
    $strColNmCnfType => null,
    'key_1' => 'Value 1_1',
    'key_2' => 1.2,
    'key_3' => null
);
$data2 = array(
    $strColNmCnfType => null,
    'key_1' => 'Value 2_1',
    'key_2' => 2.2,
    'key_3' => null
);
$data3 = array(
    $strColNmCnfType => null,
    'key_1' => 'Value 3_1',
    'key_2' => 3,
    'key_3' => null
);
$tabData = array(
    $data1,
    $data2,
    $data3
);
$tabId = array();

echo('Execute: <pre>');var_dump($objPersistor->createTabData($tabData, $tabConfig, $tabId));echo('</pre>');
echo('Id: <pre>');var_dump($tabId);echo('</pre>');

echo('<br /><br /><br />');



// Test select 1 config
echo('Test select 1 config: <br />');

echo('Get: <pre>');print_r($objPersistor->getData($id, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test select several configs
echo('Test select multi configs: <br />');

echo('Get: <pre>');var_dump($objPersistor->getTabData($tabId, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test search several configs
echo('Test search multi configs: <br />');

$tabConfigRequest = array(
    'select' => [
        ['column_name' => 'cnf_type'],
        ['column_name' => 'cnf_name'],
        ['column_name' => 'cnf_value']
    ],
    'from' => [
        ['table_name' => 'config']
    ],
    'where' => [
        'content' => [
            [
                'operand' => ['column_name' => 'cnf_type'],
                'operator' => 'less',
                'value' => ['value' => $tabId[1]]
            ],
            [
                'operand' => ['column_name' => 'cnf_name'],
                'operator' => 'equal',
                'value' => ['value' => 'key_3'],
                'not' => true
            ]
        ]
    ],
    'order' => [
        [
            'operand' => ['column_name' => 'cnf_type']
        ],
        [
            'operand' => ['column_name' => 'cnf_name']
        ]
    ]
);

echo('Search: <pre>');var_dump($objPersistor->getTabSearchData($tabConfigRequest, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test update 1 config
echo('Test update 1 config: <br />');
$data = array(
    $strColNmCnfType => $id,
    'key_1' => 'Value 1 updated',
    'key_2' => 7,
    'key_3' => null
);

echo('Execute: <pre>');var_dump($objPersistor->updateData($data, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test update several configs
echo('Test update multi configs: <br />');
$tabData[0][$strColNmCnfType] = $tabId[0];
$tabData[0]['key_1'] = 'Value 1_1 updated';
$tabData[0]['key_2'] = 1.21;

$tabData[1][$strColNmCnfType] = $tabId[1];
$tabData[1]['key_1'] = 'Value 2_1 updated';
$tabData[1]['key_2'] = 2.21;

$tabData[2][$strColNmCnfType] = $tabId[2];
$tabData[2]['key_1'] = 'Value 3_1 updated';

echo('Execute: <pre>');var_dump($objPersistor->updateTabData($tabData, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test select several configs
echo('Test select multi configs: <br />');

echo('Get: <pre>');var_dump($objPersistor->getTabData(array_merge(array($id), $tabId), $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Delete configs, if required
if(trim(ToolBoxTable::getItem($_GET, 'delete', '0')) == '1')
{
    // Test delete 1 config
    echo('Test delete 1 config: <br />');

    echo('Execute: <pre>');var_dump($objPersistor->deleteData($data, $tabConfig));echo('</pre>');

    echo('<br /><br /><br />');



    // Test delete several configs
    echo('Test delete multi configs: <br />');

    echo('Execute: <pre>');var_dump($objPersistor->deleteTabData($tabData, $tabConfig));echo('</pre>');

    echo('<br /><br /><br />');



    // Test select several configs
    echo('Test select multi configs: <br />');

    echo('Get: <pre>');var_dump($objPersistor->getTabData(array_merge(array($id), $tabId), $tabConfig));echo('</pre>');

    echo('<br /><br /><br />');
}



// Remove test database, if required
require_once($strRootAppPath . '/src/database/test/HelpRemoveTestDb.php');


