<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\insert\library;



class ConstInsertCommand
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_CLAUSE_FROM = 'from';
    const TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN = 'from_pattern';
    const TAB_CONFIG_KEY_CLAUSE_COLUMN = 'column';
    const TAB_CONFIG_KEY_CLAUSE_COLUMN_PATTERN = 'column_pattern';
    const TAB_CONFIG_KEY_CLAUSE_VALUE = 'value';
    const TAB_CONFIG_KEY_CLAUSE_VALUE_PATTERN = 'value_pattern';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the default insert command configuration standard.';



}