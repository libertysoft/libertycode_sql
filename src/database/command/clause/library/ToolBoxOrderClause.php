<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ConstOrderClause;



class ToolBoxOrderClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

    /**
     * Check if order configuration is valid.
     * Order configuration format:
     * operand (Expression format (@see ToolBoxExpressionClause::checkExpressionIsValid() ))
     *
     * OR
     *
     * {
     *     operand(required: if pattern not found): Expression format,
     *
     *     operator(optional: got 'asc' if operator not found)): "string operator" (@see ConstOrderClause::getTabConfigOperator() ),
     *
     *     pattern(required: if operand not found):
     *         "string sprintf pattern,
     *         where '%1$s' or '%s' replaced by calculated order, if found"
     * }
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkOrderIsValid($config)
    {
        // Init var
        $result =
            // Case simple config: check valid operand expression
            ToolBoxExpressionClause::checkExpressionIsValid($config) ||

            // Case complex config
            (
                // Check valid array
                is_array($config) &&

                // Check valid operand expression
                (
                    (!isset($config[ConstOrderClause::TAB_CONFIG_KEY_OPERAND])) ||
                    ToolBoxExpressionClause::checkExpressionIsValid($config[ConstOrderClause::TAB_CONFIG_KEY_OPERAND])
                ) &&

                // Check valid operator
                (
                    (!isset($config[ConstOrderClause::TAB_CONFIG_KEY_OPERATOR])) ||
                    (
                        is_string($config[ConstOrderClause::TAB_CONFIG_KEY_OPERATOR]) &&
                        in_array($config[ConstOrderClause::TAB_CONFIG_KEY_OPERATOR], ConstOrderClause::getTabConfigOperator())
                    )
                ) &&

                // Check valid pattern
                (
                    (!isset($config[ConstOrderClause::TAB_CONFIG_KEY_PATTERN])) ||
                    (
                        is_string($config[ConstOrderClause::TAB_CONFIG_KEY_PATTERN]) &&
                        (trim($config[ConstOrderClause::TAB_CONFIG_KEY_PATTERN]) != '')
                    )
                ) &&

                // Check minimum requirement
                (
                    isset($config[ConstOrderClause::TAB_CONFIG_KEY_OPERAND]) ||
                    isset($config[ConstOrderClause::TAB_CONFIG_KEY_PATTERN])
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if order clause is valid.
     * Configuration is an index array of orders,
     * where each order uses following format:
     * @see ToolBoxOrderClause::checkOrderIsValid().
     *
     * @param array $tabConfig
     * @return boolean
     */
    public static function checkClauseOrderIsValid(array $tabConfig)
    {
        // Init var
        $result = true;

        // Check each sub-config valid
        $tabKey = array_keys($tabConfig);
        for($intCpt = 0; ($intCpt < count($tabKey)) && $result; $intCpt++)
        {
            $result = static::checkOrderIsValid($tabConfig[$tabKey[$intCpt]]);
        }

        // Return result
        return $result;
    }



}