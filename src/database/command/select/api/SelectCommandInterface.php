<?php
/**
 * Description :
 * This class allows to describe behavior of select SQL command class.
 * Select command allows to design string SQL select command,
 * from following specified configuration:
 * [
 *     select(required: if select_pattern not found): array (@see ToolBoxExpressionClause::checkClauseExpressionIsValid() ),
 *
 *     select_pattern(required: if select not found):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated select clause, if found",
 *
 *     select_distinct(optional: got false if select_distinct not found): true / false,
 *
 *     from(optional): array (@see ToolBoxFromClause::checkClauseFromIsValid() ),
 *
 *     from_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated from clause, if found",
 *
 *     where(optional): array (@see ToolBoxConditionClause::checkClauseConditionIsValid() ),
 *
 *     where_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated where clause, if found",
 *
 *     group(optional): array (@see ToolBoxExpressionClause::checkClauseExpressionIsValid() ),
 *
 *     group_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated group clause, if found",
 *
 *     having(optional): array (@see ToolBoxConditionClause::checkClauseConditionIsValid() ),
 *
 *     having_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated having clause, if found",
 *
 *     order(optional): array (@see ToolBoxOrderClause::checkClauseOrderIsValid() ),
 *
 *     order_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated order clause, if found",
 *
 *     limit(optional): array (@see ToolBoxLimitClause::checkClauseLimitIsValid() ),
 *
 *     limit_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated limit clause, if found"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\select\api;

use liberty_code\sql\database\command\api\CommandInterface;

use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxConditionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxOrderClause;
use liberty_code\sql\database\command\clause\library\ToolBoxLimitClause;



interface SelectCommandInterface extends CommandInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Set select clause config array.
     * Configuration format follows @see ToolBoxExpressionClause::checkClauseExpressionIsValid().
     *
     * @param array $tabConfig
     * @param boolean $boolDistinct = false
     * @return static
     */
    public function setClauseSelect(array $tabConfig, $boolDistinct = false);



    /**
     * Set from clause config array.
     * Configuration format follows @see ToolBoxFromClause::checkClauseFromIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseFrom(array $tabConfig);



    /**
     * Set where clause config array.
     * Configuration format follows @see ToolBoxConditionClause::checkClauseConditionIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseWhere(array $tabConfig);



    /**
     * Set group clause config array.
     * Configuration format follows @see ToolBoxExpressionClause::checkClauseExpressionIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseGroup(array $tabConfig);



    /**
     * Set having clause config array.
     * Configuration format follows @see ToolBoxConditionClause::checkClauseConditionIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseHaving(array $tabConfig);



    /**
     * Set order clause config array.
     * Configuration format follows @see ToolBoxOrderClause::checkClauseOrderIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseOrder(array $tabConfig);



    /**
     * Set limit clause config array.
     * Configuration format follows @see ToolBoxLimitClause::checkClauseLimitIsValid()).
     *
     * @param integer|array $config
     * @return static
     */
    public function setClauseLimit($config);
}