<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\browser\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\browser\library\ConstBrowser;



class ToolBoxBrowser extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get full operation,
     * from specified operation configuration
     * and specified operation value.
     *
     * @param mixed|string|array $config
     * @param mixed $value
     * @return mixed|string|array
     */
    public static function getOperation($config, $value)
    {
        // Init var
        $result = $config;

        // Case configuration is string
        if(is_string($config))
        {
            // Resolve tag value, by argument value
            if($config == ConstBrowser::OPERATION_TAG_VALUE)
            {
                $result = $value;
            }
            else if(strpos($config, ConstBrowser::OPERATION_TAG_VALUE) !== false)
            {
                $result = str_replace(ConstBrowser::OPERATION_TAG_VALUE, strval($value), $config);
            }
        }
        // Case configuration is array
        else if(is_array($config))
        {
            // Run all sub-configuration
            $result = array();
            foreach($config as $subConfigKey => $subConfig)
            {
                // Resolve tag value on sub-configuration
                $result[$subConfigKey] = static::getOperation($subConfig, $value);
            }
        }

        // Return result
        return $result;
    }



}