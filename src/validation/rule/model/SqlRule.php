<?php
/**
 * Description :
 * This class allows to define SQL rule class.
 * SQL rule allows to check specified data validation,
 * using SQL data storage.
 *
 * SQL rule uses the following specified configuration:
 * [
 *     Default rule configuration
 * ]
 *
 * SQL rule uses the following specified validation configuration:
 * [
 *     Default rule validation configuration,
 *
 *     command_factory(required): command factory object,
 *
 *     data_format_callable(optional):
 *         Get data formatted value callback function: mixed function(mixed $value)
 * ]
 *
 * SQL rule uses the following specified error configuration:
 * [
 *     Default rule error configuration,
 *
 *     data_format_callable(optional):
 *         Get data formatted value callback function: mixed function(mixed $value)
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\validation\rule\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\sql\database\command\factory\api\CommandFactoryInterface;
use liberty_code\sql\validation\rule\library\ConstSqlRule;
use liberty_code\sql\validation\rule\exception\ValidConfigInvalidFormatException;
use liberty_code\sql\validation\rule\exception\ErrorConfigInvalidFormatException;



abstract class SqlRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified data is valid,
     * from specified configuration.
     *
     * Configuration format:
     * @see SqlRule validation configuration.
     *
     * @param string $strName
     * @param mixed $value
     * @param array $tabConfig
     * @return boolean
     */
    abstract protected function checkIsValidEngine(
        $strName,
        $value,
        array $tabConfig
    );



    /**
     * @inheritdoc
     * @throws ValidConfigInvalidFormatException
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $value = $this->getValidFormatValue($value, $tabConfig);
        $result = (
            is_array($tabConfig) ?
                $this->checkIsValidEngine(
                    $strName,
                    $value,
                    $tabConfig
                ) :
                false
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get command factory object,
     * from specified configuration.
     *
     * Configuration format:
     * @see SqlRule validation configuration.
     *
     * @param array $tabConfig
     * @return CommandFactoryInterface
     */
    protected function getObjCommandFactory(array $tabConfig)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = $tabValidConfig[ConstSqlRule::TAB_VALID_CONFIG_KEY_COMMAND_FACTORY];

        // Return result
        return $result;
    }



    /**
     * Get specified formatted value,
     * from specified validation configuration.
     *
     * Configuration format:
     * Validation configuration can be provided.
     * @see SqlRule validation configuration.
     *
     * @param mixed $value
     * @param array $tabConfig = null
     * @return mixed
     */
    protected function getValidFormatValue($value, array $tabConfig = null)
    {
        // Return result
        return (
            isset($tabConfig[ConstSqlRule::TAB_VALID_CONFIG_KEY_DATA_FORMAT_CALLABLE]) ?
                $tabConfig[ConstSqlRule::TAB_VALID_CONFIG_KEY_DATA_FORMAT_CALLABLE]($value) :
                $value
        );
    }



    /**
     * Get specified formatted value,
     * from specified error configuration.
     *
     * Configuration format:
     * Error configuration can be provided.
     * @see SqlRule error configuration.
     *
     * @param mixed $value
     * @param array $tabConfig = null
     * @return mixed
     */
    protected function getErrorFormatValue($value, array $tabConfig = null)
    {
        // Return result
        return (
            isset($tabConfig[ConstSqlRule::TAB_ERROR_CONFIG_KEY_DATA_FORMAT_CALLABLE]) ?
                $tabConfig[ConstSqlRule::TAB_ERROR_CONFIG_KEY_DATA_FORMAT_CALLABLE]($value) :
                $value
        );
    }



    /**
     * @inheritdoc
     * @throws ErrorConfigInvalidFormatException
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $value = $this->getErrorFormatValue($value, $tabConfig);
        $result = parent::getStrErrorMessageEngine(
            $strErrorMsgPattern,
            $strName,
            $value,
            $tabConfig
        );

        // Return result
        return $result;
    }



}