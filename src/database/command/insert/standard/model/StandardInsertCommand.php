<?php
/**
 * Description :
 * This class allows to define standard insert SQL command class.
 * Standard insert command allows to design string SQL insert command,
 * from default configuration,
 * and uses standard SQL.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\insert\standard\model;

use liberty_code\sql\database\command\insert\model\DefaultInsertCommand;

use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardExpressionClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardFromClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardValueClause;
use liberty_code\sql\database\command\insert\library\ConstInsertCommand;
use liberty_code\sql\database\command\insert\exception\ConfigInvalidFormatException;
use liberty_code\sql\database\command\insert\standard\library\ConstStandardInsertCommand;



class StandardInsertCommand extends DefaultInsertCommand
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for from clause.
     *
     * @return string
     */
    protected function getStrClauseFromCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM]) ?
            sprintf(
                ConstStandardInsertCommand::SQL_CONFIG_PATTERN_FROM,
                ToolBoxStandardFromClause::getStrClauseFromCommand(
                    $objConnection,
                    $tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for column clause.
     *
     * @return string
     */
    protected function getStrClauseColumnCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN]) ?
            sprintf(
                ConstStandardInsertCommand::SQL_CONFIG_PATTERN_COLUMN,
                ToolBoxStandardExpressionClause::getStrClauseExpressionCommand(
                    $objConnection,
                    $tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for value clause.
     *
     * @return string
     */
    protected function getStrClauseValueCommand()
    {
        // Init var
        $objConnection = $this->getObjConnection();
        $tabConfig = $this->getTabConfig();

        // Get SQL clause
        $result = (
        isset($tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE]) ?
            sprintf(
                ConstStandardInsertCommand::SQL_CONFIG_PATTERN_VALUE,
                ToolBoxStandardValueClause::getStrClauseValueCommand(
                    $objConnection,
                    $tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE]
                )
            ) :
            ''
        );

        // Get pattern value, if required
        if(isset($tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue(
                $tabConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getStrCommand()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Build SQL command
        $strFrom = $this->getStrClauseFromCommand();
        $strColumn = $this->getStrClauseColumnCommand();
        $strValue = $this->getStrClauseValueCommand();
        $result =
            $strFrom . ((trim($strColumn) != '') ? ' ' : '') .
            $strColumn . ((trim($strValue) != '') ? ' ' : '') .
            $strValue;

        // Return result
        return $result;
    }



}