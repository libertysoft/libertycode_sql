<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\command\clause\library\ConstExpressionClause;



class ToolBoxExpressionClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************

    /**
     * Check if expression configuration is valid.
     * Expression configuration format:
     * "string column name" (column_name)
     *
     * OR
     *
     * {
     *     db_name(optional): "string database name",
     *
     *     table_name(required: if column_name, value or pattern not found): "string table name",
     *
     *     column_name(required: if table_name, value or pattern not found):"string column name",
     *
     *     alias(optional): "string alias",
     *
     *     value(required: if table_name, column_name or pattern not found) => 'value 1 (string, numeric, boolean or null)',
     *
     *     pattern(required: if table_name, column_name or value not found):
     *         "string sprintf pattern,
     *         where '%1$s' or '%s' replaced by calculated expression, if found"
     * }
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkExpressionIsValid($config)
    {
        // Init var
        $result =
            // Case simple config
            (
                // Check valid column name
                is_string($config) &&
                (trim($config) != '')
            ) ||
            // Case complex config
            (
                // Check valid array
                is_array($config) &&

                // Check valid db name
                (
                    (!isset($config[ConstExpressionClause::TAB_CONFIG_KEY_DB_NAME])) ||
                    (
                        is_string($config[ConstExpressionClause::TAB_CONFIG_KEY_DB_NAME]) &&
                        (trim($config[ConstExpressionClause::TAB_CONFIG_KEY_DB_NAME]) != '')
                    )
                ) &&

                // Check valid table name
                (
                    (!isset($config[ConstExpressionClause::TAB_CONFIG_KEY_TABLE_NAME])) ||
                    (
                        is_string($config[ConstExpressionClause::TAB_CONFIG_KEY_TABLE_NAME]) &&
                        (trim($config[ConstExpressionClause::TAB_CONFIG_KEY_TABLE_NAME]) != '')
                    )
                ) &&

                // Check valid column name
                (
                    (!isset($config[ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME])) ||
                    (
                        is_string($config[ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME]) &&
                        (trim($config[ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME]) != '')
                    )
                ) &&

                // Check valid alias
                (
                    (!isset($config[ConstExpressionClause::TAB_CONFIG_KEY_ALIAS])) ||
                    (
                        is_string($config[ConstExpressionClause::TAB_CONFIG_KEY_ALIAS]) &&
                        (trim($config[ConstExpressionClause::TAB_CONFIG_KEY_ALIAS]) != '')
                    )
                ) &&

                // Check valid value
                (
                    (!array_key_exists(ConstExpressionClause::TAB_CONFIG_KEY_VALUE, $config)) ||
                    (
                        is_string($config[ConstExpressionClause::TAB_CONFIG_KEY_VALUE]) ||
                        is_numeric($config[ConstExpressionClause::TAB_CONFIG_KEY_VALUE]) ||
                        is_bool($config[ConstExpressionClause::TAB_CONFIG_KEY_VALUE]) ||
                        is_null($config[ConstExpressionClause::TAB_CONFIG_KEY_VALUE])
                    )
                ) &&

                // Check valid pattern
                (
                    (!isset($config[ConstExpressionClause::TAB_CONFIG_KEY_PATTERN])) ||
                    (
                        is_string($config[ConstExpressionClause::TAB_CONFIG_KEY_PATTERN]) &&
                        (trim($config[ConstExpressionClause::TAB_CONFIG_KEY_PATTERN]) != '')
                    )
                ) &&

                // Check minimum requirement
                (
                    isset($config[ConstExpressionClause::TAB_CONFIG_KEY_TABLE_NAME]) ||
                    isset($config[ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME]) ||
                    array_key_exists(ConstExpressionClause::TAB_CONFIG_KEY_VALUE, $config) ||
                    isset($config[ConstExpressionClause::TAB_CONFIG_KEY_PATTERN])
                )
            );

        // Return result
        return $result;
    }



	/**
     * Check if expression clause is valid.
     * Configuration is an index array of expressions,
     * where each expression uses following format:
     * @see ToolBoxExpressionClause::checkExpressionIsValid().
     *
     * @param array $tabConfig
	 * @return boolean
     */
    public static function checkClauseExpressionIsValid(array $tabConfig)
    {
        // Init var
        $result = true;

        // Check each sub-config valid
        $tabKey = array_keys($tabConfig);
        for($intCpt = 0; ($intCpt < count($tabKey)) && $result; $intCpt++)
        {
            $result = static::checkExpressionIsValid($tabConfig[$tabKey[$intCpt]]);
        }

        // Return result
        return $result;
    }



}