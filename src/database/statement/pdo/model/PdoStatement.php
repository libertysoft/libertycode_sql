<?php
/**
 * Description :
 * This class allows to define PDO SQL database statement class.
 * Based on PDO statement, to design prepared query.
 * PDO SQL database statement allows to specify following query parameters configuration:
 * [
 *     'String parameter name 1' => 'mixed value 1 (string, numeric or boolean)',
 *     ...,
 *     'String parameter name N' => 'mixed value N'
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\statement\pdo\model;

use liberty_code\sql\database\statement\model\DefaultStatement;

use liberty_code\sql\database\connection\pdo\library\ToolBoxPdoConnection;
use liberty_code\sql\database\result\pdo\model\PdoResult;
use liberty_code\sql\database\statement\library\ConstStatement;
use liberty_code\sql\database\statement\pdo\library\ConstPdoStatement;
use liberty_code\sql\database\statement\pdo\exception\ParamInvalidFormatException;
use liberty_code\sql\database\statement\pdo\exception\PdoStatementInvalidFormatException;



/**
 * @method \PDOStatement getObjPdoStatement() Get PDO statement object.
 */
class PdoStatement extends DefaultStatement
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param \PDOStatement $objStatement
     * @param array $tabParam = null
     */
    public function __construct(\PDOStatement $objStatement, array $tabParam = null)
    {
        // Call parent constructor
        parent::__construct(null);

        // Init PDO statement
        $this->setPdoStatement($objStatement);

        // Init configuration if required
        if(!is_null($tabParam))
        {
            $this->setParam($tabParam);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstPdoStatement::DATA_KEY_DEFAULT_PDO_STATEMENT))
        {
            $this->__beanTabData[ConstPdoStatement::DATA_KEY_DEFAULT_PDO_STATEMENT] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstPdoStatement::DATA_KEY_DEFAULT_PDO_STATEMENT
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstStatement::DATA_KEY_DEFAULT_PARAM:
                    ParamInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstPdoStatement::DATA_KEY_DEFAULT_PDO_STATEMENT:
                    PdoStatementInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrCommand()
    {
        // Return result
        return $this->getObjPdoStatement()->queryString;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set PDO statement object.
     *
     * @param \PDOStatement $objStatement
     */
    public function setPdoStatement(\PDOStatement $objStatement)
    {
        $this->beanSet(ConstPdoStatement::DATA_KEY_DEFAULT_PDO_STATEMENT, $objStatement);
    }



    /**
     * @inheritdoc
     */
    public function setParam(array $tabParam)
    {
        // Call parent method
        parent::setParam($tabParam);

        // Register configuration in PDO statement
        foreach($tabParam as $strKey => $value)
        {
            // Register
            $this->getObjPdoStatement()->bindValue(
                $strKey,
                $value,
                ToolBoxPdoConnection::getIntValueType($value)
            );
        }
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute(array $tabParam = array())
    {
        // Set configuration
        $this->setParam($tabParam);

        // Execute and return result
        return $this->getObjPdoStatement()->execute();
    }



    /**
     * @inheritdoc
     * @return boolean|PdoResult
     */
    public function executeResult(array $tabParam = array())
    {
        // Set configuration
        $this->setParam($tabParam);

        // Return result
        return new PdoResult($this->getObjPdoStatement(), true);
    }



}