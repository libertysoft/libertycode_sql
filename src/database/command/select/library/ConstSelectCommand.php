<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\select\library;



class ConstSelectCommand
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_CLAUSE_SELECT = 'select';
    const TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN = 'select_pattern';
    const TAB_CONFIG_KEY_OPT_SELECT_DISTINCT = 'select_distinct';
    const TAB_CONFIG_KEY_CLAUSE_FROM = 'from';
    const TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN = 'from_pattern';
    const TAB_CONFIG_KEY_CLAUSE_WHERE = 'where';
    const TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN = 'where_pattern';
    const TAB_CONFIG_KEY_CLAUSE_GROUP = 'group';
    const TAB_CONFIG_KEY_CLAUSE_GROUP_PATTERN = 'group_pattern';
    const TAB_CONFIG_KEY_CLAUSE_HAVING = 'having';
    const TAB_CONFIG_KEY_CLAUSE_HAVING_PATTERN = 'having_pattern';
    const TAB_CONFIG_KEY_CLAUSE_ORDER = 'order';
    const TAB_CONFIG_KEY_CLAUSE_ORDER_PATTERN = 'order_pattern';
    const TAB_CONFIG_KEY_CLAUSE_LIMIT = 'limit';
    const TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN = 'limit_pattern';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the default select command configuration standard.';



}