<?php
/**
 * Description :
 * This class allows to define PDO SQL database connection class.
 * Based on PDO connection, to design connection and request interface.
 * PDO SQL database connection allows to specify following configuration:
 * [
 *     Default SQL database connection configuration,
 *
 *     driver(required): "string driver (Type of database: mysql, sqlite, ...)",
 *
 *     option(optional: empty array got if option not found): PDO connection options array format
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\connection\pdo\model;

use liberty_code\sql\database\connection\model\DefaultConnection;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\database\statement\pdo\model\PdoStatement;
use liberty_code\sql\database\result\pdo\model\PdoResult;
use liberty_code\sql\database\connection\library\ConstConnection;
use liberty_code\sql\database\connection\pdo\library\ToolBoxPdoConnection;
use liberty_code\sql\database\connection\exception\ConnectionRequireException;
use liberty_code\sql\database\connection\exception\DisconnectionRequireException;
use liberty_code\sql\database\connection\pdo\library\ConstPdoConnection;
use liberty_code\sql\database\connection\pdo\exception\ConfigInvalidFormatException;



abstract class PdoConnection extends DefaultConnection
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * PDO connection
     * @var \PDO
     */
    protected $objPdoConnection;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig
     * @param boolean $boolConnect = true
     */
    public function __construct(array $tabConfig, $boolConnect = true)
    {
        // Init var
        $this->objPdoConnection = null;

        // Call parent constructor
        parent::__construct($tabConfig, $boolConnect);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstConnection::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string DSN from configuration.
     *
     * @return string
     */
    abstract public function getStrConfigDsn();



    /**
     * Get PDO connection object.
     *
     * @return null|\PDO
     */
    public function getObjPdoConnection()
    {
        // Return result
        return $this->objPdoConnection;
    }



    /**
     * @inheritdoc
     * @throws ConnectionRequireException
     */
    public function getStrLastInsertId()
    {
        // Check connected
        $this->setCheckIsConnect();

        // Return result
        return $this->objPdoConnection->lastInsertId();
    }





    // Methods connection / disconnection
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkIsConnect()
    {
        // Return result
        return (!is_null($this->objPdoConnection));
    }



    /**
     * Set check if connected to database server.
     *
     * @throws ConnectionRequireException
     */
    protected function setCheckIsConnect()
    {
        if(!$this->checkIsConnect())
        {
            throw new ConnectionRequireException();
        }
    }



    /**
     * Set check if not connected to database server.
     *
     * @throws DisconnectionRequireException
     */
    protected function setCheckIsDisconnect()
    {
        if($this->checkIsConnect())
        {
            throw new DisconnectionRequireException();
        }
    }



    /**
     * @inheritdoc
     * @throws DisconnectionRequireException
     */
    public function connect()
    {
        // Check not connected
        $this->setCheckIsDisconnect();

        // Init var
        $tabConfig = $this->getTabConfig();
        $strDsn = sprintf(
            ConstPdoConnection::DSN_CONFIG_PATTERN_MAIN,
            $tabConfig[ConstPdoConnection::TAB_CONFIG_KEY_DRIVER],
            $this->getStrConfigDsn()
        );
        $strLg = ToolBoxTable::getItem(
            $this->getTabConfig(),
            ConstConnection::TAB_CONFIG_KEY_LOGIN,
            null
        );
        $strPw = ToolBoxTable::getItem(
            $this->getTabConfig(),
            ConstConnection::TAB_CONFIG_KEY_PASSWORD,
            ''
        );
        $tabOption = ToolBoxTable::getItem(
            $this->getTabConfig(),
            ConstPdoConnection::TAB_CONFIG_KEY_OPTION,
            array()
        );

        // Connection
        $this->objPdoConnection = new \PDO($strDsn, $strLg, $strPw, $tabOption);
    }



    /**
     * @inheritdoc
     * @throws ConnectionRequireException
     */
    public function close()
    {
        // Check connected
        $this->setCheckIsConnect();

        // Disconnection
        $this->objPdoConnection = null;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConnectionRequireException
     */
    public function execute($strCommand)
    {
        // Check connected
        $this->setCheckIsConnect();

        // Execute and return result
        return $this->objPdoConnection->exec($strCommand);
    }



    /**
     * @inheritdoc
     * @return boolean|PdoResult
     * @throws ConnectionRequireException
     */
    public function executeResult($strCommand)
    {
        // Check connected
        $this->setCheckIsConnect();

        // Init var
        $result = false;
        $objStatement = $this->objPdoConnection->query($strCommand);

        // Get query result, if required
        if($objStatement !== false)
        {
            $result = new PdoResult($objStatement, false);
        }

        // Return result
        return $result;
    }



    /**
     * Configuration follows @see PDO::prepare() options standard.
     * Parameters follows @see PdoStatement parameters config standard.
     *
     * @inheritdoc
     * @return PdoStatement
     * @throws ConnectionRequireException
     */
    public function getObjStatement($strCommand, array $tabConfig = array(), array $tabParam = array())
    {
        // Check connected
        $this->setCheckIsConnect();

        // Init var
        $objStatement = $this->objPdoConnection->prepare($strCommand, $tabConfig);
        $result = new PdoStatement($objStatement, $tabParam);

        // Return result
        return $result;
    }





    // Methods escape
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrEscapeName($strNm)
    {
        // Return result
        return sprintf('`%1$s`', strval($strNm));
    }



    /**
     * @inheritdoc
     * @throws ConnectionRequireException
     */
    public function getStrEscapeValue($value)
    {
        // Check connected
        $this->setCheckIsConnect();

        // Init var
        $result = ConstPdoConnection::NULL_CONFIG_VALUE;
        if(!is_null($value))
        {
            $result = $this->objPdoConnection->quote(
                $value,
                ToolBoxPdoConnection::getIntValueType($value)
            );
        }

        // Return result
        return $result;
    }





    // Methods transaction
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConnectionRequireException
     */
    public function checkInTransaction()
    {
        // Check connected
        $this->setCheckIsConnect();

        // Return result
        return ($this->objPdoConnection->inTransaction() !== false);
    }



    /**
     * @inheritdoc
     * @throws ConnectionRequireException
     */
    public function transactionStart()
    {
        // Return result
        return (
            (!$this->checkInTransaction()) &&
            $this->objPdoConnection->beginTransaction()
        );
    }



    /**
     * @inheritdoc
     * @throws ConnectionRequireException
     */
    public function transactionEndCommit()
    {
        // Return result
        return (
            $this->checkInTransaction() &&
            $this->objPdoConnection->commit()
        );
    }



    /**
     * @inheritdoc
     * @throws ConnectionRequireException
     */
    public function transactionEndRollback()
    {
        // Return result
        return (
            $this->checkInTransaction() &&
            $this->objPdoConnection->rollBack()
        );
    }



}