<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;



class ConstFromClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_TABLE_DB_NAME = 'db_name';
    const TAB_CONFIG_KEY_TABLE_NAME = 'table_name';
    const TAB_CONFIG_KEY_TABLE_ALIAS = 'alias';
    const TAB_CONFIG_KEY_TABLE_PATTERN = 'pattern';

    const TAB_CONFIG_KEY_JOIN_TYPE = 'type';
    const TAB_CONFIG_KEY_JOIN_LEFT = 'left';
    const TAB_CONFIG_KEY_JOIN_RIGHT = 'right';
    const TAB_CONFIG_KEY_JOIN_ON = 'on';

    // Join configuration
    const JOIN_TYPE_CONFIG_INNER = 'inner';
    const JOIN_TYPE_CONFIG_LEFT_OUTER = 'left_outer';
    const JOIN_TYPE_CONFIG_RIGHT_OUTER = 'right_outer';
    const JOIN_TYPE_CONFIG_FULL_OUTER = 'full_outer';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get configuration join type options array.
     *
     * @return array
     */
    static public function getTabConfigJoinType()
    {
        // Init var
        $result = array(
            self::JOIN_TYPE_CONFIG_INNER,
            self::JOIN_TYPE_CONFIG_LEFT_OUTER,
            self::JOIN_TYPE_CONFIG_RIGHT_OUTER,
            self::JOIN_TYPE_CONFIG_FULL_OUTER
        );

        // Return result
        return $result;
    }



}