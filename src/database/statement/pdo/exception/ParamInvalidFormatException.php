<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\statement\pdo\exception;

use liberty_code\sql\database\statement\pdo\library\ConstPdoStatement;



class ParamInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $param
     */
	public function __construct($param)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPdoStatement::EXCEPT_MSG_PARAM_INVALID_FORMAT,
            mb_strimwidth(strval($param), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified parameters config has valid format.
     *
     * @param mixed $param
     * @return boolean
     */
    protected static function checkConfigIsValid($param)
    {
        // Init var
        $result = true;
        $tabKey = array_keys($param);

        for($intCpt = 0; ($intCpt < count($tabKey)) && $result; $intCpt++)
        {
            // Get info
            $key = $tabKey[$intCpt];
            $value = $param[$key];

            // Check config
            $result =
                // Check key valid string
                is_string($key) && (trim($key) != '') &&

                // Check value valid
                (
                    is_string($value) ||
                    is_numeric($value) ||
                    is_bool($value) ||
                    is_null($value)
                );
        }

        // Return result
        return $result;
    }



	/**
	 * Check if specified parameters config has valid format.
	 * 
     * @param mixed $param
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($param)
    {
		// Init var
		$result =
            // Check valid array
            is_array($param) &&

            // Check valid config
            static::checkConfigIsValid($param);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($param) ? serialize($param) : $param));
		}
		
		// Return result
		return $result;
    }
	
	
	
}