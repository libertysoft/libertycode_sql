<?php
/**
 * Description :
 * This class allows to describe behavior of database use SQL command class.
 * Database use command allows to design string SQL database use command,
 * from following specified configuration:
 * [
 *     db_name(required):"string database name"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_use\api;

use liberty_code\sql\database\command\api\CommandInterface;



interface DbUseCommandInterface extends CommandInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Set string database name config.
     *
     * @param string $strDbNm
     * @return static
     */
    public function setDbName($strDbNm);
}