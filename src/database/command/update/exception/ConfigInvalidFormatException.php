<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\update\exception;

use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxConditionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxLimitClause;
use liberty_code\sql\database\command\clause\library\ToolBoxSetClause;
use liberty_code\sql\database\command\update\library\ConstUpdateCommand;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstUpdateCommand::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid from clause
            (
                (!isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM])) ||
                ToolBoxFromClause::checkClauseFromIsValid($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM])
            ) &&

            // Check valid from clause pattern
            (
                (!isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN])) ||
                is_string($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN])
            ) &&

            // Check from minimum requirement
            (
                isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM])  ||
                isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN])
            ) &&

            // Check valid set clause
            (
                (!isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET])) ||
                ToolBoxSetClause::checkClauseSetIsValid($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET])
            ) &&

            // Check valid set clause pattern
            (
                (!isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET_PATTERN])) ||
                is_string($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET_PATTERN])
            ) &&

            // Check set minimum requirement
            (
                isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET])  ||
                isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET_PATTERN])
            ) &&

            // Check valid where clause
            (
                (!isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE])) ||
                ToolBoxConditionClause::checkClauseConditionIsValid($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE])
            ) &&

            // Check valid where clause pattern
            (
                (!isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN])) ||
                is_string($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN])
            ) &&

            // Check valid limit clause
            (
                (!isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT])) ||
                ToolBoxLimitClause::checkClauseLimitIsValid($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT])
            ) &&

            // Check valid limit clause pattern
            (
                (!isset($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN])) ||
                is_string($config[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}