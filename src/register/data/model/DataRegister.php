<?php
/**
 * Description :
 * This class allows to define data SQL register class.
 *
 * Items are stored in table like:
 * - Specific column name = list of keys
 * - Specific column name = list of items.
 * - Specific column name = list of expiration datetimes.
 *
 * Data SQL register uses the following specified configuration:
 * [
 *     Default SQL register configuration,
 *
 *     column_name_key(required): "string column name, where keys stored",
 *
 *     column_name_item(required): "string column name, where items stored"
 *
 *     column_name_expire_timeout_datetime(required):
 *         "string column name, where expiration datetime stored",
 *
 *     get_timezone_name(optional: got date_default_timezone_get() if not found):
 *         "string timezone name, used to render item expiration datetime",
 *
 *     set_timezone_name(optional: got 'UTC' if not found):
 *         "string timezone name, used to register item expiration datetime",
 *
 *     set_datetime_format(optional: got 'Y-m-d H:i:s' if not found):
 *         "string datetime format, used to register item expiration datetime"
 *
 *     set_datetime_value_null(optional: got null if not found):
 *         "datetime null value, used to register item expiration datetime null"
 * ]
 *
 * Templates used:
 * template_where, template_order, template_insert, template_update
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\register\data\model;

use liberty_code\sql\register\model\DefaultRegister;

use DateTime;
use DateTimeZone;
use DateInterval;
use liberty_code\register\item\exception\ItemInvalidFormatException;
use liberty_code\register\register\exception\KeyInvalidFormatException;
use liberty_code\register\register\exception\KeyFoundException;
use liberty_code\register\register\exception\KeyNotFoundException;
use liberty_code\sql\database\command\clause\library\ConstExpressionClause;
use liberty_code\sql\database\command\clause\library\ConstConditionClause;
use liberty_code\sql\database\command\clause\library\ConstFromClause;
use liberty_code\sql\database\command\clause\library\ConstOrderClause;
use liberty_code\sql\database\command\clause\library\ConstSetClause;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;
use liberty_code\sql\database\command\insert\library\ConstInsertCommand;
use liberty_code\sql\database\command\update\library\ConstUpdateCommand;
use liberty_code\sql\database\command\delete\library\ConstDeleteCommand;
use liberty_code\sql\register\library\ConstRegister;
use liberty_code\sql\register\data\library\ConstDataRegister;
use liberty_code\sql\register\data\exception\ConfigInvalidFormatException;
use liberty_code\sql\register\data\exception\GetConfigInvalidFormatException;
use liberty_code\sql\register\data\exception\SetConfigInvalidFormatException;



class DataRegister extends DefaultRegister
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRegister::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if specified item data is valid.
     *
     * Item data array format:
     * @see getItemDataFromItem() return array format.
     *
     * @param array $itemData
     * @return boolean
     */
    protected function checkItemDataValid(array $itemData)
    {
        // Init var
        $objExpireDt = $this->getObjExpireTimeoutDtFromItemData($itemData);
        $objDtNow = (new DateTime())
            ->setTimezone(
                new DateTimeZone($this->getStrSetTimezoneName())
            );
        $result = (
            // Check expiration
            (
                is_null($objExpireDt) ||
                ($objExpireDt > $objDtNow)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get column name format,
     * where keys stored.
     *
     * @return string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrColNameKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        $this->beanSetValidValue(ConstRegister::DATA_KEY_DEFAULT_CONFIG, $tabConfig);

        // Return result
        return $tabConfig[ConstDataRegister::TAB_CONFIG_KEY_COLUMN_NAME_KEY];
    }



    /**
     * Get column name format,
     * where items stored.
     *
     * @return string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrColNameItem()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        $this->beanSetValidValue(ConstRegister::DATA_KEY_DEFAULT_CONFIG, $tabConfig);

        // Return result
        return $tabConfig[ConstDataRegister::TAB_CONFIG_KEY_COLUMN_NAME_ITEM];
    }



    /**
     * Get column name format,
     * where expiration datetime stored.
     *
     * @return string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrColNameExpireTimeoutDt()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        $this->beanSetValidValue(ConstRegister::DATA_KEY_DEFAULT_CONFIG, $tabConfig);

        // Return result
        return $tabConfig[ConstDataRegister::TAB_CONFIG_KEY_COLUMN_NAME_EXPIRE_TIMEOUT_DATETIME];
    }



    /**
     * Get timezone name,
     * used to render item expiration timeout datetime.
     *
     * @return string
     */
    public function getStrGetTimezoneName()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDataRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME, $tabConfig) ?
                $tabConfig[ConstDataRegister::TAB_CONFIG_KEY_GET_TIMEZONE_NAME] :
                date_default_timezone_get()
        );

        // Return result
        return $result;
    }



    /**
     * Get timezone name,
     * used to register item expiration timeout datetime.
     *
     * @return string
     */
    public function getStrSetTimezoneName()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDataRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME, $tabConfig) ?
                $tabConfig[ConstDataRegister::TAB_CONFIG_KEY_SET_TIMEZONE_NAME] :
                'UTC'
        );

        // Return result
        return $result;
    }



    /**
     * Get datetime format,
     * used to register item expiration timeout datetime.
     *
     * @return string
     */
    protected function getStrSetDtFormat()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDataRegister::TAB_CONFIG_KEY_SET_DATETIME_FORMAT, $tabConfig) ?
                $tabConfig[ConstDataRegister::TAB_CONFIG_KEY_SET_DATETIME_FORMAT] :
                'Y-m-d H:i:s'
        );

        // Return result
        return $result;
    }



    /**
     * Get datetime null value,
     * used to register item expiration datetime null.
     *
     * @return string
     */
    protected function getSetDtValueNull()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstDataRegister::TAB_CONFIG_KEY_SET_DATETIME_VALUE_NULL, $tabConfig) ?
                $tabConfig[ConstDataRegister::TAB_CONFIG_KEY_SET_DATETIME_VALUE_NULL] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get item data,
     * from specified item.
     * Overwrite it to implement specific values formatting.
     *
     * Configuration array format:
     * @see setItem() configuration array format.
     *
     * Return array format:
     * [
     *     'string column name for item' (required): string serialized item,
     *     'string column name for expiration datetime' (required): string dateTime
     * ]
     *
     * @param mixed $item
     * @param array $tabConfig = null
     * @return array
     */
    protected function getItemDataFromItem($item, array $tabConfig = null)
    {
        // Init var
        $item = serialize($item);
        $strExpireTimoutDt = (
            (!is_null($objDt = $this->getObjExpireTimeoutDtFromConfig($tabConfig))) ?
                $objDt->format($this->getStrSetDtFormat()) :
                $this->getSetDtValueNull()
        );
        $result = array(
            $this->getStrColNameItem() => $item,
            $this->getStrColNameExpireTimeoutDt() => $strExpireTimoutDt
        );

        // Return result
        return $result;
    }



    /**
     * Get array of item data,
     * from specified command configuration.
     *
     * Return associative array format:
     * Key => Value: Item data: @see getItemDataFromItem() return array format.
     *
     * Remove success return true if all items success,
     * false if an error occurs on at least one item.
     *
     * @param array $tabCmdConfig
     * @param boolean &$boolRemoveSuccess = true
     * @return array
     */
    protected function getTabItemDataFromCmdConfig(array $tabCmdConfig, &$boolRemoveSuccess = true)
    {
        // Init var
        $result = array();
        $boolRemoveSuccess = true;
        $objCommandFactory = $this->getObjCommandFactory();

        // Check command factory valid
        if(!is_null($objCommandFactory))
        {
            // Init var
            $tabConfig = $this->getTabConfig();
            $strColNmKey = $this->getStrColNameKey();
            $strColNmItem = $this->getStrColNameItem();
            $strColNmExpireTimeoutDt = $this->getStrColNameExpireTimeoutDt();

            // Set select clause
            $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT] = array(
                [ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME => $strColNmKey],
                [ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME => $strColNmItem],
                [ConstExpressionClause::TAB_CONFIG_KEY_COLUMN_NAME => $strColNmExpireTimeoutDt]
            );

            // Set from clause
            $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstRegister::TAB_CONFIG_KEY_TABLE_NAME]]
            );

            // Set where clause: Set pattern, if required
            if(array_key_exists(ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE, $tabConfig))
            {
                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => array_merge(
                        [
                            [
                                ConstConditionClause::TAB_CONFIG_KEY_PATTERN =>
                                    $tabConfig[ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE]
                            ]
                        ],
                        (
                            array_key_exists(ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE, $tabCmdConfig) ?
                                [
                                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                                ] :
                                []
                        )
                    )
                );
            }

            // Set order clause: Set pattern
            if(array_key_exists(ConstRegister::TAB_CONFIG_KEY_PATTERN_ORDER, $tabConfig))
            {
                // Init order clause, if required
                if(!isset($tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER]))
                {
                    $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER] = array();
                }

                $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER][] = array(
                    ConstOrderClause::TAB_CONFIG_KEY_PATTERN =>
                        $tabConfig[ConstRegister::TAB_CONFIG_KEY_PATTERN_ORDER]
                );
            }

            // Get command
            $objCommand = $objCommandFactory->getObjSelectCommand($tabCmdConfig);

            // Run query result
            if(($resultQuery = $objCommand->executeResult()) !== false)
            {
                $tabKeyNotValid = array();
                while(($data = $resultQuery->getFetchData()) !== false)
                {
                    // Get item data
                    $strKey = $data[$strColNmKey];
                    $itemData = array(
                        $strColNmItem => $data[$strColNmItem],
                        $strColNmExpireTimeoutDt => $data[$strColNmExpireTimeoutDt]
                    );

                    // Register item, if required (item is valid)
                    if($this->checkItemDataValid($itemData))
                    {
                        $result[$strKey] = $itemData;
                    }
                    else
                    {
                        $tabKeyNotValid[] = $strKey;
                    }
                }
                $resultQuery->close();

                // Remove items, if required
                if(count($tabKeyNotValid) > 0)
                {
                    // Set where clause
                    $objConnection = $objCommandFactory->getObjConnection();
                    $tabEscapeKeyNotValid = array_map(
                        function($value) use($objConnection) {
                            return $objConnection->getStrEscapeValue($value);
                        },
                        $tabKeyNotValid
                    );
                    $strListKeyNotValid = implode(', ', $tabEscapeKeyNotValid);
                    $tabCmdConfig = array(
                        ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE => [
                            ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                            ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                                [
                                    ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmKey,
                                    ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_IN,
                                    ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                                        ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $strListKeyNotValid
                                    ]
                                ]
                            ]
                        ]
                    );

                    // Remove items
                    $boolRemoveSuccess = $this->removeItemFromCmdConfig($tabCmdConfig);
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get item data,
     * from specified key.
     *
     * Return array format:
     * @see getItemDataFromItem() return array format.
     *
     * Remove success return:
     * @see getTabItemDataFromCmdConfig() remove success return.
     *
     * @param string $strKey
     * @param boolean &$boolRemoveSuccess = true
     * @return null|array
     * @throws KeyInvalidFormatException
     */
    protected function getItemDataFromKey(
        $strKey,
        &$boolRemoveSuccess = true
    )
    {
        // Check arguments
        KeyInvalidFormatException::setCheck($strKey);

        // Init var
        $tabCmdConfig = array();

        // Set where clause
        $tabCmdConfig[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
            ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
            ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                [
                    ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $this->getStrColNameKey(),
                    ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_EQUAL,
                    ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                        ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $strKey
                    ]
                ]
            ]
        );

        // Get item data, if required
        $tabItemData = $this->getTabItemDataFromCmdConfig($tabCmdConfig, $boolRemoveSuccess);
        $result = (
            array_key_exists($strKey, $tabItemData) ?
                $tabItemData[$strKey] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get expiration timeout datetime,
     * from specified configuration.
     *
     * Configuration array format:
     * @see setItem() configuration array format.
     *
     * @param array $tabConfig = null
     * @return null|DateTime
     */
    protected function getObjExpireTimeoutDtFromConfig(array $tabConfig = null)
    {
        // Init var
        $result = (
            (
                (!is_null($tabConfig)) &&
                array_key_exists(ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME, $tabConfig)
            ) ?
                // Set specific time
                (
                    ($tabConfig[ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME] instanceof DateTime) ?
                        $tabConfig[ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME] :
                        (new DateTime())->setTimestamp($tabConfig[ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_TIME])
                ) :
                (
                    (
                        (!is_null($tabConfig)) &&
                        array_key_exists(ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND, $tabConfig)
                    ) ?
                        // Add specific second, from datetime now
                        (new DateTime())->add(new DateInterval(sprintf(
                            'PT%1$dS',
                            $tabConfig[ConstDataRegister::TAB_SET_CONFIG_KEY_EXPIRE_TIMEOUT_SECOND]
                        ))) :
                        null
                )
        );
        $result = (
            (!is_null($result)) ?
                $result->setTimezone(new DateTimeZone($this->getStrSetTimezoneName())) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get expiration timeout datetime,
     * from specified item data.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see getItemDataFromItem() return array format.
     *
     * @param array $itemData
     * @return null|DateTime
     */
    protected function getObjExpireTimeoutDtFromItemData(array $itemData)
    {
        // Init var
        $strColNmExpireTimeoutDt = $this->getStrColNameExpireTimeoutDt();
        $result = (
            (
                array_key_exists($strColNmExpireTimeoutDt, $itemData) &&
                ($itemData[$strColNmExpireTimeoutDt] !== ($this->getSetDtValueNull())) &&
                is_string($itemData[$strColNmExpireTimeoutDt]) &&
                (
                    ($objDt =
                        DateTime::createFromFormat(
                            $this->getStrSetDtFormat(),
                            $itemData[$strColNmExpireTimeoutDt],
                            new DateTimeZone($this->getStrSetTimezoneName())
                        )
                    ) instanceof DateTime
                )
            ) ?
                $objDt :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get expiration timeout datetime,
     * from specified key.
     *
     * @param string $strKey
     * @return null|DateTime
     */
    public function getObjExpireTimeoutDt($strKey)
    {
        // Init var
        $itemData = $this->getItemDataFromKey($strKey);
        $result = (
            is_null($itemData) ?
                null :
                $this->getObjExpireTimeoutDtFromItemData($itemData)
        );
        $result = (
            (!is_null($result)) ?
                $result->setTimezone(new DateTimeZone($this->getStrGetTimezoneName())) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get item,
     * from specified item data.
     * Overwrite it to implement specific feature.
     *
     * Item data array format:
     * @see getItemDataFromItem() return array format.
     *
     * @param array $itemData
     * @return null|mixed
     */
    protected function getItemFromItemData(array $itemData)
    {
        // Init var
        $strColNmItem = $this->getStrColNameItem();
        $result = (
            array_key_exists($strColNmItem, $itemData) ?
                unserialize($itemData[$strColNmItem]) :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getItem($strKey)
    {
        // Init var
        $itemData = $this->getItemDataFromKey($strKey);
        $result = (
            is_null($itemData) ?
                null :
                $this->getItemFromItemData($itemData)
        );

        // Return result
        return $result;
    }



    /**
     * Get command configuration,
     * with specified get configuration,
     * implemented on specified command configuration.
     *
     * Selection configuration format:
     * @see getTabItem() selection configuration format.
     *
     * @param array $tabCmdConfig
     * @param array $tabGetConfig
     * @param string $strCmdConfigKeyWhere
     * @return array
     * @throws GetConfigInvalidFormatException
     */
    protected function getTabGetCommandConfig(array $tabCmdConfig, array $tabGetConfig, $strCmdConfigKeyWhere)
    {
        // Check arguments
        GetConfigInvalidFormatException::setCheck($tabGetConfig);

        // Init var
        $result = $tabCmdConfig;
        $objCommandFactory = $this->getObjCommandFactory();
        $tabCondition = array();

        // Check command factory valid
        if(!is_null($objCommandFactory))
        {
            // Get selection SQL like pattern of key
            if(isset($tabGetConfig[ConstDataRegister::TAB_GET_CONFIG_KEY_PATTERN_KEY]))
            {
                $tabCondition[] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $this->getStrColNameKey(),
                    ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_LIKE,
                    ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                        ConstExpressionClause::TAB_CONFIG_KEY_VALUE =>
                            $tabGetConfig[ConstDataRegister::TAB_GET_CONFIG_KEY_PATTERN_KEY]
                    ]
                );
            }

            // Get selection index array of key
            if(isset($tabGetConfig[ConstDataRegister::TAB_GET_CONFIG_KEY_LIST_KEY]))
            {
                $objConnection = $objCommandFactory->getObjConnection();
                $tabEscapeKey = array_map(
                    function($value) use($objConnection) {
                        return $objConnection->getStrEscapeValue($value);
                    },
                    $tabGetConfig[ConstDataRegister::TAB_GET_CONFIG_KEY_LIST_KEY]
                );
                $strListKey = implode(', ', $tabEscapeKey);

                $tabCondition[] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $this->getStrColNameKey(),
                    ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_IN,
                    ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                        ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $strListKey
                    ]
                );
            }

            // Set where clause, if required
            if(count($tabCondition) > 0)
            {
                // Set where clause
                $result[$strCmdConfigKeyWhere] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => array_merge(
                        $tabCondition,
                        (
                            array_key_exists($strCmdConfigKeyWhere, $result) ?
                                [
                                    $result[$strCmdConfigKeyWhere]
                                ] :
                                []
                        )
                    )
                );
            }
        }

        // Return result
        return $result;
    }



    /**
     * Selection configuration format:
     * [
     *     pattern_key(optional): "string SQL like pattern",
     *
     *     list_key(optional): [
     *         "string key 1",
     *         ...,
     *         "string key N"
     *     ]
     * ]
     *
     * @inheritdoc
     */
    public function getTabItem(array $tabConfig = null)
    {
        // Init var
        $tabGetConfig = $tabConfig;
        $tabCmdConfig = array();

        // Set get configuration, if required
        if(is_array($tabGetConfig))
        {
            $tabCmdConfig = $this->getTabGetCommandConfig(
                $tabCmdConfig,
                $tabGetConfig,
                ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE
            );
        }

        // Register items
        $tabItemData = $this->getTabItemDataFromCmdConfig($tabCmdConfig);
        $result = array_map(
            function($itemData) {
                return $this->getItemFromItemData($itemData);
            },
            $tabItemData
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Setting configuration array format:
     * [
     *     expire_timeout_time(optional): integer timestamp / DateTime,
     *
     *     expire_timeout_second(optional): integer number of seconds, from datetime now
     * ]
     *
     * @inheritdoc
     * @throws KeyNotFoundException
     * @throws ItemInvalidFormatException
     * @throws SetConfigInvalidFormatException
     */
    public function setItem($strKey, $item, array $tabConfig = null)
    {
        // Check arguments
        ItemInvalidFormatException::setCheck($item);
        SetConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;
        $tabSetConfig = $tabConfig;

        // Set item, if found
        if($this->checkItemExists($strKey))
        {
            // Init var
            $objCommandFactory = $this->getObjCommandFactory();

            // Check command factory valid
            if(!is_null($objCommandFactory))
            {
                // Get info
                $tabCmdConfig = array();
                $strColNmKey = $this->getStrColNameKey();
                $itemData = $this->getItemDataFromItem($item, $tabSetConfig);
                $tabConfig = $this->getTabConfig();

                // Set from clause
                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                    [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstRegister::TAB_CONFIG_KEY_TABLE_NAME]]
                );

                // Set set clause
                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET] = array_values(array_map(
                    function($strColNm, $value)  {
                        return array(
                            ConstSetClause::TAB_CONFIG_KEY_OPERAND => $strColNm,
                            ConstSetClause::TAB_CONFIG_KEY_VALUE => [
                                ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $value
                            ]
                        );
                    },
                    array_keys($itemData),
                    $itemData
                ));

                // Set pattern
                if(array_key_exists(ConstRegister::TAB_CONFIG_KEY_PATTERN_SET, $tabConfig))
                {
                    $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_SET][] = array(
                        ConstSetClause::TAB_CONFIG_KEY_PATTERN =>
                            $tabConfig[ConstRegister::TAB_CONFIG_KEY_PATTERN_SET]
                    );
                }

                // Set where clause
                $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                        [
                            ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmKey,
                            ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_EQUAL,
                            ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                                ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $strKey
                            ]
                        ]
                    ]
                );

                // Set pattern
                if(array_key_exists(ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE, $tabConfig))
                {
                    $tabCmdConfig[ConstUpdateCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                    [ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT][] = array(
                        ConstConditionClause::TAB_CONFIG_KEY_PATTERN =>
                            $tabConfig[ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE]
                    );
                }

                // Get command
                $objCommand = $objCommandFactory->getObjUpdateCommand($tabCmdConfig);

                // Run query
                $result = ($objCommand->execute() !== false);
            }
        }
        // If item not found, throw exception
        else
        {
            throw new KeyNotFoundException($strKey);
        }

        // Return result
        return $result;
    }



    /**
     * Setting configuration array format:
     * @see setItem() configuration array format.
     *
     * @inheritdoc
     * @throws KeyFoundException
     * @throws ItemInvalidFormatException
     * @throws SetConfigInvalidFormatException
     */
    public function addItem($strKey, $item, array $tabConfig = null)
    {
        // Check arguments
        ItemInvalidFormatException::setCheck($item);
        SetConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;
        $tabSetConfig = $tabConfig;

        // Add item, if not found
        if(!$this->checkItemExists($strKey))
        {
            // Init var
            $objCommandFactory = $this->getObjCommandFactory();

            // Check command factory valid
            if(!is_null($objCommandFactory))
            {
                // Get info
                $tabCmdConfig = array();
                $strColNmKey = $this->getStrColNameKey();
                $itemData = $this->getItemDataFromItem($item, $tabSetConfig);
                $tabConfig = $this->getTabConfig();

                // Set from clause
                $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                    [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstRegister::TAB_CONFIG_KEY_TABLE_NAME]]
                );

                // Set column clause
                $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN] = array_merge(
                    array($strColNmKey),
                    array_keys($itemData)
                );

                // Set pattern
                if(array_key_exists(ConstRegister::TAB_CONFIG_KEY_PATTERN_COLUMN, $tabConfig))
                {
                    $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN][] = array(
                        ConstExpressionClause::TAB_CONFIG_KEY_PATTERN => $tabConfig[ConstRegister::TAB_CONFIG_KEY_PATTERN_COLUMN]
                    );
                }

                // Set value
                $tabCmdConfigValue = array_merge(
                    array(
                        [ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $strKey]
                    ),
                    array_values(array_map(
                        function($value)  {
                            return array(ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $value);
                        },
                        $itemData
                    ))
                );

                // Set pattern
                if(array_key_exists(ConstRegister::TAB_CONFIG_KEY_PATTERN_VALUE, $tabConfig))
                {
                    $tabCmdConfigValue[] = array(
                        ConstExpressionClause::TAB_CONFIG_KEY_PATTERN =>
                            $tabConfig[ConstRegister::TAB_CONFIG_KEY_PATTERN_VALUE]
                    );
                }

                // Set value clause
                $tabCmdConfig[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE] = array(
                    $tabCmdConfigValue
                );

                // Get command
                $objCommand = $objCommandFactory->getObjInsertCommand($tabCmdConfig);

                // Run query
                $result = ($objCommand->execute() !== false);
            }
        }
        // If item found, throw exception
        else
        {
            throw new KeyFoundException($strKey);
        }

        // Return result
        return $result;
    }



    /**
     * Remove items,
     * from specified command configuration.
     * Return true if all items success,
     * false if an error occurs on at least one item.
     *
     * @param array $tabCmdConfig
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function removeItemFromCmdConfig(array $tabCmdConfig)
    {
        // Init var
        $result = false;
        $objCommandFactory = $this->getObjCommandFactory();
        $tabConfig = $this->getTabConfig();

        // Check configuration
        $this->beanSetValidValue(ConstRegister::DATA_KEY_DEFAULT_CONFIG, $tabConfig);

        // Check command factory valid
        if(!is_null($objCommandFactory))
        {
            // Set from clause
            $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = array(
                [ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME => $tabConfig[ConstRegister::TAB_CONFIG_KEY_TABLE_NAME]]
            );

            // Set where clause: Set pattern, if required
            if(array_key_exists(ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE, $tabConfig))
            {
                $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                    ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => array_merge(
                        [
                            [
                                ConstConditionClause::TAB_CONFIG_KEY_PATTERN =>
                                    $tabConfig[ConstRegister::TAB_CONFIG_KEY_PATTERN_WHERE]
                            ]
                        ],
                        (
                            array_key_exists(ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE, $tabCmdConfig) ?
                                [
                                    $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE]
                                ] :
                                []
                        )
                    )
                );
            }

            // Get command
            $objCommand = $objCommandFactory->getObjDeleteCommand($tabCmdConfig);

            // Run query
            $result = ($objCommand->execute() !== false);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws KeyNotFoundException
     */
    public function removeItem($strKey)
    {
        // Remove item, if found
        if($this->checkItemExists($strKey))
        {
            // Init var
            $tabCmdConfig = array();
            $strColNmKey = $this->getStrColNameKey();

            // Set where clause
            $tabCmdConfig[ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = array(
                ConstConditionClause::TAB_CONFIG_KEY_GROUP_TYPE => ConstConditionClause::GROUP_TYPE_CONFIG_AND,
                ConstConditionClause::TAB_CONFIG_KEY_GROUP_CONTENT => [
                    [
                        ConstConditionClause::TAB_CONFIG_KEY_OPERAND => $strColNmKey,
                        ConstConditionClause::TAB_CONFIG_KEY_OPERATOR => ConstConditionClause::OPERATOR_CONFIG_EQUAL,
                        ConstConditionClause::TAB_CONFIG_KEY_VALUE => [
                            ConstExpressionClause::TAB_CONFIG_KEY_VALUE => $strKey
                        ]
                    ]
                ]
            );

            // Remove item
            $result = $this->removeItemFromCmdConfig($tabCmdConfig);
        }
        // If item not found, throw exception
        else
        {
            throw new KeyNotFoundException($strKey);
        }

        // Return result
        return $result;
    }



    /**
     * Selection configuration format:
     * @see getTabItem() configuration array format.
     *
     * @inheritdoc
     */
    public function removeItemAll(array $tabConfig = null)
    {
        // Init var
        $tabGetConfig = $tabConfig;
        $tabCmdConfig = array();

        // Set get configuration, if required
        if(is_array($tabGetConfig))
        {
            $tabCmdConfig = $this->getTabGetCommandConfig(
                $tabCmdConfig,
                $tabGetConfig,
                ConstDeleteCommand::TAB_CONFIG_KEY_CLAUSE_WHERE
            );
        }

        // Remove items
        $result = $this->removeItemFromCmdConfig($tabCmdConfig);

        // Return result
        return $result;
    }



    /**
     * Remove all items not valid.
     * Return true if all items success,
     * false if an error occurs on at least one item.
     *
     * @return boolean
     */
    public function removeItemAllNotValid()
    {
        //Init var
        $result = true;

        // Remove items, if required (try getting valid item data, remove else)
        $this->getTabItemDataFromCmdConfig(array(), $result);

        // Return result
        return $result;
    }



}