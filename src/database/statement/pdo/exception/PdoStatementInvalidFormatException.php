<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\statement\pdo\exception;

use liberty_code\sql\database\statement\pdo\library\ConstPdoStatement;



class PdoStatementInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $statement
     */
	public function __construct($statement)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPdoStatement::EXCEPT_MSG_PDO_STATEMENT_INVALID_FORMAT,
            mb_strimwidth(strval($statement), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified statement has valid format.
	 * 
     * @param mixed $statement
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($statement)
    {
		// Init var
		$result = (
			($statement instanceof \PDOStatement)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($statement);
		}
		
		// Return result
		return $result;
    }
	
	
	
}