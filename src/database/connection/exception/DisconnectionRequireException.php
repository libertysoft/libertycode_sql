<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\connection\exception;

use liberty_code\sql\database\connection\library\ConstConnection;



class DisconnectionRequireException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     */
	public function __construct()
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = ConstConnection::EXCEPT_MSG_DISCONNECTION_REQUIRE;
	}
	
	
	
}