<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;



class ConstStandardLimitClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_START = '%1$s OFFSET %2$s';
}