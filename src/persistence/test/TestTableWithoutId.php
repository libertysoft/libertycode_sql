<?php
/**
 * GET arguments:
 * - delete: = 1: Remove data.
 */

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/database/test/HelpCreateTestTable.php');

// Use
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;



// Init var
$objCommandFacto = new StandardCommandFactory($objConnection);
$objPersistor = new TablePersistor($objCommandFacto);
$tabConfig = array(
    'table_name' => $strTableNmCnf,
    //*
    'column_name_select' => [
        $strColNmCnfNm,
        $strColNmCnfVal
    ],
    //*/
    //'pattern_select' => '',
    'pattern_where' => sprintf(
        '(%1$s = %2$s)',
        $objConnection->getStrEscapeName($strColNmCnfType),
        $objConnection->getStrEscapeValue('test')
    ),
    'pattern_order' => sprintf(
        '%1$s ASC',
        $objConnection->getStrEscapeName($strColNmCnfNm)
    ),
    'pattern_column' => sprintf(
        '%1$s, %2$s, %3$s',
        $objConnection->getStrEscapeName($strColNmCnfType),
        $objConnection->getStrEscapeName($strColNmCnfDateCreate),
        $objConnection->getStrEscapeName($strColNmCnfDateUpdate)
    ),
    'pattern_value' => sprintf(
        '%1$s, NOW(), NOW()',
        $objConnection->getStrEscapeValue('test')
    ),
    'pattern_set' => sprintf(
        '%1$s = NOW()',
        $objConnection->getStrEscapeName($strColNmCnfDateUpdate)
    )
);



// Test insert 1 config attribute
echo('Test insert 1 config attribute: <br />');
$data = array(
    $strColNmCnfNm => 'key_test',
    $strColNmCnfVal => 'Value test'
);

// echo('Execute: <pre>');var_dump($objPersistor->createTabData(array($data), $tabConfig));echo('</pre>');
echo('Execute: <pre>');var_dump($objPersistor->createData($data, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test select 1 config attribute
echo('Test select 1 config attribute: <br />');

echo('Get: <pre>');print_r($objPersistor->getData(null, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test update 1 config attribute
echo('Test update 1 config attribute: <br />');
$data = array(
    $strColNmCnfNm => 'key_upd',
    $strColNmCnfVal => 'Value test updated'
);

echo('Execute: <pre>');var_dump($objPersistor->updateData($data, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Test select 1 config attribute
echo('Test select 1 config attribute: <br />');

echo('Get: <pre>');print_r($objPersistor->getData(null, $tabConfig));echo('</pre>');

echo('<br /><br /><br />');



// Delete users, if required
if(trim(ToolBoxTable::getItem($_GET, 'delete', '0')) == '1')
{
    // Test delete 1 config
    echo('Test delete 1 config: <br />');

    echo('Execute: <pre>');var_dump($objPersistor->deleteData($data, $tabConfig));echo('</pre>');

    echo('<br /><br /><br />');



    // Test select 1 config attribute
    echo('Test select 1 config attribute: <br />');

    echo('Get: <pre>');print_r($objPersistor->getData(null, $tabConfig));echo('</pre>');

    echo('<br /><br /><br />');
}



// Remove test database, if required
require_once($strRootAppPath . '/src/database/test/HelpRemoveTestDb.php');


