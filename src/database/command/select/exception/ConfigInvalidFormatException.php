<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\select\exception;

use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxConditionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxOrderClause;
use liberty_code\sql\database\command\clause\library\ToolBoxLimitClause;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstSelectCommand::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid select clause
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT])) ||
                ToolBoxExpressionClause::checkClauseExpressionIsValid($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT])
            ) &&

            // Check valid select clause pattern
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN])) ||
                is_string($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN])
            ) &&

            // Check select minimum requirement
            (
                isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT])  ||
                isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT_PATTERN])
            ) &&

            // Check valid select distinct option
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT])) ||
                (
                    (
                        (
                            (
                                is_string($config[ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT]) &&
                                ctype_digit($config[ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT])
                            ) ||
                            is_int($config[ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT])
                        ) &&
                        (intval($config[ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT]) >= 0)
                    ) ||
                    is_bool($config[ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT])
                )
            ) &&

            // Check valid from clause
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM])) ||
                ToolBoxFromClause::checkClauseFromIsValid($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM])
            ) &&

            // Check valid from clause pattern
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN])) ||
                is_string($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN])
            ) &&

            // Check valid where clause
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE])) ||
                ToolBoxConditionClause::checkClauseConditionIsValid($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE])
            ) &&

            // Check valid where clause pattern
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN])) ||
                is_string($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE_PATTERN])
            ) &&

            // Check valid group clause
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP])) ||
                ToolBoxExpressionClause::checkClauseExpressionIsValid($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP])
            ) &&

            // Check valid group clause pattern
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP_PATTERN])) ||
                is_string($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP_PATTERN])
            ) &&

            // Check valid having clause
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_HAVING])) ||
                ToolBoxConditionClause::checkClauseConditionIsValid($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_HAVING])
            ) &&

            // Check valid having clause pattern
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_HAVING_PATTERN])) ||
                is_string($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_HAVING_PATTERN])
            ) &&

            // Check valid order clause
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER])) ||
                ToolBoxOrderClause::checkClauseOrderIsValid($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER])
            ) &&

            // Check valid order clause pattern
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER_PATTERN])) ||
                is_string($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER_PATTERN])
            ) &&

            // Check valid limit clause
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT])) ||
                ToolBoxLimitClause::checkClauseLimitIsValid($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT])
            ) &&

            // Check valid limit clause pattern
            (
                (!isset($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN])) ||
                is_string($config[ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT_PATTERN])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}