<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;



class ConstConditionClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_OPERAND = 'operand';
    const TAB_CONFIG_KEY_OPERATOR = 'operator';
    const TAB_CONFIG_KEY_VALUE = 'value';
    const TAB_CONFIG_KEY_OPT_NOT = 'not';
    const TAB_CONFIG_KEY_PATTERN = 'pattern';

    const TAB_CONFIG_KEY_GROUP_TYPE = 'type';
    const TAB_CONFIG_KEY_GROUP_CONTENT = 'content';
    const TAB_CONFIG_KEY_GROUP_PATTERN = 'pattern';

    // Operator configuration
    const OPERATOR_CONFIG_REGEXP_PATTERN = '#^operator:(.+)#';

    const OPERATOR_CONFIG_EQUAL = 'equal';
    const OPERATOR_CONFIG_LIKE = 'like';
    const OPERATOR_CONFIG_REGEXP = 'regexp';
    const OPERATOR_CONFIG_IN = 'in';
    const OPERATOR_CONFIG_NULL = 'null';
    const OPERATOR_CONFIG_LESS = 'less';
    const OPERATOR_CONFIG_LESS_EQUAL = 'less_equal';
    const OPERATOR_CONFIG_GREATER = 'greater';
    const OPERATOR_CONFIG_GREATER_EQUAL = 'greater_equal';

    // Group configuration
    const GROUP_TYPE_CONFIG_AND = 'and';
    const GROUP_TYPE_CONFIG_OR = 'or';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get configuration operator options array.
     *
     * @return array
     */
    static public function getTabConfigOperator()
    {
        // Init var
        $result = array(
            self::OPERATOR_CONFIG_EQUAL,
            self::OPERATOR_CONFIG_LIKE,
            self::OPERATOR_CONFIG_REGEXP,
            self::OPERATOR_CONFIG_IN,
            self::OPERATOR_CONFIG_NULL,
            self::OPERATOR_CONFIG_LESS,
            self::OPERATOR_CONFIG_LESS_EQUAL,
            self::OPERATOR_CONFIG_GREATER,
            self::OPERATOR_CONFIG_GREATER_EQUAL,
        );

        // Return result
        return $result;
    }



    /**
     * Get configuration type options array.
     *
     * @return array
     */
    static public function getTabConfigGroupType()
    {
        // Init var
        $result = array(
            self::GROUP_TYPE_CONFIG_AND,
            self::GROUP_TYPE_CONFIG_OR
        );

        // Return result
        return $result;
    }



}