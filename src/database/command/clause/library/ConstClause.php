<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\library;



class ConstClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Exception message constants
    const EXCEPT_MSG_CLAUSE_CONFIG_INVALID_FORMAT =
        'Following config "%2$s" invalid! 
        The config must be an array, not empty and following the "%1$s" clause configuration standard.';



}