<?php
/**
 * Description :
 * This class allows to define default SQL command factory class.
 * Can be consider is base of all SQL command factory type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\sql\database\command\factory\api\CommandFactoryInterface;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\api\CommandInterface;
use liberty_code\sql\database\command\factory\library\ConstCommandFactory;
use liberty_code\sql\database\command\factory\exception\ConnectionInvalidFormatException;



abstract class DefaultCommandFactory extends DefaultFactory implements CommandFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************

    /**
     * @inheritdoc
     * @param ConnectionInterface $objConnection
     */
    public function __construct(ConnectionInterface $objConnection, ProviderInterface $objProvider = null)
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init connection
        $this->setConnection($objConnection);
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstCommandFactory::DATA_KEY_DEFAULT_CONNECTION))
        {
            $this->__beanTabData[ConstCommandFactory::DATA_KEY_DEFAULT_CONNECTION] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified command.
     * Overwrite it to set specific command hydration.
     *
     * @param CommandInterface $objCommand
     * @param array $tabConfig = null
     */
    protected function hydrateCommand(CommandInterface $objCommand, array $tabConfig = null)
    {
        // Hydrate connection
        $objCommand->setConnection($this->getObjConnection());

        // Hydrate configuration, if required
        if(is_array($tabConfig))
        {
            $objCommand->setConfig($tabConfig);
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstCommandFactory::DATA_KEY_DEFAULT_CONNECTION
		);
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstCommandFactory::DATA_KEY_DEFAULT_CONNECTION:
                    ConnectionInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjConnection()
    {
        // Return result
        return $this->beanGet(ConstCommandFactory::DATA_KEY_DEFAULT_CONNECTION);
    }



    /**
     * Get new command object instance engine.
     *
     * Get command new callable format:
     * CommandInterface function(string $strClassPath, array $tabConfig = null):
     * Allows to get new command instance.
     *
     * @param string $strClassPath
     * @param callable $callableGetObjCommandNew
     * @param array $tabConfig = null,
     * @param boolean $optUseProvider = true,
     * @return CommandInterface
     */
    protected function getObjCommandEngine(
        $strClassPath,
        $callableGetObjCommandNew,
        array $tabConfig = null,
        $optUseProvider = true
    )
    {
        // Init var
        $optUseProvider = (is_bool($optUseProvider) ? $optUseProvider : true);
        $result = ($optUseProvider ? $this->getObjInstance($strClassPath) : null);
        $result = (
            is_null($result) ?
                $callableGetObjCommandNew($strClassPath, $tabConfig) :
                $result
        );

        // Hydrate command, if required
        $this->hydrateCommand($result, $tabConfig);

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConnection(ConnectionInterface $objConnection)
    {
        $this->beanSet(ConstCommandFactory::DATA_KEY_DEFAULT_CONNECTION, $objConnection);
    }



}