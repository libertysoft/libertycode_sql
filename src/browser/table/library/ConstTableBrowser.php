<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\browser\table\library;



class ConstTableBrowser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Item count configuration
    const ITEM_COUNT_EXPRESSION_PATTERN = 'COUNT(*)';



}