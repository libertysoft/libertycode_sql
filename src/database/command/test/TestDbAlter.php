<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/database/command/test/HelpGetCommandFacto.php');



// Init var
$objCommand = $objCommandFacto->getObjDbAlterCommand();

$config_1 = array(
    'db_name' => $strDbNm,
    'charset' => 'greekd'
);

$config_2 = array(
    'db_name' => $strDbNm,
    'collation' => 'greek_general_cid',
);

$config_3 = array(
    'db_name' => $strDbNm,
    'charset' => 'greek',
    'collation' => 'greek_general_ci'
);

$tabConfig = array(
    $config_1, // Ko
    $config_2, // Ko
    $config_3 // Ok
);



// Test command
$_GET['exec'] = '1';
$_GET['exec-result'] = '1';
$_GET['exec-stmt'] = '0';
$_GET['exec-stmt-result'] = '0';
$_GET['clean'] = '0';
require($strRootAppPath . '/src/database/command/test/HelpRunCommand.php');



// Reconnection, if required
if(!$objConnection->checkIsConnect())$objConnection->connect();



// Test build config: Ko
$tabConfig = array(
    'DbName' => 'test_nm',
    'Charset' => 7,
    'Collation' => 'test_collation'
);

$objCommand = $objCommandFacto->getObjDbAlterCommand();
foreach($tabConfig as $strConfig => $config)
{
    // Get info
    $strConfigCall = sprintf('set%s', $strConfig);

    echo('Test build config: Ko ' . $strConfig . ':<br />');

    try
    {
        $objCommand->$strConfigCall($config);

        echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
        echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
    }
    catch(\Exception $e)
    {
        echo('Config failed: ' . $e->getMessage());
    }

    echo('<br /><br /><br />');
}



// Test build config: Ok
$tabConfig = array(
    'DbName' => 'test_nm',
    'Charset' => 'test_charset',
    'Collation' => 'test_collation'
);

$objCommand = $objCommandFacto->getObjDbAlterCommand();
foreach($tabConfig as $strConfig => $config)
{
    // Get info
    $strConfigCall = sprintf('set%s', $strConfig);

    echo('Test build config: Ok ' . $strConfig . ':<br />');

    try
    {
        $objCommand->$strConfigCall($config);

        echo('Config: <pre>');var_dump($objCommand->getTabConfig());echo('</pre>');
        echo('Command: <pre>');var_dump($objCommand->getStrCommand());echo('</pre>');
    }
    catch(\Exception $e)
    {
        echo('Config failed: ' . $e->getMessage());
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


