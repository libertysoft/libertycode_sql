<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\validation\rule\library;



class ConstSqlRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_COMMAND_FACTORY = 'command_factory';
    const TAB_VALID_CONFIG_KEY_DATA_FORMAT_CALLABLE = 'data_format_callable';

    // Error configuration
    const TAB_ERROR_CONFIG_KEY_DATA_FORMAT_CALLABLE = 'data_format_callable';


	
    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the SQL rule validation configuration standard.';
    const EXCEPT_MSG_ERROR_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the SQL rule error configuration standard.';



}