<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\database_alter\exception;

use liberty_code\sql\database\command\database_alter\library\ConstDbAlterCommand;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDbAlterCommand::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid database name
            isset($config[ConstDbAlterCommand::TAB_CONFIG_KEY_DB_NAME]) &&
            is_string($config[ConstDbAlterCommand::TAB_CONFIG_KEY_DB_NAME]) &&
            (trim($config[ConstDbAlterCommand::TAB_CONFIG_KEY_DB_NAME]) != '') &&

            // Check valid charset
            (
                (!array_key_exists(ConstDbAlterCommand::TAB_CONFIG_KEY_CHARSET, $config)) ||
                (
                    is_string($config[ConstDbAlterCommand::TAB_CONFIG_KEY_CHARSET]) &&
                    (trim($config[ConstDbAlterCommand::TAB_CONFIG_KEY_CHARSET]) != '')
                )
            ) &&

            // Check valid collation
            (
                (!array_key_exists(ConstDbAlterCommand::TAB_CONFIG_KEY_COLLATION, $config)) ||
                (
                    is_string($config[ConstDbAlterCommand::TAB_CONFIG_KEY_COLLATION]) &&
                    (trim($config[ConstDbAlterCommand::TAB_CONFIG_KEY_COLLATION]) != '')
                )
            ) &&

            // Check set minimum requirement
            (
                array_key_exists(ConstDbAlterCommand::TAB_CONFIG_KEY_CHARSET, $config)  ||
                array_key_exists(ConstDbAlterCommand::TAB_CONFIG_KEY_COLLATION, $config)
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}