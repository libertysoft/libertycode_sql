<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\result\exception;

use liberty_code\sql\database\result\library\ConstResult;



class ColNotFoundException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $col
     */
	public function __construct($col)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstResult::EXCEPT_MSG_COL_NOT_FOUND, strval($col));
	}
	
	
	
}