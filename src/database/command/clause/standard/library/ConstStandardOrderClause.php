<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\sql\database\command\clause\library\ConstOrderClause;



class ConstStandardOrderClause
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_OPERATOR_ASC = '%1$s ASC';
    const SQL_CONFIG_PATTERN_OPERATOR_DESC = '%1$s DESC';
    const SQL_CONFIG_PATTERN_LINK = '%1$s, %2$s';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get configuration operator options array.
     *
     * @return array
     */
    static public function getTabConfigOperator()
    {
        // Init var
        $result = array(
            ConstOrderClause::OPERATOR_CONFIG_ASC => self::SQL_CONFIG_PATTERN_OPERATOR_ASC,
            ConstOrderClause::OPERATOR_CONFIG_DESC => self::SQL_CONFIG_PATTERN_OPERATOR_DESC
        );

        // Return result
        return $result;
    }



}