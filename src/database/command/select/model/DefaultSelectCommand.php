<?php
/**
 * Description :
 * This class allows to define default select SQL command class.
 * Can be consider is base of all select SQL command type.
 *
 * Default select command allows to design string SQL select command,
 * from following specified configuration: @see SelectCommandInterface.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\select\model;

use liberty_code\sql\database\command\model\DefaultCommand;
use liberty_code\sql\database\command\select\api\SelectCommandInterface;

use liberty_code\sql\database\command\library\ConstCommand;
use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxConditionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxOrderClause;
use liberty_code\sql\database\command\clause\library\ToolBoxLimitClause;
use liberty_code\sql\database\command\clause\exception\ClauseConfigInvalidFormatException;
use liberty_code\sql\database\command\select\library\ConstSelectCommand;
use liberty_code\sql\database\command\select\exception\ConfigInvalidFormatException;



abstract class DefaultSelectCommand extends DefaultCommand implements SelectCommandInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		// $result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstCommand::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseSelect(array $tabConfig, $boolDistinct = false)
    {
        // Init var
        $boolDistinct = (is_bool($boolDistinct) ? $boolDistinct : false);

        // Check clause valid
        if(!ToolBoxExpressionClause::checkClauseExpressionIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_SELECT] = $tabConfig;
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstSelectCommand::TAB_CONFIG_KEY_OPT_SELECT_DISTINCT] = $boolDistinct;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseFrom(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxFromClause::checkClauseFromIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_FROM] = $tabConfig;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseWhere(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxConditionClause::checkClauseConditionIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_WHERE] = $tabConfig;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseGroup(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxExpressionClause::checkClauseExpressionIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_GROUP] = $tabConfig;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseHaving(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxConditionClause::checkClauseConditionIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_HAVING,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_HAVING] = $tabConfig;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseOrder(array $tabConfig)
    {
        // Check clause valid
        if(!ToolBoxOrderClause::checkClauseOrderIsValid($tabConfig))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER,
                (is_array($tabConfig) ? serialize($tabConfig) : $tabConfig)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_ORDER] = $tabConfig;

        // Return result
        return $this;
    }



    /**
     * @inheritdoc
     * @throws ClauseConfigInvalidFormatException
     */
    public function setClauseLimit($config)
    {
        // Check clause valid
        if(!ToolBoxLimitClause::checkClauseLimitIsValid($config))
        {
            throw new ClauseConfigInvalidFormatException(
                ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT,
                (is_array($config) ? serialize($config) : $config)
            );
        }

        // Set clause
        $this->__beanTabData
        [ConstCommand::DATA_KEY_DEFAULT_CONFIG]
        [ConstSelectCommand::TAB_CONFIG_KEY_CLAUSE_LIMIT] = $config;

        // Return result
        return $this;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute()
    {
        // Init var
        $result = $this->executeResult();

        if($result !== false)
        {
            $result = 0;
        }

        // Return result
        return $result;
    }



}