<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\insert\exception;

use liberty_code\sql\database\command\clause\library\ToolBoxExpressionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxValueClause;
use liberty_code\sql\database\command\insert\library\ConstInsertCommand;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstInsertCommand::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid from clause
            (
                (!isset($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM])) ||
                ToolBoxFromClause::checkClauseFromIsValid($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM])
            ) &&

            // Check valid from clause pattern
            (
                (!isset($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN])) ||
                is_string($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN])
            ) &&

            // Check from minimum requirement
            (
                isset($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM])  ||
                isset($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_FROM_PATTERN])
            ) &&

            // Check valid column clause
            (
                (!isset($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN])) ||
                ToolBoxExpressionClause::checkClauseExpressionIsValid($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_COLUMN])
            ) &&

            // Check valid value clause
            (
                (!isset($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE])) ||
                ToolBoxValueClause::checkClauseValueIsValid($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE])
            ) &&

            // Check valid value clause pattern
            (
                (!isset($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE_PATTERN])) ||
                is_string($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE_PATTERN])
            ) &&

            // Check value minimum requirement
            (
                isset($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE])  ||
                isset($config[ConstInsertCommand::TAB_CONFIG_KEY_CLAUSE_VALUE_PATTERN])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}