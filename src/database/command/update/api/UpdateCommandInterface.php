<?php
/**
 * Description :
 * This class allows to describe behavior of update SQL command class.
 * Update command allows to design string SQL update command,
 * from following specified configuration:
 * [
 *     from(required: if from_pattern not found): array (@see ToolBoxFromClause::checkClauseFromIsValid() ),
 *
 *     from_pattern(required: if from not found):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated from clause, if found",
 *
 *     set(required: if set_pattern not found): array (@see ToolBoxSetClause::checkClauseSetIsValid() ),
 *
 *     set_pattern(required: if set not found):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated set clause, if found",
 *
 *     where(optional): array (@see ToolBoxConditionClause::checkClauseConditionIsValid() ),
 *
 *     where_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated where clause, if found",
 *
 *     limit(optional): array (@see ToolBoxLimitClause::checkClauseLimitIsValid() ),
 *
 *     limit_pattern(optional):
 *         "string sprintf pattern,
 *         where '%1$s' or '%s' replaced by calculated limit clause, if found"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\update\api;

use liberty_code\sql\database\command\api\CommandInterface;

use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxConditionClause;
use liberty_code\sql\database\command\clause\library\ToolBoxLimitClause;
use liberty_code\sql\database\command\clause\library\ToolBoxSetClause;



interface UpdateCommandInterface extends CommandInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods setters
    // ******************************************************************************

    /**
     * Set from clause config array.
     * Configuration format follows @see ToolBoxFromClause::checkClauseFromIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseFrom(array $tabConfig);



    /**
     * Set set clause config array.
     * Configuration format follows @see ToolBoxSetClause::checkClauseSetIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseSet(array $tabConfig);



    /**
     * Set where clause config array.
     * Configuration format follows @see ToolBoxConditionClause::checkClauseConditionIsValid().
     *
     * @param array $tabConfig
     * @return static
     */
    public function setClauseWhere(array $tabConfig);



    /**
     * Set limit clause config array.
     * Configuration format follows @see ToolBoxLimitClause::checkClauseLimitIsValid().
     *
     * @param integer|array $config
     * @return static
     */
    public function setClauseLimit($config);
}