<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\clause\standard\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\clause\library\ConstFromClause;
use liberty_code\sql\database\command\clause\library\ToolBoxFromClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardClause;
use liberty_code\sql\database\command\clause\standard\library\ToolBoxStandardConditionClause;
use liberty_code\sql\database\command\clause\standard\library\ConstStandardFromClause;



class ToolBoxStandardFromClause extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string SQL command for table.
     * Table configuration format: @see ToolBoxFromClause::checkTableIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param string|array $config
     * @return string
     */
    public static function getStrTableCommand(ConnectionInterface $objConnection, $config)
    {
        // Init var
        $result = '';

        // Get table name or value
        if(is_string($config))
        {
            $result = $objConnection->getStrEscapeName($config);
        }
        else if(isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME]))
        {
            $result = $objConnection->getStrEscapeName($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_NAME]);
        }

        // Get database, if required
        if(isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_DB_NAME]))
        {
            $result = sprintf(
                ConstStandardFromClause::SQL_CONFIG_PATTERN_TABLE_DB,
                $objConnection->getStrEscapeName($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_DB_NAME]),
                $result
            );
        }

        // Get alias, if required
        if(isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_ALIAS]))
        {
            $result = sprintf(
                ConstStandardFromClause::SQL_CONFIG_PATTERN_TABLE_ALIAS,
                $result,
                $objConnection->getStrEscapeName($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_ALIAS])
            );
        }

        // Get pattern value, if required
        if(isset($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_PATTERN]))
        {
            $result = ToolBoxStandardClause::getStrPatternValue($config[ConstFromClause::TAB_CONFIG_KEY_TABLE_PATTERN], $result);
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for join.
     * Join configuration format: @see ToolBoxFromClause::checkJoinIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param array $tabConfig
     * @return string
     */
    public static function getStrJoinCommand(ConnectionInterface $objConnection, array $tabConfig)
    {
        // Init var
        $tabType = ConstStandardFromClause::getTabConfigJoinType();

        // Get join type
        $strType = ConstFromClause::JOIN_TYPE_CONFIG_INNER;
        if(isset($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_TYPE]))
        {
            $strType = $tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_TYPE];
        }

        // Get left element
        $strLeft = '';
        if(isset($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_LEFT]))
        {
            $strLeft = static::getStrElementCommand(
                $objConnection,
                $tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_LEFT]
            );

            // Set sub-join pattern main, if required
            if(!ToolBoxFromClause::checkTableIsValid($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_LEFT]))
            {
                $strLeft = sprintf(ConstStandardFromClause::SQL_CONFIG_PATTERN_JOIN_MAIN, $strLeft);
            }
        }

        // Get right element
        $strRight = '';
        if(isset($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_RIGHT]))
        {
            $strRight = static::getStrElementCommand(
                $objConnection,
                $tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_RIGHT]
            );

            // Set sub-join pattern main, if required
            if(!ToolBoxFromClause::checkTableIsValid($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_RIGHT]))
            {
                $strRight = sprintf(ConstStandardFromClause::SQL_CONFIG_PATTERN_JOIN_MAIN, $strRight);
            }
        }

        // Get join
         $result = sprintf($tabType[$strType], $strLeft, $strRight);

        // Set on condition
        if(isset($tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_ON]))
        {
            $strOn = ToolBoxStandardConditionClause::getStrClauseConditionCommand(
                $objConnection,
                $tabConfig[ConstFromClause::TAB_CONFIG_KEY_JOIN_ON]
            );

            $result = sprintf(ConstStandardFromClause::SQL_CONFIG_PATTERN_JOIN_ON, $result, $strOn);
        }

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for element.
     * Element configuration format: @see ToolBoxFromClause::checkElementIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param string|array $config
     * @return string
     */
    protected static function getStrElementCommand(ConnectionInterface $objConnection, $config)
    {
        // Init var
        $result =
            ToolBoxFromClause::checkTableIsValid($config) ?
                static::getStrTableCommand($objConnection, $config) :
                static::getStrJoinCommand($objConnection, $config);

        // Return result
        return $result;
    }



    /**
     * Get string SQL command for from clause.
     * Configuration array format: @see ToolBoxFromClause::checkClauseFromIsValid().
     *
     * @param ConnectionInterface $objConnection
     * @param array $tabConfig
     * @return string
     */
    public static function getStrClauseFromCommand(ConnectionInterface $objConnection, array $tabConfig)
    {
        // Init var
        $result = '';

        // Run each sub-config
        foreach($tabConfig as $config)
        {
            // Get sub-config string command
            $result =
                (trim($result) == '') ?
                    static::getStrElementCommand($objConnection, $config) :
                    sprintf(
                        ConstStandardFromClause::SQL_CONFIG_PATTERN_LINK,
                        $result,
                        static::getStrElementCommand($objConnection, $config)
                    );
        }

        // Return result
        return $result;
    }



}