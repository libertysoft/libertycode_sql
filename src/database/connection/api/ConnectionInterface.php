<?php
/**
 * Description :
 * This class allows to describe behavior of SQL database connection class.
 * Connection allows to design connection and request interface,
 * which connect and use database from specified configuration,
 * to use specific SQL data storage.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\connection\api;

use liberty_code\sql\database\statement\api\StatementInterface;
use liberty_code\sql\database\result\api\ResultInterface;



interface ConnectionInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get config array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get id from last inserted row, if possible.
     *
     * @return null|string
     */
    public function getStrLastInsertId();





    // Methods setters
    // ******************************************************************************

    /**
     * Set config array.
     *
     * @param array $tabConfig
     * @param boolean $boolConnect = true
     */
    public function setConfig(array $tabConfig, $boolConnect = true);





	// Methods connection / disconnection
	// ******************************************************************************

    /**
     * Check if connected to database server.
     *
     * @return boolean
     */
    public function checkIsConnect();



    /**
     * Connect to database server.
     */
    public function connect();



    /**
     * Disconnect to database server.
     */
    public function close();





    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified SQL query.
     * Return false if doesn't run, true or the number of row affected else, if possible.
     * Correct select command will return true or 0 (no affected rows).
     *
     * @param string $strCommand
     * @return boolean|integer
     */
    public function execute($strCommand);



    /**
     * Execute specified SQL query with data returns.
     * Return object ResultInterface if run correctly, false else.
     * Correct no-select command return empty ResultInterface.
     *
     * @param string $strCommand
     * @return boolean|ResultInterface
     */
    public function executeResult($strCommand);



    /**
     * Get statement (prepared query).
     * Return object StatementInterface if run correctly, false else.
     *
     * @param string $strCommand
     * @param array $tabConfig = array()
     * @param array $tabParam = array()
     * @return StatementInterface
     */
    public function getObjStatement($strCommand, array $tabConfig = array(), array $tabParam = array());





    // Methods escape
    // ******************************************************************************

    /**
     * Get string escaped name, from specified name.
     *
     * @param string $strNm
     * @return string
     */
    public function getStrEscapeName($strNm);



    /**
     * Get string escaped value, from specified value.
     *
     * @param mixed $value
     * @return string
     */
    public function getStrEscapeValue($value);





    // Methods transaction
    // ******************************************************************************

    /**
     * Check if process are in current transaction:
     * transaction already start and not closed.
     *
     * @return boolean
     */
    public function checkInTransaction();



    /**
     * Start transaction.
     *
     * @return boolean
     */
    public function transactionStart();



    /**
     * End transaction and commit.
     *
     * @return boolean
     */
    public function transactionEndCommit();



    /**
     * End transaction and cancel.
     *
     * @return boolean
     */
    public function transactionEndRollback();



}