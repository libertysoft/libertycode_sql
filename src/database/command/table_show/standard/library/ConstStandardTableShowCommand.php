<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\sql\database\command\table_show\standard\library;



class ConstStandardTableShowCommand
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // SQL configuration
    const SQL_CONFIG_PATTERN_SHOW = 'SHOW TABLES';
    const SQL_CONFIG_PATTERN_DATABASE = 'FROM %1$s';
}